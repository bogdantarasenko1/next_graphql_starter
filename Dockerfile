FROM node:8

EXPOSE 3000

LABEL version=0.1

ADD . /app

WORKDIR /app

RUN yarn

ENTRYPOINT ["yarn","start:prod"]
