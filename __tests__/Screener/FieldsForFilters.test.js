import {
  fieldsForFilters,
  blockchainFields,
  priceBTCFields,
  priceUSDFields
} from '../../app/components/Screener/utils.js'

describe('Screener query filters', () => {
  it('blockchain', () => {
    const filters = [
      {
        param: 'circulatingSupply'
      },
      {
        param: 'activeAddresses'
      }
    ]
    expect(blockchainFields(filters)).toContain('')
  })

  it('blockchain with amount', () => {
    const filters = [
      {
        param: 'averageTxValue'
      },
      {
        param: 'nvt'
      },
      {
        param: 'medianTxValue'
      }
    ]
    expect(blockchainFields(filters)).toContain('')
  })

  it('priceBTC', () => {
    const filters = [
      {
        param: 'priceBTC'
      }
    ]
    expect(priceBTCFields(filters)).toContain('')
  })

  it('priceUSD', () => {
    const filters = [
      {
        param: 'priceUSD'
      }
    ]
    expect(priceUSDFields(filters)).toContain('')
  })

  it('marketcap', () => {
    const filters = [
      {
        param: 'totalMarketCap'
      },
      {
        param: 'maxMarketCap'
      },
      {
        param: 'circulatingMarketCap'
      }
    ]
    expect(priceUSDFields(filters)).toContain('')
  })

  it('priceUSD and marketcup', () => {
    const filters = [
      {
        param: 'totalMarketCap'
      },
      {
        param: 'priceUSD'
      }
    ]
    expect(priceUSDFields(filters)).toContain('')
  })

  it('priceUSD and marketcup are empty', () => {
    const filters = []
    expect(priceUSDFields(filters)).toContain('')
  })

  it('all', () => {
    const filters = [
      {
        param: 'circulatingSupply'
      },
      {
        param: 'averageTxValue'
      },
      {
        param: 'priceBTC'
      },
      {
        param: 'priceUSD'
      },
      {
        param: 'totalMarketCap'
      }
    ]
    expect(fieldsForFilters(filters)).toContain('')
  })
})
