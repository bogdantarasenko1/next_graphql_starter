/* eslint-env jest */

import { mount } from 'enzyme'
import React from 'react'
import Router from 'next/router'
import Signup from '../app/components/SignUp/SignUp.screen'

const mockedRouter = { push: () => {} }
Router.router = mockedRouter
const t = str => str

describe('Sign Up', () => {
  it('Submits form', () => {
    const propsMock = jest.fn()
    const wrapper = mount(<Signup t={t} onSubmit={propsMock} />)

    const submitBtn = wrapper.find('button')
    submitBtn.simulate('submit')

    expect(propsMock).toBeCalledTimes(1)
  })

  it('has link to login page', () => {
    const wrapper = mount(<Signup t={t} />)
    const link = wrapper.find('CardActions').find('a')
    expect(link.props().href).toEqual('/login')
  })

  describe('Change text in submit button', () => {
    it('has `Sign Up` text by default', () => {
      const wrapper = mount(<Signup t={t} isProcessingSignUp={false} />)

      const submitBtn = wrapper.find('button')
      expect(submitBtn.text()).toEqual('Sign up')
    })

    it('has `Signing in` text when form is submitting', () => {
      const wrapper = mount(<Signup t={t} isProcessingSignUp={true} />)

      const submitBtn = wrapper.find('button')
      expect(submitBtn.text()).toEqual('Signing in')
    })
  })

  describe('Show error', () => {
    it('shows no error', () => {
      const wrapper = mount(<Signup t={t} />)

      const errorContainer = wrapper.find('.error')
      expect(errorContainer.exists()).toEqual(false)
    })

    it('shows error', () => {
      const wrapper = mount(<Signup t={t} signUpError={'error'} />)

      const errorContainer = wrapper.find('.error')
      expect(errorContainer.exists()).toEqual(true)
    })
  })
})
