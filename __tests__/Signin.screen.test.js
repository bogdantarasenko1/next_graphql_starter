/* eslint-env jest */

import { mount } from 'enzyme'
import React from 'react'
import Router from 'next/router'
import renderer from 'react-test-renderer'
import Login from '../app/components/Signin/Signin.screen.js'

const mockedRouter = { push: () => {} }
Router.router = mockedRouter
const t = str => str

describe('Sign In Screen', () => {
  it('Has greetings and emil label', () => {
    const app = mount(<Login t={t} />)

    expect(
      app
        .find('form')
        .find('p')
        .text()
    ).toEqual(`Welcome to MosaicPlease Sign in to continue`)
    const label = app.find('label').filterWhere(n => n.text() === 'Email')
    expect(label.length).toEqual(1)

    app.unmount()
  })

  it('Has email input and password', () => {
    const component = renderer.create(<Login t={t} />).root

    const inputs = component.findAll(n => n.type === 'input')
    expect(inputs.length).toEqual(2)

    const emailInput = inputs.filter(n => n.props.name === 'email')
    expect(emailInput.length).toEqual(1)

    const passwordInput = inputs.filter(n => n.props.name === 'password')
    expect(passwordInput.length).toEqual(1)
  })

  it('Submits form', () => {
    const propsMock = jest.fn()
    const wrapper = mount(<Login t={t} onSubmit={propsMock} />)

    const submitBtn = wrapper.find('button')
    submitBtn.simulate('submit')

    expect(propsMock).toBeCalledTimes(1)
  })

  it('has link to signup page', () => {
    const wrapper = mount(<Login t={t} />)
    const link = wrapper.find('CardActions').find('a')
    expect(link.props().href).toEqual('/signup')
  })

  describe('Change text in submit button', () => {
    it('has `Sign In` text by default', () => {
      const wrapper = mount(<Login t={t} processingSignIn={false} />)

      const submitBtn = wrapper.find('button')
      expect(submitBtn.text()).toEqual('Sign in')
    })

    it('has `Signing in` text when form is submitting', () => {
      const wrapper = mount(<Login t={t} processingSignIn={true} />)

      const submitBtn = wrapper.find('button')
      expect(submitBtn.text()).toEqual('Signing in')
    })
  })

  describe('Show error', () => {
    it('shows no error', () => {
      const wrapper = mount(<Login t={t} />)

      const errorContainer = wrapper.find('.error')
      expect(errorContainer.exists()).toEqual(false)
    })

    it('shows error', () => {
      const wrapper = mount(<Login t={t} signinError={'error'} />)

      const errorContainer = wrapper.find({ color: 'error' })
      expect(errorContainer.exists()).toEqual(true)
    })
  })
})
