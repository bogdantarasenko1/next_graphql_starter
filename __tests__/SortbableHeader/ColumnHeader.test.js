import { mount } from 'enzyme'
import React from 'react'
import { createShallow } from '@material-ui/core/test-utils'
import { Table, TableHead, TableRow } from '@material-ui/core/'
import ColumnHeader from '../../app/components/SortableHeader/ColumnHeader'

describe('Column Header', () => {
  let shallow

  describe('style for order direction', () => {
    beforeEach(() => {
      shallow = createShallow({ dive: true })
    })

    it('shows asc direction if sorted', () => {
      const wrapper = shallow(<ColumnHeader sortDirection="asc" sorted />)
      expect(wrapper.instance().iconClass()).toEqual(
        expect.stringContaining('sortAsc')
      )
    })

    it('shows desc direction if not sorted and in asc direction', () => {
      const wrapper = shallow(<ColumnHeader sortDirection="asc" />)
      expect(wrapper.instance().iconClass()).toEqual(
        expect.not.stringContaining('sortAsc')
      )
    })

    it('shows desc direction if sorted', () => {
      const wrapper = shallow(<ColumnHeader sortDirection="desc" sorted />)
      expect(wrapper.instance().iconClass()).toEqual(
        expect.not.stringContaining('sortAsc')
      )
    })

    it('shows desc direction if not sorted', () => {
      const wrapper = shallow(<ColumnHeader sortDirection="desc" />)
      expect(wrapper.instance().iconClass()).toEqual(
        expect.not.stringContaining('sortAsc')
      )
    })

    it('changes showIcon state when mouse over', () => {
      const wrapper = shallow(
        <ColumnHeader
          sortDirection="desc"
          text="Column Text"
          sortable={true}
          sorted={false}
        />
      )

      wrapper.instance().forceUpdate()
      wrapper.shallow().simulate('mouseEnter')
      expect(wrapper.state().showIcon).toBe(true)
    })

    it('changes showIcon state when mouse leave', () => {
      const wrapper = shallow(
        <ColumnHeader sortDirection="desc" text="Column Text" sortable={true} />
      )

      wrapper.instance().forceUpdate()
      wrapper.simulate('mouseLeave')
      expect(wrapper.state().showIcon).toBe(false)
    })

    it('has mouseEnter and mouseLeave handlers. Click callback in props', () => {
      const propsMock = jest.fn()
      const table = mount(
        <Table>
          <TableHead>
            <TableRow>
              <ColumnHeader
                sortDirection="desc"
                text="Column Text"
                sortable={true}
                onClick={propsMock}
                id={1}
              />
            </TableRow>
          </TableHead>
        </Table>
      )
      const wrapperWithStyles = table.find(ColumnHeader)
      const wrapper = wrapperWithStyles.children()
      const spyClick = jest.spyOn(wrapper.instance(), 'click')
      const spyMouseEnter = jest.spyOn(wrapper.instance(), 'mouseEnter')
      const spyMouseLeave = jest.spyOn(wrapper.instance(), 'mouseLeave')

      wrapper.instance().forceUpdate()

      wrapper.simulate('click')
      expect(spyClick).toBeCalledTimes(1)
      expect(propsMock).toBeCalledTimes(1)

      wrapper.simulate('mouseEnter')
      expect(spyMouseEnter).toBeCalledTimes(1)

      wrapper.simulate('mouseLeave')
      expect(spyMouseLeave).toBeCalledTimes(1)
    })
  })
})
