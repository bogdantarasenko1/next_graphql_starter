import { mount } from 'enzyme'
import React from 'react'
import _ from 'lodash'
import { Table } from '@material-ui/core/'
import SortableHeader from '../../app/components/SortableHeader/SortableHeader.screen'
import ColumnHeader from '../../app/components/SortableHeader/ColumnHeader'

const nonSortableColumns = ['number', 'name']
const stubColumns = [
  null,
  'number',
  null,
  null,
  'name',
  'marketCap',
  'price',
  'priceChangePercent',
  'volume'
].map(e => {
  if (e) {
    if (nonSortableColumns.includes(e)) {
      return { key: e, text: e, sortable: false }
    } else {
      return { key: e, text: e, sortable: true }
    }
  } else return null
})

const cellsWithTextCount = _.compact(stubColumns).length
const placeholderCellsCount = stubColumns.length - cellsWithTextCount
const nonSortableCellsCount = nonSortableColumns.length
const sortableCellsCount = cellsWithTextCount - nonSortableCellsCount

describe('Sortable Header', () => {
  it('renders cell with text in column', () => {
    const wrapper = mount(
      <Table>
        <SortableHeader columns={stubColumns} />
      </Table>
    )
    const cells = wrapper.find(ColumnHeader)
    expect(cells).toHaveLength(cellsWithTextCount)
  })

  it('renders sortable cells', () => {
    const wrapper = mount(
      <Table>
        <SortableHeader columns={stubColumns} />
      </Table>
    )
    const cells = wrapper.find('th').filterWhere(n => n.prop('onClick'))
    expect(cells).toHaveLength(sortableCellsCount)
  })

  it('renders no sortable cells', () => {
    const wrapper = mount(
      <Table>
        <SortableHeader columns={stubColumns} />
      </Table>
    )
    const cells = wrapper
      .find('th')
      .filterWhere(n => !_.isEmpty(n.text()) && !n.prop('onClick'))
    expect(cells).toHaveLength(nonSortableCellsCount)
  })

  it('renders empty cell', () => {
    const wrapper = mount(
      <Table>
        <SortableHeader columns={stubColumns} />
      </Table>
    )
    const cells = wrapper.find('th').filterWhere(n => _.isEmpty(n.text()))
    expect(cells).toHaveLength(placeholderCellsCount)
  })
})
