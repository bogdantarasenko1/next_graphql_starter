/* eslint-env jest */

import { mount } from 'enzyme'
import React from 'react'
import renderer from 'react-test-renderer'
import Icon from '@material-ui/core/Icon'
import ListItemText from '@material-ui/core/ListItemText'
import Navigation from '../app/components/Navigation/Navigation.screen'

const menuItemsStub = [
  {
    key: 'dashboard',
    link: '/dashboard',
    title: 'Dashboard',
    ico: 'dashboard'
  },
  {
    key: 'profile',
    link: '/profile',
    title: 'User Profile',
    ico: 'account_circle'
  },
  {
    key: 'coin',
    link: '/coin/btc',
    title: 'Coin View',
    ico: 'monetization_on'
  },
  {
    key: 'criticalEvents',
    link: '/critical-events',
    title: 'Critical Events',
    ico: 'assessment'
  }
]

describe('Navigation Screen', () => {
  it('renders tree', () => {
    const tree = renderer
      .create(<Navigation menuItems={menuItemsStub} />)
      .toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('has link in menu item', () => {
    const wrapper = mount(<Navigation menuItems={menuItemsStub} />)
    expect(wrapper.find('a')).toHaveLength(4)
    expect(wrapper.find('a').map(node => node.props().href)).toEqual(
      menuItemsStub.map(item => item.link)
    )
  })

  it('shows icons only in non expanded menu', () => {
    const wrapper = mount(<Navigation menuItems={menuItemsStub} />)
    expect(
      wrapper
        .find('a')
        .first()
        .find(ListItemText)
    ).toHaveLength(0)
    expect(
      wrapper
        .find('a')
        .first()
        .find(Icon)
    ).toHaveLength(1)
  })

  it('show text links in expanded menu', () => {
    const wrapper = mount(
      <Navigation menuItems={menuItemsStub} menuOpened={true} />
    )
    expect(
      wrapper
        .find('a')
        .first()
        .find(ListItemText)
    ).toHaveLength(1)
    expect(
      wrapper
        .find('a')
        .first()
        .find(Icon)
    ).toHaveLength(1)
  })
})
