import { shallow } from 'enzyme'
import React from 'react'
import Router from 'next/router'
import { SigninContainer } from '../app/containers/Signin/Signin.container.js'
import { errorStatus } from '../app/utils/apolloErrors'

const t = text => text
const mockedRouter = { replace: () => {} }
Router.router = mockedRouter

describe('Sign In Screen Container', () => {
  const homePageStub = '/'
  const saveAuthMock = () => {}
  const inputStub = { target: { value: 'test' } }

  it('renders view', () => {
    const wrapper = shallow(
      <SigninContainer homePage={homePageStub} saveAuth={saveAuthMock} />
    )

    expect(wrapper).toMatchSnapshot()
  })

  describe('field change', () => {
    it('updates email value state', () => {
      const wrapper = shallow(
        <SigninContainer homePage={homePageStub} saveAuth={saveAuthMock} />
      )
      wrapper.instance().onEmailChange(inputStub)
      expect(wrapper.state().email).toEqual(inputStub.target.value)
    })

    it('updates password value state', () => {
      const wrapper = shallow(
        <SigninContainer homePage={homePageStub} saveAuth={saveAuthMock} />
      )
      wrapper.instance().onPasswordChange(inputStub)
      expect(wrapper.state().password).toEqual(inputStub.target.value)
    })
  })

  describe('signin states', () => {
    it('updates state on success signin', () => {
      const saveAuthMock = jest.fn()
      const userStab = {
        firstName: 'User'
      }

      const wrapper = shallow(
        <SigninContainer homePage={homePageStub} saveAuth={saveAuthMock} />
      )

      const homePageSpy = jest.spyOn(wrapper.instance(), 'moveToHomePage')
      wrapper.instance().forceUpdate()

      wrapper.instance().onSignInSuccess(userStab)

      expect(wrapper.state().processingSignIn).toEqual(false)
      expect(wrapper.state().disableSignin).toEqual(true)
      expect(saveAuthMock).toBeCalledTimes(1)
      expect(homePageSpy).toBeCalledTimes(1)
    })

    it('updates state on signin error', () => {
      const errorStab = errorStatus.INVALID_CREDENTIALS
      const wrapper = shallow(<SigninContainer t={t} />)

      wrapper.instance().onSignInError(errorStab)

      expect(wrapper.state().processingSignIn).toEqual(false)
      expect(wrapper.state().signinError).toEqual(
        `login:Incorrect Email or Password`
      )
      expect(wrapper.state().disableSignin).toEqual(false)
    })

    it('updates state on submit', () => {
      const expectedError = new Error('Something broke')
      const classicSignInMock = jest.fn().mockRejectedValue(expectedError)

      const wrapper = shallow(
        <SigninContainer classicSignIn={classicSignInMock} t={t} />
      )

      wrapper.instance().forceUpdate()

      wrapper.instance().onSubmit()

      expect(wrapper.state().processingSignIn).toBe(true)
      expect(wrapper.state().signinError).toBe(null)
      expect(wrapper.state().disableSignin).toBe(true)
      expect(classicSignInMock).toBeCalledTimes(1)
      return classicSignInMock().catch(error => {
        expect(error).toBe(expectedError)
      })
    })

    it('updates state on submit', () => {
      const stub = {
        data: { signInClassic: { user: { displayName: 'user' }, jwt: 'jwt' } }
      }
      const classicSignInMock = jest.fn().mockResolvedValue(stub)

      const wrapper = shallow(
        <SigninContainer classicSignIn={classicSignInMock} />
      )

      wrapper.instance().forceUpdate()

      wrapper.instance().onSubmit()

      expect(wrapper.state().processingSignIn).toBe(true)
      expect(wrapper.state().signinError).toBe(null)
      expect(wrapper.state().disableSignin).toBe(true)
      expect(classicSignInMock).toBeCalledTimes(1)
      return classicSignInMock().then(({ data }) => {
        expect(data).toBe(stub.data)
        expect(data.signInClassic.user.jwtToken).toBe(data.signInClassic.jwt)
      })
    })
  })
})
