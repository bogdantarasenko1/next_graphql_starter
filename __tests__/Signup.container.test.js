import { shallow } from 'enzyme'
import React from 'react'
import Router from 'next/router'
import { SignUpContainer } from '../app/containers/SignUp/SignUp.container'

const t = text => text
const mockedRouter = { replace: () => {} }
Router.router = mockedRouter

describe('SignUp Screen Container', () => {
  const homePageStub = '/'
  const saveAuthMock = () => {}
  const inputStub = { target: { value: 'test' } }

  it('renders view', () => {
    const wrapper = shallow(
      <SignUpContainer homePage={homePageStub} saveAuth={saveAuthMock} />
    )

    expect(wrapper).toMatchSnapshot()
  })

  describe('field change', () => {
    it('updates first name value state', () => {
      const wrapper = shallow(
        <SignUpContainer homePage={homePageStub} saveAuth={saveAuthMock} />
      )
      wrapper.instance().onFirstNameChange(inputStub)
      expect(wrapper.state().firstName).toEqual(inputStub.target.value)
    })

    it('updates last value state', () => {
      const wrapper = shallow(
        <SignUpContainer homePage={homePageStub} saveAuth={saveAuthMock} />
      )
      wrapper.instance().onLastNameChange(inputStub)
      expect(wrapper.state().lastName).toEqual(inputStub.target.value)
    })

    it('updates email value state', () => {
      const wrapper = shallow(
        <SignUpContainer homePage={homePageStub} saveAuth={saveAuthMock} />
      )
      wrapper.instance().onEmailChange(inputStub)
      expect(wrapper.state().email).toEqual(inputStub.target.value)
    })

    it('updates password value state', () => {
      const wrapper = shallow(
        <SignUpContainer homePage={homePageStub} saveAuth={saveAuthMock} />
      )
      wrapper.instance().onPasswordChange(inputStub)
      expect(wrapper.state().password).toEqual(inputStub.target.value)
    })
  })

  describe('signup states', () => {
    it('updates state on success signup', () => {
      const saveAuthMock = jest.fn()
      const userStab = {
        firstName: 'User'
      }

      const wrapper = shallow(
        <SignUpContainer homePage={homePageStub} saveAuth={saveAuthMock} />
      )

      const homePageSpy = jest.spyOn(wrapper.instance(), 'moveToHomePage')
      wrapper.instance().forceUpdate()

      wrapper.instance().onSignUpSuccess(userStab)

      expect(wrapper.state().isProcessingSignUp).toEqual(false)
      expect(wrapper.state().shouldDisableSignUp).toEqual(true)
      expect(saveAuthMock).toBeCalledTimes(1)
      expect(homePageSpy).toBeCalledTimes(1)
    })

    it('updates state on signup error', () => {
      const errorStab = 'UNPROCESSABLE_ENTITY'
      const wrapper = shallow(<SignUpContainer t={t} />)

      wrapper.instance().onSignUpError(errorStab)

      expect(wrapper.state().isProcessingSignUp).toEqual(false)
      expect(wrapper.state().signUpError).toEqual(errorStab)
      expect(wrapper.state().shouldDisableSignUp).toEqual(false)
    })

    it('updates state on submit', () => {
      const expectedError = new Error('Something broke')
      const createUserMock = jest.fn().mockRejectedValue(expectedError)

      const wrapper = shallow(<SignUpContainer createUser={createUserMock} />)

      wrapper.instance().forceUpdate()

      wrapper.instance().onSubmit()

      expect(wrapper.state().isProcessingSignUp).toBe(true)
      expect(wrapper.state().signUpError).toBe(null)
      expect(wrapper.state().shouldDisableSignUp).toBe(true)
      expect(createUserMock).toBeCalledTimes(1)
      return createUserMock().catch(error => {
        expect(error).toBe(expectedError)
      })
    })

    it('updates state on submit', () => {
      const stub = {
        data: {
          createUser: { user: { firstName: 'user' }, jwt: 'jwt' }
        }
      }
      const createUserMock = jest.fn().mockResolvedValue(stub)

      const wrapper = shallow(<SignUpContainer createUser={createUserMock} />)

      wrapper.instance().forceUpdate()

      wrapper.instance().onSubmit()

      expect(wrapper.state().isProcessingSignUp).toBe(true)
      expect(wrapper.state().signUpError).toBe(null)
      expect(wrapper.state().shouldDisableSignUp).toBe(true)
      expect(createUserMock).toBeCalledTimes(1)

      return createUserMock().then(({ data }) => {
        expect(data).toBe(stub.data)
        expect(data.createUser.user.jwtToken).toBe(data.createUser.jwt)
      })
    })
  })

  describe('Errors in email', () => {
    it('tests if email has already been taken', () => {
      const wrapper = shallow(<SignUpContainer t={t} />)

      wrapper.instance().onSignUpError('UNPROCESSABLE_ENTITY')

      expect(wrapper.instance().emailError()).toBe(
        'login:Signup error, try later'
      )
    })

    it('tests if email is empty', () => {
      const wrapper = shallow(<SignUpContainer t={t} />)

      wrapper.instance().onSignUpError('UNPROCESSABLE_ENTITY')

      expect(wrapper.instance().emailError()).toBe(
        'login:Signup error, try later'
      )
    })
  })
})
