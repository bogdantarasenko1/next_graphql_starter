import React from 'react'
import wait from 'waait'
import renderer from 'react-test-renderer'
import { MockedProvider } from 'react-apollo/test-utils'
import {
  SIGNUP_MUTATION,
  signupMutation
} from '../../app/containers/SignUp/SignUp.container.js'

describe('SignUp Mutation', () => {
  const variables = {
    input: {
      firstName: 'firstName',
      lastName: 'lastName',
      email: 'test@email.co',
      password: 'passwd',
      deviceId: '123'
    }
  }

  const mockData = {
    data: {
      createUser: {
        user: {
          id: null,
          email: 'test@email.co',
          firstName: 'firstName',
          lastName: 'lastName',
          avatarUrl: null,
          bio: null
        },
        jwt: 'token'
      }
    }
  }

  it('returns user info after sign up ', async () => {
    class Component extends React.Component {
      constructor (props) {
        super(props)
        this.state = {}
      }
      static getDerivedStateFromProps (props) {
        const { createUser } = props
        return expect(createUser({ variables })).resolves.toEqual(mockData)
      }

      render () {
        return null
      }
    }

    const WrappedComponent = signupMutation(Component)

    const mock = (
      <MockedProvider
        addTypename={false}
        mocks={[
          {
            request: { query: SIGNUP_MUTATION, variables },
            result: mockData
          }
        ]}
      >
        <WrappedComponent />
      </MockedProvider>
    )

    renderer.create(mock)
    await wait(0)
  })
})
