import React from 'react'
import wait from 'waait'
import renderer from 'react-test-renderer'
import { MockedProvider } from 'react-apollo/test-utils'
import {
  SIGNIN_MUTATION,
  signinMutation
} from '../../app/containers/Signin/Signin.container.js'

describe('Signin Mutation', () => {
  const variables = {
    input: {
      email: 'test@email.co',
      password: 'password123',
      deviceId: '123'
    }
  }

  const mockData = {
    data: {
      signInClassic: {
        user: {
          email: 'test@email.co',
          id: null,
          displayName: null,
          avatarUrl: null,
          bio: null
        },
        jwt: 'token'
      }
    }
  }

  it('returns user info after signin ', async () => {
    class Component extends React.Component {
      constructor (props) {
        super(props)
        this.state = {}
      }
      static getDerivedStateFromProps (props) {
        const { classicSignIn } = props
        return expect(classicSignIn({ variables })).resolves.toEqual(mockData)
      }

      render () {
        return null
      }
    }

    const WrappedComponent = signinMutation(Component)

    const mock = (
      <MockedProvider
        addTypename={false}
        mocks={[
          {
            request: { query: SIGNIN_MUTATION, variables },
            result: mockData
          }
        ]}
      >
        <WrappedComponent />
      </MockedProvider>
    )

    renderer.create(mock)
    await wait(0)
  })
})
