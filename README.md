# Mosaic App

## Table of contents

* [Quick start](https://github.com/mosaicio/mosaic-web#quick-start)
* [Running Localy](https://github.com/mosaicio/mosaic-web#running-localy)
* [How to Code Drop](https://github.com/mosaicio/mosaic-web#how-to-code-drop)

## Quick start

* Production build: https://mosaic-web-prod.herokuapp.com/

## Running Localy

1.  Clone the repo

```
git clone https://github.com/mosaicio/mosaic-web
cd ./mosaic-web
```

2.  `yarn` (please-please use yarn)
3.  Development mode run `yarn start` or `yarn start:production` for prodution mode

## Running tests

```
yarn test
```
