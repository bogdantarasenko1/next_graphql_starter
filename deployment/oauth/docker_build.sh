#!/bin/sh

eval $(aws ecr get-login --no-include-email --region us-east-1)
docker build -t oauth2proxy .
docker tag oauth2proxy 761551651351.dkr.ecr.us-east-1.amazonaws.com/oauth2proxy
docker push 761551651351.dkr.ecr.us-east-1.amazonaws.com/oauth2proxy 
