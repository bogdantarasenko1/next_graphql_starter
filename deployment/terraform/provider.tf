provider "aws" {
  region = "${var.AWS_REGION}"
}

terraform {
  backend "s3" {
    bucket = "io.mosaic.prod.terraform"
    key    = "webapp/state.tf"
    region = "us-east-1"
  }
}

