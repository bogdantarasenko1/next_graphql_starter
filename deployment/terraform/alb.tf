resource "aws_alb" "webapp" {
  name     = "webapp"
  internal = true 

  security_groups = [
    "${aws_security_group.alb_webapp.id}",
  ]

  subnets = [
    "${var.AWS_SUBNET_01}",
    "${var.AWS_SUBNET_02}",
  ]
}

resource "aws_alb" "oauth2proxy" {
  name     = "oauth2proxy"
  internal = true

  security_groups = [
    "${aws_security_group.alb_webapp.id}",
  ]

  subnets = [
    "${var.AWS_SUBNET_01}",
    "${var.AWS_SUBNET_02}",
  ]
}

resource "aws_alb_target_group" "webapp" {
  name        = "webapp"
  protocol    = "HTTP"
  port        = "3000"
  vpc_id      = "${var.AWS_VPC}"
  target_type = "ip"

  health_check {
    port     = "3000"
    path     = "/login"
    interval = "20"
    timeout  = "5"
    matcher  = "302,200"
  }
}

resource "aws_alb_target_group" "oauth2proxy" {
  name        = "oauth2proxy"
  protocol    = "HTTP"
  port        = "4180"
  vpc_id      = "${var.AWS_VPC}"
  target_type = "ip"

  health_check {
    port     = "4180"
    path     = "/oauth2/start"
    interval = "20"
    timeout  = "5"
    matcher  = "302,200"
  }
}

resource "aws_alb_listener" "webapp" {
  load_balancer_arn = "${aws_alb.webapp.arn}"
  port              = "3000"
  protocol          = "HTTP"

  default_action {
    target_group_arn = "${aws_alb_target_group.webapp.arn}"
    type             = "forward"
  }

  depends_on = ["aws_alb_target_group.webapp"]
}

resource "aws_alb_listener" "oauth2proxy" {
  load_balancer_arn = "${aws_alb.oauth2proxy.arn}"
  port              = "4180"
  protocol          = "HTTP"

  default_action {
    target_group_arn = "${aws_alb_target_group.oauth2proxy.arn}"
    type             = "forward"
  }

  depends_on = ["aws_alb_target_group.oauth2proxy"]
}

output "webapp_dns" {
  value = "${aws_alb.webapp.dns_name}"
}

output "oauth2proxy_dns" {
  value = "${aws_alb.oauth2proxy.dns_name}"
}
