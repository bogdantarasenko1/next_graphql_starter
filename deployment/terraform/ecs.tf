resource "aws_ecs_cluster" "webapp" {
  name = "webapp"
}

resource "aws_ecs_task_definition" "webapp" {
  family                   = "webapp"
  requires_compatibilities = ["FARGATE"]
  network_mode             = "awsvpc"
  memory                   = 4096
  cpu                      = 2048
  container_definitions    = "${data.template_file.webapp.rendered}"
  execution_role_arn       = "${var.AWS_ECS_IAM_ROLE}"
}

resource "aws_ecs_task_definition" "oauth2proxy" {
  family                   = "oauth2proxy"
  requires_compatibilities = ["FARGATE"]
  network_mode             = "awsvpc"
  memory                   = 4096
  cpu                      = 2048
  container_definitions    = "${data.template_file.oauth2proxy.rendered}"
  execution_role_arn       = "${var.AWS_ECS_IAM_ROLE}"
}

resource "aws_ecs_service" "webapp" {
  name            = "webapp"
  cluster         = "${aws_ecs_cluster.webapp.id}"
  launch_type     = "FARGATE"
  task_definition = "${aws_ecs_task_definition.webapp.arn}"
  desired_count   = 1

  network_configuration = {
    subnets         = ["${var.AWS_SUBNET_01}", "${var.AWS_SUBNET_02}"]
    security_groups = ["${aws_security_group.ecs.id}"]
  }

  load_balancer {
    target_group_arn = "${aws_alb_target_group.webapp.arn}"
    container_name   = "webapp"
    container_port   = 3000
  }

  depends_on = [
    "aws_alb_listener.webapp",
  ]

}

resource "aws_ecs_service" "oauth2proxy" {
  name            = "oauth2proxy"
  cluster         = "${aws_ecs_cluster.webapp.id}"
  launch_type     = "FARGATE"
  task_definition = "${aws_ecs_task_definition.oauth2proxy.arn}"
  desired_count   = 1

  network_configuration = {
    subnets         = ["${var.AWS_SUBNET_01}", "${var.AWS_SUBNET_02}"]
    security_groups = ["${aws_security_group.ecs.id}"]
  }

  load_balancer {
    target_group_arn = "${aws_alb_target_group.oauth2proxy.arn}"
    container_name   = "oauth2proxy"
    container_port   = 4180 
  }

  depends_on = [
    "aws_alb_listener.oauth2proxy",
  ]

}
