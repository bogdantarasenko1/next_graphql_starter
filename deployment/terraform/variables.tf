variable "AWS_VPC" {
  type = "string"
  default = "vpc-0f8d878d9698e5d3c"
}

variable "AWS_SUBNET_01" {
  type = "string"
  default = "subnet-01e92f9ff871154a2"
}

variable "AWS_SUBNET_01_CIDR" {
  type = "string"
  default = "10.0.3.0/24"
}

variable "AWS_SUBNET_02" {
  type = "string"
  default = "subnet-06a26a0fd1269a4d6"
}

variable "AWS_SUBNET_02_CIDR" {
  type = "string"
  default = "10.0.4.0/24"
}


variable "AWS_DOCKER_REPOSITORY_OAUTH" {
  type = "string"
  default = "761551651351.dkr.ecr.us-east-1.amazonaws.com/oauth2proxy"
}

variable "AWS_DOCKER_REPOSITORY_WEBAPP" {
  type = "string"
  default = "761551651351.dkr.ecr.us-east-1.amazonaws.com/webapp"
}

variable "AWS_ECS_IAM_ROLE" {
  type = "string"
  default = "arn:aws:iam::761551651351:role/ecs_task_assume"
}

variable "AWS_REGION" {
  type = "string"
  default = "us-east-1"
}

variable "AWS_STATE_BUCKET" {
  type = "string"
  default = "io.mosaic.prod.terraform"
}

variable "AWS_STATE_BUCKET_KEY" {
  type = "string"
  default = "webapp/state.tf"
}
