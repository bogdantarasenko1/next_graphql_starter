data "template_file" "webapp" {
  template = <<EOF
[
  {
    "name": "webapp",
    "image": "${var.AWS_DOCKER_REPOSITORY_WEBAPP}:latest",
    "networkMode": "awsvpc",
    "essential": true,
    "logConfiguration": {
        "logDriver": "awslogs",
        "options": {
          "awslogs-group": "${aws_cloudwatch_log_group.webapp.name}",
          "awslogs-region": "${var.AWS_REGION}",
          "awslogs-stream-prefix": "ecs"
        }
    },
    "portMappings": [
      {
        "containerPort": 3000,
        "hostPort": 3000
      }
    ]

  }
]
EOF
}

data "template_file" "oauth2proxy" {
  template = <<EOF
[
  {
    "name": "oauth2proxy",
    "image": "${var.AWS_DOCKER_REPOSITORY_OAUTH}:latest",
    "networkMode": "awsvpc",
    "essential": true,
    "logConfiguration": {
        "logDriver": "awslogs",
        "options": {
          "awslogs-group": "${aws_cloudwatch_log_group.oauth2proxy.name}",
          "awslogs-region": "${var.AWS_REGION}",
          "awslogs-stream-prefix": "ecs"
        }
    },
    "portMappings": [
      {
        "containerPort": 4180,
        "hostPort": 4180 
      }
    ]

  }
]
EOF
}
