resource "aws_cloudwatch_log_group" "webapp" {
  name              = "/ecs/webapp"
  retention_in_days = 14

  tags {
    Name = "webapp"
  }
}


resource "aws_cloudwatch_log_group" "oauth2proxy" {
  name              = "/ecs/oauth2proxy"
  retention_in_days = 14

  tags {
    Name = "oauth2proxy"
  }
}
