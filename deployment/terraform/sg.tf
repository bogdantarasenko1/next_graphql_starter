resource "aws_security_group" "alb_webapp" {
  name        = "alb_webapp"
  description = "Allow traffic to the webapp & oauth2proxy"
  vpc_id      = "${var.AWS_VPC}"

  ingress {
    from_port   = 3000 
    to_port     = 3000 
    protocol    = "tcp"
    cidr_blocks = ["10.0.0.0/8"]
  }

  ingress {
    from_port   = 4180 
    to_port     = 4180 
    protocol    = "tcp"
    cidr_blocks = ["10.0.0.0/8"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}


resource "aws_security_group" "ecs" {
  name        = "ecs"
  description = "Allow traffic into webapp&oauth ECS services"
  vpc_id      = "${var.AWS_VPC}"

  ingress {
    from_port   = 3000
    to_port     = 3000 
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 4180 
    to_port     = 4180 
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
