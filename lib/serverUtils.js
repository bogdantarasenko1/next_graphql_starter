const LOGIN_URI = '/login'
const SIGNUP_URI = '/signup'
const RESTORE_PASSWORD = '/restore'
const CONFIRM_URI = '/confirm'
const DASHBOARD_URI = '/dashboard'

const isStaticFilesPath = path => {
  return /^\/(_next|static)/.test(path)
}

const isAuthenticated = (req, res, next) => {
  const { user } = req.cookies
  if (user) {
    return res.redirect(DASHBOARD_URI)
  }
  return next()
}

const pagesAccess = (req, res, next) => {
  if (isStaticFilesPath(req.path)) {
    return next()
  }

  const { user } = req.cookies
  if (user) {
    return next()
  }

  if (
    req.path === SIGNUP_URI ||
    req.path === LOGIN_URI ||
    req.path === RESTORE_PASSWORD ||
    req.path === CONFIRM_URI
  ) {
    return next()
  }

  return res.redirect(LOGIN_URI)
}

module.exports = {
  isAuthenticated,
  pagesAccess,
  LOGIN_URI,
  SIGNUP_URI,
  DASHBOARD_URI
}
