export default (themeName, colors) => {
  const {
    bgBody,
    textBody,
    textHeader,
    textSecondary,
    bgSecondary,
    bgPrimary,
    accentColor,
    accentColorBg,
    linkMain,
    linkHover
  } = colors

  return {
    type: themeName,
    background: {
      default: bgBody, // page body background
      paper: bgSecondary, // "widget" section backgrounds
      header: bgPrimary // section header background
    },
    text: {
      primary: textBody, // Most body text
      header: textHeader, // Brighter Header text
      secondary: textSecondary // Lighter meta/caption text
    },
    secondary: {
      main: accentColor, // Orange
      background: accentColorBg // Orange Light
    },
    link: {
      main: linkMain,
      hover: linkHover
    }
  }
}
