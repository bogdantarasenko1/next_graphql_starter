export default {
  bgBody: '#262625', // Body
  bgPrimary: '#323232', // Header bg
  bgSecondary: '#2A2A2A', // Section bg
  accentColor: '#FB771A', // Orange
  accentColorBg: '#44372D', // Orange Light
  textBody: 'rgba(255, 255, 255, 0.7)',
  textHeader: 'rgba(255, 255, 255, 1)',
  textSecondary: 'rgba(255, 255, 255, 0.4)',
  hintText: 'rgba(255, 255, 255, 0.4)',
  linkMain: 'rgba(255, 255, 255, 0.4)',
  linkHover: 'rgba(255, 255, 255, 1)'
}
