export default {
  bgBody: '#ffffff',
  bgPrimary: '#e8e8e8',
  bgSecondary: '#e8e8e8',
  accentColor: '#FB771A',
  accentColorBg: '#3e3128',
  textBody: '#333333',
  textHeader: '#333333',
  textSecondary: '#333333',
  hintText: 'rgba(0, 0, 0, 0.38)',
  linkMain: 'rgba(0, 0, 0, 0.5)',
  linkHover: 'rgba(0, 0, 0, 0.85)'
}
