export default colors => {
  const {
    textBody,
    textHeader,
    bgPrimary,
    accentColorBg,
    accentColor
  } = colors

  return {
    MuiTypography: {
      body1: {
        color: textBody
      },
      title: {
        color: textHeader
      },
      subheading: {
        fontSize: '.875rem',
        fontWeight: 'bold'
      }
    },
    MuiAppBar: {
      colorPrimary: {
        backgroundColor: bgPrimary
      }
    },
    MuiTabs: {
      root: {
        minHeight: '32px',
        minWidth: '30px'
      },
      indicator: {
        backgroundColor: accentColor,
        minWidth: `30px`
      }
    },
    MuiTab: {
      root: {
        textTransform: 'none',
        minHeight: '32px',
        minWidth: `auto`,
        '@media (min-width: 960px)': {
          minWidth: `auto`
        },
        '&$selected': {
          backgroundColor: accentColorBg
        }
      }
    },
    MuiTableRow: {
      root: {
        height: '34px',
        '&:hover': {
          backgroundColor: 'transparent !important'
        }
      }
    },
    MuiTableCell: {
      head: {
        color: textBody,
        fontSize: '.75rem',
        borderBottom: `2px solid ${bgPrimary}`,
        padding: '0 3px'
      },
      body: {
        borderColor: bgPrimary,
        color: textBody,
        fontSize: '.75rem',
        padding: '0 3px'
      }
    },
    MuiFormLabel: {
      root: {
        '&$focused': {
          color: 'rgba(255, 255, 255, 0.7)'
        }
      },
      focused: {}
    },
    MuiDialogTitle: {
      root: {
        background: bgPrimary,
        padding: '.5rem'
      }
    },
    MuiDialogContent: {
      root: {
        padding: '1.5rem'
      }
    }
  }
}
