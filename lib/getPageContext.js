import { SheetsRegistry, create } from 'jss'
import { createGenerateClassName, jssPreset } from '@material-ui/core/styles'
import theme from './MaterialUITheme'

function createPageContext (themeName) {
  return {
    theme: theme(themeName),
    jss: create(jssPreset()),
    // This is needed in order to deduplicate the injection of CSS in the page.
    sheetsManager: new Map(),
    // This is needed in order to inject the critical CSS.
    sheetsRegistry: new SheetsRegistry(),
    // The standard class name generator.
    generateClassName: createGenerateClassName()
  }
}

export default function getPageContext (themeName) {
  // Make sure to create a new context for every server-side request so that data
  // isn't shared between connections (which would be bad).
  if (!process.browser) {
    return createPageContext(themeName)
  }

  /* eslint-disable no-underscore-dangle */
  // Reuse context on the client-side.
  if (!global.__INIT_MATERIAL_UI__) {
    global.__INIT_MATERIAL_UI__ = createPageContext(themeName)
  }

  return global.__INIT_MATERIAL_UI__
}
