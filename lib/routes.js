const routes = require('next-routes')

module.exports = routes()
  .add('researchArticle', '/research/:path', 'research-article')
  .add('coinPage', '/coin/:code/:page?', 'coin')
  .add('screener', '/screener/:page?', 'screener')
  .add('legal', '/legal/:page', 'legal')
