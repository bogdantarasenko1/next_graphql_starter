import { createMuiTheme } from '@material-ui/core/styles'
import { themeName as themes } from '../app/utils/userTheme'
import themePalette from './themes/palette'
import themeOverrides from './themes/overrides'
import lightThemeColors from './themes/light'
import darkThemeColors from './themes/dark'

const createTheme = (themeName, colors) => {
  const palette = themePalette(themeName, colors)
  const overrides = themeOverrides(colors)

  return createMuiTheme({
    palette,
    overrides
  })
}

export default themeName => {
  switch (themeName) {
    case themes.light:
      return createTheme(themeName, lightThemeColors)
    default:
      return createTheme(themes.dark, darkThemeColors)
  }
}
