// @flow

import Router from 'next/router'
import { saveAuth, getUser, getServerUser } from '../app/utils/auth'
import { DASHBOARD_URI as HOMEPAGE } from '../lib/serverUtils'

type Props = {
  render: Object => void
};

const authPageHook = (props: Props) => {
  return props.render({ homePage: HOMEPAGE, saveAuth })
}

authPageHook.getInitialProps = async ({ req, res }) => {
  const isServer = !!req
  const user = isServer ? getServerUser(req.cookies) : getUser()

  if (user) {
    if (isServer) {
      res.redirect(HOMEPAGE)
      res.end()
    } else {
      Router.push(HOMEPAGE)
    }
    return {}
  }
}

export default authPageHook
