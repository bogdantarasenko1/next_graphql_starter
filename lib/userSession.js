import React from 'react'
import { getUser, getServerUser } from '../app/utils/auth'

export const withUserSession = Page => {
  const WithUserSession = props => <Page {...props} />
  WithUserSession.getInitialProps = async context => {
    const req = context.req
    const isServer = !!req
    const user = isServer ? getServerUser(req.cookies) : getUser()

    return {
      ...(Page.getInitialProps ? await Page.getInitialProps(context) : {}),
      user
    }
  }

  return WithUserSession
}
