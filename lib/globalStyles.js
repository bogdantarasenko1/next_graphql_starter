export default theme => ({
  '@global': {
    'html,body': {
      height: '100%',
      width: '100%'
    },
    body: {
      fontFamily: '"Helvetica Neue", Helvetica, Arial, sans-serif'
    },
    'body.fontLoaded': {
      fontFamily: '"Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif'
    },
    '#__next': {
      backgroundColor: theme.palette.background.default,
      minHeight: '100%',
      minWidth: '100%',
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'flex-start',
      alignItems: 'stretch',
      alignContent: 'stretch'
    },
    a: {
      color: theme.palette.link.main,
      '&:hover': {
        color: theme.palette.link.hover
      }
    },
    '.widget': {
      marginBottom: '1rem',
      backgroundColor: theme.palette.background.paper,
      [theme.breakpoints.down('sm')]: {
        border: 'none',
        marginBottom: '2rem',
        backgroundColor: 'transparent'
      }
    },
    'tr.custom-hover:hover td': {
      backgroundColor: 'rgba(255, 255, 255, 0.14)'
    }
  }
})
