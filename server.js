const path = require('path')
const express = require('express')
const next = require('next')
const cookieParser = require('cookie-parser')
const i18nextMiddleware = require('i18next-express-middleware')
const Backend = require('i18next-node-fs-backend')
const { i18nInstance } = require('./lib/i18n')
const routes = require('./lib/routes')
const cors = require('cors')


const {
  pagesAccess,
  isAuthenticated,
  LOGIN_URI,
  SIGNUP_URI,
  DASHBOARD_URI
} = require('./lib/serverUtils')


const port = process.env.PORT || 3000
const dev = process.env.NODE_ENV !== 'production'

const app = next({ dev })
let handler = routes.getRequestHandler(app)

i18nInstance
  .use(Backend)
  .use(i18nextMiddleware.LanguageDetector)
  .init(
    {
      debug: false,
      fallbackLng: 'en',
      preload: ['en'], // preload all langages
      ns: [
        'common',
        'home',
        'coin-view',
        'keyData',
        'cryptoassets-table',
        'footer',
        'about',
        'login',
        'time-chart'
      ], // need to preload all the namespaces
      backend: {
        loadPath: path.join(__dirname, '/locales/{{lng}}/{{ns}}.json'),
        addPath: path.join(__dirname, '/locales/{{lng}}/{{ns}}.missing.json')
      }
    },
    () => {
      app
        .prepare()
        .then(() => {
          const server = express()
          const cors_opt = {
            origin: '*',
            methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
            preflightContinue: false,
            optionsSuccessStatus: 204,
            allowedHeaders: 'Authorization',
            credentials: true
          }

          // server.use(cors(cors_opt))
          // server.options('*', cors(cors_opt))

          server.use(cookieParser())
          server.use(express.static(`${__dirname}/static`))

          // enable middleware for i18next
          server.use(i18nextMiddleware.handle(i18nInstance))

          // serve locales for client
          server.use(
            '/locales',
            express.static(path.join(__dirname, '/locales'))
          )

          // missing keys
          server.post(
            '/locales/add/:lng/:ns',
            i18nextMiddleware.missingKeyHandler(i18nInstance)
          )

          server.all('*', pagesAccess)

          server.get('/', (req, res) => {
            res.redirect(DASHBOARD_URI)
          })

          server.get('/analytics', (req, res) => {
            if (dev) {
              res.redirect(process.env.ANALYTICS_URL)
            } else {
              res.next()
            }
          })

          server.get('/profile', pagesAccess, (req, res) => {
            const actualPage = '/profile'
            app.render(req, res, actualPage)
          })

          server.get('/login', isAuthenticated, (req, res) => {
            const actualPage = LOGIN_URI
            app.render(req, res, actualPage)
          })

          server.get('/signup', isAuthenticated, (req, res) => {
            const actualPage = SIGNUP_URI
            app.render(req, res, actualPage)
          })

          server.use(handler).listen(port, err => {
            if (err) throw err
            console.log('> Ready on http://localhost:3000')
          })

          // if (dev) {
          //   server.use(proxy('/data/*', { target: process.env.ANALYTICS_URL, changeOrigin: true }))
          // }

        })
        .catch(ex => {
          console.error(ex.stack)
          process.exit(1)
        })
    }
  )
