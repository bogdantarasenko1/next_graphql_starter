// @flow

import React from 'react'
import Research from '../app/containers/Research/ResearchPage.container'
import { withI18next } from '../lib/withI18next'
import { withUserSession } from '../lib/userSession'

type Props = {};

const ResearchPage = (props: Props) => <Research {...props} />

export default withUserSession(withI18next(['common', 'footer'])(ResearchPage))
