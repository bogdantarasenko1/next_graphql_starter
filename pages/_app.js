import React from 'react'
import App, { Container } from 'next/app'
import { ApolloProvider } from 'react-apollo'
import { configureAnchors } from 'react-scrollable-anchor'
import { I18nextProvider } from 'react-i18next'
import { MuiThemeProvider } from '@material-ui/core/styles'
import CssBaseline from '@material-ui/core/CssBaseline'
import JssProvider from 'react-jss/lib/JssProvider'
import getPageContext from '../lib/getPageContext'
import { getTheme, getServerUserTheme } from '../app/utils/userTheme'
import withApolloClient from '../app/lib/with-apollo-client'
import { I18n } from '../lib/i18n'
import getConfig from "next/config";
import {hotjar} from "react-hotjar";


class MyApp extends App {
  pageContext = null;

  constructor (props) {
    super(props)
    // const theme = props.theme || getTheme()
    const theme = getTheme() // use default Dark theme since switching themes is disabled
    this.pageContext = getPageContext(theme)
  }

  componentDidMount () {
    configureAnchors({ offset: -48 }) // AppBar height

    // Remove the server-side injected CSS.
    const jssStyles = document.querySelector('#jss-server-side')
    if (jssStyles && jssStyles.parentNode) {
      jssStyles.parentNode.removeChild(jssStyles)
    }

    const {
      publicRuntimeConfig: { HOTJAR_ID }
    } = getConfig()

    // Add Hotjar Tracking code
    hotjar.initialize(HOTJAR_ID, 6);

  }

  static async getInitialProps ({ Component, router, ctx, req }) {
    let pageProps = {}
    let theme

    const { req: request } = ctx
    if (request) {
      // run on server
      const { cookies } = request
      if (cookies) {
        theme = getServerUserTheme(cookies)
      }
    }

    if (Component.getInitialProps) {
      pageProps = await Component.getInitialProps(ctx)
    }

    return { theme, pageProps }
  }

  render () {
    const { Component, pageProps, apolloClient } = this.props
    return (
      <Container>
        <ApolloProvider client={apolloClient}>
          <I18nextProvider
            ns="common"
            i18n={(pageProps && pageProps.i18n) || I18n}
          >
            <JssProvider
              jss={this.pageContext.jss}
              registry={this.pageContext.sheetsRegistry}
              generateClassName={this.pageContext.generateClassName}
            >
              <MuiThemeProvider
                theme={this.pageContext.theme}
                sheetsManager={this.pageContext.sheetsManager}
              >
                <CssBaseline />
                <Component pageContext={this.pageContext} {...pageProps} />
              </MuiThemeProvider>
            </JssProvider>
          </I18nextProvider>
        </ApolloProvider>
      </Container>
    )
  }
}

export default withApolloClient(MyApp)
