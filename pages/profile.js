// @flow

import Router from 'next/router'
import Profile from '../app/containers/Profile/Profile.container'
import { withI18next } from '../lib/withI18next'
import { getServerUser, getUser } from '../app/utils/auth'

type Props = {
  user: Object
};

const ProfilePage = (props: Props) => {
  const { user } = props
  if (user) {
    return <Profile {...props} user={user} />
  }
}

ProfilePage.getInitialProps = async ({ req, res }) => {
  const isServer = !!req
  const user = isServer ? getServerUser(req.cookies) : getUser()

  if (user) {
    return { user }
  } else {
    if (isServer) {
      res.redirect('/login')
      res.end()
    } else {
      Router.push('/login')
    }
    return {}
  }
}

export default withI18next(['common', 'footer'])(ProfilePage)
