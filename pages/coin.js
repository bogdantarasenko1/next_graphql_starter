// @flow

import CoinView from '../app/containers/CoinView/CoinView.container'
import { withI18next } from '../lib/withI18next'
import { withUserSession } from '../lib/userSession'

const CoinViewPage = props => <CoinView {...props} />

CoinViewPage.getInitialProps = async ({ query }) => {
  const { code, page = 'main' } = query
  const queryParams = { code, page }

  return queryParams
}

export default withUserSession(
  withI18next([
    'coin-view',
    'cryptoassets-table',
    'common',
    'footer',
    'keyData',
    'time-chart'
  ])(CoinViewPage)
)
