// @flow

import React from 'react'
import ResearchArticle from '../app/containers/Research/ResearchArticle.container'
import { withI18next } from '../lib/withI18next'
import { withUserSession } from '../lib/userSession'

type Props = {};

const ResearchArticlePage = (props: Props) => <ResearchArticle {...props} />

ResearchArticlePage.getInitialProps = async ({ query }) => {
  const { path } = query
  const queryParams = { path }

  return queryParams
}

export default withUserSession(
  withI18next(['common', 'footer'])(ResearchArticlePage)
)
