// @flow
import AuthHook from '../lib/authPageHook'
import SignUp from '../app/containers/SignUp/SignUp.container'
import { withI18next } from '../lib/withI18next'

const SignUpPage = props => (
  <AuthHook
    render={auth => (
      <SignUp homePage={auth.homePage} saveAuth={auth.saveAuth} {...props} />
    )}
  />
)

export default withI18next(['common', 'login'])(SignUpPage)
