// @flow

import { withI18next } from '../lib/withI18next'
import { withUserSession } from '../lib/userSession'
import Screener from '../app/containers/Screener/Screener.container'

const ScreenerPage = props => <Screener {...props} />

ScreenerPage.getInitialProps = async ({ query }) => {
  const { page } = query
  const queryParams = { page }

  return queryParams
}

export default withUserSession(
  withI18next(['common', 'cryptoassets-table', 'screener', 'footer'])(
    ScreenerPage
  )
)
