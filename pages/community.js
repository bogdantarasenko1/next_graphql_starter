// @flow

import AuthHook from '../lib/authPageHook'
import { withI18next } from '../lib/withI18next'
import { withUserSession } from '../lib/userSession'
import CommunityContainer from '../app/containers/Community/Community.container'

const CommunityPage = props => (
  <AuthHook
    render={auth => <CommunityContainer saveAuth={auth.saveAuth} {...props} />}
  />
)

export default withUserSession(
  withI18next(['cryptoassets-table', 'common', 'time-chart', 'footer'])(
    CommunityPage
  )
)
