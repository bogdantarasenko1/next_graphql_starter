// @flow

import React from 'react'
import Document, { Head, Main, NextScript } from 'next/document'
import flush from 'styled-jsx/server'
import globalStyles from '../lib/globalStyles'

class MosaicDocument extends Document {
  static async getInitialProps (ctx) {
    const initialProps = await Document.getInitialProps(ctx)

    let pageContext

    type Props = {
      pageContext: Object
    };

    const page = ctx.renderPage(Component => {
      const WrappedComponent = (props: Props) => {
        pageContext = props.pageContext
        return <Component {...props} />
      }

      return WrappedComponent
    })

    const style = globalStyles(pageContext.theme)

    const sheet = pageContext.jss.createStyleSheet(style)
    pageContext.sheetsRegistry.add(sheet)
    sheet.attach()

    return {
      ...initialProps,
      ...page,
      pageContext,
      styles: (
        <React.Fragment>
          <style
            id="jss-server-side"
            // eslint-disable-next-line react/no-danger
            dangerouslySetInnerHTML={{
              __html: pageContext.sheetsRegistry.toString()
            }}
          />
          {flush() || null}
        </React.Fragment>
      )
    }
  }

  render () {
    const { pageContext, buildManifest } = this.props
    const style = globalStyles(pageContext.theme)
    const sheet = pageContext.jss.createStyleSheet(style)
    const sheetStyles = sheet.toString()
    const { css } = buildManifest
    return (
      <html lang="en" dir="ltr">
        <Head>
          <title>Mosaic Web</title>
          <meta charSet="utf-8" />
          <meta
            name="viewport"
            content={
              'user-scalable=0, initial-scale=1, minimum-scale=1, width=device-width, height=device-height'
            }
          />
          <meta
            name="theme-color"
            content={pageContext.theme.palette.primary.main}
          />
          <link
            rel="stylesheet"
            href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons"
          />
          <link rel="icon" href="/static/favicon.png" type="image/png" />
          {css.map(file => (
            <link rel="stylesheet" href={`/_next/${file}`} key={file} />
          ))}
          <style>{sheetStyles}</style>
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </html>
    )
  }
}

export default MosaicDocument
