// @flow

import React from 'react'
import Tutorials from '../app/containers/MetricsTutorials/MetricsTutorials.container'
import { withI18next } from '../lib/withI18next'
import { withUserSession } from '../lib/userSession'

type Props = {};

const MetricsTutorialsPage = (props: Props) => {
  return <Tutorials {...props} />
}

export default withUserSession(
  withI18next(['common', 'footer'])(MetricsTutorialsPage)
)
