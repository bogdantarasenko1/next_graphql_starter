// @flow

import CriticalEvents from '../app/components/CriticalEvents/Page/CriticalEventsPage.screen'

const CriticalEventsPage = () => <CriticalEvents />

export default CriticalEventsPage
