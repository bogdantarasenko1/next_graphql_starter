// @flow

import React from 'react'
import { withI18next } from '../lib/withI18next'
import { withUserSession } from '../lib/userSession'
import Team from '../app/containers/Team/Team.container'

type Props = {};

const TeamPage = (props: Props) => {
  return <Team {...props} />
}

export default withUserSession(withI18next(['common', 'footer'])(TeamPage))
