// @flow

import { withI18next } from '../lib/withI18next'
import { withUserSession } from '../lib/userSession'
import Legal from '../app/containers/Legal/Legal.container'

const LegalPage = props => <Legal {...props} />

LegalPage.getInitialProps = async ({ query }) => {
  const { page } = query
  const queryParams = { page }

  return queryParams
}

export default withUserSession(withI18next(['common', 'footer'])(LegalPage))
