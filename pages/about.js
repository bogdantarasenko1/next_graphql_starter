// @flow

// Do not remove the page. It was replaced by external page, but might be changed again.

import React from 'react'
import About from '../app/containers/About/About.container'
import { withI18next } from '../lib/withI18next'
import { withUserSession } from '../lib/userSession'

type Props = {};

const AboutPage = (props: Props) => {
  return <About {...props} />
}

export default withUserSession(
  withI18next(['common', 'about', 'footer'])(AboutPage)
)
