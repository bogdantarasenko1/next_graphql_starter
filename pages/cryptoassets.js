// @flow

import Cryptoassets from '../app/components/Cryptoassets/Page/CryptoassetsPage.screen'
import { withI18next } from '../lib/withI18next'
import { withUserSession } from '../lib/userSession'

const CryptoassetsPage = props => <Cryptoassets {...props} />

export default withUserSession(
  withI18next(['cryptoassets-table', 'common', 'footer'])(CryptoassetsPage)
)
