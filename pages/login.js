// @flow
import AuthHook from '../lib/authPageHook'
import Login from '../app/containers/Signin/Signin.container'
import { withI18next } from '../lib/withI18next'

const LoginPage = props => (
  <AuthHook
    render={auth => (
      <Login homePage={auth.homePage} saveAuth={auth.saveAuth} {...props} />
    )}
  />
)

LoginPage.getInitialProps = async ({ req }) => {
  let auth = {
    email: '',
    jwtToken: ''
  }
  const isAuthenticated = req.headers.hasOwnProperty('authorization')
  if (isAuthenticated) {
    auth.email = req.headers['x-webauth-user']
    auth.jwtToken = req.headers['authorization'].substr(7)
  }

  return { auth }
}

export default withI18next(['common', 'login'])(LoginPage)
