// @flow

import AuthHook from '../lib/authPageHook'
import { withI18next } from '../lib/withI18next'
import { withUserSession } from '../lib/userSession'
import Dashboard from '../app/components/Dashboard/Dashboard.container'

const DashboardPage = props => (
  <AuthHook
    render={auth => <Dashboard saveAuth={auth.saveAuth} {...props} />}
  />
)

export default withUserSession(
  withI18next(['cryptoassets-table', 'common', 'time-chart', 'footer'])(
    DashboardPage
  )
)
