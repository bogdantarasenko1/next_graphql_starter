// @flow

import AuthHook from '../lib/authPageHook'
import RestorePassword from '../app/components/RestorePassword/RestorePassword.container'
import { withI18next } from '../lib/withI18next'

const RestorePasswordPage = props => (
  <AuthHook
    render={auth => (
      <RestorePassword
        homePage={auth.homePage}
        saveAuth={auth.saveAuth}
        {...props}
      />
    )}
  />
)

export default withI18next(['common', 'login'])(RestorePasswordPage)
