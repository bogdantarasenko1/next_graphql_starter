// @flow

import AuthHook from '../lib/authPageHook'
import EmailConfirm from '../app/components/EmailConfirm/EmailConfirm.container'
import { withI18next } from '../lib/withI18next'

const EmailConfirmPage = props => (
  <AuthHook
    render={auth => (
      <EmailConfirm
        homePage={auth.homePage}
        saveAuth={auth.saveAuth}
        {...props}
      />
    )}
  />
)

export default withI18next(['common', 'login'])(EmailConfirmPage)
