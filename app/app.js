// Import all the third party stuff
import React from 'react'
import ReactDOM from 'react-dom'
import { ApolloProvider } from 'react-apollo'
import { BrowserRouter as Router } from 'react-router-dom'
import App from './containers/App'
import { getUser } from './utils/auth'
import {
  createAuthorizedApolloClient,
  createDefaultApolloClient
} from './utils/ApolloClient'
import './global-styles'

const MOUNT_NODE = document.getElementById('app')

const user = getUser()
const authorized = user && user.jwtToken
const client = authorized
  ? createAuthorizedApolloClient(user.jwtToken)
  : createDefaultApolloClient()

ReactDOM.render(
  <ApolloProvider client={client}>
    <Router>
      <App />
    </Router>
  </ApolloProvider >,
  MOUNT_NODE
)

// Install ServiceWorker and AppCache in the end since
// it's not most important operation and if main code fails,
// we do not want it installed
if (process.env.NODE_ENV === 'production') {
  require('offline-plugin/runtime').install() // eslint-disable-line global-require
}
