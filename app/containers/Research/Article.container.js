// @flow

import React from 'react'
import moment from 'moment'
import _ from 'lodash'
import ResearchArticle from '../../components/Research/ResearchArticle.screen'
import recentArticles from '../../components/Research/recentArticles.json'

type Props = {
  path: string
};

type State = {
  article: ?Object,
  isLoading: boolean
};

class ResearchArticleContainer extends React.Component<Props, State> {
  props: Props;
  state: State;

  constructor (props: Props) {
    super(props)

    this.state = {
      isLoading: true
    }
  }

  componentDidMount () {
    this.loadArticle()
  }

  loadArticle = () => {
    const { path } = this.props

    const article = _.find(recentArticles, ['path', path])
    article.date = moment(article.publishedAt).format('MMM DD YYYY')
    setTimeout(() => this.setState({ article, isLoading: false }), 1000)
  };

  render () {
    const { isLoading, article } = this.state
    return (
      <ResearchArticle
        {...this.props}
        isLoading={isLoading}
        article={article}
      />
    )
  }
}

export default ResearchArticleContainer
