// @flow

import React from 'react'
import Layout from '../../components/MainLayout/MainLayout.screen'
import ArticleContainer from './Article.container'

type Props = {
  path: string
};

const ResearchArticleContainer = (props: Props) => {
  return <Layout {...props} content={<ArticleContainer {...props} />} />
}

export default ResearchArticleContainer
