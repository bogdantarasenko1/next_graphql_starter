// @flow

import React from 'react'
import Research from '../../components/Research/ResearchPage.screen'

type Props = {};

const ResearchPageContainer = (props: Props) => {
  return <Research {...props} />
}

export default ResearchPageContainer
