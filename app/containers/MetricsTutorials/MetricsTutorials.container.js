// @flow

import React from 'react'
import MetricsTutorials from '../../components/MetricsTutorials/MetricsTutorials.screen'

type Props = {};

const tutorials = [
  {
    header: 'Metric 1',
    text: 'Useful text to know about metrics'
  },
  {
    header: 'Metric 2',
    text: 'Useful text to know about metrics'
  },
  {
    header: 'Metric 3',
    text: 'Useful text to know about metrics'
  }
]

const MetricsTutorialsContainer = (props: Props) => {
  return <MetricsTutorials tutorials={tutorials} {...props} />
}

export default MetricsTutorialsContainer
