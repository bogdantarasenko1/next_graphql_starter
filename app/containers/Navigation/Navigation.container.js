// @flow

import React from 'react'
import Navigation from '../../components/Navigation/Navigation.screen'

type State = {
  menuOpened: boolean
};

/*
import styles from './styles.css'

 const menuItems = [
 { key: 'explore', title: 'Explore', ico: styles.searchIco },
 { key: 'research', title: 'Research', ico: styles.researchIco },
 { key: 'market', title: 'Market', ico: styles.trendIco },
 { key: 'trending', title: 'Trending', ico: styles.popularTopicIco },
 { key: 'indicators', title: 'Indicators', ico: styles.tearOffIco },
 { key: 'criticalEvents', title: 'Critical Events', ico: styles.rebalanceIco }
] */

const menuItems = [
  {
    key: 'dashboard',
    link: '/dashboard',
    title: 'Dashboard',
    ico: 'dashboard'
  },
  {
    key: 'cryptoassets',
    link: '/cryptoassets',
    title: 'Cryptoassets',
    ico: 'pie_chart'
  },
  {
    key: 'profile',
    link: '/profile',
    title: 'User Profile',
    ico: 'account_circle'
  },
  {
    key: 'coin',
    link: '/coin/btc',
    title: 'Coin View',
    ico: 'monetization_on'
  },
  {
    key: 'criticalEvents',
    link: '/critical-events',
    title: 'Critical Events',
    ico: 'assessment'
  }
]

class NavigationContainer extends React.Component<State> {
  state: State;

  constructor (props) {
    super(props)

    this.state = {
      menuOpened: false
    }
  }

  onMouseEnter = selectedKey => {
    if (this.state.menuOpened === false) {
      this.setState({ menuOpened: true })
    }
  };

  onMouseLeave = selectedKey => {
    if (this.state.menuOpened === true) {
      this.setState({ menuOpened: false })
    }
  };

  render () {
    return (
      <Navigation
        onMouseEnter={this.onMouseEnter}
        onMouseLeave={this.onMouseLeave}
        menuOpened={this.state.menuOpened}
        menuItems={menuItems}
      />
    )
  }
}

export default NavigationContainer
