// @flow

// Do not remove the page. It was replaced by external page, but might be changed again.

import React from 'react'
import About from '../../components/About/About.screen'

type Props = {};

const AboutContainer = (props: Props) => {
  return <About {...props} />
}

export default AboutContainer
