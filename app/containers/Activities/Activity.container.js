import React from 'react'
import Activity from '../../components/Activities/Activity/Activity.screen'

const items = [
  {
    image: '',
    text: 'Satoshi Nakamoto commented on Michael Stone’s research',
    date: '[Michael’s comment to article] · 42 minutes ago'
  },
  {
    image: '',
    text: 'Satoshi Nakamoto added [coin name] to her Portfolio',
    date: '42 minutes ago'
  },
  {
    image: '',
    text: 'Satoshi Nakamoto followed Michael Stone, Anthony G., Steave Smith',
    date: '2 minutes ago'
  },
  {
    image: '',
    text: 'Satoshi Nakamoto [action] [User name and/or type of content]',
    date: '[Satoshi Nakamoto’s comment] · [time stamp]'
  }
]

const ActivityContainer = () => {
  const defaultImage = e => {
    e.target.src = '/static/circle.png'
  }

  return <Activity items={items} onImageError={defaultImage} />
}

export default ActivityContainer
