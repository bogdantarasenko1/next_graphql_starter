import React from 'react'
import Research from '../../components/Activities/Research/Research.screen'

const items = [
  {
    image: '',
    tags: 'Research · Bitcoin · Trending',
    text: 'Is Facebook about to launch its own cryptocurrency?',
    date: 'May 10, 2018 at 17:30'
  },
  {
    image: '',
    tags: 'Research · Bitcoin · Trending',
    text: 'Here’s why bitcoin might be on the verge of another price explosion',
    date: 'May 10, 2018 at 17:30'
  },
  {
    image: '',
    tags: 'Research · Bitcoin · Trending',
    text: "Bitcoin Developers Build Prototype for 'Dandelion' Privacy Tool",
    date: 'May 10, 2018 at 17:30'
  },
  {
    image: '',
    tags: 'Research · Bitcoin · Trending',
    text: 'Cambridge Analytica planned its own cryptocurrency to sell data',
    date: 'May 10, 2018 at 17:30'
  }
]

const ResearchContainer = () => {
  const defaultImage = e => {
    return '/static/circle.png'
  }

  return <Research items={items} onImageError={defaultImage} />
}

export default ResearchContainer
