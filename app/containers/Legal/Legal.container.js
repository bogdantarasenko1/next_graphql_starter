// @flow

import React from 'react'
import Error from 'next/error'
import PrivacyPolicy from '../../components/Legal/PrivacyPolicy.screen'
import TermsOfUse from '../../components/Legal/TermsOfUse.screen'

type Props = {
  page: string
};

const LegalContainer = (props: Props) => {
  const { page } = props

  switch (page) {
    case 'terms-of-use':
      return <TermsOfUse {...props} />
    case 'privacy-policy':
      return <PrivacyPolicy {...props} />
    default:
      console.log(`Unhandled legal page ${page}`)
      return <Error statusCode={404} />
  }
}

export default LegalContainer
