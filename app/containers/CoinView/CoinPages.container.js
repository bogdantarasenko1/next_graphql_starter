// @flow

import React, { Component } from 'react'
import ActivityScreen from '../../components/CoinView/Pages/CoinPages.screen'
import OverviewContainer from '../../components/CoinView/Pages/Overview/Overview.container'
// import EcosystemContainer from '../../components/CoinView/Pages/Ecosystem/Ecosystem.container'
import BlockchainDataContainer from '../../components/CoinView/Pages/BlockchainData/BlockchainData.container'
import TradingContainer from '../../components/CoinView/Pages/Trading/Trading.container'
import CompetitionContainer from '../../components/CoinView/Pages/Competition/Competition.container'

type State = {
  selectedTab: number,
  tabItems: []
};

type Props = {};

class CoinPagesContainer extends Component<State, Props> {
  state: State;
  props: Props;

  constructor (props) {
    super(props)

    const tabItems = [
      {
        key: 'overview',
        title: 'Overview',
        content: <OverviewContainer {...props} />
      },
      // {
      //   key: 'ecosystem',
      //   title: 'Ecosystem',
      //   content: <EcosystemContainer {...props} />
      // },
      {
        key: 'blockchain',
        title: 'Blockchain Data',
        content: <BlockchainDataContainer {...props} />
      },
      {
        key: 'trading',
        title: 'Trading',
        content: <TradingContainer {...props} />
      },
      {
        key: 'competition',
        title: 'Competition',
        content: <CompetitionContainer {...props} />
      }
    ]

    this.state = {
      selectedTab: 0,
      tabItems: tabItems
    }
  }

  onSelectTab = (event, value) => {
    this.setState({ selectedTab: value })
  };

  render () {
    const { tabItems, selectedTab } = this.state
    return (
      <ActivityScreen
        {...this.props}
        selectedTab={selectedTab}
        onSelectTab={this.onSelectTab}
        tabItems={tabItems.map(item => ({ key: item.key, title: item.title }))}
        content={tabItems[selectedTab].content}
      />
    )
  }
}

export default CoinPagesContainer
