// @flow

import React from 'react'
import numbro from 'numbro'
import { getCrypto } from '../../utils/devQuery'
import CoinView from '../../components/CoinView/CoinView.screen'

type State = {
  crypto: ?Object,
  price: ?Object,
  asset: ?Object,
  symbol: string,
  isLoading: boolean
};

type Props = {
  t: func,
  code: string,
  page: string
};

class CoinViewContainer extends React.Component<Props, State> {
  state: State;
  props: Props;

  constructor (props: Props) {
    super(props)
    const { code } = props
    const symbol = code.toUpperCase()

    this.state = {
      symbol,
      isLoading: true
    }
  }

  setMenuRef = element => {
    this.menuRef = element
  };

  componentDidUpdate (oldProps) {
    if (oldProps.code !== this.props.code) {
      this.loadAssetData(this.props.code)
    }
  }

  componentDidMount () {
    const { symbol } = this.state
    this.loadAssetData(symbol)
    window.addEventListener('scroll', this.scrolling)
  }

  loadAssetData = assetSymbol => {
    const symbol = assetSymbol.toUpperCase()
    getCrypto(symbol, 'BTC')
      .then(data => {
        const symbolPrice = `${symbol}Price`

        const { [symbolPrice]: price, currencyPrice, ...crypto } = data
        this.setState({
          price: price,
          currencyPrice,
          crypto,
          isLoading: false
        })
      })
      .catch(e => {
        this.setState({ isLoading: false })
      })
  };

  currentPrice () {
    const price = this.state.price || {}
    const latest = price.latest || {}
    const latestPrice = latest.latestPrice || {}
    const { amount } = latestPrice

    if (amount) {
      return numbro(amount).formatCurrency({ mantissa: 2 })
    }
    return null
  }

  btcPrice () {
    const currencyPrice = this.state.currencyPrice || {}
    const lates = currencyPrice.currencyPrice || {}
    const latestPrice = (lates.latestPrice = {})
    const { amount } = latestPrice
    if (amount) {
      return numbro(amount).format({
        thousandSeparated: true,
        mantissa: 2
      })
    }
    return null
  }

  priceChange () {
    const price = this.state.price || {}
    const latest = price.latest || {}
    const percentChange = latest.percentChange || {}
    const { rollingHour: rollingHour = null } = percentChange

    return rollingHour ? numbro(rollingHour).format({ mantissa: 2 }) : 'N/A'
  }

  scrolling = () => {
    if (!this.menuRef) return

    const menuHeight = this.menuRef.clientHeight + 100 // height of left menu + 100px positioning
    const height = document.documentElement.scrollHeight // height of document
    const scrollPosition = window.pageYOffset // how much we've scrolled
    const windowHeight = window.innerHeight // height of window
    const menuBottomPos = scrollPosition + windowHeight // bottom of menu
    const menuFooterGap = windowHeight - menuHeight // Gap between menu and footer
    const footerAllowance = 425 // Space we reserve for footer. TO DO: compute actual footer height
    const availableHeight = height - footerAllowance + menuFooterGap // Available scroll
    if (menuBottomPos >= availableHeight) {
      // assigning styles directly here, because updating state on scroll event is too expensive
      Object.assign(this.menuRef.style, {
        position: 'absolute',
        top: 'auto',
        bottom: `${footerAllowance}px`
      })
    } else {
      this.menuRef.removeAttribute('style')
    }
  };

  render () {
    const { isLoading } = this.state

    if (isLoading) {
      const asset = { symbol: this.state.symbol }
      return <CoinView {...this.props} asset={asset} isLoading={isLoading} />
    }

    const { crypto } = this.state
    const { name, symbol } = crypto
    const metadata = crypto.metadata || {}
    const { description, logo, category } = metadata

    const asset = { name, symbol }
    const btcLogo = '/static/bitcoin-logo.svg'

    return (
      <CoinView
        {...this.props}
        text={description}
        category={category}
        asset={asset}
        logo={logo}
        btcLogo={btcLogo}
        rate={this.currentPrice()}
        btcPrice={this.btcPrice()}
        change={this.priceChange()}
        setRef={this.setMenuRef}
        isLoading={isLoading}
      />
    )
  }
}

export default CoinViewContainer
