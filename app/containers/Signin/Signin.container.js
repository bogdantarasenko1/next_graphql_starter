// @flow

import React, { Component } from 'react'
import Router from 'next/router'
import CircularProgress from '@material-ui/core/CircularProgress'
import Signin from '../../components/Signin/Signin.screen'
import { saveAuth } from '../../utils/auth'

const progressStyles = {
  margin: '0 auto',
  marginTop: '20%'
}

type Props = {
  classicSignIn: any => Promise<*>,
  history: Object,
  auth: Object,
  homePage: string,
  authHeader: string,
  saveAuth: (user: Object) => void,
  t: func,
  getUserQuery: func
};

type State = {
  email: string,
  password: string,
  disableSignin: boolean,
  processingSignIn: boolean,
  signinError: ?Object,
  loading: boolean
};

export class SigninContainer extends Component<Props, State> {
  props: Props;

  constructor (props) {
    super(props)

    this.state = {
      email: '',
      password: '',
      disableSignin: false,
      processingSignIn: false,
      signinError: null,
      loading: true
    }
  }

  componentDidMount () {
    const { auth } = this.props
    const user = { jwtToken: auth.jwtToken }
    saveAuth(user)
    this.moveToHomePage()
  }

  moveToHomePage = () => {
    const { homePage } = this.props
    Router.replace(homePage)
  };

  onEmailChange = ev => {
    this.setState({ email: ev.target.value })
  };

  onPasswordChange = ev => {
    this.setState({ password: ev.target.value })
  };

  render () {
    const {
      email,
      password,
      processingSignIn,
      disableSignin,
      signinError,
      loading
    } = this.state

    const { t } = this.props

    if (loading) {
      return <CircularProgress color="secondary" style={progressStyles} />
    }

    return (
      <Signin
        email={email}
        password={password}
        processingSignIn={processingSignIn}
        disableSignin={disableSignin}
        signinError={signinError}
        onEmailChange={this.onEmailChange}
        onPasswordChange={this.onPasswordChange}
        onSubmit={this.onSignIn}
        t={t}
      />
    )
  }
}

export default SigninContainer
