// @flow

import React from 'react'
import { Grid } from '@material-ui/core'
import NewScreener from '../../components/Screener/NewScreener.container'
import Screeners from '../../components/Screener/Screeners.container'
import Layout from '../../components/MainLayout/MainLayout.screen'
import ScreenerPage from '../../components/Screener/Page/Page.container'

type Props = {
  page?: string
};

const ScreenerContainer = (props: Props) => {
  const { page, ...other } = props

  const pageScreen = () => {
    if (page === 'new') {
      return <NewScreener {...other} />
    } else {
      if (page) {
        return <ScreenerPage {...props} />
      } else {
        return <Screeners {...other} />
      }
    }
  }

  const content = () => {
    return (
      <Grid container justify="center">
        <Grid item xs={12} md={12}>
          {pageScreen()}
        </Grid>
      </Grid>
    )
  }

  return <Layout content={content()} {...other} />
}

export default ScreenerContainer
