// @flow

import React, { Component } from 'react'
import { graphql } from 'react-apollo'
import Router from 'next/router'
import gql from 'graphql-tag'
import SignUp from '../../components/SignUp/SignUp.screen'
import UnapprovedUser from '../../components/SignUp/UnapprovedUser.screen'

type Props = {
  createUser: any => Promise<*>,
  history: Object,
  homePage: string,
  saveAuth: (user: Object) => void,
  t: string => string
};

type State = {
  isUserUnapproved: boolean,
  firstName: string,
  lastName: string,
  email: string,
  password: string,
  shouldDisableSignUp: boolean,
  isProcessingSignUp: boolean,
  signUpError: ?Object
};

export class SignUpContainer extends Component<Props, State> {
  props: Props;

  constructor (props) {
    super(props)

    this.state = {
      isUserUnapproved: false,
      firstName: '',
      lastName: '',
      email: '',
      password: '',
      shouldDisableSignUp: true,
      isProcessingSignUp: false,
      signUpError: null,
      loading: true
    }
  }

  componentDidMount () {
    this.moveToHomePage()
  }

  moveToHomePage = () => {
    const { homePage } = this.props
    Router.replace(homePage)
  };

  onSignUp = () => {
    const { createUser } = this.props
    const { email, password, firstName, lastName } = this.state

    this.setState({
      isProcessingSignUp: true,
      shouldDisableSignUp: true,
      signUpError: null
    })

    createUser({
      variables: {
        input: {
          firstName,
          lastName,
          email,
          password,
          deviceId: '123'
        }
      }
    })
      .then(response => {
        const { user, jwt } = response.data.createUser
        user.jwtToken = jwt
        this.onSignUpSuccess(user)
      })
      .catch(errors => {
        const { graphQLErrors } = errors
        const error = graphQLErrors[0]
        this.onSignUpError(error.status)
      })
  };

  onSignUpSuccess = user => {
    this.setState({
      isProcessingSignUp: false,
      shouldDisableSignUp: true
    })
    this.props.saveAuth(user)
    this.moveToHomePage()
  };

  onSignUpError = errorStatus => {
    this.setState({
      isProcessingSignUp: false,
      shouldDisableSignUp: false
    })

    if (errorStatus === 'UNAUTHORIZED') {
      this.setState({ isUserUnapproved: true })
    } else {
      this.setState({
        signUpError: 'UNPROCESSABLE_ENTITY'
      })
    }
  };

  emailError () {
    const { signUpError } = this.state
    if (!signUpError) {
      return null
    }
    return this.props.t('login:Signup error, try later')
  }

  onFirstNameChange = ev => {
    this.setState({ firstName: ev.target.value })
  };

  onLastNameChange = ev => {
    this.setState({ lastName: ev.target.value })
  };

  onEmailChange = ev => {
    const email = ev.target.value.trim()
    this.setState({ email, signUpError: null })
  };

  onPasswordChange = ev => {
    const password = ev.target.value.trim()
    this.setState({ password })
  };

  isValidCredentials = () => {
    const { password, email } = this.state
    return password.length && email.length
  };

  onReturnSignup = () => {
    this.setState({
      isUserUnapproved: false,
      email: '',
      password: '',
      firstName: '',
      lastName: ''
    })
  };

  render () {
    const {
      email,
      password,
      isProcessingSignUp,
      shouldDisableSignUp,
      isUserUnapproved,
      signUpError
    } = this.state

    if (isUserUnapproved) {
      return <UnapprovedUser {...this.props} submit={this.onReturnSignup} />
    }

    return (
      <SignUp
        email={email}
        password={password}
        isUserUnapproved={isUserUnapproved}
        isProcessingSignUp={isProcessingSignUp}
        shouldDisableSignUp={shouldDisableSignUp && !this.isValidCredentials()}
        signUpError={signUpError}
        emailError={this.emailError()}
        onFirstNameChange={this.onFirstNameChange}
        onLastNameChange={this.onLastNameChange}
        onEmailChange={this.onEmailChange}
        onPasswordChange={this.onPasswordChange}
        onSubmit={this.onSignUp}
      />
    )
  }
}

export const SIGNUP_MUTATION = gql`
  mutation createUser($input: createUserInput!) {
    createUser(input: $input) {
      user {
        id
        email
        firstName
        lastName
        bio
        avatarUrl
      }
      jwt
    }
  }
`

export const SIGNIN_MUTATION = gql`
  mutation signInClassic($input: signInClassicInput!) {
    signInClassic(input: $input) {
      user {
        id
        email
        displayName
        firstName
        lastName
        bio
        avatarUrl
      }
      jwt
    }
  }
`

export const signupMutation = graphql(SIGNUP_MUTATION, {
  name: 'createUser'
})

export default signupMutation(SignUpContainer)
