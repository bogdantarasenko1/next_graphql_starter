// @flow

import React from 'react'
import Profile from '../../components/Profile/Profile.screen'
import ProfileOnScreen from '../../components/ProfileDialog/ProfileOnScreen.container'

type Props = {
  user: Object
};

class ProfileContainer extends React.PureComponent<Props> {
  constructor (props: Props) {
    super(props)
    this.state = { editProfile: false }
  }

  onEditProfile = e => {
    e.preventDefault()

    this.setState({
      editProfile: true
    })
  };

  onCloseProfile = () => {
    this.setState({
      editProfile: false
    })
  };

  render () {
    const { editProfile } = this.state
    return (
      <div>
        <Profile {...this.props} editProfile={this.onEditProfile} />
        {editProfile && (
          <ProfileOnScreen
            onCloseProfile={this.onCloseProfile}
            editProfile={editProfile}
            {...this.props}
          />
        )}
      </div>
    )
  }
}

export default ProfileContainer
