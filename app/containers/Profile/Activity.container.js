// @flow

import React, { Component } from 'react'
import ActivityScreen from '../../components/CoinView/Pages/CoinPages.screen.js'
import ResearchActivityContainer from '../../components/Profile/Activity/Research/Research.container'
import ProfileActivityContainer from '../../components/Profile/Activity/Activity/Activity.container'
import ProfileFollowersContainer from '../../components/Profile/Activity/Followers/Followers.container'
import ProfileFollowingContainer from '../../components/Profile/Activity/Following/Following.container'

const tabItems = [
  {
    key: 'research',
    title: 'Research',
    content: <ResearchActivityContainer />
  },
  { key: 'activity', title: 'Activity', content: <ProfileActivityContainer /> },
  {
    key: 'following',
    title: 'Following (0)',
    content: <ProfileFollowingContainer />
  },
  {
    key: 'followers',
    title: 'Followers (0)',
    content: <ProfileFollowersContainer />
  }
]

type State = {
  selectedTab: string
};

type Props = {};

class ActivityContainer extends Component<State, Props> {
  state: State;
  props: Props;

  constructor (props) {
    super(props)
    this.state = {
      selectedTab: 0
    }
  }

  onSelectTab = (event, value) => {
    this.setState({ selectedTab: value })
  };

  render () {
    const { selectedTab } = this.state
    return (
      <ActivityScreen
        {...this.props}
        selectedTab={selectedTab}
        onSelectTab={this.onSelectTab}
        tabItems={tabItems.map(item => ({ key: item.key, title: item.title }))}
        content={tabItems[selectedTab].content}
      />
    )
  }
}

export default ActivityContainer
