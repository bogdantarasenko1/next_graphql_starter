// @flow

import React from 'react'
import { Redirect, Route } from 'react-router-dom'

type Props = {
  component: React.Element<Any>,
  authorizedUser: boolean
}

const ProtectedRoute = ({ component: Component, authorizedUser, ...rest }: Props) => (
  <Route
    {...rest}
    render={(props: Object) =>
      authorizedUser ? (<Component {...props} />) : (
        <Redirect
          to={{
            pathname: '/login',
            state: { from: props.location }
          }}
        />
      )
    }
  />
)

export default ProtectedRoute
