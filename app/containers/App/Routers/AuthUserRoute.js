// @flow

import React from 'react'
import { Redirect, Route } from 'react-router-dom'

type Props = {
  component: React.Element<Any>,
  path: string,
  authorizedUser: boolean
}

const AuthUserRoute = ({ component: Component, path, authorizedUser, ...rest }: Props) => (
  <Route
    {...rest}
    render={(props: Object) =>
      authorizedUser ? (
        <Redirect
          to={{
            pathname: path,
            state: { from: props.location }
          }}
        />
      ) : (<Component {...props} />)
    }
  />
)

export default AuthUserRoute
