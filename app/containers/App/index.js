// @flow

import React, { Component } from 'react'
import { Route, Switch, Redirect } from 'react-router-dom'
import Profile from '../../containers/Profile/Profile.container'
import SignIn from '../../containers/Signin/Signin.container'
import SignUp from '../../containers/SignUp/SignUp.container'
import Community from '../../containers/Community/Community.container'
import CoinView from '../../containers/CoinView/CoinView.container'
import CriticalEvents from '../../components/CriticalEvents/Page/CriticalEventsPage.screen'
import Dashboard from '../../components/Dashboard/Dashboard.container'
import { getUser } from '../../utils/auth'
import AuthUserRoute from './Routers/AuthUserRoute'
import ProtectedRoute from './Routers/ProtectedRoute'

class App extends Component {
  props: Props;

  authorizedUser () {
    const user = getUser()
    const authorized = user && user.jwtToken
    return authorized
  }

  render () {
    return (
      <Switch>
        <Route exact path="/" component={Dashboard} />
        <Route
          path="/login"
          render={props => <SignIn {...props} homePage="/dashboard" />}
        />
        <Route
          path="/signup"
          render={props => <SignUp {...props} homePage="/dashboard" />}
        />
        <Route path="/dashboard" component={Dashboard} />
        <Route path="/community" component={Community} />
        <Route path="/coin/:code" component={CoinView} />
        <Route path="/critical-events" component={CriticalEvents} />
        <AuthUserRoute
          path="/login"
          authorizedUser={this.authorizedUser()}
          component={Profile}
        />
        <AuthUserRoute
          path="/signup"
          authorizedUser={this.authorizedUser()}
          component={Profile}
        />
        <ProtectedRoute
          path="/profile"
          authorizedUser={this.authorizedUser()}
          component={Profile}
        />
        <Redirect to="/" />
      </Switch>
    )
  }
}

export default App
