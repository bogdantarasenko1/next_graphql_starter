// @flow

import React, { PureComponent } from 'react'
import { getUser } from '../../utils/auth'
import { browserHistory } from 'react-router'
type Props = {
  children?: React.Node,
  location?: string
}

class LoggedInContainer extends PureComponent<Props> {
  props: Props

  render () {
    const user = getUser()
    const authorized = user && user.jwtToken

    if (authorized) {
      return this.props.children
    } else {
      browserHistory.replace('/')
      return null
    }
  }
}

export default LoggedInContainer
