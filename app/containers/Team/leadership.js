export default [
  {
    image: 'Alex.png',
    fullName: 'Alex Bradford',
    position: 'Co-Founder and CEO',
    text:
      'Alex is a veteran technology investor, serial entrepreneur, and active cryptocurrency investor. He is the founder of Voyager Global Advisors, Voyager Global Capital Partners, and BIG, a group of e-commerce companies. Alex was formerly a tech/media Research Analyst at SAC Capital Advisors, a $14B hedge fund, investment banking Analyst at Goldman Sachs (TMT), and Business Analyst McKinsey & Co. He received a BA from Stanford with Honors and Distinction and an MBA from the Harvard Business School.',
    logo: ['Stanford.png', 'Goldman.png', 'Harvard.png', 'McKinsey.png']
  },
  {
    image: 'DrGarrick.png',
    fullName: 'Dr. Garrick Hileman',
    position: 'Co-Founder',
    text:
      'Garrick is one of the world\'s most-cited researchers of cryptocurrencies and distributed ledger technologies. He received his PhD from the London School of Economics and is a researcher at the University of Cambridge, where he teaches classes on cryptocurrencies and distributed leader technologies. He is the author of the "2017 Global Cryptocurrency Benchmarking Study" (University of Cambridge) and the follow-on "2017 Global Blockchain Benchmarking Study" (University of Cambridge). He also created and published the CoinDesk State of Bitcoin and State of Blockchain reports from 2013-2016. His prior work experience includes equity research and corporate ﬁnance for a San Francisco investment bank.',
    logo: ['Cambridge.png', 'Coindesk.png', 'LondonSchool.png']
  },
  {
    image: 'Alpkaan.png',
    fullName: 'Alpkaan Celik',
    position: 'Chief Data Scientist',
    text:
      'Alpkaan is a mathematician and data scientist. Before joining Mosaic, Alpkaan was a Program Manager at Microsoft, where he led various data science initiatives in the Web Platform Team. Alpkaan also worked in investment banking at Citigroup and was a Research Analyst at the Harvard Business School. He graduated from Harvard College with a BA in Applied Mathematics and a minor in Government. He also focused on Economics throughout his studies.',
    logo: ['HarvardUniversity.png', 'Harvard.png', 'Microsoft.png']
  },
  {
    image: 'DrDavid.png',
    fullName: 'Dr. David Horning, CFA',
    position: 'Chief Financial Officer',
    text:
      'For the last 5 years, David was CFO of several technology startups in San Francisco. Before that, he was an investor at Palo Alto Investors for 5 years, and a capital markets Research Analyst at Washington Mutual Bank. David has an MS in Management Science & Engineering from Stanford University, and a PhD in Mechanical Engineering from Stanford University.',
    logo: ['PaloAlto.png', 'Stanford.png']
  },
  {
    image: 'Zafar.png',
    fullName: 'Zafar Gilani',
    position: 'Chief Data Architect',
    text:
      'Zafar is currently pursuing the final stages of PhD at the University of Cambridge. The topic of his research has been understanding the agents of social media influence, with a particular focus on automated social agents (aka bots). His research has been interdisciplinary involving techniques from statistical analysis, computational social science and machine learning. He has published a number of papers at top conferences during this time. Prior to starting his doctoral research he worked at GfK SE (2013-2014), Telefonica Research (2013) and Stanford University (2009-2011). He completed joint MS in distributed computing from KTH and UPC in 2013, and won the best paper award for his work. He completed BS in IT from NUST in 2009, and won the best degree project award.',
    logo: ['Cambridge.png']
  },
  {
    image: 'Soren.png',
    fullName: 'Soren Stammers',
    position: 'Chief Technology Officer',
    text:
      'Soren is a veteran computer science professional with 40 years of experience ranging from the early days of handcoding through to the latest in decentralised web technologies. He served as CTO for a number of successful startups from the early days of Silicon Valley to London. These included the first string early cryptography development outside the USA that became the basis for the Java Cryptography Extension and development and patent for the first cryptographically secure Browser Wallet. He also designed, developed and patented a protocol for P2P smart contract exchange, predating the Blockchain by over a decade. During this time he was an active member of CypherPunks. The scope of his FinTech consultancy has been in renovation projects delivering core business and IT architecture design and delivery.',
    logo: []
  }
]
