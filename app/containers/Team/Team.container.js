// @flow

import React from 'react'
import Team from '../../components/Team/Team.screen'
import leadership from './leadership'
import technology from './technology'

type Props = {};

const TeamContainer = (props: Props) => {
  return <Team leadership={leadership} technology={technology} {...props} />
}

export default TeamContainer
