export default [
  {
    image: 'Ben.png',
    fullName: 'Ben Fisch',
    position: 'Software Architect',
    text:
      "Ben is currently a PhD candidate in the computer science department at Stanford University, working with Dan Boneh and the applied cryptography research group. His previous research affiliations include: the Weizmann Institute, Columbia University, and Cornell-Tech. His research spans various topics in cryptography and security, including cryptocurrencies, secure computation, private database search (see Blind Seer), and applications of hardware security environments such as Intel SGX. He also has industry experience as a software developer for 1010data and as a consultant for LGS Innovations. He received his bachelor's degree in Mathematics from the University of Pennsylvania in May 2013.",
    logo: ['Stanford.png']
  },
  {
    image: 'DrGreg.png',
    fullName: 'ADr. Greg Wientjes',
    position: 'Chief Data Architect',
    text:
      "Dr. Greg Wientjes, has been a software developer for the past 14 years. He was awarded his PhD at Stanford in 2010, and completed his Master's of Science in Electrical Engineering (2006) and his Bachelor of Science degree in Mathematics (2004), both from Stanford. He attended Singularity University (2009), and Draper University (2013). Dr. Wientjes is the author of four books on technology, and is founder of an Internet marketplace for university students and faculty.",
    logo: ['Stanford.png']
  }
]
