// @flow

import React, { Component } from 'react'
import Dashboard from '../../components/Dashboard/Dashboard.container'

type Props = {
  history: Object,
  auth: Object,
  saveAuth: (user: Object) => void
};

type State = {};

export class CommunityContainer extends Component<Props, State> {
  props: Props;

  constructor (props) {
    super(props)

    this.state = {}
  }

  componentDidMount () {}

  render () {
    return <Dashboard {...this.props} />
  }
}

export default CommunityContainer
