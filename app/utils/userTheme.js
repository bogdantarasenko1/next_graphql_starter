import jsCookie from 'js-cookie'
import { isLocalStorageNameSupported } from './utils'

export const themeName = {
  dark: 'dark',
  light: 'light'
}

export const saveTheme = theme => {
  jsCookie.set('theme', theme, { expires: 7 })

  if (!isLocalStorageNameSupported()) {
    return
  }

  window.localStorage.setItem('theme', theme)
}

export const getTheme = () => {
  const theme = jsCookie.get('theme')
  return theme || themeName.dark
}

export const getServerUserTheme = coockie => {
  const { theme } = coockie
  return theme || themeName.dark
}
