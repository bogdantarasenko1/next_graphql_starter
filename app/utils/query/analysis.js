export default symbol => `{
  getCrypto(symbol: "${symbol}") {
    analysis {
      industry
      technology
      competition
      risks
    }
    metadata {
      description
      logo
      tags
      category
      organizations {
        name
        investors {
          name
          photo
        }
        employees {
          person {
            name
          photo
          }
        }
        advisors {
          name
          photo
        }
        urls {
          websites
        }
      }
    }
  }
}`
