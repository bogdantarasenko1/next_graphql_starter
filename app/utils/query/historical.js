export default (symbol, timeRange, interval) => `
  {
  getCrypto(symbol:"${symbol}") {
    symbol
    market {
      historical(timeRange: {from: "${timeRange.from}", to: "${
  timeRange.to
}"}, interval: {amount: ${interval.amount}, unit: ${interval.unit}}) {
        volume {
          rollingHour {
            amount
          }
        }
        latestPrice {
          amount
          currency
        }
        marketCap {
          basedOnTotalSupply {
            amount
          }
        }
        timestamp
      }
      latest {
        volume {
          rollingHour {
            amount
          }
        }
        priceChange {
          rollingHour {
            amount
          }
        }
        percentChange {
          rollingHour
        }
        marketCap {
          basedOnCirculatingSupply {
            amount
          }
          basedOnTotalSupply {
            amount
          }
          basedOnMaxSupply {
            amount
          }
        }
      }
    }
  }
}`
