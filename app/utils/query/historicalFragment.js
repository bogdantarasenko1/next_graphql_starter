export default ({ timeRange, interval }) => `
historical(timeRange: {from: "${timeRange.from}", to: "${
  timeRange.to
}"}, interval: {amount: ${interval.amount}, unit: ${interval.unit}}) {
  volume {
    rollingHour {
      amount
    }
  }
  latestPrice {
    amount
    currency
  }
  marketCap {
    basedOnTotalSupply {
      amount
    }
  }
  timestamp
}`
