export default symbol => `{
  getCrypto(symbol: "${symbol}") {
    name
    symbol
    market {
      latest {
        marketCap {
          basedOnCirculatingSupply {
            amount
          }
          basedOnMaxSupply {
            amount
          }
        }
        priceChange {
          rollingYear {
            amount
          }
        }
      }
    }
    blockchain {
      supplyType 
      firstBlockDate
      distributionMethod
      latest {
        age {
          amount
          unit
        }
        blockTime {
          amount
          unit
        }
        blockCount
        totalBlockSize {
          amount
          unit
        }
        blockReward
        inflation
        feesPercentOfValue
        circulatingSupply
        totalSupply
        medianTxValue {
          amount
        }
        txCount
        adjustedTxVolume {
          amount
        }
        activeAddresses
      }
    }
  }
}`
