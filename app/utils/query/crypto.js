export default (symbol, currency) => `{
getCrypto(symbol: "${symbol}")
  {
    name
    symbol
    metadata {
      description
      logo
      tags
      category
    }
    ${symbol}Price: market {
      symbol
      latest {
        latestPrice {
          amount
          currency
        }
        percentChange {
          rollingHour
        }
      }
    }
    currencyPrice: market(currency: "${currency}") {
      symbol
      latest {
        latestPrice {
          amount
          currency
        }
        percentChange {
          rollingHour
        }
      }
    }
  }
}`
