export default ({ pagination, sorting, filters, fields }) => `{
  getCryptos(
    filters: { ${filters} }, 
    sorting: { field: ${sorting.field}, order: ${sorting.order}}
    pagination: { offset: ${pagination.offset}, limit: ${pagination.limit}}
  )
  {
    metadata { totalCount }
    cryptos ${fields}
  }
}
`
