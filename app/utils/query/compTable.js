export default ({ historicalFragment, pagination }) => `{
  getCryptos(pagination: { offset: ${pagination.offset}, limit: ${
  pagination.limit
} }) {
    cryptos {
      symbol
      market {
        ${historicalFragment}
        latest {
          volume {
            rollingHour {
              amount
            }
          }
          priceChange {
            rollingHour {
              amount
            }
          }
          percentChange {
            rollingHour
          }
          marketCap {
            basedOnCirculatingSupply {
              amount
            }
            basedOnTotalSupply {
              amount
            }
            basedOnMaxSupply {
              amount
            }
          }
        }
        
        latest {
          latestPrice {
            amount
          }
          priceChange {
            rollingHour {
              amount
            }
          }
          percentChange {
            rollingHour
          }
          volume {
            rollingHour {
              amount
            }
          }
          marketCap {
            basedOnCirculatingSupply {
              amount
            }
          }
        }
      }
    }
  }
}
`
