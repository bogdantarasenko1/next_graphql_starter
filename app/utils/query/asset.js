export default symbol => `
  {
  getCrypto(symbol: "${symbol}") {
    name
    symbol
    metadata {
      description
      logo
      tags
    }
    analysis {
      overview
    }
    market {
      latest {
        latestPrice {
          amount
          currency
        }
        priceChange {
          sinceMidnight {
            amount
            currency
          }
        }
        percentChange {
          sinceMidnight
        }
      }
    }
  }
}`
