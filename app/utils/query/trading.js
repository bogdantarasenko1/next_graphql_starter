export default ({ pagination, sorting }) => `{
  getCryptos(
    sorting: { field: ${sorting.field}, order: ${sorting.order}}
    pagination: { offset: ${pagination.offset}, limit: ${pagination.limit}}
  )  {
    metadata {
      totalCount
    }
    cryptos {
      symbol
      name
      metadata {
        logo
      }
      blockchain {
        latest {
          circulatingSupply
          totalSupply
        }
      }
      BTCPrice: market(currency: "BTC") {
        latest {
          latestPrice {
            amount
          }
          percentChange {
            rollingDay
          }
        }
      }
      USDPrice: market {
        latest {
          marketCap {
            basedOnCirculatingSupply {
              amount
            }
            basedOnMaxSupply {
              amount
            }
          }
          latestPrice {
            amount
          }
          percentChange {
            rollingDay
            rollingMonth
            rollingYear
          }
          volume {
            rollingDay {
              amount
            }
          }
        }
      }
    }
  }
}
`
