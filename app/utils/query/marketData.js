export default (symbol, currency) => `{
getCrypto(symbol: "${symbol}")
  {
    name
    symbol
    metadata {
      description
      logo
      tags
    }
    ${symbol}Price: market(currency: "${symbol}") {
      symbol
      latest {
        latestPrice {
          amount
          currency
        }
        percentChange {
          rollingHour
        }
      }
    }
    ${currency}Price: market(currency: "${currency}") {
      symbol
      latest {
        latestPrice {
          amount
          currency
        }
        percentChange {
          rollingHour
        }
      }
    }
  }
}`
