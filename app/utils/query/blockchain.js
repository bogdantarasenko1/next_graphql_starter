export default ({ pagination, sorting }) => `{
  getCryptos(
    sorting: { field: ${sorting.field}, order: ${sorting.order}}
    pagination: { offset: ${pagination.offset}, limit: ${pagination.limit}}
  )  {
    metadata {
      totalCount
    }
    cryptos {
      symbol
      name
      metadata {
        logo
      }
      blockchain {
        latest {
          totalFees {
            amount
          }
          feesPercentOfValue
          txVolume {
            amount
          }
          adjustedTxVolume {
            amount
          }
          averageTxValue {
            amount
          }
          txCount
          activeAddresses
          medianTxValue {
            amount
          }
          medianTxValue {
            amount
          }
          medianFee {
            amount
          }
          difficulty
        }
      }
    }
  }
}
`
