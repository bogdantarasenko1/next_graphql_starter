export default ({ symbol, historicalFragment }) => `{
  getCrypto(symbol: "${symbol}") {
    symbol
    market {
      ${historicalFragment}
    }
  }
}
`
