import getConfig from 'next/config'
import axios from 'axios'
import cryptoQuery from './query/crypto'
import markerDataQuery from './query/marketData'
import analysisQuery from './query/analysis'

const {
  publicRuntimeConfig: { API_URL, API_KEY }
} = getConfig()

export const getCrypto = (symbol, currency) => {
  const query = JSON.stringify({
    query: cryptoQuery(symbol, currency)
  })

  return fetchData(query).then(r => {
    return r.data.getCrypto
  })
}

export const analysis = symbol => {
  const query = JSON.stringify({
    query: analysisQuery(symbol)
  })

  return fetchData(query).then(r => {
    return r.data.getCrypto
  })
}

export const marketData = (symbol, currency = 'BTC') => {
  const query = JSON.stringify({
    query: markerDataQuery(symbol, currency)
  })

  return fetchData(query).then(r => {
    return r.data.getCrypto
  })
}

export const fetchData = query => {
  return axios
    .post(API_URL, query, {
      headers: {
        'Content-Type': 'application/json',
        'x-api-key': API_KEY
      }
    })
    .then(r => r.data)
}
