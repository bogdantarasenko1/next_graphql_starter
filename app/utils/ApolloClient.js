// @flow

import _ from 'lodash'
import { ApolloClient } from 'apollo-client'
import { createHttpLink } from 'apollo-link-http'
import { setContext } from 'apollo-link-context'
import { onError } from 'apollo-link-error'
import { InMemoryCache } from 'apollo-cache-inmemory'
import { ApolloLink } from 'apollo-link'
import { withClientState } from 'apollo-link-state'
import fetch from 'isomorphic-unfetch'
import getConfig from 'next/config'
import {
  typeDefs,
  resolvers,
  defaults
} from '../components/TableColumns/LocalState/ColumnsLocalState'
import {
  typeDefs as profileTypeDefs,
  resolvers as profileResolvers,
  defaults as profileDefaults
} from '../components/ProfileDialog/LocalState/ProfileLocalState'

let apolloClient = null

// Polyfill fetch() on the server (used by apollo-client)
if (!process.browser) {
  global.fetch = fetch
}

function graphqlUrl () {
  const {
    publicRuntimeConfig: { APOLLO_URL }
  } = getConfig()
  return APOLLO_URL
}

function create (initialState, { getToken }) {
  const httpLink = createHttpLink({ uri: graphqlUrl() })

  const authLink = setContext((_, { headers }) => {
    const token = window.localStorage.getItem('jwtToken')
    return {
      headers: {
        ...headers,
        authorization: token ? `Bearer ${token}` : ''
      }
    }
  })

  // Log all incoming errors just in case
  const errorLink = onError(({ graphQLErrors, networkError }) => {
    if (graphQLErrors) {
      if (Array.isArray(graphQLErrors)) {
        graphQLErrors.map(({ message, locations, path }) =>
          console.log(
            `[GraphQL error]: Message: ${message}, Location: ${locations}, Path: ${path}`
          )
        )
      } else {
        console.log(`[GraphQL error]: ${graphQLErrors}`)
      }
    }

    if (networkError) console.log(`[Network error]: ${networkError}`)
  })

  const cache = new InMemoryCache().restore(initialState || {})

  const stateLink = withClientState({
    cache,
    resolvers: _.merge(resolvers, profileResolvers),
    typeDefs: [typeDefs, profileTypeDefs].join('"\n"'),
    defaults: _.merge(defaults, profileDefaults)
  })

  const client = new ApolloClient({
    connectToDevTools: process.browser,
    ssrMode: !process.browser,
    link: ApolloLink.from([errorLink, stateLink, authLink.concat(httpLink)]),
    cache
  })

  return client
}

export default function initApollo (initialState, options) {
  // Make sure to create a new client for every server-side request so that data
  // isn't shared between connections (which would be bad)
  if (!process.browser) {
    return create(initialState, options)
  }

  // Reuse client on the client-side
  if (!apolloClient) {
    apolloClient = create(initialState, options)
  }

  return apolloClient
}
