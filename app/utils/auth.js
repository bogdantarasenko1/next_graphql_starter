import jsCookie from 'js-cookie'
import { isLocalStorageNameSupported } from './utils'

const parseUser = user => (user ? JSON.parse(user) : null)

export const saveAuth = user => {
  if (user !== undefined) {
    jsCookie.set('user', user, { expires: 7 })
  } else {
    return
  }
  if (user.jwtToken !== undefined) {
    jsCookie.set('jwtToken', user.jwtToken, { expires: 7 })
  }
  if (user.email !== undefined) {
    jsCookie.set('email', user.email, { expires: 7 })
  }

  if (!isLocalStorageNameSupported()) {
    return
  }

  if (user.jwtToken !== undefined) {
    window.localStorage.setItem('jwtToken', user.jwtToken)
  }
  if (user.email !== undefined) {
    window.localStorage.setItem('email', user.email)
  }
  window.localStorage.setItem('user', JSON.stringify(user))
}

export const getUser = () => {
  const user = jsCookie.get('user')
  return parseUser(user)
}

export const getServerUser = coockie => {
  const { user } = coockie
  return parseUser(user)
}

export const logout = () => {
  jsCookie.remove('jwtToken')
  jsCookie.remove('email')
  jsCookie.remove('user')
  if (!isLocalStorageNameSupported()) {
    return
  }
  window.localStorage.removeItem('jwtToken')
  window.localStorage.removeItem('email')
  window.localStorage.removeItem('user')
}

export const authorizedUser = () => {
  const user = parseUser(jsCookie.get('user'))
  const authorized = user && user.jwtToken
  return authorized
}
