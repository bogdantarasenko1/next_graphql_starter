// @flow

import React from 'react'
import axios from 'axios/index'
import moment from 'moment'
import numbro from 'numbro'
import _ from 'lodash'
import getConfig from 'next/config'
import blockchainQuery from './query/blockchain'
import historicalFragmentQuery from './query/historicalFragment'

const {
  publicRuntimeConfig: { API_URL, API_KEY }
} = getConfig()

type SortDirection = {
  asc: "asc",
  desc: "desc"
};

type State = {
  errors: ?[],
  sort: SortDirection,
  sortKey: string,
  sortedField: string,
  assets: [],
  isLoading: boolean,
  dataLength: number
};

type Props = {
  primaryAsset?: string
};

export const platformQuery = ({ mapExtraData } = {}) => WrappedComponent => {
  return class Component extends React.Component<Props, State> {
    props: Props;
    state: State;

    rowsOnPage = 20;

    primaryAsset = null;
    otherAssets = [];

    constructor (props) {
      super(props)

      this.state = {
        sort: 'desc',
        sortKey: 'feesPercentOfValue',
        sortedField: 'feesPercentOfValue',
        assets: [],
        isLoading: true,
        dataLength: 0
      }
    }

    componentDidMount () {
      this.loadData()
    }

    splitAssets = assets => {
      const { primaryAsset: symbol } = this.props
      if (symbol) {
        const primaryAsset = _.remove(assets, n => n.symbol === symbol).pop()
        this.primaryAsset = primaryAsset
        this.otherAssets = assets
      } else {
        this.otherAssets = assets
      }

      this.sortBy()
    };

    loadData (offset = 0) {
      const startDate = moment()
        .subtract(6, 'month')
        .startOf('day')
        .format()
      const endDate = moment().format()

      const historicalFragment = historicalFragmentQuery({
        timeRange: { from: startDate, to: endDate },
        interval: { amount: 7, unit: 'Day' }
      })

      const { sortKey, sort } = this.state

      const query = JSON.stringify({
        query: blockchainQuery({
          historicalFragment: (false && historicalFragment) || '',
          pagination: { offset: offset, limit: this.rowsOnPage },
          sorting: { field: sortKey, order: sort.toUpperCase() }
        })
      })

      return fetchData(query)
        .then(r => {
          const {
            data: {
              getCryptos: { cryptos: data }
            },
            errors
          } = r

          const dataLength = r.data.getCryptos.metadata.totalCount

          if (errors) {
            this.setState({ errors: errors })
          }

          this.setState({
            dataLength: dataLength
          })

          const assets = data.map(asset => {
            let { symbol, name, metadata, blockchain } = asset

            blockchain = Object.assign({}, blockchain)
            metadata = Object.assign({}, metadata)

            const { latest: blockchainData } = blockchain

            const info = {
              symbol,
              name,
              totalFees: null,
              feesPercentOfValue: null,
              txVolume: null,
              adjustedTxVolume: null,
              txCount: null,
              activeAddresses: null,
              averageTxValue: null,
              medianTxValue: null,
              medianFee: null,
              difficulty: null
            }

            if (metadata.logo) {
              info.ico = metadata.logo
            } else {
              info.ico = mapExtraData().ico
            }

            if (blockchainData) {
              const {
                totalFees,
                txVolume,
                adjustedTxVolume,
                averageTxValue,
                medianTxValue,
                medianFee
              } = blockchainData

              if (totalFees) {
                info.totalFees = totalFees.amount
              }

              if (txVolume) {
                info.txVolume = txVolume.amount
              }

              if (adjustedTxVolume) {
                info.adjustedTxVolume = adjustedTxVolume.amount
              }

              if (averageTxValue) {
                info.averageTxValue = averageTxValue.amount
              }

              if (medianTxValue) {
                info.medianTxValue = medianTxValue.amount
              }

              if (medianFee) {
                info.medianFee = medianFee.amount
              }

              const {
                feesPercentOfValue,
                txCount,
                activeAddresses,
                difficulty
              } = blockchainData

              info.feesPercentOfValue = feesPercentOfValue
              info.txCount = txCount
              info.activeAddresses = activeAddresses
              info.difficulty = difficulty
            }

            return info
          })

          this.splitAssets(assets)
        })
        .catch(e => {
          this.setState({ errors: e, isLoading: false })
          return e
        })
    }

    sortBy = () => {
      const otherAssets = this.otherAssets
      const assets = this.primaryAsset ? [this.primaryAsset] : []
      this.setState({
        assets: [...assets, ...otherAssets],
        isLoading: false
      })
    };

    sortData = key => {
      let ordering = ''
      switch (key) {
        case 'txVolume':
          ordering = 'txVolume'
          break
        case 'txCount':
          ordering = 'txCount'
          break
        case 'adjustedTxVolume':
          ordering = 'adjTxVolume'
          break
        case 'averageTxValue':
          ordering = 'averageTxValue'
          break
        case 'medianTxValue':
          ordering = 'medianTxValue'
          break
        case 'totalFees':
          ordering = 'totalFees'
          break
        case 'medianFee':
          ordering = 'medianFee'
          break
        case 'feesPercentOfValue':
          ordering = 'feesPercentOfValue'
          break
        case 'activeAddresses':
          ordering = 'activeAddresses'
          break
        case 'difficulty':
          ordering = 'difficulty'
          break
        default:
          console.log(`unhandled ordering key '${key}'`)
          break
      }

      const { sort: oldDirection } = this.state

      this.setState(prevState => {
        return {
          sort: this.nextSortDirection(
            prevState.sortKey,
            ordering,
            oldDirection
          ),
          sortKey: ordering,
          sortedField: key,
          errors: null
        }
      }, this.loadData)
    };

    nextSortDirection = (oldKey, newKey, oldDirection) => {
      if (oldKey === newKey) {
        return oldDirection === 'asc' ? 'desc' : 'asc'
      }

      return 'desc'
    };

    onPageUpdate = page => {
      this.setState({ isLoading: true })
      this.loadData(page * this.rowsOnPage)
    };

    render () {
      const { sortKey, ...state } = this.state

      return (
        <WrappedComponent
          sortBy={this.sortData}
          onPageUpdate={this.onPageUpdate}
          {...this.props}
          {...state}
        />
      )
    }
  }
}

export const fetchData = (query, cancelToken = null) => {
  return axios
    .post(API_URL, query, {
      headers: {
        'Content-Type': 'application/json',
        'x-api-key': API_KEY
      },
      cancelToken: cancelToken
    })
    .then(r => r.data)
}

export const formatNumbers = assets => {
  return assets.map(event => {
    const eventCopy = { ...event }

    eventCopy.totalFees = eventCopy.totalFees
      ? numbro(eventCopy.totalFees).formatCurrency({
        mantissa: 2
      })
      : 'N/A'

    eventCopy.feesPercentOfValue = eventCopy.feesPercentOfValue
      ? `${numbro(eventCopy.feesPercentOfValue).format({ mantissa: 8 })}%`
      : 'N/A'

    eventCopy.txVolume = eventCopy.txVolume
      ? numbro(eventCopy.txVolume).formatCurrency({
        thousandSeparated: true,
        mantissa: 2
      })
      : 'N/A'

    eventCopy.adjustedTxVolume = eventCopy.adjustedTxVolume
      ? numbro(eventCopy.adjustedTxVolume).formatCurrency({
        mantissa: 2
      })
      : 'N/A'

    eventCopy.txCount = eventCopy.txCount
      ? numbro(eventCopy.txCount).format({ thousandSeparated: true })
      : 'N/A'

    eventCopy.activeAddresses = eventCopy.activeAddresses
      ? numbro(eventCopy.activeAddresses).format({ thousandSeparated: true })
      : 'N/A'

    eventCopy.averageTxValue = eventCopy.averageTxValue
      ? numbro(eventCopy.averageTxValue).formatCurrency({ mantissa: 2 })
      : 'N/A'

    eventCopy.medianTxValue = eventCopy.medianTxValue
      ? numbro(eventCopy.medianTxValue).formatCurrency({ mantissa: 2 })
      : 'N/A'

    eventCopy.medianFee = eventCopy.medianFee
      ? numbro(eventCopy.medianFee).format({ mantissa: 2 })
      : 'N/A'

    eventCopy.difficulty = eventCopy.difficulty
      ? numbro(eventCopy.difficulty).format({
        thousandSeparated: true,
        mantissa: 2
      })
      : 'N/A'

    return eventCopy
  })
}
