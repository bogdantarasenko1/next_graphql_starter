export const clientStatus = Error => {
  const { graphQLErrors } = Error

  if (Array.isArray(graphQLErrors)) {
    const error = graphQLErrors[0]

    return error.status
  }
}

export const clientMessage = Error => {
  const { graphQLErrors } = Error

  if (Array.isArray(graphQLErrors)) {
    const error = graphQLErrors[0]

    return error.message
  }
}

export const errorStatus = {
  INVALID_CREDENTIALS: 'INVALID_CREDENTIALS'
}
