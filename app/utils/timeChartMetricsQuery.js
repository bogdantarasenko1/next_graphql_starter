export const availableQueries = [
  'Price (USD)',
  'Price (BTC)',
  'Transaction Volume',
  'Transaction Count',
  'Circulating Supply',
  'Active Addresses',
  'Adjusted Transaction Volume',
  'Median Transaction Value',
  'Average Transaction Value',
  'Fees',
  'Median Fees',
  // 'Difficulty',
  'Fees Percent Value',
  'Circulating Percent Total',
  'Circulating Coin Cap',
  'Max Coin Cap'
]

const blockchainQuery = [
  'TRANSACTIONVOLUME',
  'TRANSACTIONCOUNT',
  'CIRCULATINGSUPPLY',
  'ACTIVEADDRESSES',
  'ADJUSTEDTRANSACTIONVOLUME',
  'MEDIANTRANSACTIONVALUE',
  'AVERAGETRANSACTIONVALUE',
  'FEES',
  'MEDIANFEES',
  'DIFFICULTY',
  'FEESPERCENTVALUE',
  'CIRCULATINGPERCENTTOTAL'
]

const cryptoQuery = [
  'CIRCULATINGCOINCAP',
  'MAXCOINCAP',
  'PRICE(USD)',
  'PRICE(BTC)',
  'VOLUME'
]

export const getQueryField = metricField => {
  switch (metricField) {
    case 'TRANSACTIONVOLUME':
      return 'txVolume{amount}'
    case 'TRANSACTIONCOUNT':
      return 'txCount'
    case 'CIRCULATINGSUPPLY':
      return 'circulatingSupply'
    case 'ACTIVEADDRESSES':
      return 'activeAddresses'
    case 'ADJUSTEDTRANSACTIONVOLUME':
      return 'adjustedTxVolume{amount}'
    case 'MEDIANTRANSACTIONVALUE':
      return 'medianTxValue{amount}'
    case 'AVERAGETRANSACTIONVALUE':
      return 'averageTxValue{amount}'
    case 'FEES':
      return 'totalFees{amount}'
    case 'MEDIANFEES':
      return 'medianFee{amount}'
    case 'DIFFICULTY':
      return 'difficulty'
    case 'FEESPERCENTVALUE':
      return 'feesPercentOfValue'
    case 'CIRCULATINGCOINCAP':
      return 'marketCap{basedOnCirculatingSupply{amount}}'
    case 'MAXCOINCAP':
      return 'marketCap{basedOnMaxSupply{amount}}'
    case 'CIRCULATINGPERCENTTOTAL':
      return 'circulatingSupply totalSupply'
    case 'PRICE(USD)':
    case 'PRICE(BTC)':
      return 'latestPrice{amount}'
    case 'VOLUME':
      return 'volume{rollingDay{amount}}'
    default:
      return null
  }
}

export const getQueryValue = (e, metricField) => {
  switch (metricField) {
    case 'TRANSACTIONVOLUME':
      return e.txVolume && e.txVolume.amount
    case 'TRANSACTIONCOUNT':
      return e.txCount
    case 'CIRCULATINGSUPPLY':
      return e.circulatingSupply
    case 'ACTIVEADDRESSES':
      return e.activeAddresses
    case 'ADJUSTEDTRANSACTIONVOLUME':
      return e.adjustedTxVolume && e.adjustedTxVolume.amount
    case 'MEDIANTRANSACTIONVALUE':
      return e.medianTxValue && e.medianTxValue.amount
    case 'AVERAGETRANSACTIONVALUE':
      return e.averageTxValue && e.averageTxValue.amount
    case 'FEES':
      return e.totalFees && e.totalFees.amount
    case 'MEDIANFEES':
      return e.medianFee && e.medianFee.amount
    case 'DIFFICULTY':
      return e.difficulty
    case 'FEESPERCENTVALUE':
      return e.feesPercentOfValue
    case 'CIRCULATINGCOINCAP':
      return (
        e.marketCap.basedOnCirculatingSupply &&
        e.marketCap.basedOnCirculatingSupply.amount
      )
    case 'MAXCOINCAP':
      return (
        e.marketCap.basedOnMaxSupply && e.marketCap.basedOnMaxSupply.amount
      )
    case 'CIRCULATINGPERCENTTOTAL':
      return e.circulatingSupply / e.totalSupply
    case 'PRICE(USD)':
    case 'PRICE(BTC)':
      return e.latestPrice && e.latestPrice.amount
    case 'VOLUME':
      return e.volume.rollingDay && e.volume.rollingDay.amount
    default:
      return null
  }
}

export const getQuery = (metricField, symbol, timeFrom, timeTo, mdpField) => {
  let queryTemplate
  let queryString
  const currency = metricField === 'PRICE(BTC)' ? 'BTC' : 'USD'

  if (blockchainQuery.includes(metricField)) {
    queryTemplate = JSON.stringify({
      query: `{getHistoricalBlockchainData(
        symbol:"${symbol}",
        timeRange:{
          from: "${timeFrom}",
          to: "${timeTo}"
        }
      ){
        timestamp
        ${mdpField}
      }}`
    })

    queryString = 'getHistoricalBlockchainData'
  } else if (cryptoQuery.includes(metricField)) {
    queryTemplate = JSON.stringify({
      query: `{getCrypto(
        symbol:"${symbol}",
      ){
        market(currency: "${currency}") {
          historical(
            timeRange:{
              from: "${timeFrom}",
              to: "${timeTo}"
            }
            interval:{
              unit:DAY,
              amount:1
            }
          ){
            timestamp
            ${mdpField}
          }
        }
      }}`
    })

    queryString = 'getCrypto'
  }

  return { queryTemplate, queryString }
}
