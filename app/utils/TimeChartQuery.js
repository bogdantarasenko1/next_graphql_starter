// @flow

import React from 'react'
import axios from 'axios/index'
import moment from 'moment'
import { fetchMetrics } from './dataPlatformQuery'

type State = {
  fetchingData: boolean,
  data: []
};

type Props = {
  t: func,
  coin: string,
  range: string,
  l: string,
  r: string,
  children: React.Children
};

export default class TimeChartQuery extends React.Component<State, Props> {
  state: State;
  props: Props;

  cancelToken = axios.CancelToken.source();

  constructor (props) {
    super(props)

    this.state = {
      isLoading: true,
      data: []
    }
  }

  componentWillUnmount () {
    this.cancelToken.cancel('query data for chart terminated')
  }

  componentDidMount () {
    this.loadData()
  }

  componentDidUpdate (prevProps) {
    if (
      this.props.coin !== prevProps.coin ||
      this.props.range !== prevProps.range ||
      this.props.l !== prevProps.l ||
      this.props.r !== prevProps.r
    ) {
      this.loadData()
    }
  }

  loadData () {
    let startDate, dateFormat, unit, amount

    switch (this.props.range) {
      case 'hour':
        startDate = moment()
          .subtract(1, 'hour')
          .format()
        unit = 'MIN'
        amount = 1
        dateFormat = 'mm'
        break
      case '5hour':
        startDate = moment()
          .subtract(5, 'hours')
          .format()
        unit = 'MIN'
        amount = 1
        dateFormat = 'HH:mm'
        break
      case 'day':
        startDate = moment()
          .subtract(24, 'hours')
          .format()
        unit = 'MIN'
        amount = 60
        dateFormat = 'HH'
        break
      default:
        startDate = moment()
          .subtract(30, 'days')
          .format()
        unit = 'HR'
        amount = 24
        dateFormat = 'MMM DD'
    }
    const { coin, l, r } = this.props
    const endDate = moment().format()

    fetchMetrics(
      this.cancelToken.token,
      `${coin.toUpperCase()}`,
      startDate,
      endDate,
      dateFormat,
      unit,
      amount,
      l,
      r
    )
      .then(data => {
        const { error } = data
        if (error) {
          this.setState({ isLoading: false })
        } else {
          this.setState({
            data: data,
            isLoading: false
          })
        }
      })
      .catch(e => {
        this.setState({ isLoading: false })
      })
  }

  render () {
    return React.Children.map(this.props.children, child =>
      React.cloneElement(child, { ...this.props, ...this.state })
    )
  }
}
