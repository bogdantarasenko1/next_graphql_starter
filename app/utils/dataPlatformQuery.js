// @flow

import React from 'react'
import axios from 'axios/index'
import moment from 'moment'
import numbro from 'numbro'
import _ from 'lodash'
import getConfig from 'next/config'
import historicalQuery from './query/historical'
import assetQuery from './query/asset'
import tradingQuery from './query/trading'
import historicalFragmentQuery from './query/historicalFragment'
import assetChartQuery from './query/assetChart'

const {
  publicRuntimeConfig: { API_URL, API_KEY }
} = getConfig()

type SortDirection = {
  asc: "asc",
  desc: "desc"
};

type State = {
  errors: ?[],
  sort: SortDirection,
  sortKey: string,
  sortedField: string,
  assets: [],
  isLoading: boolean,
  dataLength: number
};

type Props = {
  primaryAsset?: string
};

export const platformQuery = ({
  mapExtraData,
  coins = defaultCoins
} = {}) => WrappedComponent => {
  return class Component extends React.Component<Props, State> {
    props: Props;
    state: State;

    primaryAsset = null;
    otherAssets = [];

    rowsOnPage = 20;

    constructor (props) {
      super(props)

      this.state = {
        sort: 'desc',
        sortKey: 'circulatingMarketCap',
        sortedField: 'basedOnCirculatingSupply',
        assets: [],
        isLoading: true,
        dataLength: 0
      }
    }

    componentDidMount () {
      this.loadData()
    }

    splitAssets = assets => {
      const { primaryAsset: symbol } = this.props
      if (symbol) {
        const primaryAsset = _.remove(assets, n => n.symbol === symbol).pop()
        this.primaryAsset = primaryAsset
        this.otherAssets = assets
      } else {
        this.otherAssets = assets
      }

      this.sortBy()
    };

    historicalAssetToPriceChanges = coinsData => {
      return coinsData.map(coin => ({
        priceChanges7d: coin.map(data => ({ name: data.d, uv: data.y }))
      }))
    };

    loadData (offset = 0) {
      const startDate = moment()
        .subtract(6, 'month')
        .startOf('day')
        .format()
      const endDate = moment().format()

      const historicalFragment = historicalFragmentQuery({
        timeRange: { from: startDate, to: endDate },
        interval: { amount: 7, unit: 'Day' }
      })

      const { sortKey, sort } = this.state

      const query = JSON.stringify({
        query: tradingQuery({
          historicalFragment: (false && historicalFragment) || '',
          pagination: { offset: offset, limit: this.rowsOnPage },
          sorting: { field: sortKey, order: sort.toUpperCase() }
        })
      })

      return fetchData(query)
        .then(r => {
          const {
            data: {
              getCryptos: { cryptos: data }
            },
            errors
          } = r

          const dataLength = r.data.getCryptos.metadata.totalCount

          if (errors) {
            this.setState({ errors: errors })
          }

          this.setState({
            dataLength: dataLength
          })

          const assets = mapTradingData(data, mapExtraData)
          this.splitAssets(assets)
        })
        .catch(e => {
          this.setState({ errors: e, isLoading: false })
          return e
        })
    }

    sortBy = () => {
      const otherAssets = this.otherAssets
      const assets = this.primaryAsset ? [this.primaryAsset] : []
      this.setState({
        assets: [...assets, ...otherAssets],
        isLoading: false
      })
    };

    sortData = key => {
      let ordering = ''
      switch (key) {
        case 'latestPriceBTC':
        case 'percentChangeBTC':
          ordering = 'priceBTC'
          break
        case 'latestPriceUSD':
        case 'percentChangeUSD':
          ordering = 'priceUSD'
          break
        case 'circulatingSupplyBlockchain':
        case 'percentTotalSupplyBlockchain':
          ordering = 'circulatingSupply'
          break
        case 'basedOnCirculatingSupply':
          ordering = 'circulatingMarketCap'
          break
        case 'basedOnMaxSupply':
          ordering = 'maxMarketCap'
          break
        case 'ROIMonthly':
        case 'ROIYearly':
          ordering = 'txCount'
          break
        case 'volume':
          ordering = 'txVolume'
          break
        default:
          console.log(`unhandled ordering key '${key}'`)
          break
      }

      const { sort: oldDirection } = this.state

      this.setState(prevState => {
        return {
          sort: this.nextSortDirection(
            prevState.sortKey,
            ordering,
            oldDirection
          ),
          sortKey: ordering,
          sortedField: key,
          errors: null
        }
      }, this.loadData)
    };

    nextSortDirection = (oldKey, newKey, oldDirection) => {
      if (oldKey === newKey) {
        return oldDirection === 'asc' ? 'desc' : 'asc'
      }

      return 'desc'
    };

    onPageUpdate = page => {
      this.setState({ isLoading: true })
      this.loadData(page * this.rowsOnPage)
    };

    render () {
      const { sortKey, ...state } = this.state

      return (
        <WrappedComponent
          {...this.props}
          {...state}
          onPageUpdate={this.onPageUpdate}
          sortBy={this.sortData}
        />
      )
    }
  }
}

export const mapTradingData = (data, mapExtraData) => {
  return data.map(asset => {
    let { symbol, name, metadata, BTCPrice, USDPrice, blockchain } = asset

    metadata = Object.assign({}, metadata)
    BTCPrice = Object.assign({}, BTCPrice)
    USDPrice = Object.assign({}, USDPrice)
    blockchain = Object.assign({}, blockchain)

    const { latest: BTCMarket } = BTCPrice
    const { latest: USDMarket } = USDPrice
    const { latest: blockchainData } = blockchain

    const info = {
      symbol,
      name,
      basedOnCirculatingSupply: null,
      latestPriceUSD: null,
      percentChangeUSD: null,
      latestPriceBTC: null,
      percentChangeBTC: null,
      circulatingSupplyBlockchain: null,
      percentTotalSupplyBlockchain: null,
      basedOnMaxSupply: null,
      ROIMonthly: null,
      ROIYearly: null,
      volume: null
    }

    if (metadata.logo) {
      info.ico = metadata.logo
    } else {
      info.ico = mapExtraData().ico
    }

    if (USDMarket) {
      const { marketCap, latestPrice, percentChange, volume } = USDMarket
      if (latestPrice) {
        info.latestPriceUSD = latestPrice.amount
      }

      if (percentChange) {
        const { rollingDay, rollingMonth, rollingYear } = percentChange
        if (rollingDay) {
          info.percentChangeUSD = rollingDay
        }

        if (rollingMonth) {
          info.ROIMonthly = rollingMonth
        }

        if (rollingYear) {
          info.ROIYearly = rollingYear
        }
      }

      if (volume && volume.rollingHour) {
        info.volume = volume.rollingHour.amount
      }

      if (marketCap) {
        const {
          basedOnCirculatingSupply,
          basedOnMaxSupply,
          basedOnTotalSupply
        } = marketCap
        if (basedOnCirculatingSupply) {
          info.basedOnCirculatingSupply = basedOnCirculatingSupply.amount
        }

        if (basedOnMaxSupply) {
          info.basedOnMaxSupply = basedOnMaxSupply.amount
        }

        if (basedOnTotalSupply) {
          info.basedOnTotalSupply = basedOnTotalSupply.amount
        }
      }
    }

    if (BTCMarket) {
      const { latestPrice, percentChange } = BTCMarket
      if (latestPrice) {
        info.latestPriceBTC = latestPrice.amount
      }

      if (percentChange) {
        const { rollingDay } = percentChange
        if (rollingDay) {
          info.percentChangeBTC = rollingDay
        }
      }
    }

    if (blockchainData) {
      const { circulatingSupply, totalSupply } = blockchainData

      if (circulatingSupply && totalSupply) {
        info.percentTotalSupplyBlockchain = circulatingSupply / totalSupply
      }

      info.circulatingSupplyBlockchain = circulatingSupply
      info.activeAddresses = blockchainData.activeAddresses
      info.difficulty = blockchainData.difficulty
      info.txCount = blockchainData.txCount
      info.nvt = blockchainData.nvt
      info.feesPercentOfValue = blockchainData.feesPercentOfValue

      info.adjustedTxVolume =
        blockchainData.adjustedTxVolume &&
        blockchainData.adjustedTxVolume.amount
      info.medianTxValue =
        blockchainData.medianTxValue && blockchainData.medianTxValue.amount
      info.totalFees =
        blockchainData.totalFees && blockchainData.totalFees.amount
      info.medianFee =
        blockchainData.medianFee && blockchainData.medianFee.amount
      info.averageTxValue =
        blockchainData.averageTxValue && blockchainData.averageTxValue.amount
      info.txVolume = blockchainData.txVolume && blockchainData.txVolume.amount
    }

    return info
  })
}

export const fetchAsset = symbol => {
  const date = moment().format()
  const query = JSON.stringify({
    query: `{ asset(
        symbol: "${symbol}"
        datetime: "${date}"){ name, symbol, metrics {marketCap, maxSupply, circulatingSupply, USDPrice, BTCPrice, volume24h }}}`
  })

  return fetchData(query)
    .then(r => r.data.asset)
    .then(r => Object.assign({}, r.metrics, { name: r.name, symbol: r.symbol }))
    .then(asset => {
      const maxSupplyMC = asset.maxSupply
        ? asset.maxSupply * asset.marketCap
        : 0

      asset.maxSupplyMarketCap = maxSupplyMC

      return asset
    })
}

export const fetchPrice = (symbol: string, cancelToken) => {
  const date = moment().format()
  const query = JSON.stringify({
    query: `{priceInfo(
        symbol: ${symbol},
        datetime: "${date}"){ timestamp, price, symbol,
        priceChangeAbsolute(window: {unit: Days, amount: 1}),
        priceChangePercent(window: {unit: Days, amount: 1}) }}`
  })

  return fetchData(query, cancelToken)
    .then(r => r.data.priceInfo)
    .then(r => {
      r.priceChangePercent =
        r.priceChangePercent === null ? 0 : r.priceChangePercent
      return r
    })
}

export const fetchMarketCap = (symbol: string, cancelToken) => {
  // const date = moment().format()
  // const query = JSON.stringify({
  //   query: `{ asset(
  //       symbol: "${symbol}", datetime: "${date}"){ name, symbol, metrics {circulatingSupply, marketCap, maxSupply, volume24h}}}`
  // })
  const query = JSON.stringify({
    query: assetQuery(symbol)
  })

  return fetchData(query, cancelToken).then(r => {
    return r.data.asset
  })
}

export const fetchData = (query, cancelToken = null) => {
  return axios
    .post(API_URL, query, {
      headers: {
        'Content-Type': 'application/json',
        'x-api-key': API_KEY
      },
      cancelToken: cancelToken
    })
    .then(r => r.data)
}

export const formatNumbers = assets => {
  return assets.map(event => {
    const eventCopy = { ...event }
    eventCopy.latestPriceUSD = eventCopy.latestPriceUSD
      ? numbro(eventCopy.latestPriceUSD).formatCurrency({
        thousandSeparated: true,
        mantissa: 2
      })
      : 'N/A'

    eventCopy.latestPriceBTC = eventCopy.latestPriceBTC
      ? numbro(eventCopy.latestPriceBTC).format({
        thousandSeparated: true,
        mantissa: 2
      })
      : 'N/A'

    eventCopy.percentChangeUSD = eventCopy.percentChangeUSD
      ? `${numbro(eventCopy.percentChangeUSD).format({
        output: 'percent',
        mantissa: 2
      })}`
      : 'N/A'

    eventCopy.percentChangeBTC = eventCopy.percentChangeBTC
      ? `${numbro(eventCopy.percentChangeBTC).format({
        output: 'percent',
        mantissa: 2
      })}`
      : 'N/A'

    eventCopy.feesPercentOfValue = eventCopy.feesPercentOfValue
      ? `${numbro(eventCopy.feesPercentOfValue).format({
        output: 'percent',
        mantissa: 8
      })}`
      : 'N/A'

    eventCopy.percentTotalSupplyBlockchain = eventCopy.percentTotalSupplyBlockchain
      ? `${numbro(eventCopy.percentTotalSupplyBlockchain).format({
        mantissa: 2,
        output: 'percent'
      })}`
      : 'N/A'

    eventCopy.marketCap = eventCopy.marketCap
      ? numbro(eventCopy.marketCap).formatCurrency({
        mantissa: 2,
        thousandSeparated: true
      })
      : 'N/A'

    eventCopy.volume = eventCopy.volume
      ? numbro(eventCopy.volume).formatCurrency({
        mantissa: 2,
        thousandSeparated: true
      })
      : 'N/A'

    eventCopy.circulatingSupplyBlockchain = eventCopy.circulatingSupplyBlockchain
      ? numbro(eventCopy.circulatingSupplyBlockchain).format({
        thousandSeparated: true
      })
      : 'N/A'

    eventCopy.basedOnMaxSupply = eventCopy.basedOnMaxSupply
      ? numbro(eventCopy.basedOnMaxSupply).formatCurrency({
        thousandSeparated: true,
        mantissa: 2
      })
      : 'N/A'

    eventCopy.basedOnCirculatingSupply = eventCopy.basedOnCirculatingSupply
      ? numbro(eventCopy.basedOnCirculatingSupply).formatCurrency({
        thousandSeparated: true,
        mantissa: 2
      })
      : 'N/A'

    eventCopy.basedOnTotalSupply = eventCopy.basedOnTotalSupply
      ? numbro(eventCopy.basedOnTotalSupply).formatCurrency({
        thousandSeparated: true,
        mantissa: 2
      })
      : 'N/A'

    eventCopy.ROIMonthly = eventCopy.ROIMonthly
      ? numbro(eventCopy.ROIMonthly).format({
        thousandSeparated: true,
        mantissa: 2,
        output: 'percent'
      })
      : 'N/A'

    eventCopy.ROIYearly = eventCopy.ROIYearly
      ? numbro(eventCopy.ROIYearly).format({
        thousandSeparated: true,
        mantissa: 2,
        output: 'percent'
      })
      : 'N/A'

    eventCopy.totalFees = eventCopy.totalFees
      ? numbro(eventCopy.totalFees).formatCurrency({
        mantissa: 2
      })
      : 'N/A'

    eventCopy.txVolume = eventCopy.txVolume
      ? numbro(eventCopy.txVolume).formatCurrency({
        thousandSeparated: true,
        mantissa: 2
      })
      : 'N/A'

    eventCopy.adjustedTxVolume = eventCopy.adjustedTxVolume
      ? numbro(eventCopy.adjustedTxVolume).formatCurrency({
        mantissa: 2
      })
      : 'N/A'

    eventCopy.txCount = eventCopy.txCount
      ? numbro(eventCopy.txCount).format({ thousandSeparated: true })
      : 'N/A'

    eventCopy.activeAddresses = eventCopy.activeAddresses
      ? numbro(eventCopy.activeAddresses).format({ thousandSeparated: true })
      : 'N/A'

    eventCopy.averageTxValue = eventCopy.averageTxValue
      ? numbro(eventCopy.averageTxValue).formatCurrency({ mantissa: 2 })
      : 'N/A'

    eventCopy.medianTxValue = eventCopy.medianTxValue
      ? numbro(eventCopy.medianTxValue).formatCurrency({ mantissa: 2 })
      : 'N/A'

    eventCopy.medianFee = eventCopy.medianFee
      ? numbro(eventCopy.medianFee).format({ mantissa: 2 })
      : 'N/A'

    eventCopy.difficulty = eventCopy.difficulty
      ? numbro(eventCopy.difficulty).format({
        thousandSeparated: true,
        mantissa: 2
      })
      : 'N/A'

    eventCopy.nvt = eventCopy.nvt
      ? numbro(eventCopy.difficulty).format({
        thousandSeparated: true,
        mantissa: 2
      })
      : 'N/A'

    return eventCopy
  })
}

export const fetchSMA = (
  cancelToken,
  symbol,
  startDate,
  endDate,
  unit = 'Days',
  amount = 7,
  source = 'POLONIEX'
) => {
  // const query = JSON.stringify({
  //   query: `{historicalAsset(
  //       symbol: "${symbol}",
  //       timeRange: {from: "${startDate}", to: "${endDate}"},
  //       interval: { amount: ${amount}, unit: ${unit}}
  //       ){symbol, interval {amount, unit}, metrics{volume24h, USDPrice, BTCPrice, marketCap, timestamp}}}`
  // })
  const query = JSON.stringify({
    query: historicalQuery(
      symbol,
      { from: startDate, to: endDate },
      { amount, unit }
    )
  })

  return fetchData(query, cancelToken).then(res => {
    const {
      data: { historicalAsset: sma }
    } = res
    // first value is 0 always, replace it with const value for better scaling in price changes
    // sma.metrics.splice(0, 1, { timestamp: sma.metrics[0].timestamp, value: 7000 })
    if (!sma) {
      return null
    }
    const data = sma.metrics.map((val, index) => {
      return {
        d: moment(val.timestamp).format('MMM DD'),
        p: val.USDPrice.toLocaleString('us-EN', {
          style: 'currency',
          currency: 'USD'
        }),
        x: index, // previous days
        y: val.USDPrice // numerical price
      }
    })
    return data
  })
}

export const fetchMetrics = (
  cancelToken,
  symbol,
  startDate,
  endDate,
  dateFormat,
  unit,
  amount,
  l = 'USDPrice',
  r = 'BTCPrice'
) => {
  const historicalFragment = historicalFragmentQuery({
    timeRange: { from: startDate, to: endDate },
    interval: { amount: amount, unit: unit }
  })

  const query = JSON.stringify({
    query: assetChartQuery({ symbol, historicalFragment })
  })
  // const query = JSON.stringify({
  //   query: `{historicalAsset(
  //       symbol: "${symbol}",
  //       timeRange: {from: "${startDate}", to: "${endDate}"},
  //       interval: { amount: ${amount}, unit: ${unit}}
  //       ){symbol, interval {amount, unit}, metrics{volume24h, timestamp,
  //         ${l ? `${l},` : null}
  //         ${r ? `${r},` : null}
  //       }}}`
  // })

  return fetchData(query, cancelToken)
    .then(res => {
      const {
        data: { historicalAsset: sma }
      } = res
      const data = sma.metrics.map((val, index) => {
        return {
          d: moment(val.timestamp).format(dateFormat),
          v: val.volume24h,
          x: index, // previous days
          l: val[l], // left chart data
          r: val[r] // right chart data
        }
      })
      return data
    })
    .catch(e => {
      return { error: e }
    })
}

const defaultCoins = [
  'BTC',
  'ETH',
  'XRP',
  'BCH',
  'EOS',
  'LTC',
  'ADA',
  'TRX',
  'NEO',
  'DASH',
  'XMR',
  'BNB',
  'ETC',
  'QTUM'
]
