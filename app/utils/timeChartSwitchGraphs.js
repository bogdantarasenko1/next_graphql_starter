import {
  SplineSeries,
  AreaSplineSeries,
  Series,
  ColumnSeries
} from 'react-jsx-highstock'

import numbro from 'numbro'

export const switchGraph = (symbol, metricField, data, primary, t) => {
  const seriesID = primary ? 'primaryMetric' : 'secondaryMetric'
  const seriesColor = primary
    ? 'rgba(244, 122, 32, 0.7)'
    : 'rgba(66, 137, 203, 0.7)'
  const gradientColorStart = primary
    ? 'rgba(244, 122, 32, 0.5)'
    : 'rgba(66, 137, 203, 0.5)'
  const gradientColorEnd = primary
    ? 'rgba(244, 122, 32, 0)'
    : 'rgba(66, 137, 203, 0)'

  switch (metricField) {
    case 'Price (BTC)':
    case 'Price (USD)':
      return (
        <SplineSeries
          id={seriesID}
          dashStyle="Solid"
          name={`${symbol} ${t(`time-chart:${metricField}`)}`}
          data={data}
          color={seriesColor}
          tooltip={{
            valuePrefix: metricField === 'Price (USD)' ? '$' : '',
            valueDecimals: 2
          }}
        />
      )
    case 'Transaction Count':
    case 'Active Addresses':
      return (
        <Series
          step="true"
          dashStyle="Dash"
          id={seriesID}
          name={`${symbol} ${t(`time-chart:${metricField}`)}`}
          data={data}
          color={seriesColor}
          tooltip={{ valuePrefix: '', valueDecimals: 2 }}
        />
      )
    case 'Transaction Volume':
    case 'Adjusted Transaction Volume':
      return (
        <AreaSplineSeries
          id={seriesID}
          dashStyle="Dash"
          name={`${symbol} ${t(`time-chart:${metricField}`)}`}
          data={data}
          color={seriesColor}
          fillColor={{
            linearGradient: [0, 0, 0, 180],
            stops: [[0, gradientColorStart], [1, gradientColorEnd]]
          }}
          tooltip={{ valuePrefix: '$', valueDecimals: 2 }}
        />
      )
    case 'Median Transaction Value':
    case 'Average Transaction Value':
      return (
        <SplineSeries
          dashStyle="Dot"
          id={seriesID}
          name={`${symbol} ${t(`time-chart:${metricField}`)}`}
          data={data}
          color={seriesColor}
          tooltip={{ valuePrefix: '$', valueDecimals: 2 }}
        />
      )
    case 'Fees':
    case 'Median Fees':
      return (
        <SplineSeries
          dashStyle="Dash"
          id={seriesID}
          name={`${symbol} ${t(`time-chart:${metricField}`)}`}
          data={data}
          color={seriesColor}
          tooltip={{ valuePrefix: '$', valueDecimals: 2 }}
        />
      )
    case 'Difficulty':
      return (
        <SplineSeries
          dashStyle="Dot"
          lineWidth={6}
          id={seriesID}
          name={`${symbol} ${t(`time-chart:${metricField}`)}`}
          data={data}
          color={seriesColor}
          tooltip={{ valuePrefix: '', valueDecimals: 2 }}
        />
      )
    case 'Fees Percent Value':
      return (
        <SplineSeries
          dashStyle="Dash"
          lineWidth={6}
          id={seriesID}
          name={`${symbol} ${t(`time-chart:${metricField}`)}`}
          data={data}
          color={seriesColor}
          tooltip={{
            pointFormatter: function () {
              return `<span style="color:${this.series.color}">●</span> ${
                this.series.name
              }</span>: <b>${numbro(this.y).format({
                mantissa: 8,
                output: 'percent'
              })}</b><br/>`
            },
            valuePrefix: ''
          }}
        />
      )
    case 'Circulating Supply':
      return (
        <AreaSplineSeries
          id={seriesID}
          dashStyle="Solid"
          name={`${symbol} ${t(`time-chart:${metricField}`)}`}
          data={data}
          color={seriesColor}
          fillColor={{
            linearGradient: [0, 0, 0, 180],
            stops: [[0, gradientColorStart], [1, gradientColorEnd]]
          }}
          tooltip={{ valuePrefix: '', valueDecimals: 2 }}
        />
      )
    case 'Circulating Percent Total':
      return (
        <AreaSplineSeries
          id={seriesID}
          dashStyle="Solid"
          lineWidth={6}
          name={`${symbol} ${t(`time-chart:${metricField}`)}`}
          data={data}
          color={seriesColor}
          fillColor={{
            linearGradient: [0, 0, 0, 180],
            stops: [[0, gradientColorStart], [1, gradientColorEnd]]
          }}
          tooltip={{
            pointFormatter: function () {
              return `<span style="color:${this.series.color}">●</span> ${
                this.series.name
              }</span>: <b>${numbro(this.y).format({
                mantissa: 2,
                output: 'percent'
              })}</b><br/>`
            },
            valuePrefix: ''
          }}
        />
      )
    case 'Circulating Coin Cap':
    case 'Max Coin Cap':
      return (
        <Series
          step="true"
          id={seriesID}
          dashStyle="Solid"
          name={`${symbol} ${t(`time-chart:${metricField}`)}`}
          data={data}
          color={seriesColor}
          tooltip={{ valuePrefix: '$', valueDecimals: 2 }}
        />
      )
    case 'Volume':
      return (
        <ColumnSeries
          name={`${symbol} ${t(`time-chart:${metricField}`)}`}
          data={data}
          dashStyle="Solid"
          color={seriesColor}
          tooltip={{ valuePrefix: '$', valueDecimals: 2 }}
        />
      )
    default:
      return null
  }
}
