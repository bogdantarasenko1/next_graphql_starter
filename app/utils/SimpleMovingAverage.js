// @flow

import React from 'react'
import axios from 'axios/index'
import moment from 'moment'
import { fetchSMA } from './dataPlatformQuery'

type State = {
  fetchingData: boolean,
  data: []
};

type Props = {
  t: func,
  code: string,
  children: React.Children
};

export class SimpleMovingAverage extends React.Component<State, Props> {
  state: State;
  props: Props;

  cancelToken = axios.CancelToken.source();

  constructor (props) {
    super(props)

    this.state = {
      isLoading: true,
      data: []
    }
  }

  componentWillUnmount () {
    this.cancelToken.cancel('query data for chart terminated')
  }

  componentDidMount () {
    const { code } = this.props
    const symbol = code.toUpperCase()
    const startDate = moment('2017-01-01').format()
    const endDate = moment().format()

    fetchSMA(this.cancelToken.token, `${symbol}`, startDate, endDate)
      .then(data => {
        this.setState({
          data: data,
          isLoading: false
        })
      })
      .catch(e => {
        this.setState({ isLoading: false })
      })
  }

  render () {
    return React.Children.map(this.props.children, child =>
      React.cloneElement(child, { ...this.props, ...this.state })
    )
  }
}
