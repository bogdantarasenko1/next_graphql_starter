// @flow

import React from 'react'
import EcosystemScreen from './Ecosystem.screen'

const EcosystemContainer = () => {
  return <EcosystemScreen />
}

export default EcosystemContainer
