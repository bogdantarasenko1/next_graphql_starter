// @flow

import React from 'react'
import FollowersScreen from './BlockchainData.screen'

const BlockchainDataContainer = () => {
  return <FollowersScreen />
}

export default BlockchainDataContainer
