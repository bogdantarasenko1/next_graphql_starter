// @flow

import React from 'react'
import { Tabs, Tab, Typography, withStyles } from '@material-ui/core'

type Props = {
  tabItems: [],
  onSelectTab: (event: Object, value: string) => void,
  selectedTab: string,
  content: React.ReactNode,
  classes: Object
};

const stylesMUI = theme => ({
  root: {
    marginTop: '2.5rem'
  },
  tabContent: {
    marginTop: '1rem'
  }
})

const CoinPagesScreen = (props: Props) => {
  const { selectedTab, tabItems, classes, content } = props

  return (
    <div className={classes.root}>
      <Tabs value={selectedTab} onChange={props.onSelectTab}>
        {tabItems.map(item => (
          <Tab key={item.key} disableRipple label={item.title} />
        ))}
      </Tabs>
      <Typography component="div" className={classes.tabContent}>
        {content}
      </Typography>
    </div>
  )
}

export default withStyles(stylesMUI)(CoinPagesScreen)
