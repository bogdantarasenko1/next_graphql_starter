// @flow

import React from 'react'
import TradingScreen from './Trading.screen'

const TradingContainer = () => {
  return <TradingScreen />
}

export default TradingContainer
