// @flow

import React from 'react'
import ScrollableAnchor from 'react-scrollable-anchor'
import TimeChartContainer from '../../../TimeChart/TimeChart.container'
import CompTable from '../../../CompTable/CompTable.screen'
import KeyStats from '../../../KeyStats/KeyStats.screen'
import OverViewSection from './OverviewSection.screen'

type Props = {
  asset: Object,
  category: ?string,
  text: ?string,
  isLoading: boolean,
  t: string => string
};

const OverviewScreen = (props: Props) => {
  const { asset = {}, text, category, isLoading, ...other } = props
  const { t } = props

  return (
    <div>
      <div>
        <ScrollableAnchor id={'time-chart'}>
          <div className="widget">
            <TimeChartContainer assetSymbol={asset.symbol} {...other} />
          </div>
        </ScrollableAnchor>

        <ScrollableAnchor id={'overview'}>
          <OverViewSection
            title={t(`common:Overview`)}
            text={text}
            category={category}
            isLoading={isLoading}
            t={t}
          />
        </ScrollableAnchor>
      </div>

      <ScrollableAnchor id={'key-stats'}>
        <div className="widget">
          <KeyStats t={props.t} assetSymbol={asset.symbol} {...other} />
        </div>
      </ScrollableAnchor>

      <ScrollableAnchor id={'comp-table'}>
        <div className="widget">
          <CompTable t={props.t} {...other} />
        </div>
      </ScrollableAnchor>
    </div>
  )
}

export default OverviewScreen
