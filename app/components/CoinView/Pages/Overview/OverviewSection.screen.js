// @flow

import React from 'react'
import { withStyles, Typography } from '@material-ui/core'
import BlockWithHeader from '../../../BlockWithHeader/BlockWithHeader.screen.js'
import Loader from '../../../Loaders/Overview'

const styles = theme => ({
  container: {
    marginBottom: '1rem'
  }
})

type Props = {
  category: ?string,
  text: ?string,
  title: string,
  content: React.ReactNode,
  isLoading: boolean,
  classes: Object,
  t: string => string
};

const OverviewSection = (props: Props) => {
  const { classes, category, text, isLoading, t } = props

  const content = () => {
    if (isLoading) {
      return <Loader {...props} />
    }

    return (
      <React.Fragment>
        <Typography variant="body1" paragraph>
          {text || 'N/A'}
        </Typography>
        <Typography>
          <strong>
            {t('common:Category')}
            :&nbsp;
          </strong>
          {category || 'N/A'}
        </Typography>
      </React.Fragment>
    )
  }

  return (
    <div className={classes.container}>
      <BlockWithHeader title={props.title} content={content()} />
    </div>
  )
}

export default withStyles(styles)(OverviewSection)
