// @flow

import React from 'react'
import Analysis from '../../../Analysis/Analysis.container'
import Metrics from '../../../Metrics/Metrics.sceen'
import KeysData from '../../../KeyData/KeyData.screen'
import MainView from './Overview.screen'

type Props = {
  code: string,
  text: string,
  page: string,
  asset: { name: string, symbol: string },
  isLoading: boolean
};

const OverviewContainer = (props: Props) => {
  const { asset, isLoading, ...other } = props
  const { page } = props

  const contentForPage = () => {
    switch (page) {
      case 'key-data':
        return <KeysData {...other} />

      case 'analysis':
        return <Analysis {...other} />

      case 'data-visuals':
        return <Metrics />

      default:
        return <MainView asset={asset} {...other} isLoading={isLoading} />
    }
  }

  return contentForPage()
}

export default OverviewContainer
