// @flow

import React from 'react'
import CompetitionScreen from './Competition.screen'

const CompetitionContainer = () => {
  return <CompetitionScreen />
}

export default CompetitionContainer
