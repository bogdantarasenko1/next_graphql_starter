// @flow

import React from 'react'
import { Grid, Divider, Typography, withStyles } from '@material-ui/core'
import withWidth, { isWidthUp, isWidthDown } from '@material-ui/core/withWidth'
import OverviewContainer from '../../components/CoinView/Pages/Overview/Overview.container'
import Layout from '../MainLayout/MainLayout.screen'
import NavigationMenu from '../NavigationMenu/NavigationMenu.container'
import Loader from '../Loaders/Overview'
import MobileMenu from './Mobile/MobileMenu.container'
import MobileBar from './Mobile/MobileBar.screen'
import MobileInfo from './Mobile/MobileCoinInfo.screen'
import styles from './styles.css'

type Props = {
  t: func,
  asset: { name: string, symbol: string },
  symbol: string,
  page: string,
  logo: string,
  btcLogo: string,
  btcPrice: string,
  text: ?string,
  category: ?string,
  rate: ?string,
  change: ?string,
  fetchingData: boolean,
  classes: Object,
  width: string,
  setRef: func,
  classes: Object,
  isLoading: boolean
};

const MUIStyles = () => ({
  btcPriceRoot: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    margin: '7px 0',
    '& img': {
      marginLeft: '-2px'
    }
  },
  navLoader: {
    padding: '0 1rem'
  }
})

const CoinViewScreen = (props: Props) => {
  const {
    asset,
    page,
    logo,
    btcLogo,
    btcPrice,
    text,
    category,
    rate,
    change,
    t,
    setRef,
    classes,
    isLoading,
    ...otherProps
  } = props

  const content = () => {
    if (isLoading) {
      return (
        <React.Fragment>
          {isWidthDown('sm', props.width) && (
            <MobileBar>
              <Loader height={120} rHeight={35} />
            </MobileBar>
          )}
          <Grid container>
            {isWidthUp('md', props.width) && (
              <Grid item md={2}>
                <div className={classes.navLoader}>
                  <Loader height={120} rHeight={35} />
                </div>
              </Grid>
            )}
            <Grid item xs={12} md={10}>
              <OverviewContainer
                isLoading={isLoading}
                asset={asset}
                {...otherProps}
                t={t}
              />
            </Grid>
          </Grid>
        </React.Fragment>
      )
    }

    return (
      <React.Fragment>
        {asset &&
          isWidthDown('sm', props.width) && (
          <MobileBar>
            <MobileInfo asset={asset} rate={rate} change={change} />
            <MobileMenu symbol={asset.symbol} page={page} t={t} />
          </MobileBar>
        )}
        <Grid container>
          {isWidthUp('md', props.width) && (
            <Grid item md={2}>
              <div ref={props.setRef} className={styles.infoContainer}>
                <div>
                  <img width="48px" src={logo} />
                </div>
                {asset && (
                  <React.Fragment>
                    <Typography variant="body1" component="div">
                      <p className={styles.name}>{`${asset.name} ${
                        asset.symbol
                      }`}</p>
                      <div className={styles.usdPrice}>
                        <span className={styles.rate}>{rate || 'N/A'}</span>
                        <span className={styles.positiveDynamic}>
                          {change || 'N/A'}
                        </span>
                      </div>
                    </Typography>
                    {btcPrice && (
                      <React.Fragment>
                        <Divider light />
                        <Typography
                          variant="body1"
                          className={classes.btcPriceRoot}
                        >
                          <img src={btcLogo} className={styles.btcLogo} />
                          <span className={styles.rate}>{btcPrice}</span>
                        </Typography>
                      </React.Fragment>
                    )}
                    <NavigationMenu t={t} symbol={asset.symbol} page={page} />
                  </React.Fragment>
                )}
              </div>
            </Grid>
          )}
          <Grid item xs={12} md={10}>
            {asset && (
              <OverviewContainer
                {...otherProps}
                asset={asset}
                page={page}
                text={text}
                category={category}
                t={t}
              />
            )}
          </Grid>
        </Grid>
      </React.Fragment>
    )
  }

  return <Layout {...otherProps} t={t} content={content()} />
}

export default withWidth()(withStyles(MUIStyles)(CoinViewScreen))
