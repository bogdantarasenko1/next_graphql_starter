// @flow

import React from 'react'
import { Paper, withStyles } from '@material-ui/core'

type Props = {
  children: React.ReactNode,
  classes: Object
};

const styles = theme => ({
  root: {
    margin: '-16px -16px 16px',
    top: '48px',
    left: 'auto',
    right: '0',
    position: 'sticky',
    zIndex: theme.zIndex.appBar
  },
  bar: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: '0 1rem'
  }
})

const MobileBarScreen = (props: Props) => {
  const { classes, children } = props
  return (
    <div className={classes.root}>
      <Paper square className={classes.bar}>
        {children}
      </Paper>
    </div>
  )
}

export default withStyles(styles)(MobileBarScreen)
