// @flow

import React from 'react'
import { Typography, withStyles } from '@material-ui/core'

type Props = {
  asset: Object,
  rate: string,
  change: string,
  classes: Object
};

const styles = theme => ({
  root: {
    paddingTop: '3px',
    paddingBottom: '5px'
  },
  name: {
    marginBottom: '5px'
  }
})

const MobileCoinInfoScreen = (props: Props) => {
  const { asset, rate, classes } = props
  return (
    <div className={classes.root}>
      <Typography variant="subheading" className={classes.name}>{`${
        asset.name
      } ${asset.symbol}`}</Typography>
      <Typography variant="body1" color="textSecondary">
        {rate}
      </Typography>
    </div>
  )
}

export default withStyles(styles)(MobileCoinInfoScreen)
