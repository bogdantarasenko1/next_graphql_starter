// @flow

import React from 'react'
import {
  Button,
  Paper,
  MenuList,
  MenuItem,
  withStyles
} from '@material-ui/core'
import ArrowDropDown from '@material-ui/icons/ArrowDropDown'
import { Link } from '../../../../lib/routes'

type Props = {
  t: string => string,
  closeMenu: () => void,
  isOpened: boolean,
  toggleMenu: Object => void,
  items: [],
  classes: Object,
  currentPage: string
};

const styles = theme => ({
  container: {
    width: '200px',
    textAlign: 'right',
    backgroundColor: '#2d2d2d'
  },
  paper: {
    position: 'absolute',
    right: '1rem',
    backgroundColor: '#2d2d2d',
    width: '200px'
  },
  label: {
    justifyContent: 'flex-end'
  }
})

const MobileMenuScreen = (props: Props) => {
  const {
    toggleMenu,
    isOpened,
    closeMenu,
    currentPage,
    items,
    t,
    classes
  } = props
  return (
    <div>
      <div className={classes.container}>
        <Button
          disableRipple
          fullWidth
          disableFocusRipple
          color="default"
          variant="text"
          size="small"
          onClick={toggleMenu}
          classes={{ label: classes.label }}
        >
          {t(`common:${currentPage}`)}
          <ArrowDropDown />
        </Button>
      </div>
      {isOpened && (
        <Paper className={classes.paper}>
          <MenuList>
            {items.map((item, index) => (
              <MenuItem key={index}>
                <Link route={item.link}>
                  <a
                    className={classes.link}
                    href={item.link}
                    onClick={closeMenu}
                  >
                    {t(`common:${item.title}`)}
                  </a>
                </Link>
              </MenuItem>
            ))}
          </MenuList>
        </Paper>
      )}
    </div>
  )
}

export default withStyles(styles)(MobileMenuScreen)
