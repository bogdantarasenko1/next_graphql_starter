// @flow

import React from 'react'
import { navigationNodes } from '../../NavigationMenu/NavigationMenu.container'
import MobileMenu from './MobileMenu.screen'

type Props = {
  symbol: string,
  page: string
};
type State = {
  isOpened: boolean,
  items: []
};

class MobileMenuContainer extends React.PureComponent<Props, State> {
  props: Props;
  state: State;

  constructor (props: Props) {
    super(props)

    const items = Object.values(navigationNodes).map(item => ({
      title: item.title,
      link: `/coin/${props.symbol.toLowerCase()}/${item.anchor}`,
      anchor: item.anchor
    }))

    const currentPage = this.currentPage(items, props.page)

    this.state = {
      isOpened: false,
      items: items,
      currentPage: currentPage
    }
  }

  componentDidUpdate (prevProps) {
    const { page } = this.props
    if (page !== prevProps.page) {
      const { items } = this.state
      this.setState({ currentPage: this.currentPage(items, page) })
    }
  }

  onToggleMenu = target => {
    this.setState(prevState => {
      return { isOpened: !prevState.isOpened }
    })
  };

  onCloseMenu = () => {
    this.setState({ isOpened: false })
  };

  currentPage = (items, page) => {
    const item = items.filter(item => item.anchor === page).pop()
    return item.title
  };

  render () {
    const { items, currentPage, isOpened } = this.state
    return (
      <MobileMenu
        currentPage={currentPage}
        items={items}
        isOpened={isOpened}
        toggleMenu={this.onToggleMenu}
        closeMenu={this.onCloseMenu}
        {...this.props}
      />
    )
  }
}

export default MobileMenuContainer
