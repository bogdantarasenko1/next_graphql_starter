// @flow

import React from 'react'
import ScrollableAnchor from 'react-scrollable-anchor'
import {
  List,
  ListItem,
  ListItemText,
  Typography,
  ListItemSecondaryAction,
  withStyles
} from '@material-ui/core'

type Props = {
  header: string,
  anchor: string,
  items: [],
  classes: Object,
  t: string => string
};

const styles = theme => ({
  root: {
    paddingBottom: '20px'
  }
})

const KeyDataMetricsScreen = (props: Props) => {
  const { header, anchor, items, classes, t } = props

  return (
    <div className={classes.root}>
      <ScrollableAnchor id={anchor}>
        <Typography variant="subheading">{header}</Typography>
      </ScrollableAnchor>
      <div>
        <List>
          {items.map((item, key) => (
            <ListItem disableGutters divider key={key}>
              <ListItemText
                primaryTypographyProps={{ variant: 'caption' }}
                primary={t(`keyData:${item.name}`)}
              />
              <ListItemSecondaryAction>
                <ListItemText
                  primaryTypographyProps={{ variant: 'caption' }}
                  primary={item.value}
                />
              </ListItemSecondaryAction>
            </ListItem>
          ))}
        </List>
      </div>
    </div>
  )
}

export default withStyles(styles)(KeyDataMetricsScreen)
