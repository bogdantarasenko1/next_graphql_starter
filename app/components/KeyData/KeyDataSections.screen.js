// @flow

import React from 'react'
import { Grid } from '@material-ui/core'
import Loader from '../Loaders/Overview'
import Snackbar from '../Snackbar/Snackbar.container'
import MetricsScreen from './KeyDataMetrics.screen'

type Props = {
  columnA: [],
  columnB: [],
  errors: any,
  isLoading: boolean,
  t: string => string
};

const KeyDataSectionsScreen = (props: Props) => {
  const { columnA, columnB, isLoading, errors, t } = props
  if (isLoading) {
    return (
      <React.Fragment>
        <Grid container spacing={40}>
          <Grid item xs={12} md={6}>
            <Loader height={120} rHeight={30} />
          </Grid>
          <Grid item xs={12} md={6}>
            <Loader height={120} rHeight={30} />
          </Grid>
        </Grid>
      </React.Fragment>
    )
  }

  return (
    <React.Fragment>
      <Grid container spacing={40}>
        <Grid item xs={12} md={6}>
          {columnA &&
            columnA.map((metric, key) => (
              <MetricsScreen
                key={key}
                header={metric.header}
                items={metric.items}
                anchor={metric.anchor}
                t={t}
              />
            ))}
        </Grid>
        <Grid item xs={12} md={6}>
          {columnB &&
            columnB.map((metric, key) => (
              <MetricsScreen
                key={key}
                header={metric.header}
                items={metric.items}
                anchor={metric.anchor}
                t={t}
              />
            ))}
        </Grid>
      </Grid>
      <Snackbar message={errors} />
    </React.Fragment>
  )
}

export default KeyDataSectionsScreen
