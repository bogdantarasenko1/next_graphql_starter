// @flow

import React from 'react'
import { navigationNodes } from '../NavigationMenu/NavigationMenu.container'
import keyDataQuery from '../../utils/query/keyData'
import { fetchData } from '../../utils/dataPlatformQuery'
import SectionsScreen from './KeyDataSections.screen'
import formatter from './formatters'

type Props = {
  code: string,
  t: string => string
};

type State = {
  columnA?: [],
  columnB?: [],
  isLoading: boolean,
  errors: any
};

class KeyDataSectionContainer extends React.PureComponent<Props, State> {
  props: Props;
  state: State;

  constructor (props: Props) {
    super(props)
    this.state = {
      isLoading: true
    }
  }

  componentDidMount = () => {
    const { code: asset } = this.props

    const query = JSON.stringify({
      query: keyDataQuery(asset.toUpperCase())
    })

    fetchData(query)
      .then(r => {
        if (r.errors) {
          this.setState({
            isLoading: false,
            errors: r.errors
          })
        }

        return r.data.getCrypto
      })
      .then(this.columnsData)
  };

  columnsData = data => {
    const {
      'key-data': { sections }
    } = navigationNodes

    const columnA = [
      {
        header: sections.valuation.title,
        items: this.valuation(data),
        anchor: sections.valuation.anchor
      },
      {
        header: sections.trading.title,
        items: this.trading(data),
        anchor: sections.trading.anchor
      },
      {
        header: sections.transaction.title,
        items: this.transaction(data),
        anchor: sections.transaction.anchor
      }
    ]

    const columnB = [
      {
        header: sections.coinSupply.title,
        items: this.coinSupply(data),
        anchor: sections.coinSupply.anchor
      } /*,
      {
        header: sections.ecosystem.title,
        items: [],
        anchor: sections.ecosystem.anchor
      },
      {
        header: sections.organization.title,
        items: [],
        anchor: sections.organization.anchor
      } */
    ]

    this.setState({
      isLoading: false,
      columnA,
      columnB
    })
  };

  valuation = data => {
    const market = this.latestMarket(data)
    const blockchain = this.latestBlockchain(data)

    const marketCap = market.marketCap || {}
    const basedOnCirculatingSupply = marketCap.basedOnCirculatingSupply || {}
    const basedOnMaxSupply = marketCap.basedOnMaxSupply || {}

    return formatter([
      {
        name: 'circulatingCoinCap',
        value: basedOnCirculatingSupply.amount
      },
      {
        name: 'maxSupply',
        value: basedOnMaxSupply.amount
      },
      {
        name: 'feesUSD',
        value: blockchain.feesPercentOfValue
      }
    ])
  };

  trading = data => {
    const market = this.latestMarket(data)
    const priceChange = market.priceChange || {}
    const rollingYear = priceChange.rollingYear || {}

    return formatter([
      {
        name: 'priceChangeUSD',
        value: rollingYear.amount
      }
    ])
  };

  coinSupply = data => {
    const blockchain = this.latestBlockchain(data)

    return formatter([
      {
        name: 'currentCirculatingSupply',
        value: blockchain.circulatingSupply
      },
      {
        name: 'currentTotalSupply',
        value: blockchain.totalSupply
      },
      {
        name: 'currentCirculatingSupplyPercent',
        value: (blockchain.circulatingSupply / blockchain.totalSupply) * 100
      },
      {
        name: 'supplyType',
        value: blockchain.supplyType
      },
      {
        name: 'dateFirstBlock',
        value: blockchain.firstBlockDate
      },
      {
        name: 'supplyDistributionMethod',
        value: blockchain.distributionMethod
      },
      {
        name: 'age',
        value: blockchain.age && blockchain.age.amount
      },
      {
        name: 'inflationTate',
        value: blockchain.inflation
      },
      {
        name: 'blockTime',
        value: blockchain.blockTime && blockchain.blockTime.amount
      },
      {
        name: 'blockCount',
        value: blockchain.blockCount
      },
      {
        name: 'blockSize',
        value: blockchain.totalBlockSize && blockchain.totalBlockSize.amount
      },
      {
        name: 'rewardPerBlock',
        value: blockchain.blockReward
      }
    ])
  };

  transaction = data => {
    const blockchain = this.latestBlockchain(data)

    return formatter([
      {
        name: 'medianTransactionValue',
        value: blockchain.medianTxValue && blockchain.medianTxValue.amount
      },
      {
        name: 'dailyTransactionCount',
        value: blockchain.txCount
      },
      {
        name: 'adjustedTransactionVolume',
        value: blockchain.adjustedTxVolume && blockchain.adjustedTxVolume.amount
      },
      {
        name: 'activeAddressesPerDay',
        value: blockchain.activeAddresses
      }
    ])
  };

  latestMarket = data => {
    const { market } = data
    return market.latest || null
  };

  latestBlockchain = data => {
    const { blockchain } = data
    return blockchain.latest || null
  };

  render () {
    const { isLoading, columnA, columnB, errors } = this.state
    const { t } = this.props
    return (
      <SectionsScreen
        isLoading={isLoading}
        columnA={columnA}
        columnB={columnB}
        errors={errors}
        t={t}
      />
    )
  }
}

export default KeyDataSectionContainer
