// @flow

import React from 'react'
import BlockWithHeader from '../BlockWithHeader/BlockWithHeader.screen.js'
import SectionsContainer from './KeyDataSections.container'

type Props = {
  code: string,
  t: string => string
};

const KeyDataScreen = (props: Props) => {
  const { t } = props
  return (
    <div>
      <BlockWithHeader
        title={t('common:Key Data')}
        content={<SectionsContainer {...props} t={t} />}
      />
    </div>
  )
}

export default KeyDataScreen
