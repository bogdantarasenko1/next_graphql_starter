import numbro from 'numbro'

export default data => {
  return data.map(item => {
    switch (item.name) {
      case 'circulatingCoinCap':
      case 'maxSupply':
        item.value = item.value
          ? numbro(item.value).formatCurrency({
            thousandSeparated: true,
            totalLength: 3
          })
          : 'N/A'
        return item
      case 'priceChangeUSD':
      case 'currentCirculatingSupplyPercent':
      case 'feesUSD':
        item.value = item.value
          ? `${numbro(item.value).format({ mantissa: 2 })}%`
          : 'N/A'
        return item
      case 'currentCirculatingSupply':
      case 'currentTotalSupply':
      case 'supplyType':
      case 'dateFirstBlock':
      case 'supplyDistributionMethod':
      case 'age':
      case 'inflationTate':
      case 'blockTime':
      case 'blockCount':
      case 'blockSize':
      case 'rewardPerBlock':
      case 'medianTransactionValue':
      case 'dailyTransactionCount':
      case 'adjustedTransactionVolume':
      case 'activeAddressesPerDay':
        item.value = item.value
          ? numbro(item.value).format({
            thousandSeparated: true,
            totalLength: 4
          })
          : 'N/A'
        return item
      default:
        console.log(`Unhandled Key Data name: ${item.name}`)
        return item
    }
  })
}
