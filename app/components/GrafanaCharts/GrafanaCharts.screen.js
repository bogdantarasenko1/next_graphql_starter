// @flow

import React from 'react'
import axios from 'axios'
import Button from '../Button'
import Select from '../Select'
import ChartList from '../ChartList'
import ChartSize from '../ChartSize'
import ChartTitle from '../ChartTitle'
import ChartDivideLine from '../ChartDivideLine'
import ChartOrientation from '../ChartOrientation'

import styles from '../GrafanaCharts/styles.css'

// const chartList = [
//  'Blockchain Transactions',
//  'NVT',
//  'Average Transactions Cost',
//  'Average Transactions Speed',
//  'Example #5',
//  'Example #4'
// ]

const GRAFANA_TOKEN =
  'Bearer eyJrIjoibGo3aHVINlVPWFpZUU10MTNWQ09TSG41S2N2Y1Y3bnIiLCJuIjoidGVzdCIsImlkIjoxfQ=='

class GrafanaCharts extends React.Component<State, Props> {
  constructor (props) {
    super(props)
    this.state = {
      charList:           [],
      size:               'small',
      orientation:        'horizontal',
      selected_dashboard: false,
      dashboards:         props.dashboards || [],
      panels_list:        props.panels_list || [],
      selected_chart:     props.selected_chart || false,
    }

    this.handleSize              = this.handleSize.bind(this)
    this.handleOrientation       = this.handleOrientation.bind(this)
    this.decorateDashboardsList  = this.decorateDashboardsList.bind(this)
    this.filterPanelsList         = this.filterPanelsList.bind(this)
  }

  decorateDashboardsList(){
    const _decoratedList = [];

    for (let dashboard of this.state.dashboards) {
      _decoratedList.push({value: dashboard.id, label: dashboard.title})
    }
    return _decoratedList
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      ... this.state,
      dashboards:         nextProps.dashboards,
      selected_dashboard: nextProps.selected_dashboard,
      panels_list:        nextProps.panels_list,
      selected_chart:     nextProps.selected_chart
    });

  }

  componentDidMount () {
    const email = window.localStorage.getItem('email')
    // axios
    //   .get({
    //     method: 'get',
    //     url: `https://analytics.mosaic.io/data/api/folders`,
    //     headers: { Authorization: GRAFANA_TOKEN }
    //   })
    //   .then(response => {
    //     // handle success
    //     console.log('GrafanaCharts - ', response.data)
    //     console.log(email)
    //     const chartList = response.data
    //     this.setState({ chartList })
    //   })
    //   .catch(function (error) {
    //     // handle error
    //     console.log('GrafanaCharts - ', error)
    //   })
  }

  filterPanelsList(){
    let _raw_panels_list = this.state.panels_list

    return _raw_panels_list.filter(item => item.type === "graph")
  }

  handleSize (size) {
    this.setState({ size })
  }

  handleOrientation (orientation) {
    this.setState({ orientation })
  }

  render () {
    const { size, orientation } = this.state
    return (
      <div className={styles.chartsWrapper}>
        <div>
          <ChartTitle>Chart Categories</ChartTitle>
          <div className={styles.pickerCategoryContainer}>
            <Select list={this.decorateDashboardsList()}
                    selected_val={this.state.selected_dashboard}
                    onSelect={this.props.onSelect}/>
          </div>
          <ChartList chartList={this.filterPanelsList()}
                     onChartSelect={this.props.onImportChart}
                     selected_chart={this.state.selected_chart}/>
        </div>
        <div className={styles.chartsContainer}>
          <ChartSize
            size={size}
            handleSize={this.handleSize}
            title={<ChartTitle>Size</ChartTitle>}
          />
          <ChartOrientation
            orientation={orientation}
            handleOrientation={this.handleOrientation}
            title={<ChartTitle>Orientation</ChartTitle>}
          />
        </div>
        <ChartDivideLine />
        {/*<div className={styles.chartsButton}>*/}
          {/*<Button onClick={this.props.onImportChart} title="Import Chart" />*/}
        {/*</div>*/}
      </div>
    )
  }
}

export default GrafanaCharts
