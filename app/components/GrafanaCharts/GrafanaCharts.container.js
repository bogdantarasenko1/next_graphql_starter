// @flow

import React from 'react'
import DashboardHeader from '../Dashboard/DashboardHeader.screen.js'
import GrafanaChartsScreen from './GrafanaCharts.screen'

class GrafanaCharts extends React.Component<State, Props> {

  constructor (props) {
    super(props)
    this.state = {
      dashboards:    props.dashboards || [],
    }
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      dashboards:         nextProps.dashboards,
      selected_dashboard: nextProps.selected_dashboard,
      panels_list:        nextProps.panels_list,
      selected_chart:     nextProps.selected_chart
    });
  }


  render () {
    const { title } = this.props

    return (
      <React.Fragment>
        <DashboardHeader title={title} />
        <GrafanaChartsScreen dashboards={this.state.dashboards}
                             selected_dashboard={this.state.selected_dashboard}
                             onSelect={this.props.onSelect}
                             panels_list={this.state.panels_list}
                             onChartSelect={this.props.onChartSelect}
                             onImportChart={this.props.onImportChart}
                             selected_chart={this.state.selected_chart}/>
      </React.Fragment>
    )
  }
}

export default GrafanaCharts
