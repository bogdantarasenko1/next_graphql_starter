// @flow

import React from 'react'
import { Typography } from '@material-ui/core'
import { LogoImgUnderlined } from '../../ui/logo.js'

type Props = {
  t: string => string,
  classes: Object
};

const AboutScreen = (props: Props) => {
  const { t } = props
  return (
    <div>
      <LogoImgUnderlined />
      <Typography color="textPrimary" variant="body1" paragraph>
        {t('footer:about')}
      </Typography>
      <Typography color="textPrimary" variant="body1">
        {t('footer:rightsReserved')}
      </Typography>
    </div>
  )
}

export default AboutScreen
