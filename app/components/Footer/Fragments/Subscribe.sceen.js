// @flow

import React from 'react'
import {
  Typography,
  FormControl,
  Input,
  InputAdornment,
  withStyles
} from '@material-ui/core'

type Props = {
  t: string => string,
  classes: Object
};

const styles = theme => ({
  root: {
    paddingTop: '.5rem'
  },
  subscribe: {
    background: theme.palette.background.header,
    padding: '6px 16px',
    width: '100%'
  },
  margin: {
    marginTop: '14px',
    marginBottom: '14px'
  },
  inputRoot: {
    color: theme.palette.text.primary,
    fontSize: '.875rem'
  },
  input: {
    color: theme.palette.text.primary
  },
  start: {
    whiteSpace: 'nowrap'
  }
})

const SubscribeScreen = (props: Props) => {
  const { t, classes } = props
  return (
    <div className={classes.root}>
      <Typography color="default" variant="body1">
        <b>{t('footer:Subscribe')}</b>
      </Typography>
      <Typography
        color="textPrimary"
        variant="body1"
        className={classes.margin}
      >
        {t('footer:join')}
      </Typography>
      <FormControl classes={{ root: classes.subscribe }}>
        <Input
          disableUnderline
          classes={{ root: classes.inputRoot, input: classes.input }}
          placeholder={t('footer:Enter your email here')}
          endAdornment={
            <InputAdornment disableTypography position="end">
              {t('common:Subscribe')}
            </InputAdornment>
          }
        />
      </FormControl>
    </div>
  )
}

export default withStyles(styles)(SubscribeScreen)
