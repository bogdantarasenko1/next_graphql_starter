// @flow

import React from 'react'
import {
  List,
  ListSubheader,
  ListItem,
  ListItemText,
  withStyles
} from '@material-ui/core'
import { Link } from '../../../../lib/routes'

type Props = {
  t: string => string,
  classes: Object,
  title: string,
  links: []
};

const styles = theme => ({
  dense: {
    paddingTop: '2px',
    paddingBottom: '4px',
    '& a': {
      color: theme.palette.text.primary,
      fontWeight: 'normal'
    }
  },
  subhead: {
    padding: 0,
    color: theme.palette.text.primary,
    lineHeight: '1rem',
    marginBottom: '1rem'
  }
})

const LinksScreen = (props: Props) => {
  const { title, links, t, classes } = props

  const linkTag = link => {
    if (link.external) {
      return (
        <Link href={link.url}>
          <a href={link.url} target="_blank">
            {t(`common:${link.title}`)}
          </a>
        </Link>
      )
    } else {
      return (
        <Link prefetch href={link.url}>
          <a href={link.url}>{t(`common:${link.title}`)}</a>
        </Link>
      )
    }
  }

  return (
    <List>
      <ListSubheader className={classes.subhead} component="li">
        {t(`common:${title}`)}
      </ListSubheader>
      {links.map((link, i) => (
        <ListItem
          key={i}
          dense={true}
          disableGutters={true}
          classes={{ dense: classes.dense }}
        >
          <ListItemText primary={linkTag(link)} />
        </ListItem>
      ))}
    </List>
  )
}

export default withStyles(styles)(LinksScreen)
