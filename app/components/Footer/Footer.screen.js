// @flow

import React from 'react'
import { Grid, withStyles } from '@material-ui/core'
import About from './Fragments/About.screen'
import Links from './Fragments/Links.screen'
import Subscribe from './Fragments/Subscribe.sceen'

type Props = {
  t: string => string,
  classes: Object
};

const styles = theme => ({
  root: {
    backgroundColor: theme.palette.background.default
  },
  padding: {
    padding: '40px',
    [theme.breakpoints.down('sm')]: {
      padding: '1rem'
    }
  },
  links: {
    padding: '40px 20px 40px 40px',
    [theme.breakpoints.down('sm')]: {
      padding: '1rem'
    }
  }
})

const links = {
  about: [
    { title: 'About', url: 'https://www.mosaic.io/', external: true },
    { title: 'Team', url: '/team' },
    { title: 'Careers', url: 'https://angel.co/mosaic-19', external: true },
    { title: 'Terms of use', url: '/legal/terms-of-use' },
    { title: 'Privacy Policy', url: '/legal/privacy-policy' }
  ],
  contact: [
    { title: 'Twitter', url: 'https://twitter.com/mosaic_io', external: true },
    {
      title: 'Facebook',
      url: 'https://www.facebook.com/mosaicnetworkofficial',
      external: true
    },
    {
      title: 'Medium',
      url: 'https://medium.com/mosaic-network-blog',
      external: true
    },
    { title: 'Telegram', url: 'https://t.me/mosaicnetwork', external: true },
    {
      title: 'Reddit',
      url: 'https://www.reddit.com/r/MosaicNetwork/',
      external: true
    },
    {
      title: 'LinkedIn',
      url: 'https://www.linkedin.com/company/mosaic_io/',
      external: true
    }
  ]
}

const FooterScreen = (props: Props) => {
  const { classes, ...other } = props
  const { t } = props
  return (
    <footer className={classes.root}>
      <Grid container>
        <Grid item xs={12} md={5} className={classes.padding}>
          <About {...other} />
        </Grid>
        <Grid item xs={12} md={3} className={classes.links}>
          <Grid container>
            <Grid item xs={6}>
              <Links title={t('common:About')} links={links.about} {...other} />
            </Grid>
            <Grid item xs={6}>
              <Links
                title={t('common:Contact')}
                links={links.contact}
                {...other}
              />
            </Grid>
          </Grid>
        </Grid>
        <Grid item xs={12} md={4} className={classes.padding}>
          <Subscribe {...other} />
        </Grid>
      </Grid>
    </footer>
  )
}

export default withStyles(styles)(FooterScreen)
