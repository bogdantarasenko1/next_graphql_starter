// @flow

import React from 'react'
import ReactSVG from 'react-svg'
import styles from '../ChartList/styles.css'
import { withStyles } from "@material-ui/core/styles";
import IconButton from '@material-ui/core/IconButton';
import Icon from '@material-ui/core/Icon';


const control_styles = theme => ({
  addIcon: {
    fill: '#ba621b',
    fontSize: '15px',
    marginTop: '-2px'
  },
  button: {
    padding: '3px',
    height: '30px',
    width: '30px',
    marginRight: '18px'
  }
});

class ChartList extends React.Component<State, Props> {

  constructor (props) {
    super(props)
    this.state = {
      selected_chart: props.selected_chart,
      hovered_in_item: false
    }

    this.setActiveChartClass = this.setActiveChartClass.bind(this)
    this.renderItemAddButton = this.renderItemAddButton.bind(this)
  }

  setActiveChartClass(current_chart_id){
    return current_chart_id === this.state.selected_chart.id ? styles.chartIconActive : styles.chartIcon
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      selected_chart: nextProps.selected_chart
    });
  }

  renderItemAddButton(chart){
    if (this.state.hovered_in_item === chart.id) {
      return (
        <IconButton className={this.props.classes.button} onClick={() => this.props.onChartSelect(chart)}>
          <ReactSVG className={this.props.classes.addIcon} src="/static/baseline-add_circle-24px.svg" />
        </IconButton>
      )
    } else {
      return <ReactSVG className={this.setActiveChartClass(chart.id)} src="/static/chart.svg" />
    }
  }

  render () {
    const { chartList } = this.props

    return (
      <ul className={styles.chartsList}>
        {chartList.map((chart, index) => {
          let _c_title = chart.scopedVars && chart.scopedVars.left_field && chart.scopedVars.left_field.text ? chart.scopedVars.left_field.text : chart.title

          return (
            <li key={index}
                onMouseEnter={() => this.setState({hovered_in_item: chart.id})}
                onMouseLeave={() => this.setState({hovered_in_item: false})}
            >
              {this.renderItemAddButton(chart)}
              <span>{_c_title}</span>

            </li>
          )
        })}
      </ul>
    )
  }
}

export default withStyles(control_styles)(ChartList)
