// @flow

import React from 'react'
import MiniGraph from './MiniGraph.screen'

type Props = {
  data: [],
  height: number,
  width: number
};

const MiniGraphContainer = (props: Props) => {
  return (
    <MiniGraph
      data={props.data}
      dataKey="uv"
      width={props.width}
      height={props.height}
    />
  )
}

export default MiniGraphContainer
