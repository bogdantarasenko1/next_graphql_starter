// @flow

import React from 'react'
import { LineChart, Line } from 'recharts'

type Props = {
  data: [],
  dataKey: string,
  height: ?number,
  width: ?number,
  margin: ?Object,
  stroke: ?string,
  strokeWidth: ?number
};

const MiniGraphScreen = (props: Props) => {
  return (
    <LineChart
      width={props.width}
      height={props.height}
      data={props.data}
      margin={props.margin}
    >
      <Line
        type="monotone"
        dataKey={props.dataKey}
        stroke={props.stroke}
        strokeWidth={props.strokeWidth}
        isAnimationActive={false}
        dot={() => {}}
      />
    </LineChart>
  )
}

MiniGraphScreen.defaultProps = {
  margin: { top: 2, right: 0, left: 0, bottom: 2 },
  height: 300,
  width: 600,
  stroke: '#00FFC7',
  strokeWidth: 1
}

export default MiniGraphScreen
