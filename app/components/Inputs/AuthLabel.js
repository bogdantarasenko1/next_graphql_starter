// @flow

import React from 'react'
import { InputLabel, withStyles } from '@material-ui/core'

type Props = {
  classes: Object,
  text: String,
  htmlFor: ?String
};

const styles = theme => ({
  label: {
    textTransform: 'uppercase',
    color: 'rgb(133, 145, 145)'
  }
})

const AuthLabel = (props: Props) => {
  const { classes, htmlFor, text } = props
  return (
    <InputLabel className={classes.label} htmlFor={htmlFor}>
      {text}
    </InputLabel>
  )
}

export default withStyles(styles)(AuthLabel)
