import React from 'react'
import { withStyles } from '@material-ui/core/styles'
import Button from '@material-ui/core/Button'

/* eslint react/prop-types: 0 */

const styles = theme => ({
  button: {
    cursor: 'pointer',
    color: '#000',
    width: '170px',
    height: '10px',
    background: '#ba621b',
    margin: '5px 10px',
    textTransform: 'initial',
    boxShadow: 'none'
  }
})

const ButtonComponent = props => {
  return (
    <div>
      <Button
        onClick={props.onClick}
        variant="extendedFab"
        aria-label="Delete"
        className={props.classes.button}
      >
        {props.title}
      </Button>
    </div>
  )
}

export default withStyles(styles)(ButtonComponent)
