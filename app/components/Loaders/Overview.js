// @flow

import ContentLoader from 'react-content-loader'
import React from 'react'

type Props = {
  height?: number,
  rHeight?: number
};

const OverviewLoader = (props: Props) => {
  const { height = 40, rHeight = 10, ...other } = props
  return (
    <ContentLoader
      height={height}
      width={400}
      speed={2}
      primaryColor="#666666"
      secondaryColor="#777777"
      {...other}
    >
      {[0, 1, 2].map(e => (
        <rect
          key={e}
          x="0"
          y={e * (rHeight + 5)}
          rx="1"
          ry="1"
          width="800"
          height={rHeight}
        />
      ))}
    </ContentLoader>
  )
}

export default OverviewLoader
