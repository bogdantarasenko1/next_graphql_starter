// @flow

import React from 'react'
import ColumnsListScreen from './ColumnsList.screen'

type Props = {
  t: func,
  columns: ?[],
  selectedColumns: [],
  onChangeColumns: columns => void
};

type State = {
  selectedColumns: []
};

class ColumnsListContainer extends React.PureComponent<Props, State> {
  props: Props;
  state: State;

  constructor (props: Props) {
    super(props)

    this.state = {
      selectedColumns: props.selectedColumns
    }
  }

  onChangeColumn = event => {
    const {
      target: { checked, value }
    } = event
    const { onChangeColumns } = this.props

    let { selectedColumns } = this.state
    selectedColumns = [...selectedColumns]

    if (checked) {
      if (!selectedColumns.includes(value)) {
        selectedColumns.push(value)
      }
    } else {
      if (selectedColumns.includes(value)) {
        selectedColumns.splice(selectedColumns.indexOf(value), 1)
      }
    }

    this.setState({
      selectedColumns
    })

    onChangeColumns(selectedColumns)
  };

  render () {
    const { t, columns } = this.props
    const { selectedColumns } = this.state

    return (
      <ColumnsListScreen
        columns={columns}
        selectedColumns={selectedColumns}
        onChange={this.onChangeColumn}
        t={t}
      />
    )
  }
}

export default ColumnsListContainer
