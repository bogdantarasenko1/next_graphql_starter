// @flow

import {
  Checkbox,
  FormControl,
  FormControlLabel,
  FormGroup,
  withStyles
} from '@material-ui/core'
import React from 'react'

type Props = {
  columns: [],
  selectedColumns: [],
  onChange: event => void,
  classes: Object,
  t: string => string
};

const styles = theme => ({
  root: {
    height: '30px'
  },
  checkbox: {
    color: '#999999'
  },
  label: {
    color: '#999999'
  }
})

const ColumnsListScreen = (props: Props) => {
  const { classes, columns, selectedColumns, onChange, t } = props

  return (
    <FormControl component="fieldset">
      <FormGroup>
        {columns.map((column, index) => (
          <FormControlLabel
            key={index}
            classes={{ root: classes.root, label: classes.label }}
            control={
              <Checkbox
                onChange={column => onChange(column)}
                checked={selectedColumns.includes(column)}
                value={column}
                classes={{ root: classes.checkbox }}
              />
            }
            label={t(`cryptoassets-table:${column}`)}
          />
        ))}
      </FormGroup>
    </FormControl>
  )
}

export default withStyles(styles)(ColumnsListScreen)
