// @flow

import CardScreen from './Card.screen'

type Props = {
  headerSection?: boolean,
  t: func,
  columns: [],
  selectedColumns: [],
  onChangeColumns: columns => void
};

const CardContainer = (props: Props) => {
  return <CardScreen {...props} />
}

export default CardContainer
