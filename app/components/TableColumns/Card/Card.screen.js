// @flow

import React from 'react'
import { Card, CardContent, CardHeader, withStyles } from '@material-ui/core'
import ChangeView from '../ChangeView/ChangeView.container'
import ColumnsList from '../ColumnsList/ColumnsList.container'

type Props = {
  headerSection?: boolean,
  title?: string,
  action?: React.ReactNode,
  columns: [],
  selectedColumns: [],
  classes: Object,
  onChangeColumns: columns => void,
  t: func
};

const styles = theme => ({
  root: {
    backgroundColor: '#FAFAFA'
  },
  header: {
    borderBottom: '2px solid #E0E0E0',
    paddingBottom: '4px',
    paddingTop: '4px'
  },
  title: {
    color: '#999999'
  },
  action: {
    marginTop: 0,
    marginLeft: '50px'
  }
})

const CardScreen = (props: Props) => {
  const {
    classes,
    columns,
    selectedColumns,
    t,
    onChangeColumns,
    headerSection
  } = props
  const { action = <ChangeView t={t} />, title = 'View As' } = props

  return (
    <Card classes={{ root: classes.root }}>
      {headerSection && (
        <CardHeader
          titleTypographyProps={{ variant: 'subheading' }}
          classes={{
            root: classes.header,
            title: classes.title,
            action: classes.action
          }}
          title={`${t(`common:${title}`)}:`}
          action={action}
        />
      )}
      <CardContent>
        <ColumnsList
          t={t}
          selectedColumns={selectedColumns}
          columns={columns}
          onChangeColumns={onChangeColumns}
        />
      </CardContent>
    </Card>
  )
}

export default withStyles(styles)(CardScreen)
