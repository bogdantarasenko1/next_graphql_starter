// @flow

import React from 'react'
import { Query } from 'react-apollo'
import gql from 'graphql-tag'
import Card from '../Card/Card.container'
import { customizableColumns } from '../../Cryptoassets/Market/Market.container'
import ViewLink from './ViewLink.screen'

const query = gql`
  query GetColumns {
    columns @client {
      id
    }
  }
`

type Props = {
  t: func,
  userColumns: ?[],
  onChangeColumns: columns => void
};

type State = {
  anchorEl: boolean
};

class ViewLinkContainer extends React.PureComponent<Props, State> {
  props: Props;
  state: State;

  constructor (props: Props) {
    super(props)
    this.state = { anchorEl: null }
  }

  onClickLink = (event: Object) => {
    this.setState({ anchorEl: event.currentTarget })
  };

  onClosePopover = () => {
    this.setState({ anchorEl: null })
  };

  render () {
    const customColumns = customizableColumns()
    const { anchorEl } = this.state
    const { userColumns } = this.props
    const columns = userColumns ? userColumns.map(c => c.id) : customColumns

    return (
      <ViewLink
        {...this.props}
        anchorEl={anchorEl}
        onClosePopover={this.onClosePopover}
        onClickLink={this.onClickLink}
      >
        <Card
          selectedColumns={columns}
          columns={customColumns}
          {...this.props}
        />
      </ViewLink>
    )
  }
}

const ViewLinkContainerQuery = (props: Props) => (
  <Query query={query}>
    {({ data: { columns } }) => {
      return <ViewLinkContainer {...props} userColumns={columns} />
    }}
  </Query>
)

export default ViewLinkContainerQuery
