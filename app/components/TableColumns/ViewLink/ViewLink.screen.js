// @flow

import React from 'react'
import { Popover, Button, withStyles } from '@material-ui/core'
import ArrowDropDown from '@material-ui/icons/ArrowDropDown'

type Props = {
  t: func,
  anchorEl: ?Object,
  onClickLink: (event: Object) => void,
  onClosePopover: () => void,
  children: React.ReactNode,
  classes: Object
};

const styles = theme => ({
  button: {
    color: 'white',
    backgroundColor: 'transparent',
    boxShadow: 'none',
    '&:hover': {
      backgroundColor: 'transparent'
    }
  }
})

const ViewLinkScreen = (props: Props) => {
  const { onClickLink, onClosePopover, anchorEl, children, classes, t } = props

  return (
    <div>
      <Button
        color="primary"
        variant="contained"
        size="small"
        className={classes.button}
        onClick={onClickLink}
      >
        {t('common:View')}
        <ArrowDropDown />
      </Button>
      <Popover
        open={!!anchorEl}
        anchorEl={anchorEl}
        onClose={onClosePopover}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'right'
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'center'
        }}
      >
        {children}
      </Popover>
    </div>
  )
}

export default withStyles(styles)(ViewLinkScreen)
