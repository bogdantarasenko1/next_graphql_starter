import gql from 'graphql-tag'

const typeDefs = `
  type Column {
    id: String,
    order: Int
  }
  
  type Mutation {
    saveColumns(columns: [Column]): [Column]
  }
  
  type Query {
    columns: [Column]
  }
`
const defaults = {
  columns: null
}

const resolvers = {
  Mutation: {
    saveColumns: (_, { columns }, { cache }) => {
      const data = {
        columns: columns.map(column => {
          column.__typename = 'Column'
          return column
        })
      }
      cache.writeData({ data })
      return columns
    }
  },
  Query: {
    userColumns: (_, variables, { cache }) => {
      const query = gql`
        query GetColumns {
          columns @client {
            id
            order
          }
        }
      `

      const previous = cache.readQuery({ query })
      return previous
    }
  }
}
export { defaults, typeDefs, resolvers }
