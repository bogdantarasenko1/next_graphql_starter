// @flow

import React from 'react'
import ChangeViewScreen from './ChangeView.screen'

type Props = {};
type State = {
  viewMode: string
};

class ChangeViewContainer extends React.PureComponent<Props, State> {
  props: Props;
  state: State;

  constructor (props: Props) {
    super(props)
    this.state = {
      viewMode: 'tableView'
    }
  }

  onChangeView = event => {
    this.setState({
      viewMode: event.target.value
    })
  };

  render () {
    return (
      <ChangeViewScreen
        onChangeView={this.onChangeView}
        viewMode={this.state.viewMode}
        {...this.props}
      />
    )
  }
}

export default ChangeViewContainer
