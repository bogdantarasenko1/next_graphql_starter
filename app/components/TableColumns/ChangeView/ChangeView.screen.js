// @flow

import {
  FormControl,
  FormControlLabel,
  Radio,
  RadioGroup,
  withStyles
} from '@material-ui/core'
import React from 'react'

type Props = {
  t: func,
  viewMode: string,
  onChangeView: event => void,
  classes: Object
};

const styles = theme => ({
  radio: {
    color: '#999999'
  },
  label: {
    color: '#999999'
  }
})

const ChangeViewScreen = (props: Props) => {
  const { t, classes, viewMode, onChangeView } = props

  return (
    <FormControl component="fieldset">
      <RadioGroup
        row
        aria-label="anchorReference"
        name="viewMode"
        onChange={onChangeView}
        value={viewMode}
      >
        <FormControlLabel
          classes={{ label: classes.label }}
          value="tableView"
          control={<Radio classes={{ root: classes.radio }} />}
          label={t('common:Table View')}
        />
        <FormControlLabel
          classes={{ label: classes.label }}
          value="periodicTable"
          control={<Radio classes={{ root: classes.radio }} />}
          label={t('common:Periodic Table')}
        />
      </RadioGroup>
    </FormControl>
  )
}

export default withStyles(styles)(ChangeViewScreen)
