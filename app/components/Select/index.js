import React from 'react'
import Select from 'react-select'

const options = [
  { value: 'Blockchain Category 1', label: 'Blockchain Category 1' },
  { value: 'Blockchain Category 2', label: 'Blockchain Category 2' },
  { value: 'Blockchain Category 3', label: 'Blockchain Category 3' }
]

const customStyles = {
  option: (provided, state) => ({
    ...provided,
    color: '#fff',
    cursor: 'pointer',
    backgroundColor: '#323232',
    fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif'
  }),
  singleValue: (provided, state) => ({
    ...provided,
    color: '#fff',
    fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif'
  }),
  placeholder: (provided, state) => ({
    ...provided,
    fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif'
  }),
  menuList: (provided, state) => ({
    ...provided,
    backgroundColor: '#323232'
  }),
  control: () => ({
    display: 'flex',
    backgroundColor: '#323232'
  })
}

const SelectComponent = ({ list = [], onSelect, selected_val, placeholder = 'Select' }) => {

  return <Select value={selected_val}
                 defaultValue={selected_val}
                 options={list}
                 styles={customStyles}
                 placeholder={placeholder}
                 onChange={onSelect}/>
}

export default SelectComponent
