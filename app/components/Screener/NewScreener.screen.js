// @flow

import React from 'react'
import { Typography, withStyles } from '@material-ui/core'
import DashboardHeader from '../Dashboard/DashboardHeader.screen.js'
import FiltersContainer from './FiltersContainer/FiltersContainers.container'
import ResultsTable from './Results/ResultsTable/ResultsTable.container'

type Props = {
  onShowResults: any => void,
  showResults: boolean,
  resultsFilters: any,
  onChangeFilters: any => void,
  screener?: Object,
  filters: [],
  defaultFilters: [],
  t: string => string,
  classes: Object
};

const styles = theme => ({
  container: {
    background: theme.palette.background.paper,
    padding: '1rem',
    marginBottom: '1rem',
    [theme.breakpoints.down('sm')]: {
      margin: '0 -1rem'
    }
  }
})

const NewScreenerScreen = (props: Props) => {
  const {
    filters,
    defaultFilters,
    t,
    classes,
    onShowResults,
    showResults,
    resultsFilters,
    ...other
  } = props
  return (
    <React.Fragment>
      <DashboardHeader title={'Screener Builder'} />

      <div className={classes.container}>
        <Typography>
          {t('screener:Build a screener with filters below')}
        </Typography>
        <FiltersContainer
          t={t}
          allFilters={filters}
          defaultFilters={defaultFilters}
          onShowResults={onShowResults}
          {...other}
        />
      </div>

      {showResults && <ResultsTable t={t} filters={resultsFilters} />}
    </React.Fragment>
  )
}

export default withStyles(styles)(NewScreenerScreen)
