// @flow

import React from 'react'
import Menu from './Menu.screen'

type Props = {
  items: [],
  initialItem?: any,
  changeItem: Object => void
};

type State = {
  anchorEl: Object,
  currentItem: Object
};

class FilterMenu extends React.PureComponent<Props, State> {
  props: Props;
  state: State;

  constructor (props: Props) {
    super(props)

    this.state = {
      anchorEl: null,
      currentItem: props.initialItem
    }
  }

  onClickMenu = event => {
    this.setState({ anchorEl: event.currentTarget })
  };

  onClickItem = item => {
    this.setState({
      currentItem: item,
      anchorEl: null
    })
    this.props.changeItem(item)
  };

  onCloseMenu = () => {
    this.setState({ anchorEl: null })
  };

  render () {
    const { anchorEl, currentItem } = this.state
    const { items } = this.props

    return (
      <Menu
        clickMenu={this.onClickMenu}
        closeMenu={this.onCloseMenu}
        clickItem={this.onClickItem}
        anchorEl={anchorEl}
        isMenuOpen={!!anchorEl}
        items={items}
        currentItem={currentItem}
      />
    )
  }
}

export default FilterMenu
