// @flow

import React from 'react'
import {
  Input,
  ListItem,
  ListItemSecondaryAction,
  ListItemText,
  IconButton,
  withStyles
} from '@material-ui/core'
import DeleteIcon from '@material-ui/icons/HighlightOff'
import Menu from './Menu.container'
import Select from './Select.screen'

type Props = {
  value: any,
  compFilters: [],
  compFilter: Object,
  changeLowerLimit: () => void,
  changeUpperLimit: () => void,
  changeValue: () => void,
  deleteFilter: () => void,
  type: string,
  changeFilter: Object => void,
  name: string,
  currency?: string,
  selection?: [],
  changeCurrency: string => void,
  prop: string,
  classes: Object,
  t: string => string
};

const styles = theme => ({
  root: {
    backgroundColor: theme.palette.background.header,
    margin: '6px 0',
    padding: '6px 32px 6px 24px',
    [theme.breakpoints.down('sm')]: {
      padding: '6px 24px 6px 8px'
    }
  },
  container: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    [theme.breakpoints.down('sm')]: {
      flexWrap: 'wrap'
    }
  },
  name: {
    width: '140px',
    [theme.breakpoints.down('sm')]: {
      flexBasis: '100%',
      marginBottom: '.25rem',
      marginLeft: '.5rem'
    }
  },
  filter: {
    width: '140px'
  },
  priceSelection: {
    marginLeft: '20px'
  },
  input: {
    width: '60px',
    padding: '5px 10px',
    border: `1px solid ${theme.palette.grey[700]}`,
    fontSize: '14px',
    textAlign: 'right',
    '&:focus': {
      borderColor: theme.palette.secondary.main
    }
  },
  dash: {
    padding: '0 10px'
  },
  remove: {
    right: '-50px',
    [theme.breakpoints.down('sm')]: {
      right: 'auto'
    }
  }
})

const NumericalFilterScreen = (props: Props) => {
  const {
    name,
    compFilters,
    compFilter,
    classes,
    changeFilter,
    deleteFilter,
    type,
    value,
    currency,
    selection,
    changeCurrency,
    t
  } = props
  const { changeLowerLimit, changeUpperLimit, changeValue } = props

  return (
    <ListItem classes={{ root: classes.root }}>
      <ListItemText
        primary={
          <div className={classes.container}>
            <div className={classes.name}>
              {t(`cryptoassets-table:${name}`)}
            </div>
            <div className={classes.filter}>
              <Menu
                changeItem={changeFilter}
                items={compFilters}
                initialItem={compFilter}
              />
            </div>
            {type === 'value' && (
              <Input
                inputProps={{ className: classes.input }}
                disableUnderline
                value={value}
                onChange={changeValue}
              />
            )}
            {type === 'range' && (
              <React.Fragment>
                <Input
                  inputProps={{ className: classes.input }}
                  disableUnderline
                  value={value}
                  onChange={changeLowerLimit}
                  type="number"
                />
                <span className={classes.dash}>-</span>
                <Input
                  inputProps={{ className: classes.input }}
                  value={value}
                  onChange={changeUpperLimit}
                  type="number"
                />
              </React.Fragment>
            )}
            {currency && (
              <div className={classes.priceSelection}>
                <Select
                  changeItem={changeCurrency}
                  items={selection}
                  value={currency}
                />
              </div>
            )}
          </div>
        }
      />
      <ListItemSecondaryAction>
        <IconButton onClick={deleteFilter} className={classes.remove}>
          <DeleteIcon />
        </IconButton>
      </ListItemSecondaryAction>
    </ListItem>
  )
}

export default withStyles(styles)(NumericalFilterScreen)
