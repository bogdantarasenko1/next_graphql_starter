// @flow

import React from 'react'
import { Select, FormControl, MenuItem } from '@material-ui/core'

type Props = {
  items: [],
  value?: string,
  changeItem: Object => void,
  classes: Object
};

const FilterSelectScreen = (props: Props) => {
  const { value, items, changeItem } = props

  const onChange = event => {
    const name = event.target.value
    changeItem(name)
  }

  return (
    <FormControl>
      <Select value={value} onChange={onChange} displayEmpty disableUnderline>
        {items.map((item, index) => (
          <MenuItem key={index} value={item.name}>
            {item.title}
          </MenuItem>
        ))}
      </Select>
    </FormControl>
  )
}

export default FilterSelectScreen
