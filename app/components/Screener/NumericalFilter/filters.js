export default [
  {
    value: 'gt',
    title: 'Greater than'
  },
  {
    value: 'gte',
    title: 'Greater than or equal'
  },
  {
    value: 'lt',
    title: 'Less than'
  },
  {
    value: 'lte',
    title: 'Less than or equal'
  },
  {
    value: 'eq',
    title: 'Equal to'
  } /*,
  {
    value: 'between',
    title: 'Between'
  } */
]
