// @flow

import React from 'react'
import NumericalFilter from './NumericalFilter.screen'
import compFilters from './filters'

type Props = {
  t: string => string,
  deleteFilter: (filter: Object) => void,
  updateFilter: (filter: Object) => void,
  filter: Object
};

type State = {
  value: any,
  currency?: string,
  type: string,
  name: string,
  operator: string
};

class NumericalFilterContainer extends React.PureComponent<Props, State> {
  props: Props;
  state: State;

  constructor (props: Props) {
    super(props)

    const {
      filter: { value, type = 'value', operator = 'gt' }
    } = props

    this.state = {
      value: value || '',
      type: type,
      operator: operator
    }
  }

  componentDidUpdate (props, state) {
    if (state !== this.state) {
      this.props.updateFilter(this.props.filter, this.state)
    }

    if (props.filter.value !== this.props.filter.value) {
      this.setState({ value: this.props.filter.value })
    }
  }

  onDeleteFilter = () => {
    const { deleteFilter, filter } = this.props
    deleteFilter(filter)
  };

  onChangeValue = event => {
    this.setState({ value: this.filterValue(event.currentTarget.value) })
  };

  onChangeLowerLimit = event => {
    const from = this.filterValue(event.currentTarget.value)
    this.setState(prevState => {
      const {
        value: { to }
      } = prevState
      return {
        value: { from, to }
      }
    })
  };

  onChangeUpperLimit = event => {
    const to = this.filterValue(event.currentTarget.value)
    this.setState(prevState => {
      const {
        value: { from }
      } = prevState
      return {
        value: { from, to }
      }
    })
  };

  updateValueFilter = () => {
    const { value = {} } = this.state
    const { from } = value

    if (from) {
      this.setState({ type: 'value', value: from })
    } else {
      const newValue = typeof value === 'object' ? '' : value
      this.setState({ type: 'value', value: newValue })
    }
  };

  updateRangeFilter = () => {
    const { value: from } = this.state
    this.setState({ type: 'range', value: { from } })
  };

  onChangeFilter = filter => {
    this.setState({ operator: filter.value })
    switch (filter.value) {
      case 'gt':
      case 'gte':
      case 'lt':
      case 'lte':
      case 'eq':
        this.updateValueFilter()
        break
      case 'between':
        this.updateRangeFilter()
        break
      default:
        console.log(`Unhandled numerical filter ${filter.id}`)
        break
    }
  };

  onChangeCurrency = currency => {
    this.setState({ currency: currency })
  };

  compFilter = () => {
    const { operator } = this.state
    return compFilters.find(f => f.value === operator)
  };

  filterValue = value => {
    const matches = (value.match(/\d+\.?(\d+)?/g) || []).filter(v => v)
    return matches.pop() || ''
  };

  render () {
    const {
      filter: { name, currency, selection },
      t
    } = this.props
    const { type, value } = this.state

    return (
      <NumericalFilter
        t={t}
        type={type}
        value={value}
        compFilters={compFilters}
        compFilter={this.compFilter()}
        changeLowerLimit={this.onChangeLowerLimit}
        changeUpperLimit={this.onChangeUpperLimit}
        changeValue={this.onChangeValue}
        deleteFilter={this.onDeleteFilter}
        changeFilter={this.onChangeFilter}
        name={name}
        currency={currency}
        selection={selection}
        changeCurrency={this.onChangeCurrency}
      />
    )
  }
}

export default NumericalFilterContainer
