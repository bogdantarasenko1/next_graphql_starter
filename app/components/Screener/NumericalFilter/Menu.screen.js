// @flow

import React from 'react'
import { MenuItem, Menu, Button, withStyles } from '@material-ui/core'
import ArrowDropDown from '@material-ui/icons/ArrowDropDown'

type Props = {
  currentItem: Object,
  clickMenu: Object => void,
  clickItem: Object => void,
  closeMenu: () => void,
  anchorEl: Object,
  isMenuOpen: boolean,
  items: [],
  classes: Object
};

const styles = theme => ({
  label: {
    textTransform: 'initial',
    color: theme.palette.secondary.main
  }
})

const FilterMenuScreen = (props: Props) => {
  const {
    clickMenu,
    clickItem,
    closeMenu,
    anchorEl,
    isMenuOpen,
    items,
    currentItem,
    classes
  } = props

  return (
    <div>
      <Button
        mini
        disableRipple
        size="small"
        classes={{ label: classes.label }}
        onClick={clickMenu}
      >
        {currentItem.title}
        <ArrowDropDown />
      </Button>
      <Menu anchorEl={anchorEl} open={isMenuOpen} onClose={closeMenu}>
        {items.map((item, index) => (
          <MenuItem key={index} onClick={() => clickItem(item)}>
            {item.title}
          </MenuItem>
        ))}
      </Menu>
    </div>
  )
}

export default withStyles(styles)(FilterMenuScreen)
