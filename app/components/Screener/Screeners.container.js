// @flow

import React from 'react'
import { Query } from 'react-apollo'
import gql from 'graphql-tag'
import Screeners from './Screeners.screen'
import UserScreeners from './UserScreeners/UserScreeners.container'

const query = gql`
  query GetScreeners {
    viewer {
      currentUser {
        email
        screeners {
          id
          name
          text
          filters {
            category
            name
            operator
            value
          }
        }
      }
    }
  }
`

type Props = {
  user: Object,
  isLoading: boolean,
  screeners: ?[]
};
type State = {};

class ScreenersContainer extends React.PureComponent<Props, State> {
  content = () => {
    const { screeners, isLoading, ...other } = this.props
    const { user } = this.props

    if (user) {
      return (
        <UserScreeners screeners={screeners} isLoading={isLoading} {...other} />
      )
    } else {
      return <Screeners {...other} />
    }
  };

  render () {
    return this.content()
  }
}

const ScreenersContainerQuery = (props: Props) => (
  <Query query={query}>
    {({ data, loading }) => {
      const { viewer: { currentUser: { screeners } = {} } = {} } = data
      return (
        <ScreenersContainer
          screeners={screeners}
          isLoading={loading}
          {...props}
        />
      )
    }}
  </Query>
)

export default ScreenersContainerQuery
