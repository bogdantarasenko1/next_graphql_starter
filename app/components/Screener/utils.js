export const validFilters = filters => {
  return filters.filter(f => f.name && f.operator && f.value)
}

export const filtersToString = filters => {
  const inputs = filters.map(filter => {
    let name = filter.name === 'price' ? filter.currency : filter.name
    let operator = filter.operator
    let value = filter.value
    return { name, operator, value }
  })

  const params = inputs.reduce((acc, input) => {
    const { name, operator, value } = input
    const str = `${name}: {${operator}: ${parseFloat(value)}}`
    return acc.concat(str)
  }, [])
  return params.join(', ')
}

export const fieldsForFilters = filters => {
  const blockchain = blockchainFields(filters)
  const priceUSD = priceUSDFields(filters)
  const priceBTC = priceBTCFields(filters)

  return `
  {
    symbol
    name
    metadata {
      logo
    }
    ${blockchain}
    ${priceBTC}
    ${priceUSD}
    }`
}

export const blockchainFields = filters => {
  const inputs = filters.map(filter => {
    let name = filter.name === 'price' ? filter.currency : filter.name
    // const blockChain = blockChainFields.includes(name)
    const amount = amountFields.includes(name)
    return { name, amount }
  })

  const blockchains = inputs
    .filter(f => blockChainFields.includes(f.name))
    .map(mapFiltersToFields)

  if (blockchains.length) {
    return blockchains
      .reduce(
        (acc, input) => {
          const str = createField(input)
          return acc.concat(str)
        },
        ['blockchain {', 'latest{']
      )
      .concat(['}', '}'])
      .join('\n')
  }

  return ''
}

const createField = filter => {
  if (filter.amount) {
    return `${filter.name} {
      amount
    }`
  } else {
    return `${filter.name}`
  }
}

export const priceUSDFields = filters => {
  const inputs = filters.map(filter => {
    let name = filter.name === 'price' ? filter.currency : filter.name
    const amount = amountFields.includes(name)
    return { name, amount }
  })

  const priceUSD = inputs
    .filter(f => usdFields.includes(f.name))
    .map(mapFiltersToFields)
  const marketcap = inputs
    .filter(f => marketcapFields.includes(f.name))
    .map(mapFiltersToFields)

  const marketcapStr = marketcap.length
    ? marketcap
      .reduce(
        (acc, input) => {
          const str = createField(input)
          return acc.concat(str)
        },
        ['marketCap {']
      )
      .concat(['}'])
      .join('\n')
    : ''

  const priceStr = priceUSD.length
    ? priceUSD
      .reduce((acc, input) => {
        const str = createField(input)
        return acc.concat(str)
      }, [])
      .join('\n')
    : ''

  if (marketcapStr.length || priceStr.length) {
    return `USDPrice: market {
        latest {
          ${marketcapStr}
          ${priceStr}
        }
      }`
  }

  return ''
}

export const priceBTCFields = filters => {
  const inputs = filters.map(filter => {
    let name = filter.name === 'price' ? filter.currency : filter.name
    const amount = amountFields.includes(name)
    return { name, amount }
  })

  const priceBTC = inputs
    .filter(f => btcFields.includes(f.name))
    .map(mapFiltersToFields)

  if (priceBTC.length) {
    return priceBTC
      .reduce(
        (acc, input) => {
          const str = createField(input)
          return acc.concat(str)
        },
        ['BTCPrice: market(currency: "BTC") {', 'latest {']
      )
      .concat(['}', '}'])
      .join('\n')
  }

  return ''
}

const mapFiltersToFields = f => {
  switch (f.name) {
    case 'totalMarketCap':
      f.name = 'basedOnTotalSupply'
      break
    case 'maxMarketCap':
      f.name = 'basedOnMaxSupply'
      break
    case 'circulatingMarketCap':
      f.name = 'basedOnCirculatingSupply'
      break
    case 'adjTxVolume':
      f.name = 'adjustedTxVolume'
      break
    case 'priceBTC':
    case 'priceUSD':
      f.name = 'latestPrice'
      break
    default:
      break
  }
  return f
}

const blockChainFields = [
  'circulatingSupply',
  'activeAddresses',
  'difficulty',
  'adjTxVolume',
  'txCount',
  'feesPercentOfValue',
  'txVolume',
  'averageTxValue',
  'medianTxValue',
  'totalFees',
  'medianFee',
  'nvt'
]

const amountFields = [
  'adjTxVolume',
  'txVolume',
  'averageTxValue',
  'medianTxValue',
  'totalFees',
  'medianFee',
  'totalMarketCap',
  'priceBTC',
  'priceUSD',
  'circulatingMarketCap',
  'maxMarketCap'
]

const btcFields = ['priceBTC']

const usdFields = ['priceUSD']

const marketcapFields = [
  'totalMarketCap',
  'maxMarketCap',
  'circulatingMarketCap'
]
