// @flow

import React from 'react'
import NewScreener from './NewScreener.screen'
import filters from './availableFilters'

type Props = {};

type State = {
  showResults: boolean,
  resultsFilters: any
};

const defaultFiltersId = ['totalMarketCap', 'circulatingMarketCap', 'price']

class NewScreenerContainer extends React.PureComponent<Props, State> {
  props: Props;
  state: State;

  constructor (props) {
    super(props)
    this.state = {
      showResults: false
    }
  }

  defaultFilters = () => {
    return filters.filter(e => defaultFiltersId.includes(e.name))
  };

  onShowResults = filters => {
    this.setState({
      showResults: true,
      resultsFilters: [...filters]
    })
  };

  render () {
    const { showResults, resultsFilters } = this.state
    return (
      <NewScreener
        filters={filters}
        defaultFilters={this.defaultFilters()}
        onShowResults={this.onShowResults}
        showResults={showResults}
        resultsFilters={resultsFilters}
        {...this.props}
      />
    )
  }
}

export default NewScreenerContainer
