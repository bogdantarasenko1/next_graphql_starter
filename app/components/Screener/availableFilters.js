export default [
  {
    name: 'totalMarketCap'
  },
  {
    name: 'maxMarketCap'
  },
  {
    name: 'circulatingMarketCap'
  },
  {
    name: 'circulatingSupply'
  },
  {
    name: 'activeAddresses'
  },
  {
    name: 'difficulty'
  },
  {
    name: 'txVolume'
  },
  {
    name: 'adjTxVolume'
  },
  {
    name: 'txCount'
  },
  {
    name: 'averageTxValue' // avgTransactionValue
  },
  {
    name: 'medianTxValue'
  },
  {
    name: 'totalFees'
  },
  {
    name: 'medianFee'
  },
  {
    name: 'feesPercentOfValue'
  },
  {
    name: 'nvt'
  },
  {
    name: 'price',
    currency: 'priceUSD',
    selection: [
      { name: 'priceUSD', title: 'USD' },
      { name: 'priceBTC', title: 'BTC' }
    ]
  }
]
