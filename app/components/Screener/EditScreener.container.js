// @flow

import React from 'react'
import { LinearProgress } from '@material-ui/core'
import NewScreener from './NewScreener.screen'
import filters from './availableFilters'

type Props = {
  onUpdate: () => void,
  isLoading: boolean,
  screener: Object
};

type State = {
  showResults: boolean,
  resultsFilters: any
};

class NewScreenerContainer extends React.PureComponent<Props, State> {
  props: Props;
  state: State;

  constructor (props: Props) {
    super(props)

    this.state = {
      showResults: false
    }
  }

  screenerFilters = () => {
    const {
      screener: { filters: screenerFilters }
    } = this.props

    return [...screenerFilters]
  };

  onShowResults = filters => {
    this.setState({
      showResults: true,
      resultsFilters: [...filters]
    })
  };

  render () {
    if (this.props.isLoading) {
      return <LinearProgress color="secondary" />
    }

    const { showResults, resultsFilters } = this.state
    return (
      <NewScreener
        filters={filters}
        defaultFilters={this.screenerFilters()}
        onShowResults={this.onShowResults}
        showResults={showResults}
        resultsFilters={resultsFilters}
        {...this.props}
      />
    )
  }
}

export default NewScreenerContainer
