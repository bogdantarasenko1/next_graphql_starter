// @flow

import React from 'react'
import gql from 'graphql-tag'
import { graphql, compose } from 'react-apollo'
import AddFilter from '../AddFilter/AddFilter.container'
import SaveFilters from '../SaveFilters/SaveFilters.container'
import { Router } from '../../../../lib/routes'
import { validFilters } from '../utils'
import ShowResults from '../Results/ShowResults.screen'
import FiltersContainer from './FiltersContainers.screen'

type Props = {
  onShowResults: any => void,
  screener?: Object,
  createScreener: Object => Promise,
  onUpdate: (screener: Object) => void,
  updateScreener: Object => Promise,
  allFilters: [],
  defaultFilters: []
};

type State = {};

class FiltersContainerContainer extends React.PureComponent<Props, State> {
  props: Props;
  state: State;

  constructor (props: Props) {
    super(props)
    const { defaultFilters } = props

    this.state = {
      userFilters: defaultFilters
    }
  }

  onDeleteFilter = filter => {
    const { userFilters: oldFilters } = this.state
    const userFilters = oldFilters.filter(f => f.name !== filter.name)
    this.setState({ userFilters })
  };

  onChangeFilters = filters => {
    const { userFilters } = this.state
    const indexToRemove = userFilters.findIndex(f => !filters.includes(f.name))
    if (~indexToRemove) {
      userFilters.splice(indexToRemove, 1)
      this.setState({ userFilters: [...userFilters] })
    } else {
      const { allFilters } = this.props
      const userFiltersId = userFilters.map(f => f.name)
      const newFilters = filters.filter(f => !userFiltersId.includes(f))
      const filtersToAdd = allFilters.filter(filter =>
        newFilters.includes(filter.name)
      )
      this.setState({ userFilters: userFilters.concat(filtersToAdd) })
    }
  };

  updateScreener = () => {
    const { updateScreener, onUpdate, screener } = this.props
    const { userFilters } = this.state
    const validFilters = userFilters
      .filter(f => !!f.value)
      .map(f => {
        delete f.type
        delete f.__typename
        f.category = 'finance'
        return f
      })
      .map(this.transformPriceFilter)

    updateScreener({
      variables: {
        input: {
          id: screener.id,
          name: screener.name,
          text: screener.text,
          filters: validFilters
        }
      }
    }).then(({ data: { updateScreener: { screener } } }) => {
      onUpdate(screener)
    })
  };

  createScreener = ({ name, text }) => {
    const { createScreener } = this.props
    const { userFilters } = this.state
    const validFilters = userFilters
      .filter(f => !!f.value)
      .map(f => {
        delete f.type
        delete f.__typename
        f.category = 'finance'
        return f
      })
      .map(this.transformPriceFilter)

    createScreener({
      variables: {
        input: {
          name: name,
          text: text,
          filters: validFilters
        }
      },
      refetchQueries: [
        {
          query: gql`
            query GetScreeners {
              viewer {
                currentUser {
                  email
                  screeners {
                    id
                    name
                    text
                    filters {
                      category
                      name
                      operator
                      value
                    }
                  }
                }
              }
            }
          `
        }
      ]
    })
      .then(({ data: { createScreener: { screener } } }) => {
        Router.pushRoute('screener', { page: screener.id })
      })
      .catch(e => console.log(e))
  };

  onEnterName = data => {
    this.createScreener(data)
  };

  onUpdateFilter = (filter, data) => {
    const { userFilters } = this.state
    const index = userFilters.findIndex(f => f.name === filter.name)
    const updatedFilter = Object.assign({}, userFilters[index])
    Object.assign(updatedFilter, data)
    userFilters[index] = updatedFilter
    this.setState({ ...userFilters })
  };

  showResults = () => {
    const { userFilters } = this.state
    const { onShowResults } = this.props
    onShowResults(userFilters)
  };

  isResultsDisabled = filters => {
    return !validFilters(filters).length
  };

  transformPriceFilter = filter => {
    if (filter.name === 'price') {
      filter.name = filter.currency
      delete filter.selection
      delete filter.currency
    }
    return filter
  };

  render () {
    const { userFilters } = this.state
    const { allFilters } = this.props

    return (
      <div>
        <FiltersContainer
          {...this.props}
          deleteFilter={this.onDeleteFilter}
          updateFilter={this.onUpdateFilter}
          filters={userFilters}
        />
        <AddFilter
          {...this.props}
          userFilters={userFilters.map(i => i.name)}
          allFilters={allFilters.map(i => i.name)}
          onChangeFilters={this.onChangeFilters}
        />
        <div>
          <ShowResults
            {...this.props}
            click={this.showResults}
            disabled={this.isResultsDisabled(userFilters)}
          />
          <SaveFilters
            {...this.props}
            save={this.updateScreener}
            enterName={this.onEnterName}
            isDisabled={this.isResultsDisabled(userFilters)}
            userFilters={userFilters}
          />
        </div>
      </div>
    )
  }
}

const createScreenerQuery = gql`
  mutation createScreener($input: createScreenerInput!) {
    createScreener(input: $input) {
      screener {
        id
        name
        text
        filters {
          name
          operator
          category
          value
        }
      }
    }
  }
`
const updateScreenerQuery = gql`
  mutation updateScreener($input: updateScreenerInput!) {
    updateScreener(input: $input) {
      screener {
        id
        name
        text
        filters {
          name
          operator
          category
          value
        }
      }
    }
  }
`

export default compose(
  graphql(createScreenerQuery, { name: 'createScreener' }),
  graphql(updateScreenerQuery, { name: 'updateScreener' })
)(FiltersContainerContainer)
