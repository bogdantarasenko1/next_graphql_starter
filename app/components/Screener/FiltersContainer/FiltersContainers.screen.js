// @flow

import React from 'react'
import { List, withStyles } from '@material-ui/core'
import NumericalFilter from '../NumericalFilter/NumericalFilter.container'

type Props = {
  deleteFilter: (filter: Object) => void,
  updateFilter: (Object, Object) => void,
  filters: [],
  t: string => string,
  classes: Object
};

const styles = theme => ({
  container: {
    maxWidth: '800px'
  }
})

const FiltersContainersScreen = (props: Props) => {
  const { filters, deleteFilter, updateFilter, t, classes } = props
  return (
    <div className={classes.container}>
      <List>
        {filters.map((filter, index) => (
          <NumericalFilter
            t={t}
            deleteFilter={deleteFilter}
            filter={filter}
            key={index}
            updateFilter={updateFilter}
          />
        ))}
      </List>
    </div>
  )
}

export default withStyles(styles)(FiltersContainersScreen)
