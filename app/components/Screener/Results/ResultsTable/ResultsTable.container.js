// @flow

import React from 'react'
import _ from 'lodash'
import queryResults from '../../../../utils/query/screenerResult'
import {
  fetchData,
  mapTradingData,
  formatNumbers
} from '../../../../utils/dataPlatformQuery'
import {
  filtersToString,
  validFilters as validFiltersUtil,
  fieldsForFilters
} from '../../utils'
import ResultsTable from './ResultsTable.screen'

type Props = {
  t: string => string,
  filters: any
};

type State = {
  isLoading: boolean,
  assets: [],
  sort: string,
  sortKey: string,
  sortedField: string,
  filtersQuery: string,
  errors: any
};

class ResultsTableContainer extends React.PureComponent<Props, State> {
  constructor (props: Props) {
    super(props)
    const filters = this.validFilters(props.filters)
    this.state = {
      sort: 'desc',
      sortKey: this.defaultSortKey(filters),
      sortedField: this.defaultSortKey(filters),
      filtersQuery: filtersToString(filters),
      isLoading: true,
      page: 0,
      rowsPerPage: 20,
      dataLength: 0
    }
  }

  handleChangePage = (event, page) => {
    this.setState({ page: page })
  };

  componentDidUpdate (prevProps, prevState) {
    const filters = this.validFilters()
    const filtersQuery = filtersToString(filters)
    if (!this.isValidFilters(filtersQuery)) {
      this.invalidFiltersState()
      return
    }

    const { isLoading } = this.state
    if (isLoading) return

    if (prevState.filtersQuery !== filtersQuery) {
      const sortKey = this.defaultSortKey(filters)
      this.setState({ sortKey, filtersQuery, isLoading: true, errors: null })
      const fields = fieldsForFilters(filters)
      const query = JSON.stringify({
        query: this.prepareQuery({ filtersQuery, sortKey, fields })
      })

      this.requestData(query)
    }

    const isSortChanged = prevState.sort !== this.state.sort
    const isSortKeyChanged = prevState.sortKey !== this.state.sortKey
    const isPageChanged = prevState.page !== this.state.page

    if (isSortChanged || isSortKeyChanged || isPageChanged) {
      this.setState({ isLoading: true, errors: null })
      const fields = fieldsForFilters(filters)
      const query = JSON.stringify({
        query: this.prepareQueryForNewOrdering(fields)
      })

      this.requestData(query)
    }
  }

  componentDidMount () {
    const { filtersQuery, sortKey } = this.state
    const filters = this.validFilters(this.props.filters)

    if (!this.isValidFilters(filtersQuery)) {
      this.invalidFiltersState()
      return
    }

    const fields = fieldsForFilters(filters)
    const query = JSON.stringify({
      query: this.prepareQuery({ filtersQuery, sortKey, fields })
    })

    this.requestData(query)
  }

  requestData = query => {
    const filters = this.validFilters()
    fetchData(query)
      .then(r => {
        const {
          data: {
            getCryptos: { cryptos: data }
          },
          errors
        } = r

        const dataLength = r.data.getCryptos.metadata.totalCount

        if (errors) {
          this.setState({ errors: errors })
        }

        this.setState({
          dataLength: dataLength
        })

        const assets = mapTradingData(data, () => ({
          ico: '/static/coin/btc.svg'
        }))
        const formatted = formatNumbers(assets)
        const filtered = this.filterColumns(formatted, filters)

        this.setState({
          assets: [...filtered],
          isLoading: false
        })
      })
      .catch(e => {
        this.setState({ errors: e, isLoading: false })
        return e
      })
  };

  validFilters = filters => {
    filters = filters || this.props.filters
    return validFiltersUtil(filters)
  };

  mapFields = () => {
    const filters = this.validFilters()
    return filters.map(f => {
      switch (f.name) {
        case 'totalMarketCap':
          return 'basedOnTotalSupply'
        case 'maxMarketCap':
          return 'basedOnMaxSupply'
        case 'circulatingSupply':
          return 'circulatingSupplyBlockchain'
        case 'adjTxVolume':
          return 'adjustedTxVolume'
        case 'averageTxValue':
          return 'averageTxValue'
        case 'circulatingMarketCap':
          return 'basedOnCirculatingSupply'
        case 'price':
          switch (f.currency) {
            case 'priceUSD':
              return 'latestPriceUSD'
            case 'priceBTC':
              return 'latestPriceBTC'
            default:
              console.log(`unhandled currency in filter: ${f.currency}`)
              return f.currency
          }
        default:
          return f.name
      }
    })
  };

  filterColumns = (data, filters) => {
    const columns = ['ico', 'symbol', 'name'].concat(this.mapFields())
    return data.map(o => _.pick(o, columns))
  };

  headerColumns = () => {
    const { t } = this.props
    const columns = this.columns()
    const sortableColumns = this.sortableColumns()

    return columns.map(col => {
      if (col) {
        const sortable = sortableColumns.includes(col)
        var frozen = false // col === 'symbol'
        var faded = false // col === 'name'
        return {
          noText: col === 'ico',
          key: col,
          text: t(`cryptoassets-table:${col}`),
          sortable,
          frozen,
          faded
        }
      }

      return col
    })
  };

  isValidFilters = filters => {
    return !!filters
  };

  invalidFiltersState = () => {
    this.setState({ errors: 'Filters not set', isLoading: false })
  };

  defaultSortKey = filters => {
    const defaultFilter = filters[0]
    return defaultFilter.name === 'price'
      ? defaultFilter.currency
      : defaultFilter.name
  };

  prepareQuery = ({ filtersQuery, sortKey, fields }) => {
    const { rowsPerPage } = this.state
    return queryResults({
      sorting: { field: sortKey, order: this.state.sort.toUpperCase() },
      pagination: { offset: 0, limit: rowsPerPage },
      filters: filtersQuery,
      fields: fields
    })
  };

  prepareQueryForNewOrdering = fields => {
    const { filtersQuery, sort, sortKey, rowsPerPage, page } = this.state
    const offset = page * rowsPerPage

    return queryResults({
      sorting: { field: sortKey, order: sort.toUpperCase() },
      pagination: { offset: offset, limit: rowsPerPage },
      filters: filtersQuery,
      fields: fields
    })
  };

  columns = () => {
    return ['ico', 'symbol', 'name'].concat(this.sortableColumns())
  };

  sortableColumns = () => {
    return this.mapFields()
  };

  sortData = key => {
    let ordering = ''
    switch (key) {
      case 'latestPriceBTC':
        ordering = 'priceBTC'
        break
      case 'latestPriceUSD':
        ordering = 'priceUSD'
        break
      case 'basedOnTotalSupply':
        ordering = 'totalMarketCap'
        break
      case 'basedOnMaxSupply':
        ordering = 'maxMarketCap'
        break
      case 'basedOnCirculatingSupply':
        ordering = 'circulatingMarketCap'
        break
      case 'circulatingSupplyBlockchain':
        ordering = 'circulatingSupply'
        break
      default:
        ordering = key
        break
    }

    this.setState(prevState => {
      return {
        sort: this.nextSortDirection(
          prevState.sortKey,
          ordering,
          prevState.sort
        ),
        sortKey: ordering,
        sortedField: key,
        page: 0
      }
    })
  };

  nextSortDirection = (oldKey, newKey, oldDirection) => {
    if (oldKey === newKey) {
      return oldDirection === 'asc' ? 'desc' : 'asc'
    }

    return 'desc'
  };

  render () {
    const {
      assets,
      sortedField,
      sort,
      isLoading,
      errors,
      page,
      rowsPerPage,
      dataLength
    } = this.state

    return (
      <ResultsTable
        {...this.props}
        isLoading={isLoading}
        errors={errors}
        columns={this.columns()}
        assets={assets}
        customColumns={this.sortableColumns()}
        headerColumns={this.headerColumns()}
        sortData={this.sortData}
        sortDirection={sort}
        sortedField={sortedField}
        dataLength={dataLength}
        rowsPerPage={rowsPerPage}
        page={page}
        handleChangePage={this.handleChangePage}
      />
    )
  }
}

export default ResultsTableContainer
