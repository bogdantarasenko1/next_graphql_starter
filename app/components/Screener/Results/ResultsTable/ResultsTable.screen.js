// @flow

import React from 'react'
import Link from 'next/link'
import {
  Table,
  TableCell,
  TableRow,
  TableBody,
  Typography,
  LinearProgress,
  withStyles
} from '@material-ui/core'
import DashboardHeader from '../../../Dashboard/DashboardHeader.screen'
import SortableHeader from '../../../SortableHeader/SortableHeader.screen'
import Snackbar from '../../../Snackbar/Snackbar.container'
import Pagination from '../../../Pagination/Pagination.screen'

type Props = {
  isLoading: boolean,
  columns: [],
  assets: [],
  headerColumns: [],
  customColumns: [],
  sortData: (key: string) => void,
  sortDirection: string,
  sortedField: string,
  errors?: any,
  classes: Object,
  t: string => string,
  dataLength: number,
  rowsPerPage: number,
  page: number,
  handleChangePage: func
};

const styles = theme => ({
  table: {
    padding: '0 20px',
    overflowY: 'auto',
    [theme.breakpoints.down('sm')]: {
      margin: '0 -1rem'
    }
  },
  link: {
    color: theme.palette.secondary.main
  }
})

const ResultsTableScreen = (props: Props) => {
  const { isLoading, t, classes, errors } = props
  const { sortData, sortDirection, sortedField, headerColumns } = props
  const { dataLength, rowsPerPage, page, handleChangePage } = props

  const commonCell = (asset, c, i) => {
    if (c === 'ico') {
      return (
        <TableCell key={i}>
          <img src={asset.ico} width={16} height={16} />
        </TableCell>
      )
    } else if (c === 'symbol') {
      return tableCell(asset.symbol, asset, i)
    } else if (c === 'name') {
      return tableCell(asset.name, asset, i)
    } else {
      return <TableCell key={i}>{asset[c] || 'N/A'}</TableCell>
    }
  }

  const tableCell = (title, asset, i) => {
    return (
      <TableCell key={i}>
        <Link href={`/coin/${asset.symbol.toLowerCase()}`}>
          <a
            className={classes.link}
            href={`/coin/${asset.symbol.toLowerCase()}`}
          >
            {title || 'N/A'}
          </a>
        </Link>
      </TableCell>
    )
  }

  const tableContent = () => {
    if (isLoading) {
      return (
        <TableRow>
          <TableCell colSpan={headerColumns.length + 1}>
            <LinearProgress color="secondary" />
          </TableCell>
        </TableRow>
      )
    } else {
      if (assets && assets.length) {
        return assets.map((asset, index) => {
          return (
            <TableRow key={index} hover>
              <TableCell>{index + 1 + rowsPerPage * page}</TableCell>
              {columns.map((c, i) => commonCell(asset, c, i))}
            </TableRow>
          )
        })
      } else {
        return (
          <TableRow>
            <TableCell colSpan={headerColumns.length + 1}>
              <Typography variant={'subheading'} color={'secondary'}>
                No Data
              </Typography>
            </TableCell>
          </TableRow>
        )
      }
    }
  }

  const { assets, columns } = props

  return (
    <div>
      <DashboardHeader title={t('common:Results')} />
      {errors && <Snackbar message={errors} />}
      <div className={classes.table}>
        <Table>
          <SortableHeader
            numbers
            columns={headerColumns}
            sortData={sortData}
            sortDirection={sortDirection}
            sortedField={sortedField}
          />
          <TableBody>{tableContent()}</TableBody>
        </Table>
        <Pagination
          dataLength={dataLength}
          rowsPerPage={rowsPerPage}
          page={page}
          handleChangePage={handleChangePage}
        />
      </div>
    </div>
  )
}

export default withStyles(styles)(ResultsTableScreen)
