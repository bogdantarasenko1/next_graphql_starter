// @flow

import React from 'react'
import { Button } from '@material-ui/core'

type Props = {
  click: () => void,
  disabled: boolean,
  t: string => string
};

const Results = (props: Props) => {
  const { t, click, disabled } = props
  return (
    <Button
      variant={'contained'}
      color={'secondary'}
      onClick={click}
      disabled={disabled}
    >
      {t('screeners:Show Results')}
    </Button>
  )
}

export default Results
