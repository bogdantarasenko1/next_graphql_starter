// @flow

import React from 'react'
import Card from '../../TableColumns/Card/Card.container'
import AddFilter from './AddFilter.screen'
import CardAction from './CardAction.screen'

type Props = {
  t: string => string,
  onChangeFilters: filters => void,
  allFilters: [],
  userFilters: ?[]
};

type State = {
  anchorEl: Object
};

class AddFilterContainer extends React.PureComponent<Props, State> {
  state = {
    anchorEl: null
  };

  onAddFilter = (event: Object) => {
    this.setState({ anchorEl: event.currentTarget })
  };

  onClosePopover = () => {
    this.setState({ anchorEl: null })
  };

  onCloseFilters = () => {
    this.setState({ anchorEl: null })
  };

  render () {
    const { onChangeFilters } = this.props
    const { anchorEl } = this.state
    const { userFilters, allFilters, t } = this.props
    const columns = userFilters || allFilters

    return (
      <AddFilter
        {...this.props}
        anchorEl={anchorEl}
        onClosePopover={this.onClosePopover}
        addFilter={this.onAddFilter}
      >
        <Card
          {...this.props}
          title="Choose filters to screen"
          action={<CardAction t={t} click={this.onCloseFilters} />}
          selectedColumns={columns}
          columns={allFilters}
          onChangeColumns={onChangeFilters}
          headerSection
        />
      </AddFilter>
    )
  }
}

export default AddFilterContainer
