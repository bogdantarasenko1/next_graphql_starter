// @flow

import React from 'react'
import { Button, Popover, withStyles } from '@material-ui/core'
import AddCircle from '@material-ui/icons/AddCircle'

type Props = {
  addFilter: () => void,
  anchorEl: ?Object,
  onClosePopover: () => void,
  children: React.ReactNode,
  classes: Object,
  t: string => string
};

const styles = theme => ({
  button: {
    marginBottom: '10px'
  },
  label: {
    textTransform: 'initial'
  },
  extendedIcon: {
    marginRight: theme.spacing.unit
  }
})

const AddFilterScreen = (props: Props) => {
  const { addFilter, onClosePopover, anchorEl, children, classes, t } = props

  return (
    <div>
      <Button
        className={classes.button}
        classes={{ label: classes.label }}
        onClick={addFilter}
        variant="text"
        color="secondary"
      >
        <AddCircle className={classes.extendedIcon} />
        {t('common:Add another filter')}
      </Button>
      <Popover
        open={!!anchorEl}
        anchorEl={anchorEl}
        onClose={onClosePopover}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'right'
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'center'
        }}
      >
        {children}
      </Popover>
    </div>
  )
}

export default withStyles(styles)(AddFilterScreen)
