// @flow

import React from 'react'
import { Button, withStyles } from '@material-ui/core'

type Props = {
  click: () => void,
  classes: Object,
  t: string => string
};

const styles = theme => ({
  button: {
    color: '#007AFF'
  }
})

const CardActionScreen = (props: Props) => {
  const { click, t, classes } = props
  return (
    <Button className={classes.button} onClick={click} variant="flat">
      {t('common:Done')}
    </Button>
  )
}

export default withStyles(styles)(CardActionScreen)
