// @flow

import React from 'react'
import { Button } from '@material-ui/core'
import { Link } from '../../../lib/routes'

type Props = {
  t: string => string
};

const ScreenersScreen = (props: Props) => {
  const { t } = props
  return (
    <div>
      <Button color="primary" variant="contained">
        <Link route="screener" params={{ page: 'new' }}>
          <a href="/screener/new">{t('screener:Create New Screener')}</a>
        </Link>
      </Button>
    </div>
  )
}

export default ScreenersScreen
