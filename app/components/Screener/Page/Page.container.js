// @flow

import React from 'react'
import { Query } from 'react-apollo'
import gql from 'graphql-tag'
import EditScreener from '../EditScreener.container'
import { validFilters } from '../utils'
import Page from './Page.screen'

type Props = {
  page: string,
  screener?: Object,
  refetch?: () => void,
  isLoading: boolean
};

type State = {
  isLoading: boolean,
  isEditing: boolean,
  screener: Object,
  showResults: boolean
};

class PageContainer extends React.PureComponent<Props, State> {
  props: Props;
  State: State;

  constructor (props: Props) {
    super(props)

    this.state = {
      isLoading: props.isLoading,
      isEditing: false,
      screener: props.screener && this.prepareFilters(props.screener)
    }
  }

  componentDidUpdate () {
    this.setState({
      isLoading: this.props.isLoading,
      screener: this.props.screener && this.prepareFilters(this.props.screener)
    })
  }

  prepareFilters = screener => {
    const { filters } = screener

    screener.filters = filters.map(filter => {
      const f = Object.assign({}, filter)
      if (f.name === 'priceUSD' || f.name === 'priceBTC') {
        f.currency = f.name
        f.name = 'price'
        f.selection = [
          { name: 'priceUSD', title: 'USD' },
          { name: 'priceBTC', title: 'BTC' }
        ]
      }

      return f
    })

    return screener
  };

  onUpdate = screener => {
    this.setState({ isEditing: false, screener: screener })
    this.props.refetch()
  };

  onEdit = () => {
    this.setState({ isEditing: true })
  };

  showResults = () => {
    this.setState({
      showResults: true
    })
  };

  isResultsDisabled = () => {
    const { screener } = this.state
    if (!screener) {
      return true
    }
    return !validFilters(screener.filters).length
  };

  render () {
    const { isEditing, screener, showResults, isLoading } = this.state

    if (isEditing) {
      return (
        <EditScreener
          {...this.props}
          onUpdate={this.onUpdate}
          screener={screener}
          isLoading={isLoading}
        />
      )
    } else {
      return (
        <Page
          {...this.props}
          isLoading={isLoading}
          edit={this.onEdit}
          onShowResults={this.showResults}
          showResults={showResults}
          screener={screener}
          isResultsDisabled={this.isResultsDisabled()}
        />
      )
    }
  }
}

const query = gql`
  query screeners($ids: [ID!]) {
    getScreeners(ids: $ids) {
      edges {
        node {
          id
          name
          text
          filters {
            category
            name
            operator
            value
          }
        }
      }
    }
  }
`
const ScreenerContainerQuery = (props: Props) => (
  <Query query={query} variables={{ ids: [props.page] }}>
    {({ loading, error, data, refetch }) => {
      const screenerId = props.page
      let screener
      if (!loading && data) {
        const {
          getScreeners: { edges }
        } = data

        screener =
          edges && edges.map(e => e.node).find(s => s.id === screenerId)
        screener = Object.assign({}, screener)
      }

      return (
        <PageContainer
          {...props}
          isLoading={loading}
          refetch={refetch}
          screener={screener}
        />
      )
    }}
  </Query>
)

export default ScreenerContainerQuery
