// @flow

import React from 'react'
import {
  Paper,
  Typography,
  Button,
  withStyles,
  LinearProgress
} from '@material-ui/core'
import Summary from '../Summary/Summary.container'
import { Link } from '../../../../lib/routes'
import ShowResults from '../Results/ShowResults.screen'
import ResultsTable from '../Results/ResultsTable/ResultsTable.container'

type Props = {
  isLoading: boolean,
  onShowResults: () => void,
  showResults: boolean,
  screener: Object,
  isResultsDisabled: boolean,
  edit: () => void,
  classes: Object,
  t: string => string
};

const styles = theme => ({
  paper: {
    padding: '20px'
  },
  margin: {
    marginLeft: '10px'
  },
  results: {
    marginTop: '16px'
  }
})

const PageScreen = (props: Props) => {
  const {
    screener,
    edit,
    classes,
    onShowResults,
    showResults,
    isResultsDisabled,
    isLoading,
    t
  } = props

  if (isLoading) {
    return (
      <Paper className={classes.paper}>
        {isLoading && <LinearProgress color="secondary" />}
      </Paper>
    )
  }

  return (
    <div>
      <Paper className={classes.paper}>
        {screener && (
          <div>
            <Typography variant="subheading" color="textPrimary" paragraph>
              <Link route="screener">
                <a href={'/screener/'}>{t('screener:All Screeners')}</a>
              </Link>{' '}
              / {screener.name}
            </Typography>
            <Typography gutterBottom variant="subheading">
              {t('screener:Applied Filters')}
            </Typography>
            <Summary screener={screener} t={t} />
            <ShowResults
              t={t}
              click={onShowResults}
              disabled={isResultsDisabled}
            />
            <Button
              onClick={edit}
              variant="contained"
              className={classes.margin}
            >
              {t('common:Edit')}
            </Button>
          </div>
        )}
        {!screener && (
          <React.Fragment>
            <Typography variant="title" color="textPrimary">
              {t('screener:Requested screener was not found')}
            </Typography>
          </React.Fragment>
        )}
      </Paper>
      {screener &&
        showResults && (
        <Paper className={classes.results}>
          <ResultsTable t={t} filters={screener.filters} />
        </Paper>
      )}
    </div>
  )
}

export default withStyles(styles)(PageScreen)
