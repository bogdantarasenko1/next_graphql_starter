// @flow

import React from 'react'
import { Typography, withStyles } from '@material-ui/core'

type Props = {
  data: [],
  classes: Object
};

const styles = theme => ({
  root: {
    color: theme.palette.text.primary
  },
  bold: {
    fontWeight: 'bold'
  }
})

const SummaryScreen = (props: Props) => {
  const { data, classes } = props
  const lastEl = data && data.length - 1

  return (
    <Typography variant="body1" paragraph>
      {data.map((e, i) => (
        <React.Fragment key={i}>
          {e.title}: <span className={classes.bold}>{e.text}</span>
          {i < lastEl && ', '}
        </React.Fragment>
      ))}
    </Typography>
  )
}

export default withStyles(styles)(SummaryScreen)
