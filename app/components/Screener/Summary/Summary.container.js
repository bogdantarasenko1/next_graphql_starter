// @flow

import React from 'react'
import numericalFilters from '../NumericalFilter/filters'
import Summary from './Summary.screen'

type Props = {
  t: string => string,
  screener: Object
};

class SummaryContainer extends React.PureComponent<Props> {
  props: Props;

  format = () => {
    const {
      screener: { filters },
      t
    } = this.props
    const data =
      filters &&
      filters.map(f => {
        const title = t(`cryptoassets-table:${f.name}`)

        let operator = numericalFilters
          .find(c => c.value === f.operator)
          .title.toLowerCase()
        let text = `${operator} ${f.value}`

        if (f.type === 'range') {
          const {
            value: { from, to }
          } = f
          text = `${operator} ${from} ${t('common:and')} ${to}`
        }

        return { title, text }
      })

    return data
  };

  render () {
    return <Summary data={this.format()} {...this.props} />
  }
}

export default SummaryContainer
