// @flow

import React from 'react'

import gql from 'graphql-tag'
import { graphql } from 'react-apollo'
import UserScreers from './UserScreeners.screen'

type Props = {
  deleteScreener: Object => Promise,
  screeners: []
};
type State = {};

class UserScreenersContainer extends React.PureComponent<Props, State> {
  props: Props;
  State: State;

  constructor (props: Props) {
    super(props)

    this.state = {
      showDialog: false,
      screenerID: '',
      screenerName: '',
      currentScreeners: this.props.screeners
    }
  }

  componentDidUpdate (props, state) {
    const { currentScreeners } = state
    const { screeners } = this.props

    if (
      screeners &&
      JSON.stringify(currentScreeners) !== JSON.stringify(screeners)
    ) {
      this.setState({ currentScreeners: screeners })
    }
  }

  onCloseDialog = () => {
    this.setState({
      showDialog: false,
      screenerID: '',
      screenerName: ''
    })
  };

  onOpenDialog = (id, name) => {
    this.setState({
      showDialog: true,
      screenerID: id,
      screenerName: name
    })
  };

  onDeleteScreener = () => {
    const { deleteScreener } = this.props
    const { screenerID, currentScreeners } = this.state
    const newScreeners = currentScreeners.filter(e => {
      return e.id !== screenerID
    })

    deleteScreener({
      variables: {
        input: {
          id: screenerID
        }
      },
      refetchQueries: [
        {
          query: gql`
            query GetScreeners {
              viewer {
                currentUser {
                  email
                  screeners {
                    id
                    name
                    text
                    filters {
                      category
                      name
                      operator
                      value
                    }
                  }
                }
              }
            }
          `
        }
      ]
    }).then(() =>
      this.setState({ currentScreeners: newScreeners }, this.onCloseDialog)
    )
  };

  render () {
    const {
      showDialog,
      screenerName,
      screenerID,
      currentScreeners
    } = this.state

    return (
      <UserScreers
        {...this.props}
        screenerName={screenerName}
        currentScreeners={currentScreeners}
        screenerID={screenerID}
        showDialog={showDialog}
        onCloseDialog={this.onCloseDialog}
        onOpenDialog={this.onOpenDialog}
        onDeleteScreener={this.onDeleteScreener}
      />
    )
  }
}

const deleteScreenerQuery = gql`
  mutation deleteScreener($input: deleteScreenerInput!) {
    deleteScreener(input: $input) {
      screener {
        id
      }
    }
  }
`

export default graphql(deleteScreenerQuery, { name: 'deleteScreener' })(
  UserScreenersContainer
)
