// @flow

import React from 'react'
import {
  Button,
  DialogTitle,
  Dialog,
  DialogActions,
  DialogContent,
  Typography,
  withStyles
} from '@material-ui/core'

type Props = {
  classes: Object,
  open: boolean,
  onClose: () => void,
  onDeleteScreener: () => void,
  screenerName: string,
  t: string => string
};

const styles = theme => ({
  paper: {
    backgroundColor: theme.palette.grey[100]
  },
  title: {
    color: theme.palette.grey[700]
  }
})

const DeleteScreenerDialogScreen = (props: Props) => {
  const { classes, open, onClose, screenerName, onDeleteScreener, t } = props
  return (
    <div>
      <Dialog
        fullWidth
        maxWidth="sm"
        open={open}
        onClose={onClose}
        classes={{ paper: classes.paper }}
      >
        <DialogTitle disableTypography>
          <Typography variant="title" className={classes.title}>
            {t('screener:Delete Screener')}
          </Typography>
        </DialogTitle>
        <DialogContent>
          <Typography variant="title" className={classes.title}>
            {`${t(
              'screener:Are you sure you want to delete'
            )} ${screenerName}?`}
          </Typography>
        </DialogContent>
        <DialogActions>
          <Button
            onClick={onDeleteScreener}
            color="default"
            variant="contained"
          >
            {t('screener:Delete Screener')}
          </Button>
          <Button onClick={onClose} color="default" variant="contained">
            {t('common:Cancel')}
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  )
}

export default withStyles(styles)(DeleteScreenerDialogScreen)
