// @flow

import React from 'react'
import {
  Table,
  TableHead,
  TableBody,
  TableCell,
  TableRow,
  Typography,
  withStyles,
  Button,
  IconButton
} from '@material-ui/core'
import DeleteIcon from '@material-ui/icons/HighlightOff'
import { Link } from '../../../../lib/routes'
import Loader from '../../Loaders/Overview'
import DeleteDialog from './Dialog/DeleteScreenerDialog.screen'

type Props = {
  isLoading: boolean,
  screeners: [],
  classes: Object,
  showDialog: boolean,
  onCloseDialog: () => void,
  onOpenDialog: () => void,
  onDeleteScreener: () => void,
  currentScreeners: [],
  screenerName: string,
  t: string => string
};

const styles = theme => ({
  head: {
    opacity: 1
  },
  button: {
    marginTop: '32px'
  },
  link: {
    textDecoration: 'none',
    textTransform: 'none',
    color: theme.palette.grey[900]
  }
})

const UserScreenersScreen = (props: Props) => {
  const {
    currentScreeners,
    classes,
    showDialog,
    onCloseDialog,
    onOpenDialog,
    screenerName,
    onDeleteScreener,
    isLoading,
    t
  } = props

  if (isLoading) {
    return <Loader />
  }

  return (
    <div>
      <Table>
        <TableHead>
          <TableRow>
            <TableCell colSpan={2} className={classes.head}>
              <Typography variant="subheading" color="textPrimary">
                {t('screener:My screeners')}
              </Typography>
            </TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {currentScreeners &&
            currentScreeners.map((screener, index) => (
              <TableRow key={index}>
                <TableCell>
                  <Link route="screener" params={{ page: screener.id }}>
                    <a href={`/screener/${screener.id}`}>{screener.name}</a>
                  </Link>
                </TableCell>
                <TableCell>{screener.text || ''}</TableCell>

                <IconButton
                  onClick={() => onOpenDialog(screener.id, screener.name)}
                >
                  <DeleteIcon color="disabled" />
                </IconButton>
              </TableRow>
            ))}
        </TableBody>
      </Table>

      <Button className={classes.button} color="secondary" variant="contained">
        <Link route="screener" params={{ page: 'new' }}>
          <a className={classes.link} href="/screener/new">
            {t('screener:Add New Screener')}
          </a>
        </Link>
      </Button>
      {showDialog && (
        <DeleteDialog
          screenerName={screenerName}
          open={showDialog}
          onClose={onCloseDialog}
          onDeleteScreener={onDeleteScreener}
          t={t}
        />
      )}
    </div>
  )
}

export default withStyles(styles)(UserScreenersScreen)
