// @flow

import React from 'react'
import {
  Button,
  DialogTitle,
  Dialog,
  DialogActions,
  DialogContent,
  Typography,
  withStyles
} from '@material-ui/core'
import TextField from '@material-ui/core/TextField'

type Props = {
  changeName: Object => void,
  changeText: Object => void,
  close: () => void,
  save: () => void,
  isOpen: boolean,
  isValid: boolean,
  classes: Object,
  t: string => string
};

const styles = theme => ({
  paper: {
    backgroundColor: '#FAFAFA'
  },
  title: {
    color: theme.palette.grey[700]
  },
  input: {
    color: theme.palette.grey[700]
  },
  label: {
    color: theme.palette.grey[700],
    '&$labelShrink': {
      color: theme.palette.grey[700]
    }
  },
  underline: {
    '&:hover:not($disabled):not($focused):not($error)': {
      '&::before': {
        borderColor: theme.palette.grey[700]
      }
    },
    '&::before': {
      borderColor: 'transparent'
    },
    '&::after': {
      borderColor: 'transparent'
    }
  },
  disabled: {},
  contained: {
    '&$disabled': {
      backgroundColor: theme.palette.grey[300],
      color: theme.palette.grey[700]
    }
  },
  labelShrink: {}
})

const SaveFiltersDialogScreen = (props: Props) => {
  const {
    isOpen,
    isValid,
    changeName,
    changeText,
    close,
    save,
    classes,
    t
  } = props
  return (
    <div>
      <Dialog
        fullWidth
        maxWidth="sm"
        open={isOpen}
        onClose={close}
        classes={{ paper: classes.paper }}
      >
        <DialogTitle disableTypography>
          <Typography variant="title" className={classes.title}>
            {t('screener:Save Filters')}
          </Typography>
        </DialogTitle>
        <DialogContent>
          <TextField
            onChange={changeName}
            autoFocus
            margin="dense"
            id="name"
            label={t('screener:Filter Name')}
            type="text"
            fullWidth
            InputLabelProps={{
              classes: { root: classes.label, shrink: classes.labelShrink }
            }}
            InputProps={{
              classes: { root: classes.input, underline: classes.underline }
            }}
            className={classes.input}
          />
          <TextField
            onChange={changeText}
            margin="dense"
            id="text"
            label={t('common:Description')}
            type="text"
            fullWidth
            InputLabelProps={{
              classes: { root: classes.label, shrink: classes.labelShrink }
            }}
            InputProps={{
              classes: { root: classes.input, underline: classes.underline }
            }}
            className={classes.input}
          />
        </DialogContent>
        <DialogActions>
          <Button
            onClick={save}
            disabled={!isValid}
            color="default"
            variant="contained"
            classes={{
              disabled: classes.disabled,
              contained: classes.contained
            }}
          >
            {t('common:Save')}
          </Button>
          <Button onClick={close} color="default" variant="contained">
            {t('common:Cancel')}
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  )
}

export default withStyles(styles)(SaveFiltersDialogScreen)
