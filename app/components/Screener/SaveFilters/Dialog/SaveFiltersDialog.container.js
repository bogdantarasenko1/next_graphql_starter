// @flow

import React from 'react'
import Dialog from './SaveFiltersDialog.screen'

type Props = {
  onClose: () => void,
  onSave: string => void,
  open: boolean,
  t: string => string
};

type State = {
  open: boolean,
  isValid: boolean,
  name: string,
  text: string
};

class SaveFiltersDialogContainer extends React.PureComponent<Props, State> {
  props: Props;
  state: State;

  constructor (props: Props) {
    super(props)
    this.state = {
      open: this.props.open,
      isValid: false,
      name: '',
      text: ''
    }
  }

  onSave = () => {
    const { name, text } = this.state
    this.props.onSave({ name, text })
  };

  onClose = () => {
    this.setState({ open: false })
    this.props.onClose()
  };

  onChangeName = event => {
    const name = event.currentTarget.value.trim()
    this.setState({
      name,
      isValid: !!name.length
    })
  };

  onChangeText = event => {
    const text = event.currentTarget.value.trim()
    this.setState({ text })
  };

  render () {
    const { isValid, open } = this.state

    return (
      <Dialog
        save={this.onSave}
        close={this.onClose}
        isOpen={open}
        changeName={this.onChangeName}
        changeText={this.onChangeText}
        isValid={isValid}
        t={this.props.t}
      />
    )
  }
}

export default SaveFiltersDialogContainer
