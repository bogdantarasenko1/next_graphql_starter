// @flow

import React from 'react'
import { Button, withStyles } from '@material-ui/core'
import classNames from 'classnames'

type Props = {
  saveFilters: () => void,
  btnText: string,
  margin?: boolean,
  classes: Object,
  disabled: boolean
};

const styles = () => ({
  margin: {
    margin: '0 10px'
  }
})

const SaveFilterScreen = (props: Props) => {
  const { saveFilters, btnText, margin, classes, disabled } = props
  return (
    <Button
      className={classNames({ [classes.margin]: !!margin })}
      variant="contained"
      onClick={saveFilters}
      disabled={disabled}
    >
      {btnText}
    </Button>
  )
}

export default withStyles(styles)(SaveFilterScreen)
