// @flow

import React from 'react'
import SaveFilters from './SaveFilters.screen'
import SaveDialog from './Dialog/SaveFiltersDialog.container'

type Props = {
  screener?: Object,
  enterName: Object => void,
  save: () => void,
  isDisabled: boolean,
  t: string => string,
  userFilters: []
};

type State = {
  showDialog: boolean
};

class SaveFilterContainer extends React.PureComponent<Props, State> {
  props: Props;
  state: State;

  constructor (props: Props) {
    super(props)

    this.state = {
      showDialog: false
    }
  }

  onSaveFilters = () => {
    this.setState(prev => {
      return { showDialog: !prev.showDialog }
    })
  };

  onCloseDialog = () => {
    this.setState({ showDialog: false })
  };

  createScreener = data => {
    const { enterName } = this.props
    enterName(data)
    this.onCloseDialog()
  };

  saveScreener = () => {
    const { save } = this.props
    save()
    this.onCloseDialog()
  };

  isFiltersEmpty = () => {
    return this.props.isDisabled
  };

  buttons = () => {
    const { t, screener } = this.props

    if (screener) {
      return (
        <React.Fragment>
          <SaveFilters
            saveFilters={this.saveScreener}
            btnText={t('screener:Save')}
            margin
            disabled={this.isFiltersEmpty()}
          />
          <SaveFilters
            saveFilters={this.onSaveFilters}
            btnText={t('screener:Save As')}
            margin
            disabled={this.isFiltersEmpty()}
          />
        </React.Fragment>
      )
    } else {
      return (
        <SaveFilters
          saveFilters={this.onSaveFilters}
          btnText={t('screener:Save Screener')}
          margin
          disabled={this.isFiltersEmpty()}
        />
      )
    }
  };

  render () {
    const { showDialog } = this.state
    const { t } = this.props

    return (
      <React.Fragment>
        {this.buttons()}
        {showDialog && (
          <SaveDialog
            open={showDialog}
            onSave={this.createScreener}
            onClose={this.onCloseDialog}
            t={t}
          />
        )}
      </React.Fragment>
    )
  }
}

export default SaveFilterContainer
