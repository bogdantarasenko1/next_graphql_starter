import gql from 'graphql-tag'

const typeDefs = `
  type ScreenerFilter {
    id: String!
    name: String!
    type: String!
    operator: String!
    value: String!
  }

  type Screener {
    id: String!
    name: String!
    text: String?
    filters: [ScreenerFilter]!
  }
  
  type Query {
    screener(id: String): Screener
  }
  
  type Mutation {
    addScreener(name: String!, text: String?, filters: [ScreenerFilter])
    updateScreener(id: String!, text: String?, name: String!, filters: [ScreenerFilter])
  }
`
let filterNexId = 1

const generateNextId = () => {
  return filterNexId++
}

const defaults = {
  screeners: [
    {
      id: `${generateNextId()}`,
      name: 'Screener 1',
      text: 'Default screener',
      filters: [],
      __typename: 'Screener'
    },
    {
      id: `${generateNextId()}`,
      name: 'Screener 2',
      text: 'Default screener',
      filters: [
        {
          id: `${generateNextId()}`,
          name: 'totalMarketCap',
          type: 'value',
          operator: 'gt',
          value: '100',
          __typename: 'ScreenerFilter!'
        },
        {
          id: `${generateNextId()}`,
          name: 'circulatingMarketCap',
          type: 'value',
          operator: 'gt',
          value: '80',
          __typename: 'ScreenerFilter!'
        }
      ],
      __typename: 'Screener'
    }
  ]
}

export const getScreeners = gql`
  query GetScreeners {
    screeners @client {
      id
      name
      text
      filters
    }
  }
`

const resolvers = {
  Query: {
    screener: (_, { id }, { cache }) => {
      const query = getScreeners

      const prevState = cache.readQuery({ query })
      const { screeners } = prevState
      const screener = screeners.find(s => s.id === id)

      return screener
    }
  },
  Mutation: {
    updateScreener: (
      _,
      { id: screenerId, name, text, filters },
      { cache, getCacheKey }
    ) => {
      const id = getCacheKey({ __typename: 'Screener', id: screenerId })

      const fragment = gql`
        fragment updateScreener on Screener @client {
          id
          name
          text
          filters
          __typename
        }
      `

      const screener = cache.readFragment({ fragment, id })
      if (screener) {
        const newFilters = filters.map(filter => {
          const f = Object.assign({}, filter)

          f.id = f.id || `${generateNextId()}`
          f.__typename = f.__typename || 'ScreenerFilter!'

          return f
        })

        const data = {
          ...screener,
          name: name,
          text: text,
          filters: newFilters
        }

        cache.writeFragment({
          id,
          data,
          fragment: gql`
            fragment screener on Screener {
              id
              name
              text
              filters
            }
          `
        })
        return data
      } else {
        return null
      }
    },

    addScreener: (_, { name, text, filters }, { cache }) => {
      const query = getScreeners

      const prevState = cache.readQuery({ query })
      const nextId = generateNextId()

      const newScreener = {
        id: `${nextId}`,
        text: text,
        name: name,
        filters: filters.map(filter => {
          const f = Object.assign({}, filter)

          f.id = `${generateNextId()}`
          f.__typename = 'ScreenerFilter!'

          return f
        }),
        __typename: 'Screener'
      }

      const data = {
        screeners: prevState.screeners.concat([newScreener])
      }

      cache.writeData({ data })

      return newScreener
    }
  }
}
export { defaults, typeDefs, resolvers }
