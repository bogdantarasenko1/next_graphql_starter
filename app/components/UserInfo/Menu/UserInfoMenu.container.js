// @flow

import React from 'react'
import ProfileOnScreen from '../../ProfileDialog/ProfileOnScreen.container'
import { logout } from '../../../utils/auth'
import { Router } from '../../../../lib/routes'

type Props = {
  t: string => string,
  user: Object,
  children: React.ReactNode
};

type State = {
  editProfile: boolean
};

class UserInfoMenuContainer extends React.PureComponent<Props, State> {
  props: Props;
  state: State;

  constructor (props: Props) {
    super(props)

    this.state = {
      editProfile: false
    }
  }

  logoutUser = () => {
    logout()
    Router.pushRoute('/login')
  };

  onEditProfile = e => {
    this.setState({
      editProfile: true
    })
  };

  onCloseProfile = () => {
    this.setState({
      editProfile: false
    })
  };

  render () {
    const { t, user, children } = this.props
    const { editProfile } = this.state
    return (
      <React.Fragment>
        {children({
          logoutUser: this.logoutUser,
          onEditProfile: this.onEditProfile
        })}
        {editProfile && (
          <ProfileOnScreen
            onCloseProfile={this.onCloseProfile}
            editProfile={editProfile}
            userId={user.id}
            t={t}
          />
        )}
      </React.Fragment>
    )
  }
}

export default UserInfoMenuContainer
