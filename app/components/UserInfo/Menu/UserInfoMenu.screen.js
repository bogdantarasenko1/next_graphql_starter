// @flow

import React from 'react'
import { MenuItem, Avatar, withStyles } from '@material-ui/core'
// import ThemeSwitch from '../../ThemeSwitch/ThemeSwitch.container'

type Props = {
  t: string => string,
  user: Object,
  logout: () => void,
  userProfile: () => void,
  classes: Object
};

const styles = theme => ({
  avatar: {
    height: '40px',
    width: '40px',
    marginRight: '5px'
  }
})

const UserInfoMenuScreen = (props: Props) => {
  const { user, classes, t, logout, userProfile } = props

  return (
    <React.Fragment>
      <MenuItem onClick={userProfile}>
        <Avatar className={classes.avatar} src="/static/Shape.png" />
        {user.firstName}
      </MenuItem>
      {/*
          <MenuItem>
            <ThemeSwitch t={t} />
          </MenuItem>
        */}
      <MenuItem onClick={logout}>{t('common:Logout')}</MenuItem>
    </React.Fragment>
  )
}

export default withStyles(styles)(UserInfoMenuScreen)
