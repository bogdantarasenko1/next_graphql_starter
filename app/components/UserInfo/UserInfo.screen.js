// @flow

import React from 'react'
import {
  Avatar,
  Button,
  Popper,
  ClickAwayListener,
  Paper,
  Grow,
  MenuList,
  MenuItem,
  withStyles
} from '@material-ui/core'
import ThemeSwitch from '../ThemeSwitch/ThemeSwitch.container'

const styles = theme => ({
  root: {
    display: 'inline-flex',
    alignItems: 'center'
  },
  avatar: {
    height: '40px',
    width: '40px'
  },
  link: {
    marginLeft: '10px',
    color: theme.palette.text.hint,
    '&:hover': {
      color: theme.palette.text.primary
    }
  }
})

type Props = {
  classes: Object,
  t: string => string,
  isOpened: boolean,
  user: Object,
  toggleMenu: () => void,
  closeMenu: event => void,
  logout: () => void,
  userProfile: () => void
};

class UserInfoScreen extends React.PureComponent<Props> {
  anchorEl;

  closeMenu = event => {
    const { closeMenu: _closeMenu } = this.props
    if (this.anchorEl.contains(event.target)) {
      return
    }
    _closeMenu()
  };

  render () {
    const {
      t,
      classes,
      toggleMenu,
      isOpened,
      closeMenu,
      logout,
      userProfile
    } = this.props

    return (
      <div className={classes.root}>
        <Button
          buttonRef={node => {
            this.anchorEl = node
          }}
          onClick={toggleMenu}
        >
          <Avatar className={classes.avatar} src="/static/Shape.png" />
        </Button>
        <Popper
          placement="bottom-end"
          open={isOpened}
          anchorEl={this.anchorEl}
          transition
          disablePortal
        >
          {({ TransitionProps }) => (
            <Grow
              {...TransitionProps}
              id="menu-list-grow"
              style={{
                transformOrigin: 'right top'
              }}
            >
              <Paper>
                <ClickAwayListener onClickAway={closeMenu}>
                  <MenuList>
                    <MenuItem onClick={userProfile}>
                      {t('common:Profile')}
                    </MenuItem>
                    <MenuItem>
                      <ThemeSwitch t={t} />
                    </MenuItem>
                    <MenuItem onClick={logout}>{t('common:Logout')}</MenuItem>
                  </MenuList>
                </ClickAwayListener>
              </Paper>
            </Grow>
          )}
        </Popper>
      </div>
    )
  }
}

export default withStyles(styles)(UserInfoScreen)
