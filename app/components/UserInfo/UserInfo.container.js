// @flow

import React from 'react'
import gql from 'graphql-tag'
import { graphql, compose } from 'react-apollo'
import { logout } from '../../utils/auth'
import ProfileOnScreen from '../ProfileDialog/ProfileOnScreen.container'
import UserInfo from './UserInfo.screen'

type Props = {
  user: Object,
  t: string => string
};

type State = {
  isOpened: boolean
};

class UserInfoContainer extends React.Component<Props, State> {
  props: Props;
  state: State;

  constructor (props: Props) {
    super(props)

    this.state = {
      isOpened: false,
      editProfile: false
    }
  }

  componentDidMount () {
    this.getUser()
  }

  getUser = () => {
    const { getUserQuery, saveAuth } = this.props

    getUserQuery
      .refetch()
      .then(response => {
        const {
          data: {
            viewer: { currentUser }
          }
        } = response
        saveAuth(currentUser)
      })
      .catch((e, w, r) => {
        console.error(e)
      })
  };

  toggleMenu = () => {
    this.setState(state => ({ isOpened: !state.isOpened }))
  };

  closeMenu = () => {
    this.setState({ isOpened: false })
  };

  logoutUser = () => {
    logout()
    const redirectUrl = `${window.location.origin}/logout`
    window.location.replace(redirectUrl)
  };

  onEditProfile = e => {
    this.closeMenu()
    this.setState({
      editProfile: true
    })
  };

  onCloseProfile = () => {
    this.setState({
      editProfile: false
    })
  };

  render () {
    const { editProfile } = this.state
    const { t, user } = this.props

    return (
      <React.Fragment>
        <UserInfo
          logout={this.logoutUser}
          userProfile={this.onEditProfile}
          isOpened={this.state.isOpened}
          toggleMenu={this.toggleMenu}
          closeMenu={this.closeMenu}
          {...this.props}
        />
        {editProfile && (
          <ProfileOnScreen
            onCloseProfile={this.onCloseProfile}
            editProfile={editProfile}
            userId={user.id}
            t={t}
          />
        )}
      </React.Fragment>
    )
  }
}

const getUserQuery = gql`
  query UserQuery {
    viewer {
      currentUser {
        id
        email
        displayName
        firstName
        lastName
        bio
        avatarUrl
      }
    }
  }
`

export default compose(graphql(getUserQuery, { name: 'getUserQuery' }))(
  UserInfoContainer
)
