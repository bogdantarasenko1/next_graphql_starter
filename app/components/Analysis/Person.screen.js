// @flow

import React from 'react'
import { Grid, Typography } from '@material-ui/core'

type Props = {
  slug: string,
  name: string,
  photo: string,
  roles?: []
};

const PersonsScreen = (props: Props) => {
  const { name, photo } = props
  return (
    <Grid item xs={3}>
      <img src={photo} alt={name} />
      <Typography variant="body1" component="div">
        {name}
      </Typography>
    </Grid>
  )
}

export default PersonsScreen
