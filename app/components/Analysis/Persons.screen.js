// @flow

import React from 'react'
import { Grid } from '@material-ui/core'
import Person from './Person.screen'

type Props = {
  persons: []
};

const PersonsScreen = (props: Props) => {
  const { persons } = props
  return (
    <Grid container>
      {persons.map((person, index) => (
        <Person key={index} {...person} />
      ))}
    </Grid>
  )
}

export default PersonsScreen
