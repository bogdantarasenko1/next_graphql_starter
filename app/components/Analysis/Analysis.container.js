// @flow

import React from 'react'
import { analysis as analysisQuery } from '../../../app/utils/devQuery'
import Analysis from './Analysis.screen'

type Props = {
  t: func,
  code: string,
  text: string,
  page: string
};

type State = {
  isLoading: boolean,
  data?: {
    category: string,
    industry: string,
    technology: string,
    competition: string,
    risks: string,
    organization: string,
    description: string,
    team: [],
    investors: [],
    advisors: [],
    urls: []
  }
};

class AnalysisContainer extends React.PureComponent<Props, State> {
  props: Props;
  state: State;

  constructor (props: Props) {
    super(props)

    this.state = { isLoading: true }
  }

  componentDidMount () {
    const { code: asset } = this.props
    this.getAnalysis(asset.toUpperCase())
  }

  async getAnalysis (asset) {
    const data = await analysisQuery(asset)

    let analysis = data.analysis || {}
    let metadata = data.metadata || {}

    const { industry, technology, competition, risks } = analysis
    const { description, category, name, tags, organizations } = metadata

    this.setState({
      isLoading: false,
      data: {
        category,
        industry,
        technology,
        competition,
        risks,
        description,
        name,
        tags,
        organizations
      }
    })
  }

  render () {
    const { isLoading, data } = this.state
    return <Analysis {...this.props} isLoading={isLoading} data={data} />
  }
}

export default AnalysisContainer
