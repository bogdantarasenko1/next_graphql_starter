// @flow

import React from 'react'
import BlockWithHeader from '../BlockWithHeader/BlockWithHeader.screen'
import AnalysisSections from './AnalysisSections.screen'

type Props = {
  isLoading: boolean,
  data?: Object,
  t: string => string
};

const AnalysisScreen = (props: Props) => {
  const { t } = props
  return (
    <div>
      <BlockWithHeader
        title={t('common:Analysis')}
        content={<AnalysisSections {...props} />}
      />
    </div>
  )
}

export default AnalysisScreen
