// @flow

import React from 'react'
import { Grid, Typography, withStyles } from '@material-ui/core'
import ScrollableAnchor from 'react-scrollable-anchor'
import Loader from '../Loaders/Overview'
import AnalysisBlock from './AnalysisBlock.screen'
import Persons from './Persons.screen'

type Props = {
  isLoading: boolean,
  data?: {
    industry: string,
    technology: string,
    competition: string,
    risks: string,
    organizations: [],
    description: string,
    name: string,
    tags: []
  },
  classes: Object,
  t: string => string
};

const styles = () => ({
  loader: {
    width: '100%'
  }
})

const AnalysisScreen = (props: Props) => {
  const { isLoading, data, classes, t } = props

  const employees = data => {
    return data.map(e => e.person)
  }

  const content = () => {
    if (isLoading) {
      return (
        <div className={classes.loader}>
          <Loader />
        </div>
      )
    } else {
      const { organizations } = data
      const organization =
        organizations && organizations.length ? organizations[0] : {}

      return (
        <React.Fragment>
          <ScrollableAnchor id="analysis-overview">
            <AnalysisBlock title="Overview">
              <Typography variant="body1" paragraph>
                {data.description || 'N/A'}
              </Typography>
              <Typography>
                <strong>
                  {t('common:Category')}
                  :&nbsp;
                </strong>
                {data.category || 'N/A'}
              </Typography>
            </AnalysisBlock>
          </ScrollableAnchor>
          <ScrollableAnchor id="industry">
            <AnalysisBlock title="Industry">
              {data.industry || 'N/A'}
            </AnalysisBlock>
          </ScrollableAnchor>
          <ScrollableAnchor id="technology">
            <AnalysisBlock title="Technology">
              {data.technology || 'N/A'}
            </AnalysisBlock>
          </ScrollableAnchor>
          <ScrollableAnchor id="competition">
            <AnalysisBlock title="Competition">
              {data.competition || 'N/A'}
            </AnalysisBlock>
          </ScrollableAnchor>
          <ScrollableAnchor id="risks">
            <AnalysisBlock title="Risks">{data.risks || 'N/A'}</AnalysisBlock>
          </ScrollableAnchor>
          <ScrollableAnchor id="team">
            <AnalysisBlock title="Team">
              {organization.employees && organization.employees.length ? (
                <Persons persons={employees(organization.employees)} />
              ) : (
                'N/A'
              )}
            </AnalysisBlock>
          </ScrollableAnchor>
          <ScrollableAnchor id="advisors">
            <AnalysisBlock title="Advisors">
              {organization.advisors && organization.advisors.length ? (
                <Persons persons={organization.advisors} />
              ) : (
                'N/A'
              )}
            </AnalysisBlock>
          </ScrollableAnchor>
          <ScrollableAnchor id="investors">
            <AnalysisBlock title="Investors">
              {organization.investors && organization.investors.length ? (
                <Persons persons={organization.investors} />
              ) : (
                'N/A'
              )}
            </AnalysisBlock>
          </ScrollableAnchor>
        </React.Fragment>
      )
    }
  }

  return (
    <Grid container spacing={0}>
      {content()}
    </Grid>
  )
}

export default withStyles(styles)(AnalysisScreen)
