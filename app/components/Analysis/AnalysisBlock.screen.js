// @flow

import React from 'react'
import { withStyles, Grid, Typography } from '@material-ui/core'

type Props = {
  title: string,
  children: React.ReactNode,
  classes: Object
};

const styles = theme => ({
  rootTitle: {},
  title: {
    marginBottom: theme.spacing.unit
  },
  content: {
    minHeight: '100px',
    marginBottom: 20,
    [theme.breakpoints.down('md')]: {}
  }
})

const AnalysisBlockScreen = (props: Props) => {
  const { title, children, classes } = props
  return (
    <Grid container>
      <Grid item xs={12}>
        <Typography variant="title" className={classes.title}>
          {title}
        </Typography>
        <Typography variant="body1" className={classes.content} component="div">
          {children}
        </Typography>
      </Grid>
    </Grid>
  )
}

export default withStyles(styles)(AnalysisBlockScreen)
