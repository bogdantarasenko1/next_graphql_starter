// @flow

import React from 'react'
import ReactSVG from 'react-svg'
import styles from '../ChartOrientation/styles.css'

class ChartOrientation extends React.Component<State, Props> {
  render () {
    const { title, orientation, handleOrientation } = this.props
    const isVertical = orientation === 'vertical'
    const verticalIconClass = isVertical ? 'activeChartIcon' : 'chartIcon'
    const horizontalIconClass = !isVertical ? 'activeChartIcon' : 'chartIcon'
    return (
      <div className={styles.chartsOrientationContainer}>
        {title}
        <div className={styles.chartsOrientationButtons}>
          <div
            onClick={() => handleOrientation('vertical')}
            className={styles.chartsOrientationButton}
          >
            <ReactSVG
              className={styles[verticalIconClass]}
              src="/static/vertical.svg"
            />
          </div>
          <div
            onClick={() => handleOrientation('horizontal')}
            className={styles.chartsOrientationButton}
          >
            <ReactSVG
              className={styles[horizontalIconClass]}
              src="/static/horizontal.svg"
            />
          </div>
        </div>
      </div>
    )
  }
}

export default ChartOrientation
