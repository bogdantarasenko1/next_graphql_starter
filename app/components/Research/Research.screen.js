// @flow

import React from 'react'
import Link from 'next/link'
import { Typography } from '@material-ui/core'
import { withStyles } from '@material-ui/core/styles'
import Loader from '../Loaders/Overview'
import DashboardHeader from '../Dashboard/DashboardHeader.screen.js'
import AssetsTabs from '../AssetsTabs/AssetsTabs.container'
import RecentTabContent from './Recent/Recent.container'
import PopularTabContent from './Popular/Popular.container'

type Props = {
  showHeader?: boolean,
  showMore?: boolean,
  onLoadData: () => void,
  isModuleLoading: boolean,
  t: string => string,
  classes: object
};

const styles = theme => ({
  link: {
    color: theme.palette.secondary.main,
    textDecoration: 'none',
    fontWeight: 'bold'
  },
  linkContainer: {
    padding: '0 1rem 1rem',
    textAlign: 'right'
  }
})

const ResearchScreen = (props: Props) => {
  const {
    showHeader,
    showMore,
    onLoadData,
    isModuleLoading,
    classes,
    ...other
  } = props
  const { t } = props
  const tabItems = [
    {
      key: 'recent',
      title: 'Recent',
      content: (
        <RecentTabContent
          onLoadData={onLoadData}
          isModuleLoading={isModuleLoading}
          {...props}
        />
      )
    },
    {
      key: 'popular',
      title: 'Popular',
      content: <PopularTabContent {...props} />
    }
  ]

  return (
    <React.Fragment>
      {showHeader && <DashboardHeader title={'Research'} />}
      <AssetsTabs tabItems={tabItems} selectedTab={0} {...other} />
      {isModuleLoading ? (
        <Loader height={100} rHeight={30} />
      ) : (
        <React.Fragment>
          {showMore && (
            <div className={classes.linkContainer}>
              <Typography variant="body1">
                <Link prefetch href="/research">
                  <a className={classes.link} href="/research">
                    {t('showMore')}
                  </a>
                </Link>
              </Typography>
            </div>
          )}
        </React.Fragment>
      )}
    </React.Fragment>
  )
}

export default withStyles(styles)(ResearchScreen)
