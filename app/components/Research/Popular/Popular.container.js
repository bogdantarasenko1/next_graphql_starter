// @flow

import React from 'react'
import popularArticles from '../popularArticles.json'
import ArticlesCards from '../ArticlesCards.screen'

type State = {
  articles: [],
  isLoading: boolean
};

type Props = {};

class PopularContainer extends React.Component<Props, State> {
  constructor (props) {
    super(props)

    this.state = {
      articles: [],
      isLoading: true
    }
  }

  componentDidMount () {
    const sortedArticles = popularArticles
      .sort((a, b) => {
        return a.rank - b.rank
      })
      .slice(0, 4)
    setTimeout(() => {
      this.setState({ articles: sortedArticles, isLoading: false })
    }, 1000)
  }

  render () {
    const { articles, isLoading } = this.state

    return <ArticlesCards articles={articles} isLoading={isLoading} />
  }
}

export default PopularContainer
