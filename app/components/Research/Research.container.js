// @flow

import React from 'react'
import Research from './Research.screen'

type Props = {
  t: func
};

type State = {
  isLoading: boolean
};

class ResearchContainer extends React.Component<Props, State> {
  props: Props;
  state: State;

  constructor (props: Props) {
    super(props)

    this.state = {
      isLoading: true
    }
  }

  onLoadData = () => {
    this.setState({ isLoading: false })
  };

  render () {
    const { isLoading } = this.state
    return (
      <Research
        {...this.props}
        onLoadData={this.onLoadData}
        isModuleLoading={isLoading}
      />
    )
  }
}

export default ResearchContainer
