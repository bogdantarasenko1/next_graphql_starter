// @flow

import React from 'react'
import { Typography, Card, CardMedia } from '@material-ui/core'
import { withStyles } from '@material-ui/core/styles'
import TextTruncate from 'react-text-truncate'
import { Link } from '../../../lib/routes'

type Props = {
  path: string,
  title: string,
  date: string,
  author: string,
  description: string,
  urlToImage: string,
  onUserClick: event => void,
  classes: object
};

const styles = theme => ({
  card: {
    backgroundColor: 'transparent',
    borderRadius: '0',
    boxShadow: '0px 1px rgba(244, 244, 244, 0.2)',
    display: 'flex',
    margin: '.75rem 1rem',
    [theme.breakpoints.down('sm')]: {
      margin: '.75rem 0'
    }
  },
  media: {
    height: '0px',
    padding: '40px',
    margin: '1rem 0 0 0'
  },
  details: {
    display: 'flex',
    flexDirection: 'column',
    padding: '0 .75rem .75rem 0'
  },
  link: {
    color: theme.palette.text.primary,
    textDecoration: 'none',
    lineHeight: '1.3',
    '&:hover': {
      color: theme.palette.secondary.main
    }
  },
  body: {
    lineHeight: '1.3'
  }
})

const USER_ID = '1'

const ArticleCard = (props: Props) => {
  const {
    classes,
    path,
    title,
    description,
    date,
    author,
    urlToImage,
    onUserClick
  } = props
  return (
    <Card elevation={0} className={classes.card}>
      <div className={classes.details}>
        <Typography gutterBottom variant="subheading" component="h2">
          <Link route="researchArticle" params={{ path }} prefetch>
            <a className={classes.link} href={`/research/${path}`}>
              <TextTruncate line={2} truncateText="…" text={title} />
            </a>
          </Link>
        </Typography>
        <Typography gutterBottom variant="caption" component="p">
          {`${date} · by `}
          <a href="/#" onClick={e => onUserClick(e, USER_ID)}>
            {author}
          </a>
        </Typography>
        <Typography variant="body1" className={classes.body} component="div">
          <TextTruncate line={3} truncateText="…" text={description} />
        </Typography>
      </div>
      <CardMedia className={classes.media} image={urlToImage} />
    </Card>
  )
}

export default withStyles(styles)(ArticleCard)
