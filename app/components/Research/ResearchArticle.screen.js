// @flow

import React from 'react'
import Link from 'next/link'
import {
  Card,
  CardMedia,
  Typography,
  withStyles,
  Grid
} from '@material-ui/core'
import Loader from '../Loaders/Overview'

type Props = {
  article: Object,
  isLoading: boolean,
  classes: Object
};

const styles = theme => ({
  container: {
    paddingTop: '2rem',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  },
  card: {
    backgroundColor: 'transparent',
    padding: '0 1rem'
  },
  media: {
    height: '0px',
    padding: '20%',
    margin: '1rem 0 0 0'
  },
  details: {
    padding: '1rem 0 1.5rem'
  },
  content: {
    padding: '1.5rem 0'
  }
})

const ResearchArticleScreen = (props: Props) => {
  const {
    classes,
    isLoading,
    article: { title, author, urlToImage, date, content } = {}
  } = props

  return (
    <div className={classes.container}>
      <Grid container justify="center">
        <Grid item xs={12} md={8}>
          {isLoading ? (
            <Loader />
          ) : (
            <Card elevation={0} className={classes.card}>
              <Typography variant="body1">{title}</Typography>
              <Typography
                className={classes.details}
                gutterBottom
                variant="caption"
                component="p"
              >
                {`${date} · by `}
                <Link href={'/#'}>
                  <a href="/#">{author}</a>
                </Link>
              </Typography>
              <CardMedia className={classes.media} image={urlToImage} />
              <Typography
                className={classes.content}
                variant="body1"
                component="div"
                dangerouslySetInnerHTML={{ __html: content }}
              />
            </Card>
          )}
        </Grid>
      </Grid>
    </div>
  )
}

export default withStyles(styles)(ResearchArticleScreen)
