// @flow

import React from 'react'
import recentArticles from '../recentArticles.json'
import ArticlesCards from '../ArticlesCards.screen'

type State = {
  articles: [],
  isLoading: boolean
};

type Props = {
  isModuleLoading: boolean,
  onLoadData: () => void
};

class RecentContainer extends React.Component<Props, State> {
  props: Props;
  state: State;

  constructor (props) {
    super(props)
    this.state = {
      articles: [],
      isLoading: !props.isModuleLoading
    }
  }

  componentDidMount () {
    const sortedArticles = recentArticles
      .sort((a, b) => {
        a = new Date(a.publishedAt)
        b = new Date(b.publishedAt)
        return a > b ? -1 : a < b ? 1 : 0
      })
      .slice(0, 4)
    setTimeout(() => {
      this.setState({ articles: sortedArticles, isLoading: false })
      this.props.onLoadData()
    }, 1000)
  }

  render () {
    const { articles, isLoading } = this.state
    return <ArticlesCards articles={articles} isLoading={isLoading} />
  }
}

export default RecentContainer
