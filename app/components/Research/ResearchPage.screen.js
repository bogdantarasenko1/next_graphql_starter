// @flow

import React from 'react'
import { Grid, withStyles } from '@material-ui/core'
import Layout from '../../components/MainLayout/MainLayout.screen'
import Research from './Research.container'

type Props = {
  classes: Object
};

const styles = theme => ({})

const ResearchPageScreen = (props: Props) => {
  const { classes, ...other } = props
  const content = () => {
    return (
      <div className={classes.container}>
        <Grid container justify="center">
          <Grid item xs={12} md={8}>
            <Research {...other} />
          </Grid>
        </Grid>
      </div>
    )
  }

  return <Layout {...other} content={content()} />
}

export default withStyles(styles)(ResearchPageScreen)
