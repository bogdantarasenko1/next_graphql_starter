// @flow

import React from 'react'
import moment from 'moment'
import { CircularProgress, withStyles } from '@material-ui/core'
import ArticleCard from './ArticleCard'

type Props = {
  articles: [],
  isLoading: boolean,
  classes: Object
};

const styles = {
  center: {
    textAlign: 'center'
  }
}

const ArticlesCardsScreen = (props: Props) => {
  const { articles, isLoading, classes } = props
  if (isLoading) {
    return (
      <div className={classes.center}>
        <CircularProgress color="secondary" />
      </div>
    )
  }

  return articles.map((item, index) => (
    <ArticleCard
      key={index}
      path={item.path}
      title={item.title}
      author={item.author}
      date={moment(item.publishedAt).format('MMM DD YYYY')}
      urlToImage={item.urlToImage}
      description={item.description}
    />
  ))
}

export default withStyles(styles)(ArticlesCardsScreen)
