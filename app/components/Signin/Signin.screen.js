// @flow

import React from 'react'
import Link from 'next/link'
import { FormControl, Button, Typography } from '@material-ui/core'
import AuthCard from '../AuthCard/AuthCard.screen'
import AuthTextField from '../ui/AuthTextField'
import ForgotPasswordEmail from './ForgotPasswordEmail/ForgotPasswordEmail.container'

type Props = {
  email: string,
  password: string,
  processingSignIn: boolean,
  disableSignin: boolean,
  signinError: ?Object,
  onEmailChange: () => void,
  onPasswordChange: () => void,
  onSubmit: () => void,
  t: func
};

const Signin = (props: Props) => {
  const {
    onSubmit,
    email,
    password,
    processingSignIn,
    disableSignin,
    signinError,
    onEmailChange,
    onPasswordChange,
    t
  } = props

  const errorAttr = () => {
    return signinError ? { error: !!signinError } : {}
  }

  const submitBtnText = processingSignIn ? 'Signing in' : 'Sign in'
  const title = (
    <React.Fragment>
      Welcome to Mosaic
      <br />
      Please Sign in to continue
    </React.Fragment>
  )

  const inputs = (
    <React.Fragment>
      <FormControl fullWidth>
        <AuthTextField
          {...errorAttr()}
          label="Email"
          name="email"
          id="email"
          value={email}
          type="email"
          autoComplete="email"
          onChange={onEmailChange}
        />
      </FormControl>

      <FormControl fullWidth error>
        <AuthTextField
          {...errorAttr()}
          label="Password"
          type="password"
          name="password"
          value={password}
          autoComplete="current-password"
          onChange={onPasswordChange}
          id="password"
        />
        {signinError && (
          <Typography variant="subheading" color="error" component="div">
            {signinError}
          </Typography>
        )}
      </FormControl>

      <ForgotPasswordEmail t={t} />
    </React.Fragment>
  )

  const actions = (
    <React.Fragment>
      <Typography variant="subheading" color="textSecondary" component="span">
        <Link prefetch href="/signup">
          <a href="/signup">Sign Up</a>
        </Link>{' '}
        for an account
      </Typography>
      <Button
        disabled={disableSignin}
        type="submit"
        variant="contained"
        color="secondary"
      >
        {submitBtnText}
      </Button>
    </React.Fragment>
  )

  return (
    <AuthCard
      inputs={inputs}
      title={title}
      actions={actions}
      onSubmit={onSubmit}
    />
  )
}

export default Signin
