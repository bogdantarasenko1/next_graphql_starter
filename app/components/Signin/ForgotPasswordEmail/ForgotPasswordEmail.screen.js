// @flow

import React from 'react'
import Link from 'next/link'
import Typography from '@material-ui/core/Typography'

type Props = {
  t: string => string
};

const ForgotPasswordEmailScreen = (props: Props) => {
  const { t } = props

  return (
    <Typography
      variant="subheading"
      color="textSecondary"
      component="span"
      align="right"
    >
      <Link href="/restore">
        <a href="/restore">{t('login:Forgot Password?')}</a>
      </Link>
    </Typography>
  )
}

export default ForgotPasswordEmailScreen
