// @flow

import React from 'react'
import ForgotPasswordEmailScreen from './ForgotPasswordEmail.screen'

type Props = {
  t: func
};

class ForgotPasswordEmail extends React.PureComponent {
  props: Props;

  state = {
    anchorEl: null
  };

  handleClick = event => {
    this.setState({
      anchorEl: event.currentTarget
    })
  };

  handleClose = () => {
    this.setState({
      anchorEl: null
    })
  };

  render () {
    const { anchorEl } = this.state
    const open = Boolean(anchorEl)
    const { t } = this.props

    return (
      <ForgotPasswordEmailScreen
        open={open}
        handleClick={this.handleClick}
        handleClose={this.handleClose}
        anchorEl={anchorEl}
        t={t}
      />
    )
  }
}

export default ForgotPasswordEmail
