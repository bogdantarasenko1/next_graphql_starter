// @flow

import React from 'react'
import { List } from '@material-ui/core'
import NavigationLink from './NavigationLink'
import styles from './styles.css'

type Props = {
  menuItems: [],
  menuOpened: boolean,
  onMouseLeave: () => void,
  onMouseEnter: () => void
};

const NavigationScreen = (props: Props) => {
  const { onMouseLeave, onMouseEnter, menuOpened, menuItems } = props

  const menuClass = [styles.container]

  if (menuOpened) {
    menuClass.push(styles.expanded)
  }

  return (
    <div
      className={menuClass.join(' ')}
      onMouseEnter={onMouseEnter}
      onMouseLeave={onMouseLeave}
    >
      <List component="nav" className={styles.navStacked}>
        {menuItems.map(item => (
          <NavigationLink
            showPrimary={menuOpened}
            key={item.key}
            icon={item.ico}
            primary={item.title}
            to={item.link}
          />
        ))}
      </List>
    </div>
  )
}

export default NavigationScreen
