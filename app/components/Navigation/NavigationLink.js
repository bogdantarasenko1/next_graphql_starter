// @flow

import React from 'react'
import Link from 'next/link'
import {
  ListItem,
  ListItemIcon,
  ListItemText,
  Icon,
  withStyles
} from '@material-ui/core'

type Props = {
  icon: string,
  primary: string,
  to: string,
  showPrimary: boolean,
  classes: Object
};

const styles = theme => ({
  ico: {
    color: 'rgb(128, 128, 128)',
    marginLeft: '.5rem',
    marginRight: '0',
    '&:hover': {
      color: 'white'
    }
  },
  linkText: {
    display: 'inline-block',
    whiteSpace: 'nowrap'
  },
  linkContainer: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    minHeight: '100%'
  },
  link: {
    color: 'rgba(255, 255, 255, 0.5)',
    fontSize: '0.9rem',
    '&:hover': {
      color: 'white',
      textDecoration: 'none'
    }
  }
})

const NavigationLink = (props: Props) => {
  const renderLink = itemProps => {
    const { icon, primary, to, showPrimary, classes } = props
    return (
      <Link href={to} {...itemProps}>
        <a className={classes.link} href={to}>
          <div className={classes.linkContainer}>
            <ListItemIcon className={classes.iconItem}>
              <Icon className={classes.ico}>{icon}</Icon>
            </ListItemIcon>
            {showPrimary && (
              <ListItemText
                disableTypography
                className={classes.linkText}
                primary={primary}
              />
            )}
          </div>
        </a>
      </Link>
    )
  }

  return (
    <li>
      <ListItem button component={renderLink} />
    </li>
  )
}

export default withStyles(styles)(NavigationLink)
