// @flow

import React from 'react'
import { saveTheme, getTheme, themeName } from '../../utils/userTheme'
import ThemeSwitch from './ThemeSwitch.screen'

type State = { darkTheme: boolean };

type Props = {};

class ThemeSwitchContainer extends React.PureComponent<Props, State> {
  props: Props;
  state: State;

  constructor (props: Props) {
    super(props)

    this.state = {
      darkTheme: getTheme() === themeName.dark
    }
  }

  onChangeDarkTheme = (e, checked) => {
    this.setState({ darkTheme: checked })
    const newTheme = checked ? themeName.dark : themeName.light
    saveTheme(newTheme)
    location.reload()
  };

  render () {
    const { darkTheme } = this.state
    return (
      <ThemeSwitch
        {...this.props}
        darkTheme={darkTheme}
        changeDarkTheme={this.onChangeDarkTheme}
      />
    )
  }
}

export default ThemeSwitchContainer
