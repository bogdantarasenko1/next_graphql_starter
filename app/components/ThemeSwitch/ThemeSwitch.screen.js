// @flow

import React from 'react'
import { Switch, FormControlLabel, withStyles } from '@material-ui/core'

type Props = {
  t: string => string,
  darkTheme: boolean,
  changeDarkTheme: () => void,
  classes: Object
};

const styles = theme => ({
  noMargin: {
    marginLeft: 0,
    marginRight: 0
  },
  label: {
    fontSize: '1rem'
  }
})

const ThemeSwitchScreen = (props: Props) => {
  const { darkTheme, changeDarkTheme, t, classes } = props

  return (
    <FormControlLabel
      className={classes.noMargin}
      classes={{ label: classes.label }}
      control={
        <Switch
          checked={darkTheme}
          onChange={changeDarkTheme}
          value="darkTheme"
          disableRipple
        />
      }
      labelPlacement="start"
      label={t('common:Dark Theme')}
    />
  )
}

export default withStyles(styles)(ThemeSwitchScreen)
