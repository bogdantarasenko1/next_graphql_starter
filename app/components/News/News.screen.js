// @flow

import React from 'react'
import List from '@material-ui/core/List'
import { withStyles } from '@material-ui/core'
import DashboardHeader from '../Dashboard/DashboardHeader.screen.js'
import Pagination from '../Pagination/Pagination.screen'
import Loader from '../Loaders/Overview'
import NewsItem from './newsItem.js'

type Props = {
  isLoading: boolean,
  data: [],
  classes: Object
};

const styles = theme => ({
  headline: {
    marginRight: '60px',
    color: theme.palette.text.secondary
  },
  container: {
    margin: '2rem'
  },
  newsContainer: {
    padding: '0 1rem 0.85rem',
    [theme.breakpoints.down('sm')]: {
      padding: 0
    }
  }
})

const NewsScreen = (props: Props) => {
  const {
    isLoading,
    data,
    classes,
    dataLength,
    rowsPerPage,
    page,
    handleChangePage
  } = props
  const content = () => (
    <React.Fragment>
      <DashboardHeader title={'News'} />
      {isLoading ? (
        <Loader height={100} rHeight={30} />
      ) : (
        <div className={classes.newsContainer}>
          <List>
            {data.map((item, index) => {
              return (
                <NewsItem
                  headline={item.headline}
                  domain={item.domain}
                  date={item.date}
                  url={item.url}
                  key={index}
                  className={classes.item}
                />
              )
            })}
          </List>
          <Pagination
            dataLength={dataLength}
            rowsPerPage={rowsPerPage}
            page={page}
            handleChangePage={handleChangePage}
          />
        </div>
      )}
    </React.Fragment>
  )

  return content()
}

export default withStyles(styles)(NewsScreen)
