// @flow

import React from 'react'
import { Typography } from '@material-ui/core'
import ListItem from '@material-ui/core/ListItem'
import { withStyles } from '@material-ui/core/styles'
import TextTruncate from 'react-text-truncate'

type Props = {
  headline: string,
  date: string,
  url: string,
  domain: string,
  classes: object
};

const styles = theme => ({
  newsItem: {
    display: 'block',
    boxShadow: '0px 1px rgba(244, 244, 244, 0.2)',
    padding: '8px 0'
  },
  newsLink: {
    textDecoration: 'none',
    color: theme.palette.text.primary,
    marginBottom: 0,
    '&:hover': {
      color: theme.palette.secondary.main
    }
  }
})

const NewsItemScreen = (props: Props) => {
  const { classes, domain, url, date, headline } = props

  return (
    <React.Fragment>
      <ListItem disableGutters={true} className={classes.newsItem}>
        <Typography
          gutterBottom
          variant="subheading"
          component="h2"
          className={classes.newsLink}
        >
          <a target="_blank" href={url} className={classes.newsLink}>
            <TextTruncate line={1} truncateText="…" text={headline} />
          </a>
        </Typography>
        <Typography variant="caption">
          <TextTruncate line={1} text={`${domain} | ${date}`} />
        </Typography>
      </ListItem>
    </React.Fragment>
  )
}

export default withStyles(styles)(NewsItemScreen)
