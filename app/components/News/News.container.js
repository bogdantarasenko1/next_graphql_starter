// @flow

import React from 'react'
import { fetchData } from '../../utils/dataPlatformQuery'
import News from './News.screen'

type State = {
  isLoading: boolean,
  newsData: []
};

type Props = {};

class NewsContainer extends React.PureComponent<Props, State> {
  props: Props;
  state: State;

  state = {
    isLoading: true,
    newsData: [],
    page: 0,
    rowsPerPage: 7
  };

  getDomain (url) {
    var match = url.match(/:\/\/(www[0-9]?\.)?(.[^/:]+)/i)

    if (
      match != null &&
      match.length > 2 &&
      typeof match[2] === 'string' &&
      match[2].length > 0
    ) {
      var hostName = match[2]
      var domain = hostName
      var parts = hostName.split('.').reverse()

      if (parts != null && parts.length > 1) {
        domain = `${parts[1]}.${parts[0]}`
        return domain
      }
    } else {
      return null
    }
  }

  newsQuery = (page, rowsPerPage) => {
    const query = JSON.stringify({
      query: `{ getNews(
      pagination:{
        offset: ${page * rowsPerPage},
        limit: ${rowsPerPage}
      }){
        date
        title
        url
      }}`
    })

    return fetchData(query).then(r => {
      return r.data.getNews
    })
  };

  fetchNews = async () => {
    const { page, rowsPerPage } = this.state
    const rawData = await this.newsQuery(page, rowsPerPage)
    const transformedNews = this.transformNews(rawData)

    this.setState({
      newsData: transformedNews,
      isLoading: false
    })
  };

  transformNews = data => {
    var newsDataScreen = []
    data.map((article, i) => {
      const date = new Date(article.date)
      var news = {
        date: `${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()}`,
        headline: article.title,
        url: article.url,
        domain: this.getDomain(article.url)
      }
      newsDataScreen.push(news)
    })

    return newsDataScreen
  };

  handleChangePage = (event, page) => {
    this.setState({ page: page }, () => this.fetchNews())
  };

  componentDidMount () {
    this.fetchNews()
  }

  render () {
    const { isLoading, newsData, rowsPerPage, page } = this.state
    const dataLength = 325
    return (
      <News
        isLoading={isLoading}
        data={newsData}
        dataLength={dataLength}
        rowsPerPage={rowsPerPage}
        page={page}
        handleChangePage={this.handleChangePage}
      />
    )
  }
}

export default NewsContainer
