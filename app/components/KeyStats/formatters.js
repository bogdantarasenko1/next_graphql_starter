import numbro from 'numbro'

export default data => {
  return data.map(item => {
    switch (item.name) {
      case 'circulatingCoinCap':
      case 'priceChangeYTD':
      case 'pricechangeThreeM':
      case 'twentyThirtyCoinCap':
        item.value = item.value
          ? numbro(item.value).formatCurrency({
            mantissa: 2
          })
          : 'N/A'
        return item
      case 'currentCirculatingSupplyPercent':
        item.value = item.value
          ? `${numbro(item.value).format({ mantissa: 2 })}%`
          : 'N/A'
        return item
      case 'feesPercentOfValue':
        item.value = item.value
          ? `${numbro(item.value).format({
            mantissa: 8
          })}%`
          : 'N/A'
        return item
      default:
        console.log(`Unhandled Key Data name: ${item.name}`)
        return item
    }
  })
}
