// @flow

import React from 'react'
import {
  List,
  ListItem,
  ListItemText,
  ListItemSecondaryAction,
  withStyles
} from '@material-ui/core'

type Props = {
  items: [],
  classes: Object,
  t: string => string
};

const styles = theme => ({
  root: {
    paddingBottom: '20px'
  }
})

const KeyDataMetricsScreen = (props: Props) => {
  const { items, classes, t } = props

  return (
    <div className={classes.root}>
      <div>
        <List>
          {items.map((item, key) => (
            <ListItem disableGutters divider key={key}>
              <ListItemText
                primaryTypographyProps={{ variant: 'caption' }}
                primary={t(`keyData:${item.name}`)}
              />
              <ListItemSecondaryAction>
                <ListItemText
                  primaryTypographyProps={{ variant: 'caption' }}
                  primary={item.value}
                />
              </ListItemSecondaryAction>
            </ListItem>
          ))}
        </List>
      </div>
    </div>
  )
}

export default withStyles(styles)(KeyDataMetricsScreen)
