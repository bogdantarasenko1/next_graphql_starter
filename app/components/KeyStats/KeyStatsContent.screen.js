// @flow

import React from 'react'
import { Grid } from '@material-ui/core'
import Loader from '../Loaders/Overview'
import Snackbar from '../Snackbar/Snackbar.container'
import MetricsScreen from './KeyStatsMetrics.screen'

type Props = {
  columnA: [],
  columnB: [],
  errors: any,
  isLoading: boolean,
  t: string => string
};

const KeyStatsContentScreen = (props: Props) => {
  const { columnA, columnB, errors, isLoading, t } = props
  if (isLoading) {
    return (
      <React.Fragment>
        <Grid container spacing={40}>
          <Grid item xs={12} md={6}>
            <Loader />
          </Grid>
          <Grid item xs={12} md={6}>
            <Loader />
          </Grid>
        </Grid>
      </React.Fragment>
    )
  }

  return (
    <React.Fragment>
      <Grid container spacing={40}>
        <Grid item xs={12} md={6}>
          {columnA &&
            columnA.map((metric, key) => (
              <MetricsScreen key={key} items={metric.items} t={t} />
            ))}
        </Grid>
        <Grid item xs={12} md={6}>
          {columnB &&
            columnB.map((metric, key) => (
              <MetricsScreen key={key} items={metric.items} t={t} />
            ))}
        </Grid>
      </Grid>
      <Snackbar message={errors} />
    </React.Fragment>
  )
}

export default KeyStatsContentScreen
