// @flow

import React from 'react'
import BlockWithHeader from '../BlockWithHeader/BlockWithHeader.screen.js'
import KeyStatsContent from './KeyStatsContent.container'

type Props = {
  assetSymbol: string,
  t: string => string
};

const KeyStatsScreen = (props: Props) => {
  const { t, assetSymbol } = props

  return (
    <div>
      <BlockWithHeader
        title={t('common:Key Stats')}
        content={<KeyStatsContent t={t} assetSymbol={assetSymbol} {...props} />}
      />
    </div>
  )
}

export default KeyStatsScreen
