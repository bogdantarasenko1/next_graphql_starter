// @flow

import React, { Component } from 'react'
import keyStatsQuery from '../../utils/query/keyStats'
import { fetchData } from '../../utils/dataPlatformQuery'
import formatter from './formatters'
import KeyStatsContentScreen from './KeyStatsContent.screen'

type Props = {
  assetSymbol: string,
  t: func
};

type State = {
  columnA: [],
  columnB: [],
  isLoading: boolean,
  errors: any
};

class KeyStatsContentContainer extends Component {
  props: Props;
  state: State;

  state = {
    isLoading: true
  };

  componentDidMount = () => {
    const { assetSymbol } = this.props

    const query = JSON.stringify({
      query: keyStatsQuery(assetSymbol)
    })

    fetchData(query)
      .then(r => {
        if (r.errors) {
          this.setState({
            isLoading: false,
            errors: r.errors
          })
        }

        return r.data.getCrypto
      })
      .then(this.columnsData)
  };

  columnsData = data => {
    const columnA = [
      {
        items: this.columnAItems(data)
      }
    ]

    const columnB = [
      {
        items: this.columnBItems(data)
      }
    ]

    this.setState({
      isLoading: false,
      columnA,
      columnB
    })
  };

  columnAItems = data => {
    const market = this.latestMarket(data)
    const blockchain = this.latestBlockchain(data)

    const priceChange = market.priceChange || {}
    const YTD = priceChange.YTD || {}

    return formatter([
      {
        name: 'priceChangeYTD',
        value: YTD.amount
      },
      {
        name: 'feesPercentOfValue',
        value: blockchain.feesPercentOfValue
      }
    ])
  };

  columnBItems = data => {
    const market = this.latestMarket(data)
    const blockchain = this.latestBlockchain(data)

    const marketCap = market.marketCap || {}
    const basedOnCirculatingSupply = marketCap.basedOnCirculatingSupply || {}
    const twentyThirtyCoinCap = marketCap.by2030 || {}

    return formatter([
      {
        name: 'circulatingCoinCap',
        value: basedOnCirculatingSupply.amount
      },
      {
        name: 'twentyThirtyCoinCap',
        value: twentyThirtyCoinCap.amount
      },
      {
        name: 'currentCirculatingSupplyPercent',
        value: (blockchain.circulatingSupply / blockchain.totalSupply) * 100
      }
    ])
  };

  latestMarket = data => {
    const { market } = data
    return market.latest || null
  };

  latestBlockchain = data => {
    const { blockchain } = data
    return blockchain.latest || null
  };

  render () {
    const { t } = this.props
    const { isLoading, columnA, columnB, errors } = this.state

    return (
      <KeyStatsContentScreen
        isLoading={isLoading}
        columnA={columnA}
        columnB={columnB}
        errors={errors}
        t={t}
      />
    )
  }
}

export default KeyStatsContentContainer
