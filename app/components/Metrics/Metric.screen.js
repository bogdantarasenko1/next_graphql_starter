// @flow

import React from 'react'
import { withStyles } from '@material-ui/core'
import BlockWithHeader from '../BlockWithHeader/BlockWithHeader.screen'

type Props = {
  title: string,
  content: React.ReactNode,
  classes: Object
};

const styles = theme => ({
  block: {
    marginBottom: '40px'
  }
})

const MetricScreen = (props: Props) => {
  const { title, content, classes } = props
  return (
    <div className={classes.block}>
      <BlockWithHeader title={title} content={content} />
    </div>
  )
}

export default withStyles(styles)(MetricScreen)
