// @flow

import React from 'react'
import ScrollableAnchor from 'react-scrollable-anchor'
import { navigationNodes } from '../NavigationMenu/NavigationMenu.container'
import Metric from './Metric.screen'

type Props = {};

const MetricsScreen = (props: Props) => {
  const {
    'data-visuals': { sections }
  } = navigationNodes

  return Object.values(sections).map((section, key) => (
    <ScrollableAnchor key={key} id={section.anchor}>
      <Metric title={section.title} content={section.title} />
    </ScrollableAnchor>
  ))
}

export default MetricsScreen
