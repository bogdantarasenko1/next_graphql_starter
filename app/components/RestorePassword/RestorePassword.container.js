// @flow

import React from 'react'
import gql from 'graphql-tag'
import { graphql, compose } from 'react-apollo'
import { clientMessage } from '../../utils/apolloErrors'
import RestorePassword from './RestorePassword.screen'

type Props = {
  resetPassword: Object => Promise,
  updatePassword: Object => Promise,
  t: string => string
};

type State = {
  isLoading: boolean,
  firstName: string,
  errors: any,
  email: string,
  pin: string,
  password: string,
  rpassword: string,
  isDisabledPasswords: boolean,
  isDisabledEmail: boolean,
  isShouldShowLogin: boolean,
  isDisabledSubmit: boolean
};

const RESET_PASSWORD_MUTATION = gql`
  mutation ResetPassword($input: userResetPasswordInput!) {
    userResetPassword(input: $input) {
      messageBody
    }
  }
`

const UPDATE_PASSWORD_MUTATION = gql`
  mutation UpdatePassword($input: updateUserPasswordInput!) {
    updateUserPassword(input: $input) {
      user {
        firstName
      }
    }
  }
`

class RestorePasswordContainer extends React.PureComponent<Props, State> {
  constructor (props: Props) {
    super(props)

    this.state = {
      firstName: '',
      errors: null,
      isLoading: false,
      email: '',
      password: '',
      rpassword: '',
      isDisabledPasswords: true,
      isDisabledEmail: false,
      isDisabledSendEmail: true,
      isShouldShowLogin: false,
      isDisabledSubmit: true
    }
  }

  onEmailChange = event => {
    const email = event.currentTarget.value.trim()
    this.setState({ isDisabledSendEmail: !email, email, errors: null })
  };

  onPinChange = event => {
    const pin = event.currentTarget.value.trim()
    this.setState({ pin })
  };

  onPasswordChange = event => {
    const password = event.currentTarget.value.trim()
    this.setState({ password })
  };

  onRepeatPasswordChange = event => {
    const rpassword = event.currentTarget.value.trim()
    this.setState({ rpassword })
  };

  isResetPasswordValid = () => {
    const { password, rpassword, pin } = this.state
    const passwordMatch = password === rpassword
    return password.length && pin.length && passwordMatch
  };

  onEnterEmail = () => {
    const { resetPassword } = this.props
    const { email } = this.state
    const channel = 'EMAIL'

    this.setState({
      isDisabledSendEmail: true,
      isDisabledEmail: true,
      isLoading: true
    })
    resetPassword({
      variables: {
        input: { email, channel }
      }
    })
      .then(result => {
        const {
          data: { userResetPassword }
        } = result
        const { messageBody } = userResetPassword
        if (messageBody === 'Success') {
          this.setState({ isDisabledPasswords: false, errors: null })
        } else {
          console.log(`Unhandled reset password message: ${messageBody}`)
        }
      })
      .catch(errors => {
        if (clientMessage(errors) === 'User not found') {
          this.setState({ errors: 'Email not found' })
        } else {
          this.setState({ errors: JSON.stringify(errors) })
        }
      })
      .finally(() => {
        this.setState({
          isLoading: false,
          isDisabledSendEmail: false,
          isDisabledEmail: false
        })
      })
  };

  onUpdatePassword = () => {
    const { password: newPassword, email, pin: pinNumber } = this.state
    const { updatePassword, t } = this.props
    this.setState({ isDisabledSubmit: true, isLoading: true })

    updatePassword({
      variables: {
        input: {
          pinNumber,
          newPassword,
          email
        }
      }
    })
      .then(result => {
        const {
          data: { updateUserPassword }
        } = result
        const {
          user: { firstName }
        } = updateUserPassword
        this.setState({
          firstName,
          isDisabledSubmit: true,
          isShouldShowLogin: true,
          errors: null,
          isLoading: false
        })
      })
      .catch(error => {
        const { message } = error
        if (message === 'GraphQL error: User not found') {
          this.setState({
            errors: t('common:Email not found or pin code invalid'),
            isLoading: false
          })
        } else {
          this.setState({ errors: message, isLoading: false })
        }
      })
  };

  render () {
    const {
      isLoading,
      isDisabledPasswords,
      isDisabledEmail,
      isShouldShowLogin,
      isDisabledSendEmail,
      errors
    } = this.state
    return (
      <RestorePassword
        {...this.props}
        errors={errors}
        isLoading={isLoading}
        pinChange={this.onPinChange}
        emailChange={this.onEmailChange}
        enterEmail={this.onEnterEmail}
        isDisabledPasswords={isDisabledPasswords}
        isDisabledEmail={isDisabledEmail}
        isDisabledSubmit={!this.isResetPasswordValid()}
        isDisabledSendEmail={isDisabledSendEmail}
        isShouldShowLogin={isShouldShowLogin}
        passwordChange={this.onPasswordChange}
        repeatPasswordChange={this.onRepeatPasswordChange}
        submit={this.onUpdatePassword}
      />
    )
  }
}

export const resetPasswordMutation = graphql(RESET_PASSWORD_MUTATION, {
  name: 'resetPassword'
})

export const updatePasswordMutation = graphql(UPDATE_PASSWORD_MUTATION, {
  name: 'updatePassword'
})

export default compose(
  resetPasswordMutation,
  updatePasswordMutation
)(RestorePasswordContainer)
