// @flow

import React from 'react'
import Link from 'next/link'
import {
  Button,
  FormControl,
  Typography,
  IconButton,
  withStyles
} from '@material-ui/core'
import SendIcon from '@material-ui/icons/Send'
import AuthCard from '../AuthCard/AuthCard.screen'
import AuthTextField from '../ui/AuthTextField'

type Props = {
  isShouldShowLogin: boolean,
  errors: any,
  isLoading: boolean,
  isDisabledPasswords: boolean,
  isDisabledEmail: boolean,
  isDisabledSendEmail: boolean,
  isDisabledSubmit: boolean,
  pinChange: () => void,
  emailChange: () => void,
  passwordChange: () => void,
  repeatPasswordChange: () => void,
  enterEmail: () => void,
  submit: () => void,
  classes: Object,
  t: string => string
};

const styles = () => ({
  margin: {
    marginTop: 15
  },
  flex: {
    display: 'flex'
  }
})

const RestorePasswordScreen = (props: Props) => {
  const {
    isLoading,
    isShouldShowLogin,
    errors,
    isDisabledPasswords,
    isDisabledEmail,
    isDisabledSendEmail,
    isDisabledSubmit,
    emailChange,
    passwordChange,
    repeatPasswordChange,
    pinChange,
    enterEmail,
    submit,
    classes,
    t
  } = props
  const submitBtnText = t('common:Submit')

  const errorAttr = () => {
    return errors ? { error: !!errors } : {}
  }

  const inputs = (
    <React.Fragment>
      <div className={classes.flex}>
        <FormControl fullWidth>
          <AuthTextField
            {...errorAttr()}
            label="Email"
            name="email"
            id="email"
            type="email"
            autoComplete="email"
            disabled={isDisabledEmail}
            onChange={emailChange}
          />
          {errors && (
            <Typography variant="subheading" color="error" component="div">
              {errors}
            </Typography>
          )}
        </FormControl>
        {!isDisabledEmail && (
          <IconButton
            onClick={enterEmail}
            disabled={isDisabledSendEmail}
            color="secondary"
          >
            <SendIcon />
          </IconButton>
        )}
      </div>
      {!isDisabledPasswords && (
        <div className={classes.margin}>
          <Typography variant="body2" color="secondary">
            {t(
              'login:A pin code was sent to your email. Check it and enter the pin code below.',
              { keySeparator: '_' }
            )}
          </Typography>
          <FormControl fullWidth>
            <AuthTextField
              label={t('login:Pin')}
              name="pin"
              id="pin"
              type="text"
              onChange={pinChange}
            />
          </FormControl>
          <FormControl fullWidth>
            <AuthTextField
              label={t('login:New password')}
              name="password"
              type="password"
              onChange={passwordChange}
              id="password"
            />
          </FormControl>
          <FormControl fullWidth>
            <AuthTextField
              label={t('login:Repeat password')}
              name="rpassword"
              type="password"
              onChange={repeatPasswordChange}
              id="rpassword"
            />
          </FormControl>
        </div>
      )}
    </React.Fragment>
  )

  const messageToLogin = (
    <Typography color="secondary">
      {t('login:Your password was updated. You can login with new password.', {
        keySeparator: '_'
      })}
    </Typography>
  )

  const loginAction = () => {
    return (
      <Typography variant="subheading" color="textSecondary" component="span">
        <Link prefetch href="/login">
          <a href="/login">{t('login:Login')}</a>
        </Link>{' '}
        {t('login:to your account')}
      </Typography>
    )
  }

  const actions = () => {
    if (isShouldShowLogin) {
      return loginAction()
    } else {
      return (
        <React.Fragment>
          {loginAction()}
          <Button
            disabled={isDisabledSubmit}
            type="submit"
            variant="contained"
            color="secondary"
          >
            {submitBtnText}
          </Button>
        </React.Fragment>
      )
    }
  }

  return (
    <AuthCard
      isLoading={isLoading}
      inputs={isShouldShowLogin ? messageToLogin : inputs}
      title={t('login:Restore password')}
      actions={actions()}
      onSubmit={submit}
    />
  )
}

export default withStyles(styles)(RestorePasswordScreen)
