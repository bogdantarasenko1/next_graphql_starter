export default [
  {
    image: 'bbc.svg',
    title: 'Mosaic network will reward researchers with its own cryptocurrency',
    source: 'bbc.com · Jul 14 2018',
    text:
      'NEW YORK (Reuters) - A group of academics and technology experts will soon launch a blockchain-powered network that will reward quality research with its own cryptocurrency called Mosaic.'
  },
  {
    image: 'cnbc.svg',
    title: 'The Rise of Bitcoin Factories: Mining for the Masses',
    source: 'wsj.com · Jul 14 2018',
    text:
      'NEW YORK (Reuters) - A group of academics and technology experts will soon launch a blockchain-powered network that will reward quality research with its own cryptocurrency called Mosaic.'
  },
  {
    image: 'wsj.svg',
    title: 'The Rise of Bitcoin Factories: Mining for the Masses',
    source: 'wsj.com · Jul 14 2018',
    text:
      'NEW YORK (Reuters) - A group of academics and technology experts will soon launch a blockchain-powered network that will reward quality research with its own cryptocurrency called Mosaic.'
  },
  {
    image: 'bbc.svg',
    title: 'Mosaic network will reward researchers with its own cryptocurrency',
    source: 'nasdaq.com · Jul 14 2018',
    text:
      'NEW YORK (Reuters) - A group of academics and technology experts will soon launch a blockchain-powered network that will reward quality research with its own cryptocurrency called Mosaic.'
  }
]
