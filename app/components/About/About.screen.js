// @flow

// Do not remove the page. It was replaced by external page, but might be changed again.

import React from 'react'
import { withStyles } from '@material-ui/core'
import Layout from '../MainLayout/MainLayout.screen'
import leadership from '../../containers/Team/leadership'
import AboutUs from './fragment/AboutUs.screen'
import Product from './fragment/Product.screen'
import Team from './fragment/Team.screen'
import Press from './fragment/Press.screen'
import ReadyToStart from './fragment/ReadyToStart.screen'
import Latest from './fragment/Latest.screen'
import pressReleases from './pressReleases'
import latestData from './lates'

type Props = {
  classes: Object
};

const styles = theme => ({})

const AboutScreen = (props: Props) => {
  const { classes, ...other } = props

  const content = () => {
    return (
      <div>
        <AboutUs {...other} />
        <Product {...other} />
        <Team data={leadership.slice(0, 3)} {...other} />
        <Press data={pressReleases} {...other} />
        <Latest data={latestData} {...other} />
        <ReadyToStart {...other} />
      </div>
    )
  }

  return <Layout {...props} content={content()} />
}

export default withStyles(styles)(AboutScreen)
