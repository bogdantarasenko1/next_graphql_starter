// @flow

import React from 'react'
import classnames from 'classnames'
import { Grid, Typography, withStyles } from '@material-ui/core'

type Props = {
  classes: Object,
  t: string => string
};

const styles = theme => ({
  root: {
    paddingTop: '120px',
    paddingBottom: '108px'
  },
  headline: {
    paddingTop: '25px',
    width: '50vw',
    [theme.breakpoints.down('sm')]: {
      width: '80vw'
    },
    marginLeft: 'auto',
    marginRight: 'auto'
  },
  text3: {
    paddingTop: '30px'
  },
  grid: {
    marginTop: '110px'
  },
  text4: {
    marginTop: '16px',
    marginBottom: '16px'
  }
})

const ProductScreen = (props: Props) => {
  const { classes, t } = props

  return (
    <div className={classes.root}>
      <Typography
        gutterBottom
        align="center"
        color="textSecondary"
        variant="subheading"
      >
        {t('about:The Product').toUpperCase()}
      </Typography>
      <Typography
        paragraph
        align="center"
        variant="headline"
        className={classes.headline}
      >
        {t('about:Text2')}
      </Typography>
      <Typography
        align="center"
        variant="subheading"
        color="textSecondary"
        className={classnames(classes.headline, classes.text3)}
      >
        {t('about:Text3', { context: 'product' })}
      </Typography>

      <Grid container className={classes.grid}>
        <Grid item xs={12}>
          <Grid container spacing={16} justify="center">
            <Grid item xs={12} md={4}>
              <img src="/static/product_ico1.svg" />
              <Typography className={classes.text4} variant="headline">
                {t('about:Text4')}
              </Typography>
              <Typography variant="subheading" color="textSecondary">
                {t('about:Text3')}
              </Typography>
            </Grid>
            <Grid item xs={12} md={4}>
              <img src="/static/product_ico2.svg" />
              <Typography className={classes.text4} variant="headline">
                {t('about:Text5')}
              </Typography>
              <Typography variant="subheading" color="textSecondary">
                {t('about:Text3')}
              </Typography>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </div>
  )
}

export default withStyles(styles)(ProductScreen)
