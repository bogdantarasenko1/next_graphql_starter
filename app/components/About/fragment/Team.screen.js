// @flow

import React from 'react'
import { Typography, withStyles } from '@material-ui/core'
import CardsCollection from '../../Team/CarsCollection.screen'

type Props = {
  data: [],
  t: string => string,
  classes: Object
};

const styles = theme => ({
  root: {
    paddingTop: '120px',
    paddingBottom: '10px',
    backgroundColor: '#1A3333'
  },
  headline: {
    width: '66%',
    margin: '25px auto 50px',
    [theme.breakpoints.down('xs')]: {
      width: 'auto'
    }
  }
})

const TeamScreen = (props: Props) => {
  const { classes, t, data } = props

  return (
    <div className={classes.root}>
      <Typography
        gutterBottom
        align="center"
        color="textSecondary"
        variant="subheading"
      >
        {t('about:Mosaic Team').toUpperCase()}
      </Typography>
      <Typography
        paragraph
        align="center"
        variant="headline"
        className={classes.headline}
      >
        {t('about:Text2')}
      </Typography>
      <CardsCollection data={data} />
    </div>
  )
}

export default withStyles(styles)(TeamScreen)
