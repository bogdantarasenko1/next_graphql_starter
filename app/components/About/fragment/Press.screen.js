// @flow

import React from 'react'
import { Grid, Typography, withStyles } from '@material-ui/core'

type Props = {
  data: [],
  t: string => string,
  classes: Object
};

const styles = theme => ({
  root: {
    paddingBottom: '60px',
    paddingTop: '120px'
  },
  headline: {
    marginTop: '25px',
    marginBottom: '50px'
  },
  title: {
    marginTop: '17px',
    marginBottom: '16px'
  },
  source: {
    marginBottom: '10px'
  }
})

const PressScreen = (props: Props) => {
  const { classes, t, data } = props

  return (
    <div className={classes.root}>
      <Typography
        gutterBottom
        align="center"
        color="textSecondary"
        variant="subheading"
      >
        {t('about:Press Coverage').toUpperCase()}
      </Typography>
      <Typography
        paragraph
        align="center"
        variant="headline"
        className={classes.headline}
      >
        {t('about:Text6')}
      </Typography>
      <Grid container justify="center" spacing={16}>
        {data.map((e, i) => (
          <Grid key={i} item md={2} xs={12}>
            <div>
              <img src={e.image} />
              <Typography
                className={classes.title}
                variant="subheading"
                color="textSecondary"
              >
                {e.title}
              </Typography>
              <Typography
                className={classes.source}
                color="secondary"
                variant="caption"
              >
                {e.source}
              </Typography>
              <Typography color="secondary">{e.text}</Typography>
            </div>
          </Grid>
        ))}
      </Grid>
    </div>
  )
}

export default withStyles(styles)(PressScreen)
