// @flow

import React from 'react'
import { Grid, Typography, withStyles } from '@material-ui/core'

type Props = {
  t: string => string,
  data: Object,
  classes: Object
};

const styles = theme => ({
  root: {
    backgroundColor: '#1A3333',
    paddingBottom: '120px',
    paddingTop: '120px'
  },
  link: {
    color: '#50E3C2'
  },
  copy: {
    [theme.breakpoints.down('sm')]: {
      padding: '16px'
    }
  }
})

const LatestScreen = (props: Props) => {
  const { classes, t, data } = props
  return (
    <div className={classes.root}>
      <Grid container justify="center" spacing={16}>
        <Grid item md={4} xs={12}>
          <div>
            <img src={data.image} width="100%" />
          </div>
        </Grid>
        <Grid item md={4} xs={12}>
          <div className={classes.copy}>
            <Typography gutterBottom variant="headline">
              {data.title}
            </Typography>
            <Typography gutterBottom color="secondary" variant="subheading">
              {data.text}
            </Typography>
            <Typography variant="body2">
              <a className={classes.link} href="#/">
                {t('common:Read More')}
              </a>
            </Typography>
          </div>
        </Grid>
      </Grid>
    </div>
  )
}

export default withStyles(styles)(LatestScreen)
