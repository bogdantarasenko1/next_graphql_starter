// @flow

import React from 'react'
import classNames from 'classnames'
import {
  FormControl,
  InputAdornment,
  Input,
  withStyles,
  Typography
} from '@material-ui/core'

type Props = {
  t: string => string,
  classes: Object
};

const styles = theme => ({
  root: {
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'column',
    paddingBottom: '120px',
    paddingTop: '120px'
  },
  headline: {
    marginTop: '24px',
    marginBottom: '30px'
  },
  margin: {
    margin: theme.spacing.unit
  },
  withoutLabel: {
    marginTop: theme.spacing.unit * 3
  },
  textField: {
    flexBasis: 200
  },
  input: {
    backgroundColor: theme.palette.common.white,
    borderRadius: '4px',
    padding: '4px'
  },
  adornment: {
    color: theme.palette.grey[500]
  }
})

const ReadyToStartScreen = (props: Props) => {
  const { classes, t } = props
  return (
    <div className={classes.root}>
      <Typography align="center" color="textSecondary" variant="subheading">
        {t('common:Ready To Start?').toUpperCase()}
      </Typography>
      <Typography
        align="center"
        variant="headline"
        className={classes.headline}
      >
        {t('about:Text7')}
      </Typography>
      <FormControl
        className={classNames(
          classes.margin,
          classes.withoutLabel,
          classes.textField
        )}
      >
        <Input
          disableUnderline
          classes={{ root: classes.input }}
          startAdornment={
            <InputAdornment
              disableTypography
              classes={{ positionStart: classes.adornment }}
              position="start"
            >
              {t('common:Request Invite')}
            </InputAdornment>
          }
          endAdornment={
            <InputAdornment
              disableTypography
              classes={{ positionEnd: classes.adornment }}
              position="end"
            >
              {t('common:Enter your email')}
            </InputAdornment>
          }
        />
      </FormControl>
    </div>
  )
}

export default withStyles(styles)(ReadyToStartScreen)
