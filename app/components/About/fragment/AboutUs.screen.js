// @flow

import React from 'react'
import { Grid, Typography, withStyles } from '@material-ui/core'

type Props = {
  t: string => string,
  classes: Object
};

const styles = theme => ({
  root: {
    paddingTop: '200px',
    paddingBottom: '240px',
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#1A3333'
  },
  card: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    textAlign: 'center',
    width: '168px'
  },
  headline: {
    width: '40vw',
    [theme.breakpoints.down('sm')]: {
      width: '80vw'
    },
    margin: '16px auto 64px'
  },
  badge: {
    marginBottom: '12px',
    color: theme.palette.common.black,
    borderRadius: '50%',
    height: '32px',
    width: '32px',
    backgroundColor: theme.palette.common.white,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  }
})

const AboutUsScreen = (props: Props) => {
  const { classes, t } = props
  return (
    <div className={classes.root}>
      <Grid container justify="center">
        <Grid item xs={12}>
          <Typography align="center" color="textSecondary" variant="subheading">
            {t('about:About Us').toUpperCase()}
          </Typography>
        </Grid>
        <Grid item xs={12}>
          <Typography
            align="center"
            variant="headline"
            className={classes.headline}
          >
            {t('about:Text1')}
          </Typography>
        </Grid>
        <Grid container justify="center" spacing={16}>
          <Grid className={classes.card} item>
            <span className={classes.badge}>1</span>
            <Typography variant="body2">{t('about:Badge1')}</Typography>
          </Grid>
          <Grid className={classes.card} item>
            <span className={classes.badge}>2</span>
            <Typography variant="body1">{t('about:Badge2')}</Typography>
          </Grid>
          <Grid className={classes.card} item>
            <span className={classes.badge}>3</span>
            <Typography variant="body1">{t('about:Badge3')}</Typography>
          </Grid>
        </Grid>
      </Grid>
    </div>
  )
}

export default withStyles(styles)(AboutUsScreen)
