export default {
  title: 'Interview with our Co-Founder, Garrick Hileman',
  text:
    'Co-Founder, Garrick Hileman, appeared on CNBC to discuss Venezuela’s state-backed currency ‘Petro’, Bitcoin, and cryptoasset valuations.',
  image: '/static/lates.png'
}
