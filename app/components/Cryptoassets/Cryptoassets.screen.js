// @flow

import React from 'react'
import AssetsTabs from '../AssetsTabs/AssetsTabs.container'
import HeaderTabs from '../HeaderTabs/HeaderTabs.container'

type Props = {
  t: func,
  onChangeHeaderTab: (key: string) => void,
  headerTabItems: [],
  tabItems: [],
  onChangeColumns: columns => void
};

class CryptoassetsScreen extends React.Component<Props> {
  render () {
    const {
      tabItems,
      onChangeHeaderTab,
      headerTabItems,
      ...other
    } = this.props
    return (
      <React.Fragment>
        <HeaderTabs
          tabItems={headerTabItems}
          onChangeTab={onChangeHeaderTab}
          {...other}
        />
        <AssetsTabs tabItems={tabItems} selectedTab={0} {...other} />
      </React.Fragment>
    )
  }
}

export default CryptoassetsScreen
