// @flow

import React from 'react'
import MarketTabContent from './Market/Market.container'

export default props => [
  {
    key: 'market',
    title: 'Market',
    content: <MarketTabContent {...props} />
  }
  // {
  //   key: 'watchlist',
  //   title: 'Watchlist',
  //   content: <div>WatchList Tab Content</div>
  // },
  // {
  //   key: 'portfolio',
  //   title: 'Portfolio',
  //   content: <div>Portfolio Tab Content</div>
  // }
]
