// @flow

import React from 'react'
import _ from 'lodash'
import Table from './BlockchainTable.container'

const columns = [
  'symbol',
  'name',
  'feesPercentOfValue',
  'totalFees',
  'txVolume',
  'adjustedTxVolume',
  'txCount',
  'activeAddresses',
  'averageTxValue',
  'medianTxValue',
  'medianFee',
  'difficulty'
]

const sortableColumns = [
  'feesPercentOfValue',
  'totalFees',
  'txVolume',
  'adjustedTxVolume',
  'txCount',
  'activeAddresses',
  'averageTxValue',
  'medianTxValue',
  'medianFee',
  'difficulty'
]

const persistentColumns = ['symbol', 'name']

export const customizableColumns = () => {
  // number and name are always visible
  return columns.slice(2)
}

type Props = {
  t: func,
  usersColumns?: []
};

const MarketContainer = (props: Props) => {
  const usersColumns = props.usersColumns || []

  const orderColumnsToShow = () => {
    // return def()
    return _.intersection(columns, usersColumns)
  }

  const visibleColumns = () => {
    let headerColumns = [...columns]

    return _.filter(headerColumns, column => {
      if (!column) {
        return true
      }

      if (persistentColumns.includes(column)) {
        return true
      }

      return usersColumns.includes(column)
    })
  }

  const visibleSortableColumns = () => {
    let columns = [...sortableColumns]
    return _.filter(columns, column => {
      if (persistentColumns.includes(column)) {
        return true
      }

      return usersColumns.includes(column)
    })
  }

  return (
    <Table
      columns={visibleColumns()}
      sortableColumns={visibleSortableColumns()}
      customColumns={orderColumnsToShow()}
      {...props}
    />
  )
}

export default MarketContainer
