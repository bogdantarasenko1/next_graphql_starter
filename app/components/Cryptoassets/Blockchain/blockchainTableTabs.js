// @flow

import React from 'react'
import BlockchainTabContent from './Blockchain.container'

export default props => [
  {
    key: 'market',
    title: 'Market',
    content: <BlockchainTabContent {...props} />
  }
  // {
  //   key: 'watchlist',
  //   title: 'Watchlist',
  //   content: <div>WatchList Tab Content</div>
  // },
  // {
  //   key: 'portfolio',
  //   title: 'Portfolio',
  //   content: <div>Portfolio Tab Content</div>
  // }
]
