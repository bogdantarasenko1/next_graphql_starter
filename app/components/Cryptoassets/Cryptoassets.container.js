// @flow

import React from 'react'
import { graphql } from 'react-apollo'
import gql from 'graphql-tag'
import Cryptoassets from './Cryptoassets.screen'
import tradingTabs from './tradingTableTabs'
import blockchainTableTabs from './Blockchain/blockchainTableTabs'
import placeholderTabs from './placeholderTableTabs'
import { customizableColumns } from './Market/Market.container'
import { customizableColumns as blockchainCustomizableColumns } from './Blockchain/Blockchain.container'

const SAVE_COLUMNS = gql`
  mutation SaveColumns($columns: [Column]) {
    saveColumns(columns: $columns) @client
  }
`

type Props = {
  t: func,
  headerTabItems: [],
  mutate: func
};

type State = {
  tabItems: [],
  activeTab: string,
  usersColumns?: []
};

class CryptoassetsContainer extends React.Component<Props, State> {
  props: Props;
  state: State;

  constructor (props) {
    super(props)

    this.state = {
      activeTab: 'trading',
      tabItems: tradingTabs({
        ...props,
        ...{ defaultColumns: customizableColumns() }
      }),
      usersColumns: this.userColumnsFroTab('trading')
    }
  }

  componentDidUpdate (props, state) {
    const { activeTab, usersColumns } = this.state
    if (state.usersColumns !== usersColumns && state.activeTab !== activeTab) {
      this.onChangeHeaderTab(activeTab)
    }
  }

  tableTabsForHeaderTab = headerTabKey => {
    const { usersColumns } = this.state
    switch (headerTabKey) {
      case 'trading':
        return tradingTabs({
          ...this.props,
          ...{ defaultColumns: usersColumns }
        })
      case 'blockchain':
        return blockchainTableTabs({ ...this.props, ...{ usersColumns } })
      case 'ecosystem':
      case 'macro':
        return placeholderTabs
      default:
        console.log(`Unhandled tab key ${headerTabKey}`)
        break
    }
  };

  userColumnsFroTab = tabKey => {
    switch (tabKey) {
      case 'blockchain':
        return blockchainCustomizableColumns()
      default:
        return customizableColumns()
    }
  };

  onChangeHeaderTab = tabKey => {
    this.setState({
      activeTab: tabKey,
      tabItems: this.tableTabsForHeaderTab(tabKey),
      usersColumns: this.userColumnsFroTab(tabKey)
    })
  };

  onChangeColumns = columns => {
    const { mutate } = this.props
    this.setState({ usersColumns: columns })
    const userColumns = columns.map(column => ({ id: column }))
    mutate({ variables: { columns: userColumns } })
  };

  render () {
    const { tabItems } = this.state
    const { headerTabItems } = this.props
    return (
      <Cryptoassets
        headerTabItems={headerTabItems}
        tabItems={tabItems}
        onChangeHeaderTab={this.onChangeHeaderTab}
        onChangeColumns={this.onChangeColumns}
        {...this.props}
      />
    )
  }
}

CryptoassetsContainer.defaultProps = {
  title: 'Cryptoassets'
}

export default graphql(SAVE_COLUMNS)(CryptoassetsContainer)
