// @flow

import React from 'react'
import _ from 'lodash'
import { Query } from 'react-apollo'
import gql from 'graphql-tag'
import Table from '../Table/CryptoassetsTable.container'

const columns = [
  'symbol',
  'name',
  'basedOnCirculatingSupply',
  'latestPriceUSD',
  'percentChangeUSD',
  'latestPriceBTC',
  'percentChangeBTC',
  'circulatingSupplyBlockchain',
  'percentTotalSupplyBlockchain',
  'basedOnMaxSupply',
  'ROIMonthly',
  'ROIYearly',
  'volume'
]

const sortableColumns = [
  'basedOnCirculatingSupply',
  'latestPriceUSD',
  'latestPriceBTC',
  'circulatingSupplyBlockchain',
  'basedOnMaxSupply'
]

const persistentColumns = ['symbol', 'name']

export const customizableColumns = () => {
  // number and name are always visible
  return columns.slice(2)
}

type Props = {
  sortBy: func,
  sort: string,
  errors: any,
  sortKey: string,
  assets: [],
  userColumns?: [],
  defaultColumns: [],
  t: func
};

class MarketContainer extends React.PureComponent<Props> {
  props: Props;

  userColumns = () => {
    const { userColumns, defaultColumns } = this.props
    const columns = userColumns ? userColumns.map(c => c.id) : defaultColumns
    return columns
  };

  orderColumnsToShow = () => {
    return _.intersection(columns, this.userColumns())
  };

  visibleColumns = () => {
    let headerColumns = [...columns]

    return _.filter(headerColumns, column => {
      if (!column) {
        return true
      }

      if (persistentColumns.includes(column)) {
        return true
      }

      return this.userColumns().includes(column)
    })
  };

  visibleSortableColumns = () => {
    let columns = [...sortableColumns]
    return _.filter(columns, column => {
      if (persistentColumns.includes(column)) {
        return true
      }

      return this.userColumns().includes(column)
    })
  };

  render () {
    const { sort, sortBy, sortKey, assets, errors, t } = this.props

    return (
      <Table
        sortBy={sortBy}
        sort={sort}
        sortKey={sortKey}
        assets={assets}
        columns={this.visibleColumns()}
        sortableColumns={this.visibleSortableColumns()}
        customColumns={this.orderColumnsToShow()}
        errors={errors}
        t={t}
      />
    )
  }
}

const query = gql`
  query GetColumns {
    columns @client {
      id
    }
  }
`

const MarketContainerQuery = (props: Props) => (
  <Query query={query}>
    {({ data: { columns } }) => {
      return <MarketContainer {...props} userColumns={columns} />
    }}
  </Query>
)

export default MarketContainerQuery
