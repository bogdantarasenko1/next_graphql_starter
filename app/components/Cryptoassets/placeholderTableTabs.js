// @flow

import React from 'react'

export default [
  {
    key: 'market',
    title: 'Market',
    content: <div>Market Tab Content</div>
  },
  {
    key: 'watchlist',
    title: 'Watchlist',
    content: <div>Watchlist Tab Content</div>
  } /*,
  {
    key: 'portfolio',
    title: 'Portfolio',
    content: <div>Portfolio Tab Content</div>
  } */
]
