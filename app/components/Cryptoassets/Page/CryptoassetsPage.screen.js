// @flow

import React from 'react'
import { Grid } from '@material-ui/core'
import Layout from '../../MainLayout/MainLayout.screen'
import Cryptoassets from '../Cryptoassets.container'
import { cryptoassetsTabItems } from '../headerTabItems'

const CryptoassetsPageScreen = props => {
  const content = () => {
    return (
      <Grid container>
        <Grid item xs={12}>
          <Cryptoassets headerTabItems={cryptoassetsTabItems()} {...props} />
        </Grid>
      </Grid>
    )
  }

  return <Layout {...props} content={content()} />
}

export default CryptoassetsPageScreen
