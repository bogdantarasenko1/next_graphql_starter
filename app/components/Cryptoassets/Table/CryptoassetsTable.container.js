// @flow

import React from 'react'
import { formatNumbers, platformQuery } from '../../../utils/dataPlatformQuery'
import Cryptoassets from './CryptoassetsTable.screen'

type Props = {
  isLoading: boolean,
  dataLength: number,
  onPageUpdate: (e: Object, page: number) => void,
  sortBy: func,
  sort: string,
  errors: any,
  sortedField: string,
  assets: [],
  columns: [],
  customColumns: [],
  sortableColumns: [],
  t: func
};

class CryptoassetsTableContainer extends React.Component<Props> {
  props: Props;

  state = {
    page: 0,
    rowsPerPage: 20
  };

  handleChangePage = (event, page) => {
    const { onPageUpdate } = this.props
    this.setState({ page: page }, () => onPageUpdate(page))
  };

  headerColumns () {
    const { t, columns, sortableColumns } = this.props
    return columns.map(col => {
      if (col) {
        const sortable = sortableColumns.includes(col)
        var frozen = col === 'symbol'
        var faded = col === 'name'
        return {
          key: col,
          text: t(`cryptoassets-table:${col}`),
          sortable,
          frozen,
          faded
        }
      }

      return col
    })
  }

  sortBy = key => {
    this.setState({ page: 0 }, () => this.props.sortBy(key))
  };

  render () {
    const {
      sort,
      sortedField,
      assets,
      customColumns,
      errors,
      isLoading,
      dataLength
    } = this.props
    const { page, rowsPerPage } = this.state

    const formattedData = formatNumbers(assets)

    return (
      <Cryptoassets
        isLoading={isLoading}
        data={formattedData}
        dataLength={dataLength}
        rowsPerPage={rowsPerPage}
        page={page}
        handleChangePage={this.handleChangePage}
        sortDirection={sort}
        sortedField={sortedField}
        errors={errors}
        sortData={this.sortBy}
        customColumns={customColumns}
        headerColumns={this.headerColumns()}
      />
    )
  }
}

export default platformQuery({
  mapExtraData: () => ({ ico: '/static/coin/btc.svg' })
})(CryptoassetsTableContainer)
