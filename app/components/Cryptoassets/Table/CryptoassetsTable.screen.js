// @flow

import React from 'react'
import Link from 'next/link'
import {
  Table,
  TableBody,
  TableRow,
  TableCell,
  withStyles
} from '@material-ui/core'
import ContentLoader from 'react-content-loader'
import SortableHeader from '../../SortableHeader/SortableHeader.screen'
import MiniGraph from '../../MiniGraph/MiniGraph.container.js'
import Snackbar from '../../Snackbar/Snackbar.container'
import Pagination from '../../Pagination/Pagination.screen'

type Props = {
  isLoading: boolean,
  data: [],
  headerColumns: [],
  customColumns: [],
  sortData: (key: string) => void,
  sortDirection: string,
  sortedField: string,
  errors: any,
  classes: Object,
  dataLength: number,
  rowsPerPage: number,
  page: number,
  handleChangePage: func
};

const styles = theme => ({
  frozen: {
    position: 'absolute',
    left: 0,
    paddingLeft: '10px',
    width: '115px',
    height: '35px',
    '&:after': {
      content: '""',
      position: 'absolute',
      bottom: 0,
      right: '-20px',
      height: '34px',
      width: '20px',
      display: 'block',
      background:
        'linear-gradient(to right, rgba(42,42,42,1) 0%,rgba(42,42,42,0) 100%)',
      filter:
        'progid:DXImageTransform.Microsoft.gradient( startColorstr="2#A2A2A", endColorstr="#007db9e8",GradientType=1 )' /* IE6-9 */,
      [theme.breakpoints.down('sm')]: {
        background:
          'linear-gradient(to right, rgba(38,38,37,1) 0%,rgba(38,38,37,0) 100%)',
        filter:
          'progid:DXImageTransform.Microsoft.gradient( startColorstr="#262625", endColorstr="#00262625",GradientType=1 )' /* IE6-9 */
      }
    }
  },
  symbol: {
    lineHeight: '39px'
  },
  icon: {
    verticalAlign: 'middle',
    margin: '0 10px 0 7px',
    display: 'inline-block',
    width: '18px',
    height: '18px',
    '& img': {
      objectFit: 'contain',
      width: '100%'
    }
  },
  index: {
    width: '25px',
    display: 'inline-block'
  },
  table: {
    '& a': {
      color: theme.palette.secondary.main,
      fontWeight: 'bold'
    }
  },
  cell: {
    padding: '0 5px'
  },
  nowrap: {
    whiteSpace: 'nowrap'
  },
  firstColumn: {
    paddingLeft: '30px'
  },
  tableContainer: {
    position: 'relative',
    paddingRight: '8px'
  },
  overflowContainer: {
    overflow: 'scroll',
    marginLeft: '115px'
  },
  loader: {
    left: 0
  }
})

const CryptoassetsTableScreen = (props: Props) => {
  const {
    isLoading,
    data,
    sortData,
    sortDirection,
    sortedField,
    headerColumns,
    errors,
    dataLength,
    rowsPerPage,
    page,
    handleChangePage,
    classes
  } = props

  const { customColumns } = props

  const cellForCustomColumn = item => {
    return customColumns.map((column, index) => {
      if (item[column]) {
        return <TableCell key={index}>{item[column]}</TableCell>
      } else {
        if (column === 'priceGraph') {
          return (
            <TableCell className={classes.cell} key={index}>
              {<MiniGraph data={item.priceChanges7d} width={88} height={29} />}
            </TableCell>
          )
        } else {
          return <TableCell key={index}>N/A</TableCell>
        }
      }
    })
  }

  const tableLoader = props => (
    <ContentLoader
      height={225}
      width={800}
      speed={2}
      primaryColor="#666666"
      secondaryColor="#777777"
      preserveAspectRatio="none"
      {...props}
    >
      <rect x="0" y="10" rx="1" ry="1" width="800" height="35" />
      <rect x="0" y="55" rx="1" ry="1" width="800" height="35" />
      <rect x="0" y="100" rx="1" ry="1" width="800" height="35" />
      <rect x="0" y="145" rx="1" ry="1" width="800" height="35" />
      <rect x="0" y="190" rx="1" ry="1" width="800" height="35" />
    </ContentLoader>
  )

  return (
    <div className={classes.tableContainer}>
      <div className={data.length !== 0 ? classes.overflowContainer : ''}>
        <Table className={classes.table}>
          {data.length !== 0 ? (
            <SortableHeader
              columns={headerColumns}
              sortData={sortData}
              sortDirection={sortDirection}
              sortedField={sortedField}
            />
          ) : (
            <TableBody>
              <TableRow />
            </TableBody>
          )}
          <TableBody>
            {isLoading ? (
              <TableRow>
                <TableCell className={classes.loader} colSpan={20}>
                  {tableLoader()}
                </TableCell>
              </TableRow>
            ) : (
              data &&
              data.map((item, index) => {
                return (
                  <TableRow className={'custom-hover'} key={index} hover>
                    <TableCell className={classes.frozen}>
                      <span className={classes.index}>{index + 1}</span>
                      <span className={classes.icon}>
                        <img src={item.ico} width="18" height="18" />
                      </span>
                      <Link href={`/coin/${item.symbol.toLowerCase()}`}>
                        <a
                          className={classes.symbol}
                          href={`/coin/${item.symbol.toLowerCase()}`}
                        >
                          {item.symbol}
                        </a>
                      </Link>
                    </TableCell>
                    <TableCell
                      className={[classes.nowrap, classes.firstColumn].join(
                        ' '
                      )}
                    >
                      {item.name ? (
                        <Link href={`/coin/${item.symbol.toLowerCase()}`}>
                          <a href={`/coin/${item.symbol.toLowerCase()}`}>
                            {item.name}
                          </a>
                        </Link>
                      ) : (
                        'N/A'
                      )}
                    </TableCell>
                    {cellForCustomColumn(item)}
                  </TableRow>
                )
              })
            )}
          </TableBody>
        </Table>
        <Pagination
          dataLength={dataLength}
          rowsPerPage={rowsPerPage}
          page={page}
          handleChangePage={handleChangePage}
        />
      </div>
      <Snackbar message={errors} />
    </div>
  )
}

export default withStyles(styles)(CryptoassetsTableScreen)
