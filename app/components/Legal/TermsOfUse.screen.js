// @flow

import React from 'react'
import { Grid, Typography } from '@material-ui/core'
import Layout from '../MainLayout/MainLayout.screen'

type Props = {};

const TermsOfUseScreen = (props: Props) => {
  const content = () => {
    return (
      <div>
        <Grid container justify="center">
          <Grid item xs={12} md={8}>
            <Typography variant="title">Terms Of Use</Typography>
            <Typography component="div">
              <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque
                molestie lobortis luctus. Quisque luctus eros vulputate ex
                tristique, in pellentesque diam viverra. Vivamus congue
                porttitor leo in posuere. Mauris imperdiet et erat at ornare.
                Vestibulum ante ipsum primis in faucibus orci luctus et ultrices
                posuere cubilia Curae; Aliquam erat volutpat. Etiam sed ante
                egestas, aliquet justo quis, aliquet justo. Curabitur molestie
                mi vel maximus pretium. Mauris accumsan odio et lorem dapibus
                sagittis. Praesent faucibus blandit risus, in hendrerit nisl
                consequat at. In hac habitasse platea dictumst. Morbi ligula
                est, suscipit vel lacinia id, molestie at urna. Duis viverra sed
                orci ac congue. Pellentesque ut viverra massa. Ut commodo eros
                eget eros bibendum venenatis. Quisque et pulvinar sem. Nunc id
                semper ipsum. Nulla et vestibulum turpis. In pellentesque
                elementum tortor pretium pharetra. In gravida velit finibus,
                mattis urna et, blandit mi. In hac habitasse platea dictumst.
              </p>

              <p>
                Sed interdum iaculis dui at laoreet. Nulla tempus odio mattis,
                porta neque vel, malesuada quam. Aenean imperdiet et quam id
                porta. Integer fringilla, urna quis feugiat varius, enim eros
                pretium sapien, ac placerat odio mauris id erat. Vestibulum
                bibendum fermentum eros at volutpat. Aliquam imperdiet euismod
                scelerisque. Mauris semper lectus sed quam tincidunt venenatis.
                Duis dui felis, posuere non velit vitae, mollis egestas eros.
                Phasellus nec velit augue. Integer vel justo sit amet nisl
                dapibus tempor ac quis neque. Nulla semper quis sem ac
                malesuada. Ut congue sodales mauris, a elementum libero
                imperdiet ut.
              </p>

              <p>
                Donec at tincidunt ante. Integer congue congue diam, nec
                hendrerit libero vulputate ut. Maecenas in felis imperdiet,
                varius neque a, mollis diam. Nulla vitae luctus lectus. Cras
                nibh velit, auctor nec dolor sit amet, condimentum blandit ante.
                Sed sit amet faucibus risus, ut tristique orci. Nullam pretium
                pharetra accumsan. Curabitur vitae ante nec orci accumsan
                imperdiet. Sed ornare vitae purus quis molestie. Cras a velit
                sit amet magna eleifend fermentum. Quisque interdum nisl ac est
                tempus sodales. Nam ornare sem non risus vestibulum, vitae
                condimentum dui blandit. Donec congue sodales enim eget sodales.
                Suspendisse in leo vel nulla consequat posuere a eu purus.
              </p>

              <p>
                Vivamus porta magna vitae eros facilisis pellentesque. Nam elit
                justo, aliquet vitae efficitur non, consequat sed turpis.
                Quisque vel ultrices mi. Sed tincidunt, quam et dignissim
                condimentum, orci risus sollicitudin magna, eu semper lacus quam
                non nisl. Duis viverra efficitur orci eget varius. Mauris
                lobortis magna ac quam tincidunt, lobortis viverra ligula
                pellentesque. Etiam sed velit molestie, varius sapien vitae,
                ultricies erat. Curabitur pulvinar orci felis, ac elementum nisi
                pharetra nec.
              </p>

              <p>
                Integer lacinia sit amet risus et ultrices. Pellentesque
                vulputate neque in tortor interdum facilisis. Vivamus
                sollicitudin pellentesque purus, eget blandit ligula scelerisque
                ac. Suspendisse tempor, magna vel facilisis fermentum, tortor
                augue condimentum justo, at placerat ligula nulla eu orci. Nunc
                vel tempus lectus. Ut auctor nisi sed diam sagittis, eget tempus
                erat sollicitudin. Nullam vitae pretium massa. Curabitur sit
                amet suscipit leo. Aenean lacinia nisi suscipit sollicitudin
                ullamcorper. Donec faucibus justo mi, nec cursus metus bibendum
                ut. In vulputate et massa vel convallis. Curabitur elementum vel
                purus.
              </p>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque
                molestie lobortis luctus. Quisque luctus eros vulputate ex
                tristique, in pellentesque diam viverra. Vivamus congue
                porttitor leo in posuere. Mauris imperdiet et erat at ornare.
                Vestibulum ante ipsum primis in faucibus orci luctus et ultrices
                posuere cubilia Curae; Aliquam erat volutpat. Etiam sed ante
                egestas, aliquet justo quis, aliquet justo. Curabitur molestie
                mi vel maximus pretium. Mauris accumsan odio et lorem dapibus
                sagittis. Praesent faucibus blandit risus, in hendrerit nisl
                consequat at. In hac habitasse platea dictumst. Morbi ligula
                est, suscipit vel lacinia id, molestie at urna. Duis viverra sed
                orci ac congue. Pellentesque ut viverra massa. Ut commodo eros
                eget eros bibendum venenatis. Quisque et pulvinar sem. Nunc id
                semper ipsum. Nulla et vestibulum turpis. In pellentesque
                elementum tortor pretium pharetra. In gravida velit finibus,
                mattis urna et, blandit mi. In hac habitasse platea dictumst.
              </p>

              <p>
                Sed interdum iaculis dui at laoreet. Nulla tempus odio mattis,
                porta neque vel, malesuada quam. Aenean imperdiet et quam id
                porta. Integer fringilla, urna quis feugiat varius, enim eros
                pretium sapien, ac placerat odio mauris id erat. Vestibulum
                bibendum fermentum eros at volutpat. Aliquam imperdiet euismod
                scelerisque. Mauris semper lectus sed quam tincidunt venenatis.
                Duis dui felis, posuere non velit vitae, mollis egestas eros.
                Phasellus nec velit augue. Integer vel justo sit amet nisl
                dapibus tempor ac quis neque. Nulla semper quis sem ac
                malesuada. Ut congue sodales mauris, a elementum libero
                imperdiet ut.
              </p>

              <p>
                Donec at tincidunt ante. Integer congue congue diam, nec
                hendrerit libero vulputate ut. Maecenas in felis imperdiet,
                varius neque a, mollis diam. Nulla vitae luctus lectus. Cras
                nibh velit, auctor nec dolor sit amet, condimentum blandit ante.
                Sed sit amet faucibus risus, ut tristique orci. Nullam pretium
                pharetra accumsan. Curabitur vitae ante nec orci accumsan
                imperdiet. Sed ornare vitae purus quis molestie. Cras a velit
                sit amet magna eleifend fermentum. Quisque interdum nisl ac est
                tempus sodales. Nam ornare sem non risus vestibulum, vitae
                condimentum dui blandit. Donec congue sodales enim eget sodales.
                Suspendisse in leo vel nulla consequat posuere a eu purus.
              </p>

              <p>
                Vivamus porta magna vitae eros facilisis pellentesque. Nam elit
                justo, aliquet vitae efficitur non, consequat sed turpis.
                Quisque vel ultrices mi. Sed tincidunt, quam et dignissim
                condimentum, orci risus sollicitudin magna, eu semper lacus quam
                non nisl. Duis viverra efficitur orci eget varius. Mauris
                lobortis magna ac quam tincidunt, lobortis viverra ligula
                pellentesque. Etiam sed velit molestie, varius sapien vitae,
                ultricies erat. Curabitur pulvinar orci felis, ac elementum nisi
                pharetra nec.
              </p>

              <p>
                Integer lacinia sit amet risus et ultrices. Pellentesque
                vulputate neque in tortor interdum facilisis. Vivamus
                sollicitudin pellentesque purus, eget blandit ligula scelerisque
                ac. Suspendisse tempor, magna vel facilisis fermentum, tortor
                augue condimentum justo, at placerat ligula nulla eu orci. Nunc
                vel tempus lectus. Ut auctor nisi sed diam sagittis, eget tempus
                erat sollicitudin. Nullam vitae pretium massa. Curabitur sit
                amet suscipit leo. Aenean lacinia nisi suscipit sollicitudin
                ullamcorper. Donec faucibus justo mi, nec cursus metus bibendum
                ut. In vulputate et massa vel convallis. Curabitur elementum vel
                purus.
              </p>
            </Typography>
          </Grid>
        </Grid>
      </div>
    )
  }

  return <Layout {...props} content={content()} />
}

export default TermsOfUseScreen
