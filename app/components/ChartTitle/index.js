import React from 'react'
import styles from '../ChartTitle/styles.css'

/* eslint react/prop-types: 0 */

const ChartTitle = ({ children }) => (
  <p className={styles.chartsTitle}>{children}</p>
)

export default ChartTitle
