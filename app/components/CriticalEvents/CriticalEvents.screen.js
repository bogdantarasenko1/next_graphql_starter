// @flow

import React from 'react'
import {
  Table,
  TableHead,
  TableBody,
  TableRow,
  TableCell
} from '@material-ui/core'
import styles from './styles.css'

type Props = {
  data: [],
  sortData: (key: string) => void,
  sortDirection: string
};

const CriticalEventsScreen = (props: Props) => {
  const { data, sortData, sortDirection } = props

  const sortIconClass = () => {
    const cssClass = ['material-icons', styles.sortIcon]
    if (sortDirection === 'desc') {
      cssClass.push(styles.sortDesc)
    }
    return cssClass.join(' ')
  }

  const content = () => (
    <React.Fragment>
      <div className={styles.header}>
        <h3 className={styles.title}>Critical Events</h3>
      </div>
      <div className={styles.tableContainer}>
        <Table className={`table-dark ${styles.table}`}>
          <TableHead>
            <TableRow>
              <TableCell padding="checkbox" />
              <TableCell padding="checkbox">When</TableCell>
              <TableCell padding="checkbox" onClick={() => sortData('coin')}>
                <div className={styles.iconContainer}>
                  Coin&nbsp;<i className={sortIconClass()}>filter_list</i>
                </div>
              </TableCell>
              <TableCell padding="dense">Event</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {data.map((item, index) => {
              return (
                <TableRow key={index}>
                  <TableCell padding="checkbox">
                    {item.sign ? <i className={styles.sign} /> : null}
                  </TableCell>
                  <TableCell padding="checkbox">{item.date}</TableCell>
                  <TableCell padding="checkbox">{item.coin}</TableCell>
                  <TableCell padding="checkbox">{item.event}</TableCell>
                </TableRow>
              )
            })}
          </TableBody>
        </Table>
      </div>
    </React.Fragment>
  )

  return content()
}

export default CriticalEventsScreen
