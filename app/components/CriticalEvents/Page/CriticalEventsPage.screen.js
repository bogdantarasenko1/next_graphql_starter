// @flow

import React from 'react'
import Layout from '../../MainLayout/MainLayout.screen'
import CriticalEvents from '../CriticalEvents.container'
import styles from '../styles.css'

const CriticalEventsPageScreen = () => {
  const content = () => {
    return (
      <div className={styles.container}>
        <CriticalEvents/>
      </div>
    )
  }

  return (
    <Layout content={content()} />
  )
}

export default CriticalEventsPageScreen
