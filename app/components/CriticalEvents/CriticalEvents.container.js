// @flow

import React from 'react'
import CriticalEvents from './CriticalEvents.screen'
import events from './events.json'

type SortDirection = {
  asc: 'asc',
  desc: 'desc'
}

type State = {
  sort: SortDirection,
  events: []
}

type Props = {

}

class CriticalEventsContainer extends React.PureComponent<Props, State> {
  props: Props
  state: State

  constructor (props) {
    super(props)

    const { data } = events
    this.state = {
      sort: 'asc',
      events: data
    }
  }

  compareAscBy (key) {
    return function (a, b) {
      if (a[key] < b[key]) return -1
      if (a[key] > b[key]) return 1
      return 0
    }
  }

  compareDescBy (key) {
    return function (a, b) {
      if (a[key] < b[key]) return 1
      if (a[key] > b[key]) return -1
      return 0
    }
  }

  sortBy = (key) => {
    let arrayCopy = [...this.state.events]
    switch (this.state.sort) {
      case 'asc':
        arrayCopy.sort(this.compareAscBy(key))
        this.setState({ events: arrayCopy, sort: 'desc' })
        break
      case 'desc':
        arrayCopy.sort(this.compareDescBy(key))
        this.setState({ events: arrayCopy, sort: 'asc' })
        break
    }
  }

  render () {
    return (<CriticalEvents data={this.state.events} sortDirection={this.state.sort} sortData={this.sortBy} />)
  }
}

export default CriticalEventsContainer
