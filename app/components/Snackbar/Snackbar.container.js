// @flow

import React from 'react'
import Snackbar from './Snackbar.screen'

type State = {
  open: boolean,
  vertical: string,
  horizontal: string
};

type Props = {
  message: any
};

class SnackbarContainer extends React.PureComponent<Props, State> {
  props: Props;
  state: State;

  constructor (props: Props) {
    super(props)

    this.state = {
      open: !!props.message,
      vertical: 'bottom',
      horizontal: 'right'
    }
  }

  componentDidUpdate (props, state) {
    if (!state.open && !!this.props.message) {
      this.setState({ open: true })
    }
  }

  onClose = () => {
    this.setState({ open: false })
  };

  render () {
    const { message } = this.props
    if (!message) {
      return null
    }

    const error = Array.isArray(message) ? message[0] : message
    const text = JSON.stringify(error)
    const { open, vertical, horizontal } = this.state
    return (
      <Snackbar
        open={open}
        vertical={vertical}
        horizontal={horizontal}
        message={text}
        close={this.onClose}
      />
    )
  }
}

export default SnackbarContainer
