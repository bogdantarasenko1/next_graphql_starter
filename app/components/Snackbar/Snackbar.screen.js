// @flow

import React from 'react'
import { Snackbar } from '@material-ui/core'
import CloseIcon from '@material-ui/icons/Close'
import IconButton from '@material-ui/core/IconButton'

type Props = {
  message: any,
  open: boolean,
  close: () => void,
  vertical: string,
  horizontal: string
};

const SnackbarScreen = (props: Props) => {
  const { open, close, message, horizontal, vertical } = props
  return (
    <Snackbar
      open={open}
      onClose={close}
      anchorOrigin={{ vertical, horizontal }}
      message={message}
      action={
        <IconButton color="inherit" onClick={close}>
          <CloseIcon />
        </IconButton>
      }
    />
  )
}

export default SnackbarScreen
