import React, { Fragment } from 'react'
import styles from '../ChartDivideLine/styles.css'

const ChartDivideLine = () => (
  <Fragment>
    <div className={styles.divideLine} />
  </Fragment>
)

export default ChartDivideLine
