// @flow

import React from 'react'
import styles from '../ChartSize/styles.css'

const sizes = ['small', 'medium', 'large']

class ChartSize extends React.Component<State, Props> {
  render () {
    const { title, size, handleSize } = this.props
    return (
      <div className={styles.chartsContainer}>
        <div className={styles.chartsSizeContainer}>
          {title}
          <div className={styles.chartsSizeButtons}>
            {sizes.map((content, key) => {
              const chartButtonClass =
                size === content
                  ? 'chartsSizeButtonActive'
                  : 'chartsSizeButton'
              return (
                <div
                  key={key}
                  className={styles[chartButtonClass]}
                  onClick={() => handleSize(content)}
                >
                  {content}
                </div>
              )
            })}
          </div>
        </div>
      </div>
    )
  }
}

export default ChartSize
