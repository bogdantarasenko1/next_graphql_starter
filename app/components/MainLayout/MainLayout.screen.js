// @flow

import React from 'react'
import { Grid, withStyles } from '@material-ui/core'
import withWidth, { isWidthUp } from '@material-ui/core/withWidth'
import { withRouter } from 'next/router'
import AppBar from '../AppBar/AppBar.container'
import UserMenuContainer from '../UserInfo/Menu/UserInfoMenu.container'
import UserInfoMenu from '../UserInfo/Menu/UserInfoMenu.screen'
import UserInfoContainer from '../UserInfo/UserInfo.container'
import Footer from '../Footer/Footer.screen'

type Props = {
  content: React.Element<Any>,
  classes: Object,
  router: Object,
  user?: Object,
  width: string
};

const styles = theme => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    flexGrow: 1,
    backgroundColor: theme.palette.background.default,
    position: 'relative'
  },
  wrapper: {
    flexGrow: 1,
    padding: '1rem'
  }
})

const menuItems = [
  {
    link: '/dashboard',
    title: 'Dashboard',
    isSelected: false
  },
  {
    link: '/community',
    title: 'Community',
    isSelected: false
  },
  {
    link: '/cryptoassets',
    title: 'Cryptoassets',
    isSelected: false
  },
  {
    link: '/coin/btc',
    title: 'Coin View',
    isSelected: false
  },
  {
    link: '/research',
    title: 'Research',
    isSelected: false
  },
  {
    link: '/screener',
    title: 'Screener',
    isSelected: false
  },
  {
    link: '/analytics',
    title: 'Analytics',
    isSelected: false
  }
]

const MainLayoutScreen = (props: Props) => {
  const {
    content,
    classes,
    router: { route },
    ...other
  } = props

  menuItems.forEach(item => {
    item.isSelected = item.link.startsWith(route) // coin link is not always leads to /btc
  })

  const userInfoMenu = config => {
    const { width, user, saveAuth } = props
    if (!user) return null

    if (isWidthUp('md', width)) {
      return <UserInfoContainer saveAuth={saveAuth} {...other} />
    } else {
      return (
        <UserInfoMenu
          userProfile={config.onEditProfile}
          logout={config.logoutUser}
          {...other}
        />
      )
    }
  }

  return (
    <div className={classes.root}>
      <UserMenuContainer {...other}>
        {userInfo => (
          <React.Fragment>
            <AppBar
              {...other}
              menuItems={menuItems}
              profileMenu={userInfoMenu(userInfo)}
              route={route}
            />
            <Grid container className={classes.wrapper}>
              <Grid item xs={12}>
                {content}
              </Grid>
            </Grid>
            <Footer {...other} />
          </React.Fragment>
        )}
      </UserMenuContainer>
    </div>
  )
}

export default withRouter(withStyles(styles)(withWidth()(MainLayoutScreen)))
