// @flow

import React from 'react'
import Link from 'next/link'
import { MenuList, MenuItem, withStyles, Divider } from '@material-ui/core'
import { LogoImg } from '../ui/logo'

const stylesMUI = theme => ({
  drawer: {
    width: 250
  },
  logo: {
    display: 'block',
    padding: '7px 16px 6px'
  },
  link: {
    textDecoration: 'none'
  },
  menuItem: {
    '&$selected': {
      backgroundColor: theme.palette.secondary.background
    }
  },
  selected: {
    '& $link': {
      color: theme.palette.secondary.main
    }
  }
})

type Props = {
  t: func,
  menuItems: [],
  classes: Object,
  rightContent: React.ReactNode,
  homeLink: string
};

const AppBarDrawerScreen = (props: Props) => {
  const { classes, t, menuItems, rightContent, homeLink } = props

  return (
    <div className={classes.drawer}>
      <Link href={homeLink}>
        <a className={classes.logo} href={homeLink}>
          <LogoImg />
        </a>
      </Link>
      <MenuList className={classes.list}>
        <Divider />
        {menuItems.map((item, index) => (
          <MenuItem
            key={index}
            selected={item.isSelected}
            className={classes.listItemRoot}
            classes={{ root: classes.menuItem, selected: classes.selected }}
          >
            <Link href={item.link}>
              <a className={classes.link} href={item.link} {...item.props}>
                {t(`common:${item.title}`)}
              </a>
            </Link>
          </MenuItem>
        ))}
        <Divider />
        {rightContent}
      </MenuList>
    </div>
  )
}

export default withStyles(stylesMUI)(AppBarDrawerScreen)
