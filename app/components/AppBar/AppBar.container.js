// @flow

import React from 'react'
import Link from 'next/link'
import {
  AppBar,
  Toolbar,
  withStyles,
  Drawer,
  IconButton,
  Hidden,
  MenuItem
} from '@material-ui/core'
import { isWidthUp } from '@material-ui/core/withWidth'
import classNames from 'classnames'
import MenuIcon from '@material-ui/icons/Menu'
import SearchBar from '../Search/SearchBar/SearchBar.container'
import { LogoImgUnderlined } from '../ui/logo'
// import Feedback from '../Feedback/Feedback.container'
import AppBarDrawer from './AppBarDrawer.screen'
import AppBarNav from './AppBarNav.screen'

const stylesMUI = theme => ({
  toolBarRoot: {
    minHeight: '48px'
  },
  listRoot: {
    display: 'inline-flex',
    flexGrow: 1,
    marginLeft: '100px'
  },
  listItemRoot: {
    paddingTop: '2px',
    paddingBottom: '2px'
  },
  menuItem: {
    '&$selected $link': {
      color: theme.palette.text.primary,
      textDecoration: 'none'
    }
  },
  selected: {},
  link: {
    color: theme.palette.text.hint,
    fontSize: '0.9rem',
    '&:hover': {
      color: theme.palette.text.primary,
      textDecoration: 'none'
    }
  },
  menuButton: {
    marginLeft: '-12px',
    color: theme.palette.text.primary
  },
  search: {
    marginRight: 30,
    flexGrow: 1
  }
})

const HOME_LINK = '/dashboard'

type Props = {
  t: func,
  user: Object,
  menuItems: [],
  profileMenu?: React.ReactNode,
  classes: Object,
  width: string,
  route: string
};

type State = {
  drawer: boolean
};

class AppBarScreen extends React.PureComponent<Props, State> {
  props: Props;
  state: State;

  constructor (props) {
    super(props)
    this.state = {
      drawer: false
    }
  }

  SignUpLink = () => {
    const { width, classes, t } = this.props

    const link = (
      <Link href="/signup">
        <a
          className={classNames({ [classes.link]: isWidthUp('md', width) })}
          href="/signup"
        >
          {t('common:Sign Up')}
        </a>
      </Link>
    )

    return isWidthUp('md', width) ? link : <MenuItem>{link}</MenuItem>
  };

  rightContent = () => {
    const { profileMenu } = this.props
    return profileMenu || this.SignUpLink()
  };

  toggleDrawer = open => () => {
    this.setState({
      drawer: open
    })
  };

  render () {
    return (
      <AppBar position="sticky">
        <Toolbar className={this.props.classes.toolBarRoot}>
          <Hidden mdUp>
            <IconButton
              color="inherit"
              aria-label="Open drawer"
              onClick={this.toggleDrawer(true)}
              className={this.props.classes.menuButton}
            >
              <MenuIcon />
            </IconButton>
          </Hidden>
          <Link href={HOME_LINK}>
            <a href={HOME_LINK}>
              <LogoImgUnderlined />
            </a>
          </Link>
          <Hidden smDown>
            <AppBarNav t={this.props.t} menuItems={this.props.menuItems} />
            <div className={this.props.classes.search}>
              <SearchBar t={this.props.t} />
            </div>
          </Hidden>
          {/* <MenuItem>
            <Feedback
              t={this.props.t}
              user={this.props.user}
              route={this.props.route}
            />
          </MenuItem> */}
          <Hidden smDown>{this.rightContent()}</Hidden>
        </Toolbar>
        <Drawer open={this.state.drawer} onClose={this.toggleDrawer(false)}>
          <div
            tabIndex={0}
            role="button"
            onClick={this.toggleDrawer(false)}
            onKeyDown={this.toggleDrawer(false)}
          >
            <AppBarDrawer
              t={this.props.t}
              menuItems={this.props.menuItems}
              rightContent={this.rightContent()}
              homeLink={HOME_LINK}
              user={this.props.user}
            />
          </div>
        </Drawer>
      </AppBar>
    )
  }
}

export default withStyles(stylesMUI)(AppBarScreen)
