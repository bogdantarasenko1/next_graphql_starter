// @flow

import React from 'react'
import { MenuList, MenuItem, withStyles } from '@material-ui/core'
import { Link } from '../../../lib/routes'

const stylesMUI = theme => ({
  listRoot: {
    display: 'inline-flex',
    flexGrow: 1,
    marginLeft: '100px',
    padding: 0
  },
  menuItem: {
    paddingTop: 0,
    paddingBottom: 0,
    height: '48px',
    borderBottom: '3px solid transparent',
    '&:hover': {
      background: 'transparent'
    },
    '&$selected': {
      borderColor: theme.palette.secondary.main,
      backgroundColor: theme.palette.secondary.background
    },
    '&$selected:hover': {
      backgroundColor: theme.palette.secondary.background
    }
  },
  selected: {
    '& $link': {
      color: theme.palette.secondary.main
    }
  },
  link: {
    color: theme.palette.text.hint,
    fontSize: '0.9rem',
    textDecoration: 'none',
    '&:hover': {
      color: theme.palette.text.primary
    }
  }
})

type Props = {
  t: func,
  menuItems: [],
  classes: Object
};

const AppBarNav = (props: Props) => {
  const { classes, t, menuItems } = props

  return (
    <MenuList className={classes.listRoot}>
      {menuItems.map((item, index) => (
        <Link key={index} href={item.link}>
          <MenuItem
            selected={item.isSelected}
            classes={{
              root: classes.menuItem,
              selected: classes.selected
            }}
          >
            <a className={classes.link} href={item.link} {...item.props}>
              {t(`common:${item.title}`)}
            </a>
          </MenuItem>
        </Link>
      ))}
    </MenuList>
  )
}

export default withStyles(stylesMUI)(AppBarNav)
