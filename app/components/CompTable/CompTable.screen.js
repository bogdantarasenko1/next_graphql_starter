// @flow

import React from 'react'
import Cryptoassets from '../../components/Cryptoassets/Cryptoassets.container.js'
import { compTableTabItems } from '../../components/Cryptoassets/headerTabItems'

type Props = {
  t: func,
  code: string
};

const CompTableScreen = (props: Props) => {
  return (
    <Cryptoassets
      headerTabItems={compTableTabItems()}
      primaryAsset={props.code.toUpperCase()}
      title="Comp Table"
      {...props}
    />
  )
}

export default CompTableScreen
