// @flow

import React from 'react'
import { Grid, withStyles } from '@material-ui/core'
import Layout from '../MainLayout/MainLayout.screen'
import BlockWithHeader from '../BlockWithHeader/BlockWithHeader.screen.js'
import TutorialPaper from './TutorialPaper.sceen'

type Props = {
  t: func,
  tutorials: [],
  classes: Object
};

const styles = theme => ({
  container: {
    paddingTop: '2rem'
  }
})

const MetricsTutorials = (props: Props) => {
  const { tutorials, classes, t, ...other } = props
  const content = () => {
    return (
      <div className={classes.container}>
        <Grid container justify="center">
          <Grid item xs={8}>
            <BlockWithHeader title={t('common:Metrics Tutorials')}>
              <Grid container spacing={16}>
                {tutorials.map((item, index) => (
                  <Grid key={index} item xs={4}>
                    <TutorialPaper {...item} />
                  </Grid>
                ))}
              </Grid>
            </BlockWithHeader>
          </Grid>
        </Grid>
      </div>
    )
  }

  return <Layout {...other} t={t} content={content()} />
}

export default withStyles(styles)(MetricsTutorials)
