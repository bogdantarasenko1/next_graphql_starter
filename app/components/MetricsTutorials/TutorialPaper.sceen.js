// @flow

import React from 'react'
import { Typography, Paper, withStyles } from '@material-ui/core'

type Props = {
  header: string,
  text: string,
  classes: Object
};

const styles = theme => ({
  root: {
    ...theme.mixins.gutters(),
    paddingTop: theme.spacing.unit * 2,
    paddingBottom: theme.spacing.unit * 2
  }
})

const TutorialPaper = (props: Props) => {
  const { header, text, classes } = props

  return (
    <Paper className={classes.root} elevation={1}>
      <Typography variant="headline" component="h3">
        {header}
      </Typography>
      <Typography component="p">{text}</Typography>
    </Paper>
  )
}

export default withStyles(styles)(TutorialPaper)
