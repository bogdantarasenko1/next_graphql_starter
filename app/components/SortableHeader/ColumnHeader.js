// @flow

import React from 'react'
import { TableCell, withStyles } from '@material-ui/core'
import classnames from 'classnames'

type Props = {
  id: string,
  text: string,
  sortDirection: ?string,
  sorted: ?boolean,
  sortable: boolean,
  onClick: (id: string) => void,
  classes: Object,
  frozen: boolean,
  faded: boolean,
  noText?: boolean,
  colSpan?: number
};

type State = {
  showIcon: boolean
};

const styles = theme => ({
  container: {
    display: 'flex',
    alignItems: 'center'
  },
  columnHeader: {
    height: '82px',
    lineHeight: '1.5',
    padding: '0 5px'
  },
  sortAsc: {
    transform: 'rotate(180deg)'
  },
  pointer: {
    '&:hover': {
      cursor: 'pointer'
    }
  },
  iconContainer: {
    display: 'flex',
    height: '24px',
    width: '40px'
  },
  frozen: {
    position: 'absolute',
    left: 0,
    width: '115px',
    lineHeight: '82px',
    padding: '0 6px 0 45px',
    '&:after': {
      content: '""',
      position: 'absolute',
      top: 0,
      right: '-20px',
      height: '82px',
      width: '20px',
      display: 'block',
      background:
        'linear-gradient(to right, rgba(42,42,42,1) 0%,rgba(42,42,42,0) 100%)',
      filter:
        'progid:DXImageTransform.Microsoft.gradient( startColorstr="#2A2A2A", endColorstr="#002A2A2A",GradientType=1 )' /* IE6-9 */,
      [theme.breakpoints.down('sm')]: {
        background:
          'linear-gradient(to right, rgba(38,38,37,1) 0%,rgba(38,38,37,0) 100%)',
        filter:
          'progid:DXImageTransform.Microsoft.gradient( startColorstr="#262625", endColorstr="#00262625",GradientType=1 )' /* IE6-9 */
      }
    }
  },
  faded: {
    paddingLeft: '30px'
  }
})

class ColumnHeader extends React.PureComponent<Props, State> {
  constructor (props) {
    super(props)
    this.state = {
      showIcon: this.columnSorted()
    }
  }

  componentDidUpdate (prevProps) {
    if (prevProps.sorted !== null && this.props.sorted === null) {
      this.setState({
        showIcon: false
      })
    }
  }

  mouseEnter = () => {
    if (this.columnSorted()) return
    this.setState({ showIcon: true })
  };

  mouseLeave = () => {
    if (this.columnSorted()) return
    this.setState({ showIcon: false })
  };

  click = () => {
    const { id, onClick } = this.props
    onClick(id)
  };

  columnSorted = () => {
    const { sorted } = this.props
    return sorted
  };

  iconClass = () => {
    const { sortDirection, sorted, classes } = this.props
    return classnames('material-icons', classes.pointer, {
      [classes.sortAsc]: sortDirection === 'asc' && sorted
    })
  };

  render () {
    const {
      text,
      sortable,
      classes,
      frozen,
      faded,
      noText,
      colSpan
    } = this.props
    const { showIcon } = this.state

    if (sortable) {
      return (
        <TableCell
          colSpan={colSpan}
          onClick={this.click}
          onMouseEnter={this.mouseEnter}
          onMouseLeave={this.mouseLeave}
          className={`${classes.columnHeader} ${faded ? classes.faded : ''}`}
        >
          <div className={classes.container}>
            {noText ? '' : text}
            <span className={classes.iconContainer}>
              {showIcon && <i className={this.iconClass()}>filter_list</i>}
            </span>
          </div>
        </TableCell>
      )
    } else {
      return (
        <TableCell
          colSpan={colSpan}
          className={`${classes.columnHeader} ${
            frozen ? classes.frozen : faded ? classes.faded : ''
          }`}
        >
          {noText ? '' : text}
        </TableCell>
      )
    }
  }
}

export default withStyles(styles)(ColumnHeader)
