// @flow

import React from 'react'
import { TableRow, TableHead, TableCell } from '@material-ui/core'
import ColumnHeader from './ColumnHeader'

type Props = {
  numbers?: boolean,
  columns: [],
  sortedField: string,
  sortData: (key: string) => void,
  sortDirection: string
};

const SortableHeaderScreen = (props: Props) => {
  const { sortDirection, sortedField, numbers } = props
  const { columns, sortData } = props

  const columnSortedInDirection = col => {
    if (col === sortedField) {
      return !!sortDirection
    }
    return null
  }

  return (
    <TableHead>
      <TableRow>
        {numbers && <TableCell>#</TableCell>}
        {columns.map(
          (item, index) =>
            item ? (
              <ColumnHeader
                key={index}
                id={item.key}
                text={item.text}
                sortable={item.sortable}
                sorted={columnSortedInDirection(item.key)}
                sortDirection={sortDirection}
                onClick={sortData}
                frozen={item.frozen}
                faded={item.faded}
                noText={item.noText}
                colSpan={item.colSpan}
              />
            ) : (
              <TableCell key={index} />
            )
        )}
      </TableRow>
    </TableHead>
  )
}

export default SortableHeaderScreen
