// @flow

import React from 'react'
import {
  Card,
  CardHeader,
  CardContent,
  CardActions,
  Typography,
  LinearProgress,
  withStyles
} from '@material-ui/core'
import classnames from 'classnames'
import { LogoImgUnderlined } from '../ui/logo'
import styles from './styles.css'

type Props = {
  isLoading?: boolean,
  classes: Object,
  onSubmit: () => void,
  title: React.ReactNode,
  inputs: React.ReactNode,
  actions: React.ReactNode
};

const stylesMUI = theme => ({
  card: {
    backgroundColor: 'transparent'
  },
  textHeader: {
    color: 'white',
    textAlign: 'left',
    fontSize: '1.25rem',
    margin: '15px 20px 20px 0px'
  },
  cardActionsRoot: {
    justifyContent: 'space-between',
    padding: '1.5rem'
  },
  container: {
    backgroundColor: theme.palette.background.default
  },
  action: {
    backgroundColor: theme.palette.background.header,
    position: 'relative'
  },
  progress: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0
  }
})

const AuthCardScreen = (props: Props) => {
  const { isLoading, classes, title, actions, inputs, onSubmit } = props

  const submit = e => {
    e.preventDefault()
    onSubmit()
  }

  const header = (
    <Typography component="p" className={classes.textHeader}>
      {title}
    </Typography>
  )

  return (
    <div className={classnames(classes.container, styles.container)}>
      <div className={classnames(classes.action, styles.action)}>
        {isLoading && (
          <LinearProgress className={classes.progress} color="secondary" />
        )}
        <form onSubmit={submit}>
          <Card square elevation={0} className={classes.card}>
            <div className={styles.logoContainer}>
              <LogoImgUnderlined />
            </div>
            <CardHeader title={header} />
            <CardContent>{inputs}</CardContent>
            <CardActions classes={{ root: classes.cardActionsRoot }}>
              {actions}
            </CardActions>
          </Card>
        </form>
      </div>
    </div>
  )
}

export default withStyles(stylesMUI)(AuthCardScreen)
