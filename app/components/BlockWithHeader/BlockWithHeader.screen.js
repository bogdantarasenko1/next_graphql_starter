// @flow

import React from 'react'
import classnames from 'classnames'
import { withStyles } from '@material-ui/core'
import DashboardHeader from '../Dashboard/DashboardHeader.screen.js'

type Props = {
  title: React.ReactNode,
  content?: React.ReactNode,
  children?: React.ReactNode,
  classes: Object,
  disableGutters?: boolean
};

const styles = theme => ({
  content: {
    backgroundColor: theme.palette.background.paper,
    [theme.breakpoints.down('sm')]: {
      backgroundColor: 'transparent'
    }
  },
  gutters: {
    padding: theme.spacing.unit * 2,
    [theme.breakpoints.down('sm')]: {
      padding: 0,
      paddingTop: theme.spacing.unit * 2
    }
  }
})

const BlockWithHeader = (props: Props) => {
  const { title, content, disableGutters, classes, children } = props
  const gutters = disableGutters || classes.gutters
  return (
    <React.Fragment>
      <DashboardHeader title={title} />
      <div className={classnames(classes.content, gutters)}>
        {content || children}
      </div>
    </React.Fragment>
  )
}

export default withStyles(styles)(BlockWithHeader)
