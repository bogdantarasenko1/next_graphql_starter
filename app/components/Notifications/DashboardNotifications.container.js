import React from 'react'
import ReactMaterialUiNotifications from 'react-materialui-notifications'
import {MuiThemeProvider} from "@material-ui/core/styles";
import JssProvider from "react-jss/lib/JssProvider";
import {getTheme} from "../../utils/userTheme";
import getPageContext from "../../../lib/getPageContext";

class DashboardNotifications extends React.Component<State, Props> {

  constructor (props) {
    super(props)
    // this.state = {
    //
    // }

    // this.fetchDashboards        = this.fetchDashboards.bind(this)

    const theme = getTheme() // use default Dark theme since switching themes is disabled
    this.pageContext = getPageContext(theme)
  }

  showNotification(){
    console.log('!-- showNotification');
    ReactMaterialUiNotifications.showNotification({
      title: 'Title',
      additionalText: `Some message to be displayed`,
      // icon: <Message />,
      // iconBadgeColor: deepOrange500,
      overflowText: "joe@gmail.com",
      timestamp: '12-31-2019'
    })
    // update notifications count

  }

  componentDidMount(){
    this.showNotification()
  }

  render() {
    return (
      <MuiThemeProvider
        theme={this.pageContext.theme}
        muiTheme={this.pageContext.theme}
        sheetsManager={this.pageContext.sheetsManager}
      >
        <ReactMaterialUiNotifications
          desktop={true}
          transitionName={{
            leave: 'dummy',
            leaveActive: 'fadeOut',
            appear: 'dummy',
            appearActive: 'zoomInUp'
          }}
          transitionAppear={true}
          transitionLeave={true}
        />
      </MuiThemeProvider>

    )
  }

}


export default DashboardNotifications
