// @flow

import React from 'react'
import {
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow
} from '@material-ui/core'

type Props = {
  fetchingData: boolean,
  marketcap: ?number,
  currentSupply: ?number,
  tradeVolume24h: ?number,
  currentPrice: ?number
};

const CoinmarketcapScreen = (props: Props) => {
  const {
    fetchingData,
    marketcap,
    currentSupply,
    tradeVolume24h,
    currentPrice
  } = props
  return fetchingData ? null : (
    <Table>
      <TableHead>
        <TableRow>
          <TableCell>Current Marketcap</TableCell>
          <TableCell>Current Supply</TableCell>
          <TableCell>24hr Trade Volume</TableCell>
          <TableCell>Current Price</TableCell>
        </TableRow>
      </TableHead>
      <TableBody>
        <TableRow>
          <TableCell>{marketcap}</TableCell>
          <TableCell>{currentSupply}</TableCell>
          <TableCell>{tradeVolume24h}</TableCell>
          <TableCell>{currentPrice}</TableCell>
        </TableRow>
      </TableBody>
    </Table>
  )
}

export default CoinmarketcapScreen
