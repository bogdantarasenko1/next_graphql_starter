// @flow

import React from 'react'
import numbro from 'numbro'
import axios from 'axios'
import { fetchMarketCap, fetchPrice } from '../../utils/dataPlatformQuery'
import Coinmarketcap from './Coinmarketcap.screen'

type State = {
  fetchingData: boolean,
  data: []
};

type Props = {
  code: string
};

class CoinmarketcapContainer extends React.Component<State, Props> {
  state: State;
  props: Props;

  marketCapToken = axios.CancelToken.source();
  currentPriceToken = axios.CancelToken.source();

  constructor (props) {
    super(props)
    this.state = {
      isLoading: true,
      data: {},
      price: null
    }
  }

  componentDidMount () {
    const { code } = this.props
    const symbol = code.toUpperCase()

    fetchMarketCap(symbol, this.marketCapToken.token).then(data => {
      this.setState({
        data: Object.assign(
          { name: data.name, symbol: data.symbol },
          data.metrics
        ),
        isLoading: false
      })
    })

    fetchPrice(`USDT_${symbol}`, this.currentPriceToken.token).then(price => {
      this.setState({
        price: price,
        isLoading: false
      })
    })
  }

  componentWillUnmount () {
    this.marketCapToken.cancel('marketCapToken query terminated')
    this.currentPriceToken.cancel('currentPriceToken query terminated')
  }

  marketcap () {
    const {
      data: { marketCap }
    } = this.state
    if (marketCap) {
      return numbro(marketCap).formatCurrency({ mantissa: 2 })
    }
    return null
  }

  currentSupply () {
    const {
      data: { maxSupply }
    } = this.state
    if (maxSupply) {
      return numbro(maxSupply).formatCurrency({ mantissa: 2 })
    }
    return null
  }

  tradeVolume24h () {
    const {
      data: { circulatingSupply }
    } = this.state
    if (circulatingSupply) {
      return numbro(circulatingSupply).formatCurrency({
        mantissa: 2
      })
    }
    return null
  }

  currentPrice () {
    const { price } = this.state
    if (price && price.price) {
      return numbro(price.price).formatCurrency({ mantissa: 2 })
    }
    return null
  }

  render () {
    return (
      <Coinmarketcap
        fetchingData={this.state.isLoading}
        marketcap={this.marketcap()}
        currentSupply={this.currentSupply()}
        tradeVolume24h={this.tradeVolume24h()}
        currentPrice={this.currentPrice()}
      />
    )
  }
}

export default CoinmarketcapContainer
