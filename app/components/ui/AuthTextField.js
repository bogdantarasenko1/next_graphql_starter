// @flow

import React from 'react'
import { TextField, withStyles } from '@material-ui/core'

type Props = {
  classes: Object
};

const styles = theme => ({
  underline: {
    '&:hover:not($disabled):not($focused):not($error)': {
      '&::before': {
        borderColor: theme.palette.secondary.main
      }
    },
    '&::before': {
      borderColor: theme.palette.secondary.main,
      borderWidth: '2px'
    },
    '&::after': {
      borderColor: theme.palette.secondary.main
    }
  },
  disabled: {},
  focused: {},
  error: {},
  labelRoot: {
    color: theme.palette.text.secondary,
    '&$labelShrink': {
      color: theme.palette.text.secondary
    }
  },
  labelShrink: {}
})

const AuthTextField = (props: Props) => {
  const { classes, ...other } = props
  return (
    <TextField
      InputLabelProps={{
        classes: { root: classes.labelRoot, shrink: classes.labelShrink }
      }}
      InputProps={{ classes: { underline: classes.underline } }}
      {...other}
    />
  )
}

export default withStyles(styles)(AuthTextField)
