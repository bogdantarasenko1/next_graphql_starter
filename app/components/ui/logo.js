// @flow

import React from 'react'
import { withStyles } from '@material-ui/core'

type Props = {
  classes: Object
};

const styles = theme => ({
  logo: {
    backgroundImage: `url('/static/Mosaic.svg')`,
    backgroundRepeat: 'no-repeat',
    width: '6.15rem',
    height: '2.1rem',
    display: 'inline-block'
  },
  img: {
    width: '4rem',
    height: '2rem'
  },
  underlined: {
    width: '2rem',
    height: '2rem'
  }
})

const logo = (props: Props) => {
  return <i className={props.classes.logo} />
}

const logoImg = (props: Props) => {
  return <img src="/static/Mosaic.svg" className={props.classes.img} />
}

const logoImgUnderlined = (props: Props) => {
  return (
    <img src="/static/Mosaic-box.svg" className={props.classes.underlined} />
  )
}

export const Logo = withStyles(styles)(logo)
export const LogoImg = withStyles(styles)(logoImg)
export const LogoImgUnderlined = withStyles(styles)(logoImgUnderlined)
