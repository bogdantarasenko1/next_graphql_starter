// @flow

import React from 'react'
import DashboardHeader from '../Dashboard/DashboardHeader.screen.js'
import GrafanaScreen from './Grafana.screen'

class GrafanaContainer extends React.Component<State, Props> {

  render () {
    const { title, data, removeButton = '', onChangePanelDate } = this.props

    return (
      <React.Fragment>
        <div style={{marginBottom: '10px'}}>
          <DashboardHeader
            data={data}
            removeButton={removeButton}
            title={title}
            onChangePanelDate={onChangePanelDate}
          />
          <div style={{ display: 'flex', justifyContent: 'space-between' }}>
            {data.map((item, key) => {
              const { url, width, height, border } = item
              return (
                <GrafanaScreen
                  key={key}
                  url={url}
                  width={width}
                  height={height}
                  border={border}
                />
              )
            })}
          </div>
        </div>
      </React.Fragment>
    )
  }
}

export default GrafanaContainer
