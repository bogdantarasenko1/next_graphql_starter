// @flow

import React from 'react'

type Props = {
  url: string,
  width: string,
  height: string,
  border: string
};

const GrafanaScreen = (props: Props) => {
  const { url, width, height, border } = props

  return (
    <React.Fragment>
      <iframe src={url} width={width} height={height} frameBorder={border} />
    </React.Fragment>
  )
}

export default GrafanaScreen
