export default query => `{
  suggestCrypto(query: "${query}") {
    symbol
    name
    displayName
  }
}`
