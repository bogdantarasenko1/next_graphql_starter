// @flow

import React from 'react'
import { MenuItem } from '@material-ui/core'
import match from 'autosuggest-highlight/match/index'
import parse from 'autosuggest-highlight/parse/index'

type Props = {
  query: string,
  isHighlighted: boolean,
  suggestion: Object,
  onSelect: Object => void
};

const SuggestionScreen = (props: Props) => {
  const { isHighlighted, suggestion, query, onSelect } = props
  const matches = match(suggestion.label, query)
  const parts = parse(suggestion.label, matches)

  return (
    <MenuItem
      selected={isHighlighted}
      component="div"
      onClick={() => onSelect(suggestion)}
    >
      <div>
        {parts.map((part, index) => {
          return part.highlight ? (
            <span key={String(index)} style={{ fontWeight: 500 }}>
              {part.text}
            </span>
          ) : (
            <strong key={String(index)} style={{ fontWeight: 300 }}>
              {part.text}
            </strong>
          )
        })}
      </div>
    </MenuItem>
  )
}

export default SuggestionScreen
