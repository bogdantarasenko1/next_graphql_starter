// @flow

import React from 'react'
import _ from 'lodash'
import { Router } from '../../../../lib/routes'
import SearchBar from './SearchBar.screen'
import suggestionProvider from './suggestionProvider'

type Props = {
  isLoading: boolean,
  query: string => void,
  suggestions: [],
  t: string => string
};

type State = {
  labels: [],
  value: ""
};

function getSuggestions (value, suggestions) {
  return suggestions
}

class SearchBarContainer extends React.Component<Props, State> {
  props: Props;
  state: State;

  constructor (props: Props) {
    super(props)
    this.state = {
      value: '',
      labels: []
    }

    this.queryChanged = _.debounce(this.fetchSuggestions, 300)
  }

  componentDidUpdate (props, state) {
    if (this.props.suggestions !== props.suggestions) {
      const labels = getSuggestions(
        this.state.value,
        this.props.suggestions
      ).map(e => ({ label: e.displayName, symbol: e.symbol }))
      this.setState({ labels })
    }
  }

  componentWillUnmount () {
    this.queryChanged.cancel()
  }

  fetchSuggestions = query => {
    this.props.query(query)
  };

  onFetch = ({ value }) => {
    this.queryChanged(value)
  };

  onClear = () => {
    this.setState({
      labels: []
    })
  };

  onChange = () => (event, { newValue }) => {
    this.setState({
      value: newValue
    })
  };

  mapValue = suggestion => {
    return suggestion.label
  };

  onSelect = suggestion => {
    Router.pushRoute('coinPage', { code: suggestion.symbol.toLowerCase() })
  };

  render () {
    const { labels, value } = this.state
    const { t } = this.props

    const props = {
      suggestions: labels,
      onSelect: this.onSelect,
      onFetch: this.onFetch,
      onClear: this.onClear,
      mapValue: this.mapValue,
      onChange: this.onChange,
      value: value,
      t: t
    }

    return <SearchBar {...props} t={t} />
  }
}

export default suggestionProvider()(SearchBarContainer)
