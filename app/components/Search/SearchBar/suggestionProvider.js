// @flow

import React from 'react'
import { fetchData } from '../../../utils/dataPlatformQuery'
import suggestionQuery from './suggestionQuery'

type Props = {
  query: string => void
};

type State = {
  isLoading: boolean,
  errors: any,
  suggestions: []
};

export const suggestionProvider = () => WrappedComponent => {
  return class SuggestionProvider extends React.PureComponent<Props, State> {
    props: Props;
    state: State;

    constructor (props: Props) {
      super(props)

      this.state = {
        isLoading: false,
        suggestions: []
      }
    }

    fetchData = q => {
      const query = JSON.stringify({
        query: suggestionQuery(q)
      })

      fetchData(query).then(r => {
        const {
          data: { suggestCrypto: data },
          errors
        } = r

        this.setState({ suggestions: data, errors, isLoading: false })
      })
    };

    onQuery = q => {
      this.setState({ isLoading: true, suggestions: [], errors: null })
      this.fetchData(q)
    };

    render () {
      const { isLoading, suggestions } = this.state
      return (
        <WrappedComponent
          {...this.props}
          isLoading={isLoading}
          suggestions={suggestions}
          query={this.onQuery}
        />
      )
    }
  }
}

export default suggestionProvider
