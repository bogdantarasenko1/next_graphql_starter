// @flow

import React from 'react'
import Autosuggest from 'react-autosuggest'
import {
  TextField,
  Paper,
  InputAdornment,
  withStyles
} from '@material-ui/core'
import SearchIcon from '@material-ui/icons/Search'
import Suggestion from './Suggestion.screen'

type Props = {
  onSelect: Object => void,
  suggestions: any,
  onFetch: any,
  onClear: any,
  mapValue: any,
  onChange: any,
  value: any,
  classes: Object,
  t: string => string
};

const styles = theme => ({
  root: {
    backgroundColor: theme.palette.grey[800],
    borderRadius: 4,
    paddingLeft: 8,
    paddingRight: 8
  },
  container: {
    position: 'relative'
  },
  suggestionsContainerOpen: {
    position: 'absolute',
    zIndex: 1,
    marginTop: theme.spacing.unit,
    left: 0,
    right: 0
  },
  suggestion: {
    display: 'block'
  },
  suggestionsList: {
    margin: 0,
    padding: 0,
    listStyleType: 'none'
  }
})

const SearchBarScreen = (props: Props) => {
  const {
    mapValue,
    suggestions,
    onFetch,
    onClear,
    onChange,
    value,
    classes,
    t
  } = props

  const renderSuggestion = (suggestion, { query, isHighlighted }) => {
    return (
      <Suggestion
        onSelect={props.onSelect}
        query={query}
        isHighlighted={isHighlighted}
        suggestion={suggestion}
      />
    )
  }

  const inputComponent = inputProps => {
    const { classes, inputRef = () => {}, ref, ...other } = inputProps

    return (
      <TextField
        fullWidth
        InputProps={{
          inputRef: node => {
            ref(node)
            inputRef(node)
          },
          classes: {
            input: classes.input
          },
          disableUnderline: true,
          endAdornment: (
            <InputAdornment position="end">
              <SearchIcon />
            </InputAdornment>
          )
        }}
        {...other}
      />
    )
  }

  return (
    <div className={classes.root}>
      <Autosuggest
        suggestions={suggestions}
        onSuggestionsFetchRequested={onFetch}
        onSuggestionsClearRequested={onClear}
        getSuggestionValue={mapValue}
        renderInputComponent={inputComponent}
        renderSuggestion={renderSuggestion}
        inputProps={{
          classes,
          placeholder: t('common:Search for coins'),
          value: value,
          onChange: onChange()
        }}
        theme={{
          container: classes.container,
          suggestionsContainerOpen: classes.suggestionsContainerOpen,
          suggestionsList: classes.suggestionsList,
          suggestion: classes.suggestion
        }}
        renderSuggestionsContainer={options => (
          <Paper {...options.containerProps} square>
            {options.children}
          </Paper>
        )}
      />
    </div>
  )
}

export default withStyles(styles)(SearchBarScreen)
