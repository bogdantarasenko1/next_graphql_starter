// @flow

import React from 'react'
import { Tabs, Tab, Typography, withStyles } from '@material-ui/core'

type Props = {
  tabItems: [],
  onSelectTab: (event: Object, value: string) => void,
  selectedTab: string,
  content: React.ReactNode,
  classes: Object
};

const stylesMUI = theme => ({
  root: {
    padding: '.5rem 0',
    [theme.breakpoints.down('sm')]: {
      padding: 0
    }
  },
  tabsRoot: {
    borderBottom: `1px solid ${theme.palette.divider}`
  },
  tabContent: {
    marginTop: '.5rem'
  }
})

const AssetsTabsScreen = (props: Props) => {
  const { selectedTab, tabItems, classes, content } = props

  return (
    <div className={classes.root}>
      <Tabs
        value={selectedTab}
        onChange={props.onSelectTab}
        className={classes.tabsRoot}
        textColor="secondary"
      >
        {tabItems.map(item => (
          <Tab key={item.key} disableRipple label={item.title} />
        ))}
      </Tabs>
      <Typography component="div" className={classes.tabContent}>
        {content}
      </Typography>
    </div>
  )
}

export default withStyles(stylesMUI)(AssetsTabsScreen)
