// @flow

import React, { Component } from 'react'
import _ from 'lodash'
import TabsScreen from './AssetsTabs.screen'

type State = {
  selectedTab: number,
  tabItems: []
};

type Props = {
  selectedTab: number,
  tabItems: []
};

class AssetsTabsContainer extends Component<State, Props> {
  state: State;
  props: Props;

  constructor (props) {
    super(props)

    this.state = {
      selectedTab: props.selectedTab,
      tabItems: props.tabItems
    }
  }

  componentDidUpdate () {
    const { tabItems: currentTabs } = this.state
    const { tabItems: nextTabs } = this.props

    if (!_.eq(currentTabs, nextTabs)) {
      this.setState({
        tabItems: nextTabs,
        selectedTab: 0
      })
    }
  }

  onSelectTab = (event, value) => {
    this.setState({ selectedTab: value })
  };

  render () {
    const { tabItems, selectedTab } = this.state
    return (
      <TabsScreen
        {...this.props}
        selectedTab={selectedTab}
        onSelectTab={this.onSelectTab}
        tabItems={tabItems.map(item => ({ key: item.key, title: item.title }))}
        content={tabItems[selectedTab].content}
      />
    )
  }
}

export default AssetsTabsContainer
