// @flow

import React from 'react'
import Link from 'next/link'
import {
  Button,
  FormControl,
  Typography,
  CircularProgress,
  withStyles
} from '@material-ui/core'
import AuthTextField from '../ui/AuthTextField'
import AuthCard from '../AuthCard/AuthCard.screen'

type Props = {
  pinChange: Object => void,
  onSubmit: () => void,
  isSubmitDisabled: boolean,
  isLoading: boolean,
  classes: Object,
  t: string => string
};

const styles = () => ({
  center: {
    position: 'absolute',
    top: '50%',
    left: '50%',
    marginRight: '-50%'
  }
})

const EmailConfirmScreen = (props: Props) => {
  const {
    pinChange,
    isSubmitDisabled,
    onSubmit,
    isLoading,
    classes,
    t
  } = props

  const title = (
    <React.Fragment>
      A pin code was sent to your email.
      <br />
      Copy and paste it to the field below.
    </React.Fragment>
  )

  const inputs = (
    <FormControl fullWidth>
      <AuthTextField
        label={t('common:Pin code')}
        name="pin"
        id="pin"
        autoComplete="off"
        type="text"
        onChange={pinChange}
      />
    </FormControl>
  )

  const actions = (
    <React.Fragment>
      <Typography variant="subheading" color="textSecondary" component="span">
        <Link prefetch href="/signup">
          <a href="/signup">Sign Up</a>
        </Link>{' '}
        for an account
      </Typography>
      <Button
        disabled={isSubmitDisabled}
        type="submit"
        variant="contained"
        color="secondary"
      >
        {t('common:Submit')}
      </Button>
    </React.Fragment>
  )

  if (isLoading) {
    return <CircularProgress className={classes.center} color="secondary" />
  }

  return (
    <AuthCard
      inputs={inputs}
      title={title}
      actions={actions}
      onSubmit={onSubmit}
    />
  )
}

export default withStyles(styles)(EmailConfirmScreen)
