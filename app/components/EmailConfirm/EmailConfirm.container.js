// @flow

import React from 'react'
import { graphql, Query } from 'react-apollo'
import gql from 'graphql-tag'
import EmailConfirm from './EmailConfirm.screen'

type Props = {
  verifyEmail: Object => Promise,
  isLoading: boolean,
  viewer: ?Object,
  error: ?Object,
  t: string => string
};

type State = {
  pinCode: string,
  isSubmitDisabled: boolean,
  isProcessingSubmit: boolean
};

class EmailConfirmContainer extends React.PureComponent<Props, State> {
  props: Props;
  state: State;

  constructor (props) {
    super(props)

    this.state = {
      pinCode: '',
      isSubmitDisabled: true,
      isProcessingSubmit: false
    }
  }

  onSubmit = () => {
    this.setState({ isProcessingSubmit: true })
    const {
      verifyEmail,
      viewer: {
        currentUser: { email }
      }
    } = this.props
    const { pinCode } = this.state
    verifyEmail({
      variables: {
        input: {
          pinCode,
          email,
          action: 'activate_email'
        }
      }
    })
      .then(res => {})
      .finally(() => {
        this.setState({
          isProcessingSubmit: false
        })
      })
  };

  onPinChange = event => {
    const pinCode = event.target.value.trim()
    this.setState({ pinCode, isSubmitDisabled: !pinCode.length })
  };

  render () {
    const { t, isLoading } = this.props
    const { isProcessingSubmit, isSubmitDisabled } = this.state

    return (
      <EmailConfirm
        isLoading={isLoading}
        processingSubmit={isProcessingSubmit}
        isSubmitDisabled={isSubmitDisabled}
        pinChange={this.onPinChange}
        submit={this.onSubmit}
        t={t}
      />
    )
  }
}

const VERIFY_EMAIL = gql`
  mutation verifyEmail($input: verifyEmailInput!) {
    verifyEmail(input: $input)
  }
`

const QUERY_USER = gql`
  query UserQuery {
    viewer {
      currentUser {
        email
      }
    }
  }
`

const userQuery = (props: Props) => (
  <Query query={QUERY_USER}>
    {({ loading, error, data }) => {
      const viewer = data && data.viewer
      return (
        <EmailConfirmContainer
          isLoading={loading}
          error={error}
          viewer={viewer}
          {...props}
        />
      )
    }}
  </Query>
)

export default graphql(VERIFY_EMAIL)(userQuery)
