// @flow

import React from 'react'
import SignedUpUser from './SignedUpUser.screen'

type Props = {};
type State = {};

class SignedUpUserContainer extends React.PureComponent<Props, State> {
  props: Props;
  state: State;

  render () {
    return <SignedUpUser />
  }
}

export default SignedUpUserContainer
