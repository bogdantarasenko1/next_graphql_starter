// @flow

import React from 'react'
import ActivityScreen from './Activity.screen'

const ActivityScreenContainer = () => {
  return <ActivityScreen />
}

export default ActivityScreenContainer
