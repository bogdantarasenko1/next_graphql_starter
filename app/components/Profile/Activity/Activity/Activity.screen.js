// @flow

import React from 'react'
import Activity from '../../../../containers/Activities/Activity.container'

const ActivityScreen = () => {
  return <Activity />
}

export default ActivityScreen
