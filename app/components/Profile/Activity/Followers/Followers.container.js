// @flow

import React from 'react'
import FollowersScreen from './Followers.screen'

const FollowersScreenContainer = () => {
  return <FollowersScreen />
}

export default FollowersScreenContainer
