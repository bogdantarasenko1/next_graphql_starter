// @flow

import React from 'react'

const FollowersScreen = () => {
  return <div>No users are following you.</div>
}

export default FollowersScreen
