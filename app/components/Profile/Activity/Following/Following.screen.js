// @flow

import React from 'react'

const FollowingScreen = () => {
  return <div>You are not following any users.</div>
}

export default FollowingScreen
