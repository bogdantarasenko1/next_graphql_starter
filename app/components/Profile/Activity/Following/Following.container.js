// @flow

import React from 'react'
import FollowingScreen from './Following.screen'

const FollowingScreenContainer = () => {
  return <FollowingScreen />
}

export default FollowingScreenContainer
