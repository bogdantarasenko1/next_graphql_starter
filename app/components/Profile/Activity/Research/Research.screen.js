// @flow

import React from 'react'
import ResearchActivity from '../../../../containers/Activities/Research.container'

const ResearchScreen = () => {
  return <ResearchActivity />
}

export default ResearchScreen
