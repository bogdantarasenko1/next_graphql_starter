// @flow

import React from 'react'
import ResearchScreen from './Research.screen'

const ResearchScreenContainer = () => {
  return <ResearchScreen />
}

export default ResearchScreenContainer
