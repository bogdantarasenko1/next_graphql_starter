// @flow

import React from 'react'
import { Grid, Button } from '@material-ui/core'
import Activity from '../../containers/Profile/Activity.container'
import Layout from '../MainLayout/MainLayout.screen'
import styles from './styles.css'

type Props = {
  t: string => string,
  user: Object,
  editProfile: () => void
};

const ProfileScreen = (props: Props) => {
  const { editProfile, ...other } = props
  const {
    t,
    user: { userName }
  } = props

  const content = () => {
    return (
      <div className={styles.container}>
        <Grid container>
          <Grid item md={4}>
            <Grid container>
              <Grid item lg={3} md={4}>
                <div>
                  <i className={styles.tmpAvatar} />
                </div>
              </Grid>
              <Grid item lg={9} md={8}>
                <div className={styles.userName}>{userName}</div>
                <div className={styles.coins}>MZX 579K</div>
                <Button
                  variant="contained"
                  color="secondary"
                  onClick={editProfile}
                >
                  {t('common:Edit Profile')}
                </Button>
              </Grid>
            </Grid>
          </Grid>
          <Grid item md={8}>
            <Activity />
          </Grid>
        </Grid>
      </div>
    )
  }

  return <Layout {...other} content={content()} />
}

export default ProfileScreen
