// @flow

import React from 'react'
import { Typography, withStyles } from '@material-ui/core'
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import { getFullDay, getFullMonth } from './dashboards_utils'


type Props = {
  title: string,
  classes: Object
};

const styles = theme => ({
  header: {
    padding: '.5rem',
    alignItems: 'center',
    backgroundColor: theme.palette.background.header,
    color: theme.palette.primary.header,
    [theme.breakpoints.down('sm')]: {
      height: 'auto',
      margin: '0 -1rem'
    }
  },
  headline: {
    color: theme.palette.text.header,
    lineHeight: '1rem',
    [theme.breakpoints.down('sm')]: {
      fontSize: '1rem'
    }
  },
  headerTitleContainer: {
    flexDirection: 'column',
    alignItems: 'center',
    // marginTop: 12,
    [theme.breakpoints.down('sm')]: {
      marginTop: 0
    }
  },
  dateField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 150,
    fontSize: '14px'
  },
  dateFieldsContainer: {
    textAlign: 'right'
  }
})

const DashboardHeader = (props: Props) => {

  const { title,
      classes,
      removeButton,
      data = false,
      onChangePanelDate
    } = props,
    onChangeFromDate = (event) => {
      onChangePanelDate(event.target.value, data[0], 'start_date')
    },
    onChangeToDate = (event) => {
      onChangePanelDate(event.target.value, data[0], 'end_date')
    },
    getPickerDate = (key) => {
      const _date = new Date(data[0].full_panel_object[key])
      return `${_date.getFullYear()}-${getFullMonth(_date)}-${getFullDay(_date)}`
    }

  const renderHeaderContent = () => {
    if (removeButton) {
      return (
        <Grid container spacing={16}>
          <Grid item xs={12} md={6}>
            <div className={classes.headerTitleContainer}>
              <Typography
                variant="subheading"
                component="h3"
                classes={{ subheading: classes.headline }}
              >
                {removeButton(data)}
                {title}
              </Typography>
            </div>
          </Grid>
          <Grid item xs={12} md={6}>
            <div className={classes.dateFieldsContainer}>
              <React.Fragment>
                <TextField
                  value={getPickerDate('start_date')}
                  id={`${data[0].full_panel_object.graf_panel_id}-from-input`}
                  label="From"
                  type="date"
                  className={classes.dateField}
                  InputLabelProps={{
                    shrink: true,
                  }}
                  onChange={onChangeFromDate}
                />
              </React.Fragment>

              <React.Fragment>
                <TextField
                  value={getPickerDate('end_date')}
                  id={`${data[0].full_panel_object.graf_panel_id}-to-input`}
                  label="To"
                  type="date"
                  className={classes.dateField}
                  InputLabelProps={{
                    shrink: true,
                  }}
                  onChange={onChangeToDate}
                />
              </React.Fragment>

            </div>
          </Grid>
        </Grid>
      )
    } else {
      return (
        <Typography
          variant="subheading"
          component="h3"
          classes={{ subheading: classes.headline }}
        >
          {title}
        </Typography>
      )
    }
  }

  return (
    <React.Fragment>
      <div className={classes.header}>
        {renderHeaderContent()}
      </div>
    </React.Fragment>
  )
}

export default withStyles(styles)(DashboardHeader)
