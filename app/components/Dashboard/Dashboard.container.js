// @flow

import React from 'react'
import axios from 'axios'
import gql from 'graphql-tag'
import { graphql, compose } from 'react-apollo'
import Icon from '@material-ui/core/Icon'
import IconButton from '@material-ui/core/IconButton'

import getConfig from 'next/config'

import CryptoassetsContainer from '../Cryptoassets/Cryptoassets.container'
import { compTableTabItems } from '../Cryptoassets/headerTabItems'
import TimeChartContainer from '../TimeChart/TimeChart.container'
import ResearchContainer from '../Research/Research.container'
import NewsContainer from '../News/News.container'
import GrafanaCharts from '../GrafanaCharts/GrafanaCharts.container'
import GrafanaContainer from '../Grafana/Grafana.container'
import DashboardDialogs from '../Dialogs/DashboardDialogs.container'
import DashboardNotifications from '../Notifications/DashboardNotifications.container'
import Dashboard from './Dashboard.screen'
import DashboardConfigNavScreen from './DashboardConfigNav.screen'
import { getSlug, getRawPanelData, reorderArray } from './dashboards_utils'

import { PREDEFINED_DASHBOARDS } from './predefined_dashboards/index'

type Props = {};

const {
  publicRuntimeConfig: { GRAFANA_TOKEN },
  publicRuntimeConfig: { ANALYTICS_URL }
} = getConfig()

const __ANALYTICS_URL = ANALYTICS_URL ? '//beta.mosaic.io' : ''

class DashboardContainer extends React.Component<State, Props> {
  constructor (props) {
    super(props)
    this.state = {
      folders: [],
      dashboards: [],
      selected_dashboard: {},
      selected_dashboard_full: {},
      panels_list: [],
      selected_chart: {},

      add_dialog_open: false,
      save_dialog_open: false,
      notification_dialog_open: false,
      notification_dialog_content: {
        title: '',
        text: ''
      },
      draft_dashboard: false,

      current_dashboard_panels: [],

      predefined_dashboards_scope: PREDEFINED_DASHBOARDS,
      users_dashboards_scope: [],

      selected_users_dashboard: '',
      is_current_dash_changed: false,
      assembled_dashboards_list: []
    }

    this.fetchDashboards = this.fetchDashboards.bind(this)
    this.onDashboardSelect = this.onDashboardSelect.bind(this)
    // this.onChartSelect          = this.onChartSelect.bind(this)
    this.onImportChart = this.onImportChart.bind(this)
    this.renderPanels = this.renderPanels.bind(this)
    this.onUsersDashboardSelect = this.onUsersDashboardSelect.bind(this)
    this.onAddDashboardClick = this.onAddDashboardClick.bind(this)
    this.onSaveDashboardClick = this.onSaveDashboardClick.bind(this)
    this.onAddDashboardSubmit = this.onAddDashboardSubmit.bind(this)
    this.onSaveDashboardSubmit = this.onSaveDashboardSubmit.bind(this)
    this.onCloseDialog = this.onCloseDialog.bind(this)
    this.renderRemovePanelButton = this.renderRemovePanelButton.bind(this)
    this.onPanelRemove = this.onPanelRemove.bind(this)
    this.assembleDashboardsList = this.assembleDashboardsList.bind(this)
    this.onChangePanelDate = this.onChangePanelDate.bind(this)
  }

  // ------- Fetching data -------

  fetchDashboards (folders) {
    const dashboards = []

    // folders
    // for(let folder of folders){
    axios
      // .get(`http://analytics.mosaic.io/data/api/search?folderIds=${folder.id}`, {
      .get(`${__ANALYTICS_URL}/data/api/search?folderIds=91`, {
        headers: {
          Authorization: `Bearer ${GRAFANA_TOKEN}`
        }
      })
      .then(response => {
        dashboards.push(...response.data)

        this.setState({
          ...this.state,
          selected_dashboard: {
            value: dashboards[0].id,
            label: dashboards[0].title
          },
          dashboards: dashboards
        })

        this.fetchPanelsList({
          value: dashboards[0].id,
          label: dashboards[0].title
        })
      })
      .catch(function (error) {
        // handle error
        console.log('Dashboards - ', error)
      })
    // }
  }

  fetchPanelsList (selected_dashboard) {
    const selected_dash_obj = this.state.dashboards.filter(
      dashboard => selected_dashboard.value === dashboard.id
    )

    if (selected_dash_obj.length > 0) {
      axios
        .get(
          `${__ANALYTICS_URL}/data/api/dashboards/${selected_dash_obj[0].uri}`,
          {
            headers: {
              Authorization: `Bearer ${GRAFANA_TOKEN}`
            }
          }
        )
        .then(response => {
          this.setState({
            ...this.state,
            selected_dashboard_full: response.data,
            panels_list: response.data.dashboard.panels
          })
        })
        .catch(function (error, w1, w2) {
          console.log('folders - ', error)
        })
    }
  }

  fetchFoldersList () {
    axios
      .get(`${__ANALYTICS_URL}/data/api/folders`, {
        headers: {
          Authorization: `Bearer ${GRAFANA_TOKEN}`
        }
      })
      .then(response => {
        //  const chartList = response.data
        this.setState({
          ...this.state,
          folders: response.data
        })
        this.fetchDashboards(response.data)
      })
      .catch(function (error, w1, w2) {
        console.log('folders - ', error)
      })
  }

  fetchSavedUsersDashboards () {
    this.props.getAllDashboards.refetch().then(response => {
      // console.log('----- response - ', response);
      const edited_raw_list = this.prepareUserDashboards(
        response.data.getDashboards.edges
      )
      // let correct_dashboards_list = edited_raw_list.filter(item => (item.panels.length > 0) && (item.panels[0].panelVars) && (item.panels[0].panelVars.length > 0) && (item.panels[0].panelVars[0].name === 'grafana_dash_uri'))

      this.setState({
        users_dashboards_scope: edited_raw_list
      })
      this.assembleDashboardsList(false, edited_raw_list)
    })
  }

  // ------- Fetching data -------

  // -------- Dialogs --------------
  onAddDashboardClick () {
    if (this.state.is_current_dash_changed) {
      this.setState({
        add_dialog_open: true
        // current_dashboard_panels: [],
        // is_current_dash_changed: false
      })
    } else {
      this.onAddDashboardSubmit()
    }
  }

  onAddDashboardSubmit () {
    let _new_draf_dash = {
      id: false,
      title: '-- Draft dashboard',
      slug: '__draft_dash__',
      panels: []
    }

    this.setState({
      add_dialog_open: false,
      is_current_dash_changed: false,
      draft_dashboard: _new_draf_dash
    })

    this.assembleDashboardsList(_new_draf_dash)
  }

  onSaveDashboardClick () {
    if (!this.state.selected_users_dashboard.id) {
      this.setState({
        save_dialog_open: true
      })
    } else {
      this.onSaveDashboardSubmit()
    }
  }

  onSaveDashboardSubmit (new_dash_name = '') {
    const { createDashboard, updateDashboard, getAllDashboards } = this.props,
      _current_dashboard = this.state.selected_users_dashboard,
      is_new_dashboard =
        _current_dashboard.id === false || _current_dashboard.id === undefined

    let decorated_panels_scope = this.decoratePanelsScopeForSave(),
      mutator = false,
      query_data = {}

    if (is_new_dashboard) {
      // If new Dashboard - Create
      query_data = {
        title: new_dash_name,
        slug: getSlug(new_dash_name),
        panels: [...decorated_panels_scope]
      }
      mutator = createDashboard
    } else {
      // If existing Dashboard - Update
      query_data = {
        id: _current_dashboard.id,
        title: _current_dashboard.title,
        slug: _current_dashboard.slug,
        panels: [...decorated_panels_scope]
      }
      mutator = updateDashboard
    }

    console.log('query_data- ', query_data)

    mutator({
      variables: {
        input: query_data
      }
    })
      .then(({ data }) => {
        getAllDashboards.refetch().then(response => {
          console.log(response)
          const edited_raw_list = this.prepareUserDashboards(
            response.data.getDashboards.edges
          )

          // let correct_dashboards_list = edited_raw_list.filter(item => (item.panels.length > 0) && (item.panels[0].panelVars) && (item.panels[0].panelVars.length > 0) && (item.panels[0].panelVars[0].name === 'grafana_dash_uri'))

          // const _selected_user_dashboard = {}
          //
          // if (edited_raw_list.length > 0) {
          //   _selected_user_dashboard.value = edited_raw_list[0].slug
          //   _selected_user_dashboard.label = edited_raw_list[0].title
          // }

          this.setState({
            draft_dashboard: false,
            save_dialog_open: false,
            notification_dialog_open: true,
            notification_dialog_content: {
              title: 'Success',
              text: is_new_dashboard
                ? 'Dashboard is saved successfully.'
                : 'Dashboard is updated successfully.'
            },
            is_current_dash_changed: false,
            users_dashboards_scope: edited_raw_list,
            selected_users_dashboard: edited_raw_list[0]
          })

          this.assembleDashboardsList(false, edited_raw_list)
        })
      })
      .catch((e, w, r) => {
        console.log('Apollo error')
        console.log(e)

        this.setState({
          notification_dialog_open: true,
          notification_dialog_content: {
            title: 'Error',
            text: 'Something went wrong. Please, try later.'
          }
        })
      })
  }

  onCloseDialog (dialog_name) {
    this.setState({
      [dialog_name]: false
    })
  }

  onChangePanelDate (value, selected_panel, key) {
    const selected_date = new Date(value)
    let updated_current_dashboard_panels = []

    this.state.current_dashboard_panels.map(panel => {
      if (
        panel.full_panel_object.graf_panel_id ===
        selected_panel.full_panel_object.graf_panel_id
      ) {
        panel.full_panel_object[key] = selected_date.getTime()
        updated_current_dashboard_panels.push(panel.full_panel_object)
      }
    })

    this.setState({
      ...this.state,
      current_dashboard_panels: this.decoratePanelsScopeForRender({
        panels: [...updated_current_dashboard_panels]
      }),
      is_current_dash_changed: true
    })
  }

  // -------- Dialogs --------------

  onUsersDashboardSelect (selected_val) {
    this.setState({
      selected_users_dashboard: selected_val,
      current_dashboard_panels: this.decoratePanelsScopeForRender(selected_val)
    })
  }

  onDashboardSelect (selected_val) {
    this.setState({
      selected_dashboard: selected_val
    })

    this.fetchPanelsList(selected_val)
  }

  onImportChart (selected_chart) {
    this.setState(
      {
        selected_chart: selected_chart
      },
      () => {
        this.insertPanel()
      }
    )
  }

  decoratePanelsScopeForRender (dashboard) {
    let decorarted_list = []

    dashboard.panels.map(item => {
      if (!dashboard.predefined) {
        const root_url = __ANALYTICS_URL ? `https:${__ANALYTICS_URL}` : ''
        const time_frame = `&from=${item.start_date}&to=${item.end_date}`
        const vars_string = () => {
          let _vars = ''
          item.panelVars.map(
            _var => (_vars = `${_vars}&var-${_var.name}=${_var.value}`)
          )
          return _vars
        }
        const url = `${root_url}/analytics/d-solo/${
          item.graf_dash_uri
        }?orgId=1&panelId=${item.graf_panel_id}${vars_string()}${time_frame}`
        let _current_item = item

        if (_current_item.id) {
          delete _current_item.id
          delete _current_item.__typename
        }

        decorarted_list.push({
          title: item.name,
          url: url,
          width: '100%',
          height: '300',
          border: '0',
          full_panel_object: _current_item
        })
      } else {
        let _decorated_item = item
        _decorated_item.full_panel_object = getRawPanelData(item)
        decorarted_list.push(_decorated_item)
      }
    })

    return decorarted_list
  }

  decoratePanelsScopeForSave () {
    let decorated_list = []

    this.state.current_dashboard_panels.map(item => {
      if (item.full_panel_object) {
        let _current_item = item.full_panel_object

        if (_current_item.id) {
          delete _current_item.id
          delete _current_item.__typename
        }

        _current_item.panelVars.map(p_var => delete p_var.__typename)

        decorated_list.push(_current_item)
      } else {
        decorated_list.push(getRawPanelData(item))
      }
    })

    return decorated_list
  }

  assembleDashboardsList (
    new_draft_dashboard = false,
    new_saved_dashboards = false
  ) {
    const _draft = new_draft_dashboard ? [new_draft_dashboard] : []
    const _saved_dashboards =
      new_saved_dashboards || this.state.users_dashboards_scope
    const _new_assembled_dashboards_list = [
      ..._draft,
      ..._saved_dashboards,
      ...this.state.predefined_dashboards_scope
    ]
    let _new_selected_user_dashboard = {}

    if (_draft.length > 0) {
      _new_selected_user_dashboard = _draft[0]
      this.onUsersDashboardSelect(_draft[0])
    } else if (_saved_dashboards.length > 0 && !_draft.length) {
      _new_selected_user_dashboard = _saved_dashboards[0]
      this.onUsersDashboardSelect(_saved_dashboards[0])
    } else {
      _new_selected_user_dashboard = this.state.predefined_dashboards_scope[0]
      this.onUsersDashboardSelect(this.state.predefined_dashboards_scope[0])
    }

    this.setState({
      assembled_dashboards_list: _new_assembled_dashboards_list,
      selected_users_dashboard: _new_selected_user_dashboard
    })
  }

  insertPanel () {
    let new_current_dashboard_panels = this.state.current_dashboard_panels
    const current_selected_chart = this.state.selected_chart,
      dashboard_obj = this.state.selected_dashboard_full,
      raw_panel_vars = this.extractVars(dashboard_obj.dashboard, 'raw'),
      from_date = this.getTimeChartFrame('from'),
      to_date = this.getTimeChartFrame('to')

    new_current_dashboard_panels.unshift({
      title: this.state.selected_chart.title,
      url: this.assembleURL(),
      width: '100%',
      height: '300',
      border: '0',
      full_panel_object: {
        name: current_selected_chart.title,
        slug: getSlug(current_selected_chart.title),
        description: '',
        start_date: from_date,
        end_date: to_date,
        graf_dash_uri: `${dashboard_obj.dashboard.uid}/${
          dashboard_obj.meta.slug
        }`,
        graf_panel_id: current_selected_chart.id,
        panelVars: [...raw_panel_vars]
      }
    })

    this.setState({
      current_dashboard_panels: new_current_dashboard_panels,
      is_current_dash_changed: true
    })

    console.log(
      'new_current_dashboard_panels - ',
      new_current_dashboard_panels
    )
  }

  assembleURL () {
    const chart_obj = this.state.selected_chart
    const dashboard_obj = this.state.selected_dashboard_full
    let vars_string = this.extractVars(dashboard_obj.dashboard)
    let time_frame = this.getTimeChartFrame()
    const root_url = __ANALYTICS_URL ? `https:${__ANALYTICS_URL}` : ''

    console.log('dashboard_obj - ', dashboard_obj)

    let url = `${root_url}/analytics/d-solo/${dashboard_obj.dashboard.uid}/${
      dashboard_obj.meta.slug
    }?orgId=1&panelId=${chart_obj.id}${vars_string}${time_frame}`
    // let url = 'https://analytics.mosaic.io/d-solo/pJm9_gaik/1-blockchain-vs-exchange-historical?orgId=1&var-left_field=All&var-right_field=averageDifficulty&var-symbol=ETH&panelId=11&from=1511776884037&to=1543312884038'
    // https://analytics.mosaic.io/d-solo/pJm9_gaik/1-blockchain-vs-exchange-historical?orgId=1&amp;panelId=4&amp;var-left_field=$__all&amp;var-right_field=price&amp;var-symbol=ETH&amp;from=1543314026274&amp;to=1511778026274
    console.log('url - ', url)

    return url
  }

  extractVars (dashboard, return_type = false) {
    let vars_string = '',
      lists_scope = dashboard.templating.list,
      raw_vars_scope = []

    if (lists_scope && lists_scope.length > 0) {
      for (let list of lists_scope) {
        let _current_val =
          typeof list.current.value === 'string'
            ? list.current.text
            : list.current.value[0]
        vars_string = `${vars_string}&var-${list.name}=${_current_val}`
        raw_vars_scope.push({
          name: list.name,
          value: _current_val
        })
      }
      return return_type ? raw_vars_scope : vars_string
    }

    return return_type ? [] : ''
  }

  getTimeChartFrame (return_param = false) {
    let to_timestamp = new Date()
    let tmp_date = new Date()
    let from_timestamp = new Date(tmp_date.setMonth(tmp_date.getMonth() - 6))

    if (!return_param) {
      return `&from=${from_timestamp.getTime()}&to=${to_timestamp.getTime()}`
    } else if (return_param === 'to') {
      return to_timestamp.getTime()
    } else if (return_param === 'from') {
      return from_timestamp.getTime()
    }
  }

  renderPanels () {
    let _lists = []

    this.state.current_dashboard_panels.map((panel, index) => {
      _lists.push(
        <GrafanaContainer
          title={panel.title}
          data={[panel]}
          key={index}
          removeButton={this.renderRemovePanelButton}
          onChangePanelDate={this.onChangePanelDate}
        />
      )
    })

    return _lists
  }

  renderRemovePanelButton (panel) {
    return (
      <IconButton
        style={{ width: '25px', height: '25px', marginRight: '10px' }}
        onClick={() => this.onPanelRemove(panel)}
      >
        <Icon style={{ fontSize: '18px' }}>cancel</Icon>
      </IconButton>
    )
  }

  onPanelRemove (panel) {
    let _new_panel_list = this.state.current_dashboard_panels.filter(
      item => item.url !== panel[0].url
    )

    this.setState({
      current_dashboard_panels: _new_panel_list,
      is_current_dash_changed: _new_panel_list.length > 0
    })
  }

  prepareUserDashboards (raw_list) {
    let _edited_list = []

    raw_list.map(item => {
      _edited_list.push(item.node)
    })

    return reorderArray(_edited_list)
  }

  componentDidMount () {
    this.fetchFoldersList()
    this.assembleDashboardsList()
    this.fetchSavedUsersDashboards()
  }

  render () {
    const { saveAuth } = this.props
    return (
      <Dashboard
        saveAuth={saveAuth}
        dialogs={
          <DashboardDialogs
            add_dialog_open={this.state.add_dialog_open}
            save_dialog_open={this.state.save_dialog_open}
            notification_dialog_open={this.state.notification_dialog_open}
            notification_dialog_content={this.state.notification_dialog_content}
            onAddDashboardSubmit={this.onAddDashboardSubmit}
            onSaveDashboardSubmit={this.onSaveDashboardSubmit}
            onCloseDialog={this.onCloseDialog}
          />
        }
        dashboardconfignav={
          <DashboardConfigNavScreen
            assembled_dashboards_list={this.state.assembled_dashboards_list}
            selected_users_dashboard={this.state.selected_users_dashboard}
            onUserDashboardSelect={this.onUsersDashboardSelect}
            onAddDashboardClick={this.onAddDashboardClick}
            onSaveDashboardClick={this.onSaveDashboardClick}
            is_current_dash_changed={this.state.is_current_dash_changed}
            draft_dashboard={this.state.draft_dashboard}
          />
        }
        cryptoassets={
          <CryptoassetsContainer
            headerTabItems={compTableTabItems()}
            {...this.props}
          />
        }
        twitterpanel={this.renderPanels()}
        //
        // datalake={<GrafanaContainer title="Data Lake" data={data_lake_iframes} />}
        // datalakestaging={<GrafanaContainer title="Data Lake - Staging" data={iframes} />}
        timechart={<TimeChartContainer {...this.props} />}
        research={<ResearchContainer showHeader showMore {...this.props} />}
        news={<NewsContainer {...this.props} />}
        chartspicker={
          <GrafanaCharts
            title="Chart Library"
            dashboards={this.state.dashboards}
            selected_dashboard={this.state.selected_dashboard}
            onSelect={this.onDashboardSelect}
            panels_list={this.state.panels_list}
            // onChartSelect={this.onChartSelect}
            onImportChart={this.onImportChart}
            selected_chart={this.state.selected_chart}
          />
        }
        // notifications={<DashboardNotifications />}
        {...this.props}
      />
    )
  }
}

const getAllDashboardsQuery = gql`
  query allDashboards {
    getDashboards {
      pageInfo {
        hasNextPage
      }
      edges {
        cursor
        node {
          createdAt
          updatedAt
          id
          slug
          title
          uid
          panels {
            description
            id
            name
            slug
            end_date
            start_date
            graf_dash_uri
            graf_panel_id
            panelVars {
              name
              value
            }
          }
        }
      }
    }
  }
`

const createDashboardQuery = gql`
  mutation createDashboard($input: createDashboardInput!) {
    createDashboard(input: $input) {
      dashboard {
        uid
        title
        slug
        panels {
          name
          slug
          description
          start_date
          end_date
          graf_dash_uri
          graf_panel_id
          panelVars {
            name
            value
          }
        }
      }
    }
  }
`

const updateDashboardQuery = gql`
  mutation updateDashboard($input: updateDashboardInput!) {
    updateDashboard(input: $input) {
      dashboard {
        id
        uid
        title
        slug
        panels {
          name
          slug
          description
          start_date
          end_date
          graf_dash_uri
          graf_panel_id
          panelVars {
            name
            value
          }
        }
      }
    }
  }
`

export default compose(
  graphql(createDashboardQuery, { name: 'createDashboard' }),
  graphql(updateDashboardQuery, { name: 'updateDashboard' }),
  graphql(getAllDashboardsQuery, { name: 'getAllDashboards' })
)(DashboardContainer)
