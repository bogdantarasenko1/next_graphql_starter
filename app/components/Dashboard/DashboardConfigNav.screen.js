// @flow

import React from 'react'
import {Grid, withStyles} from '@material-ui/core'
import DashboardsList from './DashboardsList.container'
// import CoinsList from './CoinsList.container'
import DashConfigControls from './DashboardConfigNavControls.container'

// import styles from './styles.css'

const styles = theme => ({
  nav_container: {
    borderBottom: '1px solid #807e7e',
    paddingBottom: '10px'
  }
})

type Props = {
    classes: Object,
};

const DashboardConfigNavScreen = (props: Props) => {
  const {
    classes,
    onUserDashboardSelect,
    onAddDashboardClick,
    onSaveDashboardClick,
    is_current_dash_changed,
    draft_dashboard,
    assembled_dashboards_list,
    selected_users_dashboard
  } = props


  return(
    <Grid container className={classes.nav_container}>
      <Grid container>
        <Grid item md={3}>
          <DashboardsList
            onUserDashboardSelect={onUserDashboardSelect}
            draft_dashboard={draft_dashboard}
            assembled_dashboards_list={assembled_dashboards_list}
            selected_users_dashboard={selected_users_dashboard}
          />
        </Grid>
        <Grid item md={7}>
          {/*<CoinsList />*/}
        </Grid>
        <Grid item md={2}>
          <DashConfigControls
            onAddDashboardClick={onAddDashboardClick}
            onSaveDashboardClick={onSaveDashboardClick}
            is_current_dash_changed={is_current_dash_changed}
          />
        </Grid>
      </Grid>
    </Grid>
  )
}

export default withStyles(styles)(DashboardConfigNavScreen)
