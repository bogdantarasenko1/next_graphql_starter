// @flow

import React from 'react'
import {withStyles} from '@material-ui/core'
import IconButton from '@material-ui/core/IconButton';
import Icon from '@material-ui/core/Icon';
// import styles from "../GrafanaCharts/styles.css";
// import Select from "../Select";
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';


const controls_styles = theme => ({
  root: {
    textAlign: 'right'
  },
  addIcon: {
    color: '#FB771A',
    fontSize: '20px'
  },
  saveIcon: {
    color: '#fff',
    fontSize: '20px'
  },
  button: {
    padding: '3px',
    height: '30px',
    width: '30px'
  }
});

type Props = {};


class DashConfigControls extends React.Component<State, Props> {
  constructor (props) {
    super(props)
    this.state = {
      is_current_dash_changed: props.is_current_dash_changed
    }

    this.onAddDashboardClick    = this.onAddDashboardClick.bind(this);
    this.onSaveDashboardClick   = this.onSaveDashboardClick.bind(this);

  }

  onAddDashboardClick(){
    this.props.onAddDashboardClick();
  }

  onSaveDashboardClick(){
    this.props.onSaveDashboardClick();
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      is_current_dash_changed: nextProps.is_current_dash_changed,
    });
  }

  render () {
    let classes = this.props.classes

    return (
      <div className={classes.root}>
        <IconButton className={classes.button} onClick={this.onSaveDashboardClick} disabled={!this.state.is_current_dash_changed}>
          <Icon className={classes.saveIcon}>save</Icon>
        </IconButton>
        <IconButton className={classes.button} onClick={this.onAddDashboardClick}>
          <Icon className={classes.addIcon}>add_circle</Icon>
        </IconButton>

      </div>

    )
  }
}

export default withStyles(controls_styles)(DashConfigControls)
