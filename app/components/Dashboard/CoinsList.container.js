// @flow

import React from 'react'
import axios from 'axios'
import {Grid, withStyles, Chip} from '@material-ui/core'
import styles from "../GrafanaCharts/styles.css";
import Select from "../Select";

const def_coins_list = [
  {
    value: 'btc',
    label: 'BTC'
  },
  {
    value: 'eth',
    label: 'ETH'
  }
]

const coins_list_styles = theme => ({
  root: {
    flexGrow: 1,
  },
  coinSelector: {
    paddingLeft: theme.spacing.unit * 2,
    paddingRight: theme.spacing.unit,
  },
  chip: {
    marginRight: theme.spacing.unit,
    border: 'none',
    background: 'transparent',
    borderBottom: '2px solid #fff',
    borderRadius: 0,
    paddingBottom: '4px',
    paddingReft: '0px',
    paddingLeft: '0px',

  }

});

type Props = {};


class CoinsList extends React.Component<State, Props> {
  constructor (props) {
    super(props)
    this.state = {
      selected_coins: []
    }

    this.onCoinSelect = this.onCoinSelect.bind(this);
    this.onCoinDelete = this.onCoinDelete.bind(this);

  }


  onCoinSelect(selected_val){
    let current_selected_coins = this.state.selected_coins

    if (current_selected_coins.filter(item => item.value === selected_val.value).length === 0) {
      current_selected_coins.push(selected_val)

      this.setState({
        ...this.state,
        selected_coins: current_selected_coins
      })
    }

  }

  onCoinDelete(item_index){
    let current_selected_coins = this.state.selected_coins

    current_selected_coins.splice(item_index, 1)

    this.setState({
      ...this.state,
      selected_coins: current_selected_coins
    })
  }

  componentDidMount () {
    // this.fetchFoldersList()
  }

  render () {
    let classes = this.props.classes
    return (
      <Grid container>
        <Grid item md={3} className={classes.coinSelector}>
          <div className={styles.pickerCategoryContainer}>
            <Select list={ def_coins_list }
                    selected_val={''}
                    placeholder="Select coin"
                    onSelect={this.onCoinSelect}/>
          </div>
        </Grid>
        <Grid item md={9}>
          { this.state.selected_coins.map((coin_item, index) => {
            return <Chip
              className={classes.chip}
              variant="outlined"
              color="primary"
              label={coin_item.label}
              onDelete={index => this.onCoinDelete(index)} />
          }) }
        </Grid>
      </Grid>

    )
  }
}

export default withStyles(coins_list_styles)(CoinsList)
