import blockchain_exchange from './blockchain_exchange'
import blockchain_github from './blockchain_github'
import blockchain_social from './blockchain_social'
import cryptoeconomic_ratios from './cryptoeconomic_ratios'
import currency_coins from './currency_coins'
import customized from './customized'
import platform_tokens from './platform_tokens'
import sentiment from './sentiment'
import stablecoins from './stablecoins'
import _dev_test_dashboard from './_dev_test_dashboard'

export const PREDEFINED_DASHBOARDS = [
  ..._dev_test_dashboard,
  ...blockchain_exchange,
  ...blockchain_github,
  ...blockchain_social,
  ...cryptoeconomic_ratios,
  ...currency_coins,
  ...customized,
  ...platform_tokens,
  ...sentiment,
  ...stablecoins
]
