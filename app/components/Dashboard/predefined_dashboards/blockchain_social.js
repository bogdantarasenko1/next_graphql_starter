export default [
  {
    predefined: true,
    select_data: {
      value: 'blockchain_social',
      label: 'Blockchain Social'
    },
    panels: [
      {
        title: 'Active Addresses',
        url:
          '//beta.mosaic.io/analytics/d-solo/cut3Umaiz/3-blockchain-vs-social-data-historical?orgId=1&panelId=2&from=1525807792250&to=1540851632250&var-blockchain_field=All&var-social_field=subjectivity&var-symbol=BTC',
        width: '100%',
        height: '300',
        border: '0'
      },
      {
        title: 'Adjusted Transaction Vol',
        url:
          '//beta.mosaic.io/analytics/d-solo/cut3Umaiz/3-blockchain-vs-social-data-historical?orgId=1&panelId=6&from=1525807792250&to=1540851632250&var-blockchain_field=All&var-social_field=subjectivity&var-symbol=BTC',
        width: '100%',
        height: '300',
        border: '0'
      },
      {
        title: 'Average Difficulty',
        url:
          '//beta.mosaic.io/analytics/d-solo/cut3Umaiz/3-blockchain-vs-social-data-historical?orgId=1&panelId=8&from=1525807792250&to=1540851632250&var-blockchain_field=All&var-social_field=subjectivity&var-symbol=BTC',
        width: '100%',
        height: '300',
        border: '0'
      },
      {
        title: 'Blockcount',
        url:
          '//beta.mosaic.io/analytics/d-solo/cut3Umaiz/3-blockchain-vs-social-data-historical?orgId=1&panelId=10&from=1525807792250&to=1540851632250&var-blockchain_field=All&var-social_field=subjectivity&var-symbol=BTC',
        width: '100%',
        height: '300',
        border: '0'
      },
      {
        title: 'Blocksize',
        url:
          '//beta.mosaic.io/analytics/d-solo/cut3Umaiz/3-blockchain-vs-social-data-historical?orgId=1&panelId=12&from=1525807792250&to=1540851632250&var-blockchain_field=All&var-social_field=subjectivity&var-symbol=BTC',
        width: '100%',
        height: '300',
        border: '0'
      },
      {
        title: 'Exchange Volume',
        url:
          '//beta.mosaic.io/analytics/d-solo/cut3Umaiz/3-blockchain-vs-social-data-historical?orgId=1&panelId=14&from=1525807792250&to=1540851632250&var-blockchain_field=All&var-social_field=subjectivity&var-symbol=BTC',
        width: '100%',
        height: '300',
        border: '0'
      },
      {
        title: 'Fees',
        url:
          '//beta.mosaic.io/analytics/d-solo/cut3Umaiz/3-blockchain-vs-social-data-historical?orgId=1&panelId=16&from=1525807792250&to=1540851632250&var-blockchain_field=All&var-social_field=subjectivity&var-symbol=BTC',
        width: '100%',
        height: '300',
        border: '0'
      },
      {
        title: 'Generated Coins',
        url:
          '//beta.mosaic.io/analytics/d-solo/cut3Umaiz/3-blockchain-vs-social-data-historical?orgId=1&panelId=18&from=1525807792250&to=1540851632250&var-blockchain_field=All&var-social_field=subjectivity&var-symbol=BTC',
        width: '100%',
        height: '300',
        border: '0'
      },
      {
        title: 'MarketCap',
        url:
          '//beta.mosaic.io/analytics/d-solo/cut3Umaiz/3-blockchain-vs-social-data-historical?orgId=1&panelId=20&from=1525807792250&to=1540851632250&var-blockchain_field=All&var-social_field=subjectivity&var-symbol=BTC',
        width: '100%',
        height: '300',
        border: '0'
      },
      {
        title: 'MedianFee',
        url:
          '//beta.mosaic.io/analytics/d-solo/cut3Umaiz/3-blockchain-vs-social-data-historical?orgId=1&panelId=22&from=1525807792250&to=1540851632250&var-blockchain_field=All&var-social_field=subjectivity&var-symbol=BTC',
        width: '100%',
        height: '300',
        border: '0'
      },
      {
        title: 'Median Transaction Value',
        url:
          '//beta.mosaic.io/analytics/d-solo/cut3Umaiz/3-blockchain-vs-social-data-historical?orgId=1&panelId=24&from=1525807792250&to=1540851632250&var-blockchain_field=All&var-social_field=subjectivity&var-symbol=BTC',
        width: '100%',
        height: '300',
        border: '0'
      },
      {
        title: 'PaymentCount',
        url:
          '//beta.mosaic.io/analytics/d-solo/cut3Umaiz/3-blockchain-vs-social-data-historical?orgId=1&panelId=26&from=1525807792250&to=1540851632250&var-blockchain_field=All&var-social_field=subjectivity&var-symbol=BTC',
        width: '100%',
        height: '300',
        border: '0'
      },
      {
        title: 'Price',
        url:
          '//beta.mosaic.io/analytics/d-solo/cut3Umaiz/3-blockchain-vs-social-data-historical?orgId=1&panelId=28&from=1525807792250&to=1540851632250&var-blockchain_field=All&var-social_field=subjectivity&var-symbol=BTC',
        width: '100%',
        height: '300',
        border: '0'
      },
      {
        title: 'Transaction Count',
        url:
          '//beta.mosaic.io/analytics/d-solo/cut3Umaiz/3-blockchain-vs-social-data-historical?orgId=1&panelId=30&from=1525807792250&to=1540851632250&var-blockchain_field=All&var-social_field=subjectivity&var-symbol=BTC',
        width: '100%',
        height: '300',
        border: '0'
      },
      {
        title: 'Transaction Volume',
        url:
          '//beta.mosaic.io/analytics/d-solo/cut3Umaiz/3-blockchain-vs-social-data-historical?orgId=1&panelId=32&from=1525807792250&to=1540851632250&var-blockchain_field=All&var-social_field=subjectivity&var-symbol=BTC',
        width: '100%',
        height: '300',
        border: '0'
      }
    ]
  }
]