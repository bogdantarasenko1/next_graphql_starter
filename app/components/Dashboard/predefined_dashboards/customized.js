export default [
  {
    predefined: true,
    select_data: {
      value: 'customized',
      label: 'Customized'
    },
    panels: [
      {
        title: 'NVT',
        url:
          '//beta.mosaic.io/analytics/d-solo/E9-hhC-mz/6-example-customized-dashboard?orgId=1&panelId=6&from=1511669657185&to=1543205657185',
        width: '100%',
        height: '300',
        border: '0'
      },
      {
        title: 'MACD',
        url:
          '//beta.mosaic.io/analytics/d-solo/E9-hhC-mz/6-example-customized-dashboard?orgId=1&panelId=2&from=1511669682610&to=1543205682610',
        width: '100%',
        height: '300',
        border: '0'
      },
      {
        title: 'Ratio of Volume in Comparison to BTC',
        url:
          '//beta.mosaic.io/analytics/d-solo/E9-hhC-mz/6-example-customized-dashboard?orgId=1&panelId=99&from=1511669696830&to=1543205696830',
        width: '100%',
        height: '300',
        border: '0'
      },
      {
        title: 'EOS/ETH Active Addresses',
        url:
          '//beta.mosaic.io/analytics/d-solo/E9-hhC-mz/6-example-customized-dashboard?orgId=1&panelId=98&from=1511669722823&to=1543205722823',
        width: '100%',
        height: '300',
        border: '0'
      },
      {
        title: 'Transaction Count vs. Block Size',
        url:
          '//beta.mosaic.io/analytics/d-solo/E9-hhC-mz/6-example-customized-dashboard?orgId=1&panelId=14&from=1511669737539&to=1543205737539',
        width: '100%',
        height: '300',
        border: '0'
      },
      {
        title: 'Social Sentiment',
        url:
          '//beta.mosaic.io/analytics/d-solo/E9-hhC-mz/6-example-customized-dashboard?orgId=1&panelId=97&from=1511669753651&to=1543205753651',
        width: '100%',
        height: '300',
        border: '0'
      },
      {
        title: 'Correlation',
        url:
          '//beta.mosaic.io/analytics/d-solo/E9-hhC-mz/6-example-customized-dashboard?orgId=1&panelId=12&from=1511669770558&to=1543205770558',
        width: '100%',
        height: '300',
        border: '0'
      },
      {
        title: 'Transactions per Block',
        url:
          '//beta.mosaic.io/analytics/d-solo/E9-hhC-mz/6-example-customized-dashboard?orgId=1&panelId=10&from=1511669782044&to=1543205782044',
        width: '100%',
        height: '300',
        border: '0'
      }
    ]
  }
]