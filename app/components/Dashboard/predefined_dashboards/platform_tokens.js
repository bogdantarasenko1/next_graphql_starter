export default [
  {
    predefined: true,
    select_data: {
      value: 'platform_tokens',
      label: 'Platform Tokens'
    },
    panels: [
      {
        title: 'Active Addresses',
        url:
          '//beta.mosaic.io/analytics/d-solo/Nq6rHhfmz/5-platform-tokens-since-the-launch-of-the-eos-blockchain?orgId=1&panelId=12&from=1511669837197&to=1543205837197',
        width: '100%',
        height: '300',
        border: '0'
      },
      {
        title: 'Inflation',
        url:
          '//beta.mosaic.io/analytics/d-solo/Nq6rHhfmz/5-platform-tokens-since-the-launch-of-the-eos-blockchain?orgId=1&panelId=4&from=1511669849606&to=1543205849606',
        width: '100%',
        height: '300',
        border: '0'
      },
      {
        title: 'Commits',
        url:
          '//beta.mosaic.io/analytics/d-solo/Nq6rHhfmz/5-platform-tokens-since-the-launch-of-the-eos-blockchain?orgId=1&panelId=8&from=1511669863017&to=1543205863017',
        width: '100%',
        height: '300',
        border: '0'
      },
      {
        title: 'Average Transaction Amount',
        url:
          '//beta.mosaic.io/analytics/d-solo/Nq6rHhfmz/5-platform-tokens-since-the-launch-of-the-eos-blockchain?orgId=1&panelId=28&from=1511669883282&to=1543205883282',
        width: '100%',
        height: '300',
        border: '0'
      }
    ]
  }
]