export default [
  {
    predefined: true,
    select_data: {
      value: 'blockchain_github',
      label: 'Blockchain Github'
    },
    panels: [
      {
        title: 'Active Addresses',
        url:
          'https://analytics.mosaic.io/d-solo/dw2r-W-ik/4-github-vs-blockchain-historical?orgId=1&panelId=4&from=1527294627420&to=1543195827420&var-blockchain_field=All&var-symbol=BTC&var-gfield=commits',
        width: '100%',
        height: '300',
        border: '0'
      },
      {
        title: 'Adjusted Transaction Volume',
        url:
          'https://analytics.mosaic.io/d-solo/dw2r-W-ik/4-github-vs-blockchain-historical?orgId=1&panelId=8&from=1527294678532&to=1543195878532&var-blockchain_field=All&var-symbol=BTC&var-gfield=commits',
        width: '100%',
        height: '300',
        border: '0'
      },
      {
        title: 'Average Difficulty',
        url:
          '//beta.mosaic.io/analytics/d-solo/dw2r-W-ik/4-github-vs-blockchain-historical?orgId=1&panelId=10&from=1527294699925&to=1543195899925&var-blockchain_field=All&var-symbol=BTC&var-gfield=commits',
        width: '100%',
        height: '300',
        border: '0'
      },
      {
        title: 'Blockcount',
        url:
          '//beta.mosaic.io/analytics/d-solo/dw2r-W-ik/4-github-vs-blockchain-historical?orgId=1&panelId=12&from=1527294719421&to=1543195919421&var-blockchain_field=All&var-symbol=BTC&var-gfield=commits',
        width: '100%',
        height: '300',
        border: '0'
      },
      {
        title: 'Blocksize',
        url:
          '//beta.mosaic.io/analytics/d-solo/dw2r-W-ik/4-github-vs-blockchain-historical?orgId=1&panelId=14&from=1527294739505&to=1543195939505&var-blockchain_field=All&var-symbol=BTC&var-gfield=commits',
        width: '100%',
        height: '300',
        border: '0'
      },
      {
        title: 'Exchange Volume',
        url:
          '//beta.mosaic.io/analytics/d-solo/dw2r-W-ik/4-github-vs-blockchain-historical?orgId=1&panelId=16&from=1527294765950&to=1543195965950&var-blockchain_field=All&var-symbol=BTC&var-gfield=commits',
        width: '100%',
        height: '300',
        border: '0'
      },
      {
        title: 'Fees',
        url:
          '//beta.mosaic.io/analytics/d-solo/dw2r-W-ik/4-github-vs-blockchain-historical?orgId=1&panelId=18&from=1527294786310&to=1543195986310&var-blockchain_field=All&var-symbol=BTC&var-gfield=commits',
        width: '100%',
        height: '300',
        border: '0'
      },
      {
        title: 'Generated Coins',
        url:
          '//beta.mosaic.io/analytics/d-solo/dw2r-W-ik/4-github-vs-blockchain-historical?orgId=1&panelId=20&from=1527294801362&to=1543196001362&var-blockchain_field=All&var-symbol=BTC&var-gfield=commits',
        width: '100%',
        height: '300',
        border: '0'
      },
      {
        title: 'MarketCap',
        url:
          '//beta.mosaic.io/analytics/d-solo/dw2r-W-ik/4-github-vs-blockchain-historical?orgId=1&panelId=22&from=1527294825930&to=1543196025930&var-blockchain_field=All&var-symbol=BTC&var-gfield=commits',
        width: '100%',
        height: '300',
        border: '0'
      },
      {
        title: 'MedianFee',
        url:
          '//beta.mosaic.io/analytics/d-solo/dw2r-W-ik/4-github-vs-blockchain-historical?orgId=1&panelId=24&from=1527294841517&to=1543196041518&var-blockchain_field=All&var-symbol=BTC&var-gfield=commits',
        width: '100%',
        height: '300',
        border: '0'
      },
      {
        title: 'Median Transaction Value',
        url:
          '//beta.mosaic.io/analytics/d-solo/dw2r-W-ik/4-github-vs-blockchain-historical?orgId=1&panelId=26&from=1527294865898&to=1543196065898&var-blockchain_field=All&var-symbol=BTC&var-gfield=commits',
        width: '100%',
        height: '300',
        border: '0'
      },
      {
        title: 'PaymentCount',
        url:
          '//beta.mosaic.io/analytics/d-solo/dw2r-W-ik/4-github-vs-blockchain-historical?orgId=1&panelId=28&from=1527294897099&to=1543196097099&var-blockchain_field=All&var-symbol=BTC&var-gfield=commits',
        width: '100%',
        height: '300',
        border: '0'
      },
      {
        title: 'Price',
        url:
          '//beta.mosaic.io/analytics/d-solo/dw2r-W-ik/4-github-vs-blockchain-historical?orgId=1&panelId=30&from=1527294914551&to=1543196114551&var-blockchain_field=All&var-symbol=BTC&var-gfield=commits',
        width: '100%',
        height: '300',
        border: '0'
      },
      {
        title: 'Transaction Count',
        url:
          '//beta.mosaic.io/analytics/d-solo/dw2r-W-ik/4-github-vs-blockchain-historical?orgId=1&panelId=32&from=1527294932691&to=1543196132691&var-blockchain_field=All&var-symbol=BTC&var-gfield=commits',
        width: '100%',
        height: '300',
        border: '0'
      },
      {
        title: 'Transaction Volume',
        url:
          '//beta.mosaic.io/analytics/d-solo/dw2r-W-ik/4-github-vs-blockchain-historical?orgId=1&panelId=34&from=1527294946220&to=1543196146220&var-blockchain_field=All&var-symbol=BTC&var-gfield=commits',
        width: '100%',
        height: '300',
        border: '0'
      }
    ]
  }
]