export default [
  {
    predefined: true,
    select_data: {
      value: 'currency_coins',
      label: 'Currency Coins'
    },
    panels: [
      {
        title: 'Block Size',
        url:
          '//beta.mosaic.io/analytics/d-solo/ZbMGqhfik/2-currency-coins?orgId=1&panelId=22&from=1511669086062&to=1543205086062',
        width: '100%',
        height: '300',
        border: '0'
      },
      {
        title: 'Average Fee Amount Per Address',
        url:
          '//beta.mosaic.io/analytics/d-solo/ZbMGqhfik/2-currency-coins?orgId=1&panelId=18&from=1511669096889&to=1543205096889',
        width: '100%',
        height: '300',
        border: '0'
      },
      {
        title: 'Average Transactions Per Block (Daily)',
        url:
          '//beta.mosaic.io/analytics/d-solo/ZbMGqhfik/2-currency-coins?orgId=1&panelId=12&from=1511669107675&to=1543205107675',
        width: '100%',
        height: '300',
        border: '0'
      },
      {
        title: 'Volatility Volume Ratio (26-day)',
        url:
          '//beta.mosaic.io/analytics/d-solo/ZbMGqhfik/2-currency-coins?orgId=1&panelId=10&from=1511669138638&to=1543205138638',
        width: '100%',
        height: '300',
        border: '0'
      },
      {
        title: 'Average Transaction Value Per Address (Daily)',
        url:
          '//beta.mosaic.io/analytics/d-solo/ZbMGqhfik/2-currency-coins?orgId=1&panelId=2&from=1511669151788&to=1543205151788',
        width: '100%',
        height: '300',
        border: '0'
      },
      {
        title: 'Average Transaction Value (Daily)',
        url:
          '//beta.mosaic.io/analytics/d-solo/ZbMGqhfik/2-currency-coins?orgId=1&panelId=4&from=1511669170311&to=1543205170311',
        width: '100%',
        height: '300',
        border: '0'
      }
    ]
  }
]