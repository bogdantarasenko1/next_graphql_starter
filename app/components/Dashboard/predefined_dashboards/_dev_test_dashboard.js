export default [
  {
    predefined: true,
    select_data: {
      value: 'dev_test_dash',
      label: 'Dev Test Dashboard'
    },
    panels: [
      {
        title: 'Active Addresses',
        url:
          '//beta.mosaic.io/analytics/d-solo/pJm9_gaik/1-blockchain-vs-exchange-historical?orgId=1&panelId=7&from=1511661305810&to=1543197305810&var-left_field=All&var-right_field=price&var-symbol=ETH',
        width: '100%',
        height: '300',
        border: '0'
      },
      {
        title: 'Adjusted Transaction Vol',
        url:
          '//beta.mosaic.io/analytics/d-solo/pJm9_gaik/1-blockchain-vs-exchange-historical?orgId=1&panelId=9&from=1511661321636&to=1543197321636&var-left_field=All&var-right_field=price&var-symbol=ETH',
        width: '100%',
        height: '300',
        border: '0'
      }
    ]
  }
]