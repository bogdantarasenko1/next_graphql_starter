export default [
  {
    predefined: true,
    select_data: {
      value: 'sentiment',
      label: 'Sentiment'
    },
    panels: [
      {
        title: 'Sentiment',
        url:
          '//beta.mosaic.io/analytics/d-solo/FmPlpTfmk/7-sentiment?orgId=1&panelId=2&from=1541272746433&to=1542325156100',
        width: '100%',
        height: '300',
        border: '0'
      },
      {
        title: 'News & Sentiment',
        url:
          '//beta.mosaic.io/analytics/d-solo/FmPlpTfmk/7-sentiment?orgId=1&panelId=8&from=1541272746433&to=1542325156100',
        width: '100%',
        height: '300',
        border: '0'
      },
      {
        title: 'Social Activity',
        url:
          '//beta.mosaic.io/analytics/d-solo/FmPlpTfmk/7-sentiment?orgId=1&panelId=12&from=1541272746433&to=1542325156100',
        width: '100%',
        height: '300',
        border: '0'
      }
    ]
  }
]