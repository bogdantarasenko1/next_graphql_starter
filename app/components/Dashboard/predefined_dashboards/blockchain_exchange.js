export default [
  {
    predefined: true,
    select_data: {
      value: 'blockchain_exchange',
      label: 'Blockchain Exchange'
    },
    panels: [
      {
        title: 'Active Addresses',
        url:
          '//beta.mosaic.io/analytics/d-solo/pJm9_gaik/1-blockchain-vs-exchange-historical?orgId=1&panelId=7&from=1511661305810&to=1543197305810&var-left_field=All&var-right_field=price&var-symbol=ETH',
        width: '100%',
        height: '300',
        border: '0'
      },
      {
        title: 'Adjusted Transaction Vol',
        url:
          '//beta.mosaic.io/analytics/d-solo/pJm9_gaik/1-blockchain-vs-exchange-historical?orgId=1&panelId=9&from=1511661321636&to=1543197321636&var-left_field=All&var-right_field=price&var-symbol=ETH',
        width: '100%',
        height: '300',
        border: '0'
      },
      {
        title: 'Average Difficulty',
        url:
          '//beta.mosaic.io/analytics/d-solo/pJm9_gaik/1-blockchain-vs-exchange-historical?orgId=1&panelId=11&from=1511661373833&to=1543197373833&var-left_field=All&var-right_field=price&var-symbol=ETH',
        width: '100%',
        height: '300',
        border: '0'
      },
      {
        title: 'Blockcount',
        url:
          '//beta.mosaic.io/analytics/d-solo/pJm9_gaik/1-blockchain-vs-exchange-historical?orgId=1&panelId=13&from=1511661388429&to=1543197388429&var-left_field=All&var-right_field=price&var-symbol=ETH',
        width: '100%',
        height: '300',
        border: '0'
      },
      {
        title: 'Blocksize',
        url:
          '//beta.mosaic.io/analytics/d-solo/pJm9_gaik/1-blockchain-vs-exchange-historical?orgId=1&panelId=15&from=1511661404815&to=1543197404815&var-left_field=All&var-right_field=price&var-symbol=ETH',
        width: '100%',
        height: '300',
        border: '0'
      },
      {
        title: 'Exchange Volume',
        url:
          '//beta.mosaic.io/analytics/d-solo/pJm9_gaik/1-blockchain-vs-exchange-historical?orgId=1&panelId=17&from=1511661419320&to=1543197419320&var-left_field=All&var-right_field=price&var-symbol=ETH',
        width: '100%',
        height: '300',
        border: '0'
      },
      {
        title: 'Fees',
        url:
          '//beta.mosaic.io/analytics/d-solo/pJm9_gaik/1-blockchain-vs-exchange-historical?orgId=1&panelId=19&from=1511661443149&to=1543197443149&var-left_field=All&var-right_field=price&var-symbol=ETH',
        width: '100%',
        height: '300',
        border: '0'
      },
      {
        title: 'Generated Coins',
        url:
          '//beta.mosaic.io/analytics/d-solo/pJm9_gaik/1-blockchain-vs-exchange-historical?orgId=1&panelId=21&from=1511661456661&to=1543197456661&var-left_field=All&var-right_field=price&var-symbol=ETH',
        width: '100%',
        height: '300',
        border: '0'
      },
      {
        title: 'MarketCap',
        url:
          '//beta.mosaic.io/analytics/d-solo/pJm9_gaik/1-blockchain-vs-exchange-historical?orgId=1&panelId=23&from=1511661472577&to=1543197472577&var-left_field=All&var-right_field=price&var-symbol=ETH',
        width: '100%',
        height: '300',
        border: '0'
      },
      {
        title: 'MedianFee',
        url:
          '//beta.mosaic.io/analytics/d-solo/pJm9_gaik/1-blockchain-vs-exchange-historical?orgId=1&panelId=25&from=1511661488300&to=1543197488300&var-left_field=All&var-right_field=price&var-symbol=ETH',
        width: '100%',
        height: '300',
        border: '0'
      },
      {
        title: 'Median Transaction Value',
        url:
          '//beta.mosaic.io/analytics/d-solo/pJm9_gaik/1-blockchain-vs-exchange-historical?orgId=1&panelId=27&from=1511661514404&to=1543197514404&var-left_field=All&var-right_field=price&var-symbol=ETH',
        width: '100%',
        height: '300',
        border: '0'
      },
      {
        title: 'PaymentCount',
        url:
          '//beta.mosaic.io/analytics/d-solo/pJm9_gaik/1-blockchain-vs-exchange-historical?orgId=1&panelId=29&from=1511661566652&to=1543197566652&var-left_field=All&var-right_field=price&var-symbol=ETH',
        width: '100%',
        height: '300',
        border: '0'
      },
      {
        title: 'Price',
        url:
          '//beta.mosaic.io/analytics/d-solo/pJm9_gaik/1-blockchain-vs-exchange-historical?orgId=1&panelId=31&from=1511661579043&to=1543197579043&var-left_field=All&var-right_field=price&var-symbol=ETH',
        width: '100%',
        height: '300',
        border: '0'
      },
      {
        title: 'Transaction Count',
        url:
          '//beta.mosaic.io/analytics/d-solo/pJm9_gaik/1-blockchain-vs-exchange-historical?orgId=1&panelId=33&from=1511661592763&to=1543197592763&var-left_field=All&var-right_field=price&var-symbol=ETH',
        width: '100%',
        height: '300',
        border: '0'
      },
      {
        title: 'Transaction Volume',
        url:
          '//beta.mosaic.io/analytics/d-solo/pJm9_gaik/1-blockchain-vs-exchange-historical?orgId=1&panelId=35&from=1511661606046&to=1543197606046&var-left_field=All&var-right_field=price&var-symbol=ETH',
        width: '100%',
        height: '300',
        border: '0'
      }
    ]
  }
]