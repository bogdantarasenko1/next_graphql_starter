export default [
  {
    predefined: true,
    select_data: {
      value: 'stablecoins',
      label: 'Stablecoins'
    },
    panels: [
      {
        title: 'Average Transaction Volume',
        url:
          '//beta.mosaic.io/analytics/d-solo/oI1jr2fmk/8-stablecoins?orgId=1&panelId=20&from=1535426302361&to=1543205902361',
        width: '100%',
        height: '300',
        border: '0'
      },
      {
        title: 'Active addresses',
        url:
          '//beta.mosaic.io/analytics/d-solo/oI1jr2fmk/8-stablecoins?orgId=1&panelId=18&from=1535426312596&to=1543205912596',
        width: '100%',
        height: '300',
        border: '0'
      },
      {
        title: 'Deviation from Peg',
        url:
          '//beta.mosaic.io/analytics/d-solo/oI1jr2fmk/8-stablecoins?orgId=1&panelId=16&from=1535426324725&to=1543205924725',
        width: '100%',
        height: '300',
        border: '0'
      },
      {
        title: 'Circulating Supply',
        url:
          '//beta.mosaic.io/analytics/d-solo/oI1jr2fmk/8-stablecoins?orgId=1&panelId=8&from=1535426335775&to=1543205935775',
        width: '100%',
        height: '300',
        border: '0'
      }
    ]
  }
]