export default [
  {
    predefined: true,
    select_data: {
      value: 'cryptoeconomic_ratios',
      label: 'Cryptoeconomic Ratios'
    },
    panels: [
      {
        title: 'NVT',
        url:
          '//beta.mosaic.io/analytics/d-solo/3JF9j2fmk/9-cryptoeconomic-ratios?orgId=1&panelId=18&from=1527303253421&to=1543204453421',
        width: '100%',
        height: '300',
        border: '0'
      },
      {
        title: 'Metcalfe\'s Law',
        url:'//beta.mosaic.io/analytics/d-solo/3JF9j2fmk/9-cryptoeconomic-ratios?orgId=1&panelId=14&from=1527303266696&to=1543204466696',
        width: '100%',
        height: '300',
        border: '0'
      },
      {
        title: 'Price to Metcalfe\'s Law',
        url:
          '//beta.mosaic.io/analytics/d-solo/3JF9j2fmk/9-cryptoeconomic-ratios?orgId=1&panelId=10&from=1527303280840&to=1543204480840',
            width: '100%',
        height: '300',
        border: '0'
      },
      {
        title: 'Fees',
          url:
        '//beta.mosaic.io/analytics/d-solo/3JF9j2fmk/9-cryptoeconomic-ratios?orgId=1&panelId=8&from=1527303296409&to=1543204496409',
          width: '100%',
        height: '300',
        border: '0'
      },
      {
        title: 'Price to Commits',
          url:
        '//beta.mosaic.io/analytics/d-solo/3JF9j2fmk/9-cryptoeconomic-ratios?orgId=1&panelId=2&from=1527303308611&to=1543204508611',
          width: '100%',
        height: '300',
        border: '0'
      }
    ]
  }
]