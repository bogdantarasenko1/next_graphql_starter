// @flow

import React from 'react'
import { Grid } from '@material-ui/core'
import Layout from '../MainLayout/MainLayout.screen'
// import styles from './styles.css'

type Props = {
  cryptoassets: React.Element<Any>,
  timechart: React.Element<Any>,
  research: React.Element<Any>,
  news: React.Element<Any>,
  twitterpanel: React.Element<Any>,
  transactionspanel: React.Element<Any>,
  datalake: React.Element<Any>,
  datalakestaging: React.Element<Any>,
  chartspicker: React.Element<Any>,
  t: func
};

const DashboardScreen = (props: Props) => {
  const {
    /* cryptoassets, */
    news,
    research,
    timechart,
    twitterpanel,
    transactionspanel,
    datalake,
    chartspicker,
    datalakestaging,
    dashboardconfignav,
    dialogs,
    notifications,
    saveAuth
    // t
  } = props

  const content = () => {
    return (
      <Grid container>
        {dialogs}
        {dashboardconfignav}
        <Grid container spacing={16}>
          <Grid item xs={12} md={8} lg={9}>
            <div className="widget">{timechart}</div>
            <div className="widget">{twitterpanel}</div>
            <div className="widget">{transactionspanel}</div>
            <div
              className="widget"
              style={{ display: 'flex', justifyContent: 'space-between' }}
            >
              <div style={{ width: '49%' }}>{datalake}</div>
              <div style={{ width: '49%' }}>{datalakestaging}</div>
            </div>
            {/* <div className="widget">
            {cryptoassets}
            {
              // <div className={styles.linkContainer}>
              //   <a href="/cryptoassets">{t('showMore')}</a>
              // </div>
            }
          </div> */}
          </Grid>
          <Grid item xs={12} md={4} lg={3}>
            <div className="widget">{news}</div>
            <div className="widget">{chartspicker}</div>
            <div className="widget">{research}</div>
          </Grid>
        </Grid>
      </Grid>
    )
  }

  return <Layout saveAuth={saveAuth} content={content()} {...props} />
}

export default DashboardScreen
