import _ from 'lodash'
export const getSlug = name => {

  let str = name.replace(/^\s+|\s+$/g, ""); // trim

  str = str.toLowerCase();

  // remove accents, swap ñ for n, etc
  var from = "åàáãäâèéëêìíïîòóöôùúüûñç·/_,:;";
  var to = "aaaaaaeeeeiiiioooouuuunc------";

  for (var i = 0, l = from.length; i < l; i++) {
    str = str.replace(new RegExp(from.charAt(i), "g"), to.charAt(i));
  }

  str = str
    .replace(/[^a-z0-9 -]/g, "") // remove invalid chars
    .replace(/\s+/g, "-") // collapse whitespace and replace by -
    .replace(/-+/g, "-") // collapse dashes
    .replace(/^-+/, "") // trim - from start of text
    .replace(/-+$/, ""); // trim - from end of text

  // return str;
  return str
}

const getUrlParams = (search = '') => {
  const hashes = search.slice(search.indexOf('?') + 1).split('&');
  return hashes.reduce((acc, hash) => {
    // eslint-disable-next-line
    const [key, val] = hash.split('=');
    return {
      ...acc,
      [key]: decodeURIComponent(val)
    };
  }, {});
};

export const getRawPanelData = item => {

  const url_object = item.url.split('?')
  const trimed_url = url_object[0].split('/')
  const url_params = getUrlParams(url_object[1]);
  const vars_scope = () => {
      let _scope = []
      _.forOwn(url_params, (value, key) => {
        if (key.indexOf('var-') === 0) {
          _scope.push({
            name: key.replace('var-', ''),
            value: value
          })
        }
      } );
      return _scope
    }

  let _tpl = {
    name:           item.title,
    slug:           getSlug(item.title),
    description:    '',
    start_date:     parseInt(url_params.from),
    end_date:       parseInt(url_params.to),
    graf_dash_uri:  `${trimed_url[trimed_url.length - 2]}/${trimed_url[trimed_url.length - 1]}`,
    graf_panel_id:  parseInt(url_params.panelId),
    panelVars: [
      ...vars_scope()
    ]
  }

  return _tpl

}

export const reorderArray = raw_array => {

  let _reordered_array = []

  raw_array.map((val, index) => {
    _reordered_array[raw_array.length - index - 1] = val
  })

  return _reordered_array
}

export const getFullMonth = date => {
  const month = date.getMonth() + 1
  return month < 10 ? '0' + month : '' + month
}

export const getFullDay = date => {
  const day = date.getDate()
  return day < 10 ? '0' + day : '' + day
}