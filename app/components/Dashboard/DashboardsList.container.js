// @flow

import React from 'react'
import axios from 'axios'
import styles from "../GrafanaCharts/styles.css";
import Select from "../Select";
import { PREDEFINED_DASHBOARDS } from './predefined_dashboards/index'


type Props = {};


class DashboardsList extends React.Component<State, Props> {
  constructor (props) {
    super(props)
    this.state = {
      selected_dashboard:           props.selected_users_dashboard,
      predefined_dashboards_scope:  PREDEFINED_DASHBOARDS,
      draft_dashboard:              props.draft_dashboard,
      assembled_dashboards_list:    props.assembled_dashboards_list
    }

    this.decorateDashboardsList         = this.decorateDashboardsList.bind(this);
    this.onDashSelect                   = this.onDashSelect.bind(this);
    this.decorateSelectedUserDashboard  = this.decorateSelectedUserDashboard.bind(this);

  }


  onDashSelect(selected_val){

    let selected_user_dashboard = this.state.assembled_dashboards_list.filter(item => {
      if (!item.predefined) {
        return item.slug === selected_val.value
      }

      return item.select_data.value === selected_val.value
    })

    if (selected_user_dashboard.length > 0) {
      this.props.onUserDashboardSelect(selected_user_dashboard[0]);
    }
  }

  decorateDashboardsList(){
    let select_list = []

    for(let _dash of this.state.assembled_dashboards_list) {
      if (_dash.predefined) {
        select_list.push(_dash.select_data);
      } else {
        select_list.push({value: _dash.slug, label: _dash.title})
      }

    }
    return select_list
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      assembled_dashboards_list: nextProps.assembled_dashboards_list,
      selected_dashboard: nextProps.selected_users_dashboard
    })
  }

  decorateSelectedUserDashboard(){
    if (this.state.selected_dashboard.predefined) {
      return this.state.selected_dashboard.select_data
    }
    return {
      label: this.state.selected_dashboard.title,
      value: this.state.selected_dashboard.slug
    }
  }

  render () {
    return (
      <div className={styles.pickerCategoryContainer}>
        <Select list={this.decorateDashboardsList()}
                placeholder="Select dashboard"
                selected_val={this.decorateSelectedUserDashboard()}
                onSelect={this.onDashSelect}/>
      </div>
    )
  }
}

export default DashboardsList
