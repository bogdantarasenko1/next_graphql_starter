// @flow

import React, { Component } from 'react'

import { withStyles } from '@material-ui/core'

import AssetInput from './AssetInput.screen'
import MetricsInput from './MetricsInput.screen'

type Props = {
  primarySymbolText: string,
  secondarySymbol: string,
  primaryMetricField: string,
  secondaryMetricField: string,
  onMetricChange: () => void,
  onSymbolChange: () => void,
  classes: Object,
  t: string => string
};

const styles = theme => ({
  root: {
    width: '100%',
    display: 'flex',
    [theme.breakpoints.down('sm')]: {
      flexDirection: 'column'
    },
    justifyContent: 'space-around',
    alignItems: 'center'
  },
  asset: {
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'baseline'
  }
})

class ChartInputs extends Component {
  props: Props;

  render () {
    const {
      primarySymbolText,
      secondarySymbol,
      primaryMetricField,
      secondaryMetricField,
      onMetricChange,
      onSymbolChange,
      classes,
      t
    } = this.props

    return (
      <div className={classes.root}>
        <div className={classes.asset}>
          <AssetInput
            primary={true}
            t={t}
            symbol={primarySymbolText}
            onSymbolChange={onSymbolChange}
          />
          <MetricsInput
            primary={true}
            metricField={primaryMetricField}
            onMetricChange={onMetricChange}
          />
        </div>
        <div className={classes.asset}>
          <AssetInput
            primary={false}
            t={t}
            symbol={secondarySymbol}
            onSymbolChange={onSymbolChange}
          />
          <MetricsInput
            primary={false}
            metricField={secondaryMetricField}
            onMetricChange={onMetricChange}
          />
        </div>
      </div>
    )
  }
}

export default withStyles(styles)(ChartInputs)
