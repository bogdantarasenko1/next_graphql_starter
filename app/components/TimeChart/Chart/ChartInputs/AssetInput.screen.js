// @flow
import React from 'react'

import TextField from '@material-ui/core/TextField'

import { withStyles } from '@material-ui/core'

type Props = {
  t: func,
  symbol: string,
  onSymbolChange: () => void,
  primary: boolean,
  classes: Object
};

const styles = theme => ({
  root: {
    width: theme.spacing.unit * 13
  },
  primary: {
    '&:hover': {
      '&::before': {
        borderColor: '#f47a20 !important'
      }
    },
    '&:after': {
      borderBottom: '2px solid #f47a20 !important'
    }
  },
  secondary: {
    '&:hover': {
      '&::before': {
        borderColor: '#4289cb !important'
      }
    },
    '&:after': {
      borderBottom: '2px solid #4289cb !important'
    }
  }
})

const AssetInput = (props: Props) => {
  const { t, symbol, onSymbolChange, primary, classes } = props

  const underlineStyle = primary ? classes.primary : classes.secondary
  return (
    <TextField
      className={classes.root}
      label={t('time-chart:Enter Asset')}
      type="search"
      margin="dense"
      onKeyPress={e => onSymbolChange(e, primary)}
      value={symbol}
      onChange={e => onSymbolChange(e, primary)}
      inputProps={{
        style: { textTransform: 'uppercase' }
      }}
      InputProps={{
        classes: { underline: underlineStyle }
      }}
    />
  )
}

export default withStyles(styles)(AssetInput)
