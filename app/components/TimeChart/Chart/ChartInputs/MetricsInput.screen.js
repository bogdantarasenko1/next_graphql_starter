// @flow
import React from 'react'

import { withStyles } from '@material-ui/core'
import MenuItem from '@material-ui/core/MenuItem'
import FormControl from '@material-ui/core/FormControl'
import Select from '@material-ui/core/Select'
import Input from '@material-ui/core/Input'

import { availableQueries } from '../../../../utils/timeChartMetricsQuery'

type Props = {
  onMetricChange: () => void,
  metricField: string,
  classes: Object,
  primary: Boolean
};

const styles = theme => ({
  root: {
    width: theme.spacing.unit * 25
  },
  primary: {
    '&:hover': {
      '&::before': {
        borderColor: '#f47a20 !important'
      }
    },
    '&:after': {
      borderBottom: '2px solid #f47a20 !important'
    }
  },
  secondary: {
    '&:hover': {
      '&::before': {
        borderColor: '#4289cb !important'
      }
    },
    '&:after': {
      borderBottom: '2px solid #4289cb !important'
    }
  }
})

const MetricsInput = (props: Props) => {
  const { onMetricChange, metricField, primary, classes } = props
  const underlineStyle = primary ? classes.primary : classes.secondary
  return (
    <FormControl className={classes.root}>
      <Select
        input={<Input className={underlineStyle} />}
        value={metricField}
        onChange={e => onMetricChange(e, primary)}
      >
        {availableQueries.map(e => {
          const menuLabel = e
          const menuValue = e.replace(/\s/g, '').toUpperCase()

          return (
            <MenuItem key={menuValue} value={menuLabel}>
              {menuLabel}
            </MenuItem>
          )
        })}
      </Select>
    </FormControl>
  )
}

export default withStyles(styles)(MetricsInput)
