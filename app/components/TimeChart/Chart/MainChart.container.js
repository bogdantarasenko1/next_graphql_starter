// @flow

import React, { Component } from 'react'
import { debounce } from 'lodash'
import { fetchData } from '../../../utils/dataPlatformQuery'
import {
  getQueryField,
  getQueryValue,
  getQuery
} from '../../../utils/timeChartMetricsQuery'
import MainChartScreen from './MainChart.screen'
import ChartInputs from './ChartInputs/ChartInputs.container'

type Props = {
  assetSymbol: string,
  t: string => string
};

class MainChartContainer extends Component {
  props: Props;

  constructor (props: Props) {
    super(props)
    const symbol = this.props.assetSymbol || 'BTC'

    this.state = {
      isLoading: true,
      fromDate: '',
      toDate: '',
      primarySymbol: symbol,
      primarySymbolText: symbol,
      secondarySymbol: '',
      primaryMetricField: 'Price (USD)',
      secondaryMetricField: 'Price (USD)',
      primaryMetricData: [],
      secondaryMetricData: [],
      primaryBarData: [],
      secondaryBarData: []
    }
  }

  onMetricChange = async (e, primary) => {
    const newMetric = e.target.value

    if (primary) {
      await this.setState({ primaryMetricField: newMetric, isLoading: true })
    } else {
      await this.setState({ secondaryMetricField: newMetric, isLoading: true })
    }

    this.onMetricDataChange(primary)
  };

  onMetricDataChange = async primary => {
    if (primary) {
      const metricData = await this.loadMetrics(true, false)
      this.setState({
        primaryMetricData: metricData,
        isLoading: false
      })
    } else {
      const metricData = await this.loadMetrics(false, false)
      this.setState({
        secondaryMetricData: metricData,
        isLoading: false
      })
    }
  };

  onSymbolChange = async (e, primary) => {
    const newSymbol = e.target.value.toUpperCase()

    if (primary) {
      if (e.key === 'Enter') {
        await this.setState({ primarySymbol: newSymbol, isLoading: true })
        this.onSymbolDataChange(primary)
      } else {
        this.setState({ primarySymbolText: newSymbol })
      }
    } else {
      if (e.key === 'Enter') {
        await this.setState({ secondarySymbol: newSymbol, isLoading: true })
        this.onSymbolDataChange(primary)
      } else {
        this.setState({ secondarySymbol: newSymbol })
      }
    }
  };

  onSymbolDataChange = debounce(async (primary = true) => {
    if (primary) {
      const barData = await this.loadMetrics(true, true)
      const metricData = await this.loadMetrics(true, false)
      this.setState({
        primaryMetricData: metricData,
        primaryBarData: barData,
        isLoading: false
      })
    } else {
      const barData = await this.loadMetrics(false, true)
      const metricData = await this.loadMetrics(false, false)
      this.setState({
        secondaryMetricData: metricData,
        secondaryBarData: barData,
        isLoading: false
      })
    }
  }, 150);

  loadMetrics = async (primary = true, bar = false) => {
    const {
      fromDate,
      toDate,
      primarySymbol,
      secondarySymbol,
      primaryMetricField,
      secondaryMetricField
    } = this.state

    const querySymbol = primary ? primarySymbol : secondarySymbol
    const queryMetricField = bar
      ? 'VOLUME'
      : primary
        ? primaryMetricField.replace(/\s/g, '').toUpperCase()
        : secondaryMetricField.replace(/\s/g, '').toUpperCase()

    const mdpField = await getQueryField(queryMetricField)
    const { queryTemplate, queryString } = await getQuery(
      queryMetricField,
      querySymbol,
      fromDate,
      toDate,
      mdpField
    )

    const rawData = await fetchData(queryTemplate)

    let transformedData

    if (queryString === 'getHistoricalBlockchainData') {
      transformedData = this.transformMetric(
        queryMetricField,
        rawData.data.getHistoricalBlockchainData.map(e => e)
      )
    } else if (queryString === 'getCrypto') {
      transformedData = this.transformMetric(
        queryMetricField,
        rawData.data.getCrypto.market.historical
      )
    }

    return transformedData
  };

  transformMetric = (metricField, data) => {
    let transformedData = []
    data.forEach(e => {
      let tempMetric = []
      const timestamp = Date.parse(e.timestamp)
      const dataPoint = getQueryValue(e, metricField)
      tempMetric.push(timestamp)
      tempMetric.push(dataPoint)
      transformedData.push(tempMetric)
    })

    return transformedData
  };

  componentDidUpdate (oldProps, oldState) {
    const { primarySymbol: oldSymbol } = oldState
    const { assetSymbol } = this.props

    if (assetSymbol && assetSymbol !== oldSymbol) {
      this.setState(
        { primarySymbol: assetSymbol, primarySymbolText: assetSymbol },
        this.onSymbolDataChange
      )
    }
  }

  async componentDidMount () {
    let dateNow = new Date()
    let dateOneYear = dateNow.setFullYear(dateNow.getFullYear() - 1)
    dateNow = new Date().toISOString()
    dateOneYear = new Date(dateOneYear).toISOString()

    await this.setState({
      fromDate: dateOneYear,
      toDate: dateNow
    })

    this.onSymbolDataChange()
  }

  render () {
    const {
      isLoading,
      primarySymbol,
      secondarySymbol,
      primaryMetricField,
      secondaryMetricField,
      primaryMetricData,
      secondaryMetricData,
      primaryBarData,
      secondaryBarData,
      primarySymbolText
    } = this.state

    const { t } = this.props

    return (
      <React.Fragment>
        <ChartInputs
          t={t}
          primarySymbolText={primarySymbolText}
          primarySymbol={primarySymbol}
          secondarySymbol={secondarySymbol}
          primaryMetricField={primaryMetricField}
          secondaryMetricField={secondaryMetricField}
          onSymbolChange={this.onSymbolChange}
          onMetricChange={this.onMetricChange}
        />
        <MainChartScreen
          isLoading={isLoading}
          t={t}
          primaryData={{
            metric: primaryMetricData,
            volume: primaryBarData
          }}
          secondaryData={{
            metric: secondaryMetricData,
            volume: secondaryBarData
          }}
          metricField={{
            primary: primaryMetricField,
            secondary: secondaryMetricField
          }}
          symbol={{
            primary: primarySymbol,
            secondary: secondarySymbol
          }}
        />
      </React.Fragment>
    )
  }
}

export default MainChartContainer
