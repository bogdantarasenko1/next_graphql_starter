// @flow

import React from 'react'
import { CircularProgress, withStyles } from '@material-ui/core'
import Highcharts from 'highcharts/highstock'
import {
  HighchartsStockChart,
  Chart,
  withHighcharts,
  XAxis,
  YAxis,
  Navigator,
  RangeSelector,
  Tooltip,
  Legend
} from 'react-jsx-highstock'

import { switchGraph } from '../../../utils/timeChartSwitchGraphs'

type Props = {
  primaryData: Object,
  secondaryData: Object,
  metricField: Object,
  symbol: Object,
  isLoading: boolean,
  classes: Object,
  t: string => string
};

const styles = {
  root: {
    position: 'relative'
  },
  center: {
    position: 'absolute',
    top: '50%',
    left: '50%',
    marginRight: '-50%',
    zIndex: 100
  }
}

const MainChartScreen = (props: Props) => {
  const {
    symbol,
    primaryData,
    secondaryData,
    metricField,
    isLoading,
    classes,
    t
  } = props
  Highcharts.setOptions({
    lang: {
      thousandsSep: ','
    }
  })

  // Circulating Percent Total should will never exceed 1.0. By default, max tick allows over 1.0.
  // This function ensures that when that field is chosen, max tick is set to 1.0
  const ceilingChecker = metricField => {
    if (metricField === 'Circulating Percent Total') {
      return 1
    }
  }

  return (
    <div className={classes.root}>
      {isLoading && (
        <div className={classes.center}>
          <CircularProgress color="secondary" />
        </div>
      )}
      <HighchartsStockChart
        responsive={{
          rules: [
            {
              condition: {
                maxWidth: 600
              },
              chartOptions: {
                legend: {
                  align: 'center',
                  verticalAlign: 'bottom',
                  layout: 'horizontal'
                }
              }
            }
          ]
        }}
      >
        <Chart backgroundColor={'#292929'} />

        {primaryData.metric.length !== 0 && (
          <RangeSelector
            labelStyle={{ display: 'none' }}
            buttonTheme={{
              fill: '#292929',
              style: {
                color: '#acaaaa'
              }
            }}
            x={-40}
          >
            <RangeSelector.Button count={5} type="day">
              {t('time-chart:5d')}
            </RangeSelector.Button>
            <RangeSelector.Button count={1} type="month">
              {t('time-chart:1m')}
            </RangeSelector.Button>
            <RangeSelector.Button count={3} type="month">
              {t('time-chart:3m')}
            </RangeSelector.Button>
            <RangeSelector.Button count={6} type="month">
              {t('time-chart:6m')}
            </RangeSelector.Button>
            <RangeSelector.Button count={12} type="month">
              {t('time-chart:1y')}
            </RangeSelector.Button>
          </RangeSelector>
        )}

        <Tooltip valueDecimals={2} shared />

        <Legend
          layout="vertical"
          align="right"
          verticalAlign="middle"
          borderWidth={0}
          itemStyle={{ color: '#666' }}
          itemHoverStyle={{ color: '#999' }}
          itemHiddenStyle={{ color: '#333333' }}
        />

        <XAxis crosshair={{ dashStyle: 'Dash' }}>
          <XAxis.Title>{t('time-chart:Time')}</XAxis.Title>
        </XAxis>

        {primaryData.metric.length !== 0 && (
          <YAxis
            gridLineColor="#666"
            height="71%"
            crosshair={{ dashStyle: 'Dash' }}
            tickAmount={6}
            floor={0.00001}
            ceiling={ceilingChecker(metricField.primary)}
          >
            <YAxis.Title>{t(`time-chart:${metricField.primary}`)}</YAxis.Title>
            {switchGraph(
              symbol.primary,
              metricField.primary,
              primaryData.metric,
              true,
              t
            )}
          </YAxis>
        )}

        {secondaryData.metric.length !== 0 && (
          <YAxis
            gridLineColor="#666"
            height="71%"
            opposite
            crosshair={{ dashStyle: 'Dash' }}
            tickAmount={6}
            floor={0.00001}
            ceiling={ceilingChecker(metricField.secondary)}
          >
            <YAxis.Title>
              {t(`time-chart:${metricField.secondary}`)}
            </YAxis.Title>
            {switchGraph(
              symbol.secondary,
              metricField.secondary,
              secondaryData.metric,
              false,
              t
            )}
          </YAxis>
        )}

        {(primaryData.volume.length !== 0 ||
          secondaryData.volume.length !== 0) && (
          <YAxis
            gridLineColor="#666"
            crosshair={{ dashStyle: 'Dash' }}
            top="75%"
            height="25%"
            offset={0}
          >
            <YAxis.Title>{t('time-chart:Volume')}</YAxis.Title>
            {primaryData.volume.length !== 0 &&
              switchGraph(
                symbol.primary,
                'Volume',
                primaryData.volume,
                true,
                t
              )}
            {secondaryData.volume.length !== 0 &&
              switchGraph(
                symbol.secondary,
                'Volume',
                secondaryData.volume,
                false,
                t
              )}
          </YAxis>
        )}

        <Navigator height={30} margin={1}>
          {primaryData.metric.length !== 0 && (
            <Navigator.Series seriesId="primaryMetric" />
          )}
          {secondaryData.metric.length !== 0 && (
            <Navigator.Series seriesId="secondaryMetric" />
          )}
        </Navigator>
      </HighchartsStockChart>
    </div>
  )
}

export default withStyles(styles)(withHighcharts(MainChartScreen, Highcharts))
