// @flow

import React from 'react'
import MainChart from './Chart/MainChart.container'

type Props = {
  t: func,
  assetSymbol: string
};

const TimeChartScreen = (props: Props) => {
  const { t, assetSymbol } = props

  return (
    <React.Fragment>
      <MainChart t={t} assetSymbol={assetSymbol} />
    </React.Fragment>
  )
}

export default TimeChartScreen
