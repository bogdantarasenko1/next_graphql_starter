// @flow

import React from 'react'
import DashboardHeader from '../Dashboard/DashboardHeader.screen.js'
import TimeChartScreen from './TimeChart.screen'

class TimeChartContainer extends React.Component<State, Props> {
  render () {
    const { assetSymbol, t } = this.props

    return (
      <React.Fragment>
        <DashboardHeader title={'Time Chart'} />
        <TimeChartScreen t={t} assetSymbol={assetSymbol} />
      </React.Fragment>
    )
  }
}

export default TimeChartContainer
