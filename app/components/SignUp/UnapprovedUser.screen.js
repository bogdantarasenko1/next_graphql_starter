// @flow

import React from 'react'
import { Button, Typography, withStyles } from '@material-ui/core'
import AuthCard from '../AuthCard/AuthCard.screen'

type Props = {
  submit: () => void,
  classes: Object,
  t: string => string
};

const styles = () => ({
  actions: {
    flexGrow: 1,
    textAlign: 'right'
  }
})

const UnapprovedUserScreen = (props: Props) => {
  const { submit, classes, t } = props

  const title = (
    <React.Fragment>
      <Typography
        variant="subheading"
        color="secondary"
        component="span"
        paragraph
      >
        {t('login:Thank you for your interest')}
      </Typography>
      <Typography variant="body1" color="default" component="span">
        {t(
          'login:Mosiac is currently in a limited beta and your request to join will be reviewed. If you are approved you will recieve a confirmation email with instructions to finish the sign up process.'
        )}
      </Typography>
    </React.Fragment>
  )

  const actions = (
    <div className={classes.actions}>
      <Button
        onClick={submit}
        type="button"
        variant="contained"
        color="secondary"
      >
        {t('common:OK')}
      </Button>
    </div>
  )

  return <AuthCard title={title} actions={actions} />
}

export default withStyles(styles)(UnapprovedUserScreen)
