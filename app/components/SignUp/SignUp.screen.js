// @flow

import React from 'react'
import Link from 'next/link'
import { FormControl, Typography, Button } from '@material-ui/core'
import AuthCard from '../AuthCard/AuthCard.screen'
import AuthTextField from '../ui/AuthTextField'

type Props = {
  firstName: string,
  lastName: string,
  email: string,
  password: string,
  isUserUnapproved: boolean,
  isProcessingSignUp: boolean,
  shouldDisableSignUp: boolean,
  signUpError: ?Object,
  emailError: ?string,
  onEmailChange: () => void,
  onPasswordChange: () => void,
  onFirstNameChange: () => void,
  onLastNameChange: () => void,
  onSubmit: () => void
};

const SignUp = (props: Props) => {
  const {
    onSubmit,
    firstName,
    lastName,
    email,
    password,
    isProcessingSignUp,
    shouldDisableSignUp,
    signUpError,
    emailError,
    onEmailChange,
    onPasswordChange,
    onFirstNameChange,
    onLastNameChange
  } = props

  const submitBtnText = isProcessingSignUp ? 'Signing in' : 'Sign up'

  const emailDisplayError = () => {
    return emailError ? { error: !!emailError } : {}
  }

  const otherError = () => {
    if (emailError) {
      return false
    }

    return signUpError
  }

  const title = (
    <React.Fragment>
      Welcome to Mosaic
      <br />
      Please Sign up to continue
    </React.Fragment>
  )

  const inputs = (
    <React.Fragment>
      <FormControl fullWidth>
        <AuthTextField
          label="First Name"
          name="firstName"
          type="text"
          value={firstName}
          onChange={onFirstNameChange}
          id="firstName"
        />
      </FormControl>

      <FormControl fullWidth>
        <AuthTextField
          label="Last Name"
          name="lastName"
          type="text"
          value={lastName}
          onChange={onLastNameChange}
          id="lastName"
        />
      </FormControl>

      <FormControl fullWidth>
        <AuthTextField
          {...emailDisplayError()}
          label="Email"
          name="email"
          type="email"
          value={email}
          autoComplete="email"
          onChange={onEmailChange}
          id="email"
        />
        {emailError && (
          <Typography variant="subheading" color="error" component="div">
            {emailError}
          </Typography>
        )}
      </FormControl>

      <FormControl fullWidth>
        <AuthTextField
          label="Password"
          name="password"
          type="password"
          value={password}
          autoComplete="current-password"
          onChange={onPasswordChange}
          id="password"
        />
      </FormControl>
    </React.Fragment>
  )

  const actions = (
    <React.Fragment>
      <Typography
        variant="subheading"
        color="textSecondary"
        component="span"
        noWrap
      >
        Already have an account?
        <br />
        <Link prefetch href="/login">
          <a href="/login">Sign In</a>
        </Link>
      </Typography>
      <Button
        disabled={shouldDisableSignUp}
        type="submit"
        variant="contained"
        color="secondary"
      >
        {submitBtnText}
      </Button>
      {otherError() && <div className="error">{otherError()}</div>}
    </React.Fragment>
  )

  return (
    <AuthCard
      inputs={inputs}
      title={title}
      actions={actions}
      onSubmit={onSubmit}
    />
  )
}

export default SignUp
