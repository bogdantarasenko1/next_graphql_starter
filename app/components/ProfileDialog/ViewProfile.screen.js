// @flow

import React from 'react'
import {
  Avatar,
  Grid,
  List,
  ListItem,
  ListItemText,
  withStyles
} from '@material-ui/core'

type Props = {
  classes: Object,
  t: string => string,
  email: ?string,
  user: Object
};

const styles = theme => ({
  avatarContainer: {
    display: 'flex',
    justifyContent: 'center'
  },
  avatar: {
    height: 60,
    width: 60
  },
  primary: {
    color: '#999999',
    fontSize: '0.85rem'
  },
  secondary: {
    fontSize: '1rem',
    color: '#1F1F1F'
  },
  cell: {
    backgroundColor: '#F2F2F2'
  }
})

const ViewProfileScreen = (props: Props) => {
  const { classes, t, user, email } = props

  return (
    <div>
      <List>
        <Grid container spacing={24}>
          <Grid item xs={12} md={2}>
            <div className={classes.avatarContainer}>
              <Avatar
                src={user.avatarUrl || '/static/Shape.png'}
                className={classes.avatar}
              />
            </div>
          </Grid>
          <Grid item container xs={12} md={10} spacing={24}>
            <Grid item xs={6}>
              <ListItem className={classes.cell}>
                <ListItemText
                  classes={{
                    primary: classes.primary,
                    secondary: classes.secondary
                  }}
                  primary={t('common:First Name')}
                  secondary={user.firstName}
                />
              </ListItem>
            </Grid>
            <Grid item xs={6}>
              <ListItem className={classes.cell}>
                <ListItemText
                  classes={{
                    primary: classes.primary,
                    secondary: classes.secondary
                  }}
                  primary={t('common:Last Name')}
                  secondary={user.lastName}
                />
              </ListItem>
            </Grid>
            {
              // <Grid item xs={12}>
              //   <ListItem className={classes.cell}>
              //     <ListItemText
              //       classes={{
              //         primary: classes.primary,
              //         secondary: classes.secondary
              //       }}
              //       primary={t('common:Account Type')}
              //       secondary="Mosaic Research Team"
              //     />
              //   </ListItem>
              // </Grid>
            }
            {email && (
              <Grid item xs={12}>
                <ListItem className={classes.cell}>
                  <ListItemText
                    classes={{
                      primary: classes.primary,
                      secondary: classes.secondary
                    }}
                    primary={t('common:Email')}
                    secondary={email}
                  />
                </ListItem>
              </Grid>
            )}
            <Grid item xs={12}>
              <ListItem className={classes.cell}>
                <ListItemText
                  classes={{
                    primary: classes.primary,
                    secondary: classes.secondary
                  }}
                  primary={t('common:Short Bio')}
                  secondary={user.bio}
                />
              </ListItem>
            </Grid>
          </Grid>
        </Grid>
      </List>
    </div>
  )
}

export default withStyles(styles)(ViewProfileScreen)
