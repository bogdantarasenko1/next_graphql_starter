// @flow

import { FormControl, Grid, TextField, Typography } from '@material-ui/core'
import React from 'react'

type Props = {
  t: string => string
};

const ChangePasswordScreen = (props: Props) => {
  const { t } = props

  return (
    <div>
      <Grid item xs={12}>
        <Typography>{t('common:Change Password')}</Typography>
      </Grid>
      <Grid item xs={12}>
        <FormControl fullWidth>
          <TextField
            id="oldPassword"
            name="oldPassword"
            label={t('common:Old Password')}
            type="password"
            margin="normal"
          />
        </FormControl>
        <FormControl fullWidth>
          <TextField
            id="newPassword"
            name="password"
            label={t('common:New Password')}
            type="password"
            margin="normal"
          />
        </FormControl>
        <FormControl fullWidth>
          <TextField
            id="repeatPassword"
            name="repeatPassword"
            label={t('common:Repeat')}
            type="password"
            margin="normal"
          />
        </FormControl>
      </Grid>
    </div>
  )
}

export default ChangePasswordScreen
