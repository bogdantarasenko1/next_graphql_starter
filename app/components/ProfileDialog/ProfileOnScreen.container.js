// @flow

import React from 'react'
import EditProfile from '../../components/ProfileDialog/ProfileDialog.container'

type Props = {
  userId: string,
  editProfile: boolean,
  onCloseProfile: () => boolean
};

type State = {
  editProfile: boolean
};

class ProfileOnScreenContainer extends React.PureComponent<Props, State> {
  constructor (props: Props) {
    super(props)
    this.state = {
      editProfile: props.editProfile
    }
  }

  hideProfile = () => {
    this.setState({
      editProfile: false
    })
    this.props.onCloseProfile()
  };

  render () {
    const { editProfile } = this.state

    return (
      <div>
        <EditProfile
          close={this.hideProfile}
          isOpened={editProfile}
          {...this.props}
        />
      </div>
    )
  }
}

export default ProfileOnScreenContainer
