// @flow

import React from 'react'
import {
  Dialog,
  Toolbar,
  DialogContent,
  withMobileDialog,
  CircularProgress,
  withStyles
} from '@material-ui/core'

type Props = {
  fullScreen: boolean,
  classes: Object,
  toolBarAction: React.ReactNode,
  changePassword?: React.ReactNode,
  t: string => string,
  content: React.ReactNode,
  isLoading: boolean,
  isOpened: boolean,
  close: () => void,
  cancel: () => void,
  save: () => void
};

const styles = theme => ({
  paper: {
    backgroundColor: '#FAFAFA'
  },
  loader: {
    height: 200,
    width: 600,
    textAlign: 'center'
  }
})

const ProfileDialogScreen = (props: Props) => {
  const {
    classes,
    isOpened,
    isLoading,
    close,
    toolBarAction,
    content,
    fullScreen
  } = props
  return (
    <div>
      <Dialog
        fullScreen={fullScreen}
        open={isOpened}
        onClose={close}
        maxWidth="sm"
        classes={{ paper: classes.paper }}
      >
        <div>
          <Toolbar>{toolBarAction}</Toolbar>
        </div>
        {isLoading ? (
          <div className={classes.loader}>
            {' '}
            <CircularProgress color="secondary" />
          </div>
        ) : (
          <DialogContent className={classes.dialog}>{content}</DialogContent>
        )}
      </Dialog>
    </div>
  )
}

export default withStyles(styles)(withMobileDialog()(ProfileDialogScreen))
