// @flow

import React from 'react'
import { Query, graphql } from 'react-apollo'
import gql from 'graphql-tag'
import ProfileDialog from './ProfileDialog.sceen'
import { EditProfile, ViewProfile } from './ToolBarActions.screen'
import ChangePassword from './ChangePassword.screen'
import ViewContent from './ViewProfile.screen'
import FormContent from './ProfileForm.screen'

const QUERY_USER = gql`
  query UserQuery($userId: ID!) {
    viewer {
      user(id: $userId) {
        id
        avatarUrl
        bio
        firstName
        lastName
      }
      currentUser {
        id
        email
        avatarUrl
        bio
        firstName
        lastName
      }
    }
  }
`

const QUERY_PROFILE = gql`
  query UserProfileQuery($userId: String) {
    userProfile(id: $userId) @client {
      id
      firstName
      lastName
      avatarUrl
      bio
    }
  }
`

const UPDATE_USER = gql`
  mutation UpdateUser(
    $id: ID!
    $firstName: String
    $lastName: String
    $bio: String
    $avatarUrl: String
  ) {
    updateUser(
      input: {
        id: $id
        firstName: $firstName
        lastName: $lastName
        bio: $bio
        avatarUrl: $avatarUrl
      }
    ) {
      user {
        id
        firstName
        lastName
        bio
        avatarUrl
      }
    }
  }
`

type Props = {
  t: string => string,
  isLoading: boolean,
  userId: string,
  isOpened: boolean,
  close: () => void,
  viewer: Object,
  userProfile: ?Object,
  mutate: func
};

type State = {
  user: {
    firstName: ?string,
    lastName: ?string,
    email: ?string,
    bio: ?string,
    avatarUrl: ?string,
    accountType?: number
  }
};

class ProfileDialogContainer extends React.PureComponent<Props, State> {
  props: Props;
  state: State;

  constructor (props) {
    super(props)
    const { viewer: { currentUser } = {} } = props
    this.state = {
      editForm: false,
      user: { ...currentUser },
      isLoading: props.isLoading
    }
  }

  componentDidUpdate (props, state) {
    const { isLoading, viewer: { currentUser } = {} } = this.props

    if (!currentUser || isLoading === state.isLoading) {
      return
    }

    if (JSON.stringify(currentUser) !== JSON.stringify(this.state.user)) {
      this.setState({ user: { ...currentUser }, isLoading })
    }
  }

  dialogContent = () => {
    const { t, userProfile, viewer: { user } = {} } = this.props

    const { editForm, user: currentUser } = this.state
    if (!currentUser || !user) {
      return null
    }
    if (editForm) {
      return (
        <FormContent
          onChangeField={this.onChangeField}
          user={currentUser}
          t={t}
        />
      )
    } else {
      return (
        <ViewContent
          email={currentUser.id === user.id ? currentUser.email : null}
          user={userProfile || user}
          t={t}
        />
      )
    }
  };

  toolBarActions = () => {
    const { close, t, viewer: { currentUser, user } = {} } = this.props
    if (!currentUser || !user) {
      return null
    }
    const { editForm } = this.state
    if (editForm) {
      return <EditProfile save={this.save} cancel={close} t={t} />
    } else {
      if (currentUser.id === user.id) {
        return <ViewProfile close={close} edit={this.edit} t={t} />
      } else {
        return <ViewProfile close={close} t={t} />
      }
    }
  };

  changePassword = () => {
    const { t } = this.props
    return <ChangePassword t={t} />
  };

  edit = () => {
    this.setState({ editForm: true })
  };

  save = () => {
    const {
      close,
      viewer: { currentUser },
      mutate
    } = this.props
    const { user } = this.state
    if (JSON.stringify(currentUser) !== JSON.stringify(user)) {
      this.setState({ isLoading: true })
      mutate({ variables: { ...user } }).then(() => {
        this.setState({ isLoading: true }, close)
      })
    } else {
      close()
    }
  };

  onChangeField = (fieldName, event) => {
    const { user: oldUser } = this.state
    const newUser = { ...oldUser }
    newUser[fieldName] = event.target.value

    this.setState({ user: newUser })
  };

  render () {
    const { isOpened, close } = this.props
    const { isLoading } = this.state
    return (
      <ProfileDialog
        {...this.props}
        isLoading={isLoading}
        isOpened={isOpened}
        close={close}
        toolBarAction={this.toolBarActions()}
        changePassword={this.changePassword()}
        content={this.dialogContent()}
      />
    )
  }
}

const wrapper = (props: Props) => (
  <Query query={QUERY_USER} variables={{ userId: props.userId }}>
    {({ loading: loadingUser, error: errorUser, data: { viewer } }) => (
      <Query query={QUERY_PROFILE} variables={{ userId: props.userId }}>
        {({
          loading: loadingProfile,
          error: errorProfile,
          data: { userProfile }
        }) => {
          const isLoading = loadingUser || loadingProfile
          if (errorUser || errorProfile) {
            return `Error!: ${errorUser} ${errorProfile} `
          }
          return (
            <ProfileDialogContainer
              {...props}
              isLoading={isLoading}
              viewer={viewer}
              userProfile={userProfile}
            />
          )
        }}
      </Query>
    )}
  </Query>
)

export default graphql(UPDATE_USER)(wrapper)
