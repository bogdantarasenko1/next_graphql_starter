// @flow

import {
  Avatar,
  Button,
  FormControl,
  Grid,
  // InputLabel,
  // MenuItem,
  // Select,
  TextField,
  withStyles
} from '@material-ui/core'
import React from 'react'

type Props = {
  t: string => string,
  classes: Object,
  changePassword: React.ReactNode,
  user: Object,
  onChangeField: (fieldName: string, event: Object) => void
};

const styles = theme => ({
  changeAvatar: {
    display: 'flex'
  },
  avatarContainer: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center'
  },
  avatar: {
    height: 60,
    width: 60
  },
  inputFile: {
    display: 'none'
  },
  root: {
    height: '30px'
  },
  input: {
    color: '#1F1F1F'
  },
  label: {
    color: '#999999',
    '&$labelShrink': {
      color: '#999999'
    }
  },
  cell: {
    padding: '0 15px',
    backgroundColor: '#F2F2F2'
  },
  select: {
    marginBottom: '8px',
    marginTop: '16px'
  },
  underline: {
    '&:hover:not($disabled):not($focused):not($error)': {
      '&::before': {
        borderColor: '#999999'
      }
    },
    '&::before': {
      borderColor: 'transparent'
    },
    '&::after': {
      borderColor: 'transparent'
    }
  },
  disabled: {},
  focused: {},
  error: {},
  labelShrink: {}
})

const ProfileFormScreen = (props: Props) => {
  const { changePassword, classes, t, user, onChangeField } = props

  return (
    <Grid container spacing={24}>
      <Grid item xs={12} md={2}>
        <div className={classes.avatarContainer}>
          <Avatar
            src={user.avatarUrl || '/static/Shape.png'}
            className={classes.avatar}
          />
          <input
            accept="image/*"
            id="flat-button-file"
            type="file"
            className={classes.inputFile}
          />
          <label htmlFor="flat-button-file" className={classes.changeAvatar}>
            <Button component="span" color="primary">
              {t('common:Change')}
            </Button>
          </label>
        </div>
      </Grid>
      <Grid container item xs={12} md={10} spacing={24}>
        <Grid item xs={6}>
          <div className={classes.cell}>
            <FormControl fullWidth>
              <TextField
                id="firstName"
                name="firstName"
                InputLabelProps={{
                  classes: { root: classes.label, shrink: classes.labelShrink }
                }}
                InputProps={{
                  classes: { root: classes.input, underline: classes.underline }
                }}
                defaultValue={user.firstName || undefined}
                label={t('common:First Name')}
                type="text"
                onChange={e => onChangeField('firstName', e)}
                margin="normal"
                className={classes.input}
              />
            </FormControl>
          </div>
        </Grid>
        <Grid item xs={6}>
          <div className={classes.cell}>
            <FormControl fullWidth>
              <TextField
                id="lastName"
                name="lastName"
                InputLabelProps={{
                  classes: { root: classes.label, shrink: classes.labelShrink }
                }}
                InputProps={{
                  classes: { root: classes.input, underline: classes.underline }
                }}
                defaultValue={user.lastName || undefined}
                label={t('common:Last Name')}
                type="text"
                onChange={e => onChangeField('lastName', e)}
                margin="normal"
              />
            </FormControl>
          </div>
        </Grid>
        {
          // <Grid item xs={12}>
          //   <div className={classes.cell}>
          //     <FormControl fullWidth>
          //       <InputLabel
          //         htmlFor="accountType"
          //         classes={{ root: classes.label, shrink: classes.labelShrink }}
          //       >
          //         {t('common:Account Type')}
          //       </InputLabel>
          //       <Select
          //         disableUnderline
          //         classes={{ root: classes.input, underline: classes.underline }}
          //         className={classes.select}
          //         inputProps={{
          //           name: 'accountType',
          //           id: 'accountType'
          //         }}
          //         value=""
          //         onChange={e => onChangeField('accountType', e)}
          //       >
          //         <MenuItem value="">
          //           <em>None</em>
          //         </MenuItem>
          //         <MenuItem value="user">{t('common:User')}</MenuItem>
          //       </Select>
          //     </FormControl>
          //   </div>
          // </Grid>
        }
        <Grid item xs={12}>
          <div className={classes.cell}>
            <FormControl fullWidth>
              <TextField
                id="email"
                name="email"
                InputLabelProps={{
                  classes: { root: classes.label, shrink: classes.labelShrink }
                }}
                InputProps={{
                  classes: { root: classes.input, underline: classes.underline }
                }}
                defaultValue={user.email}
                label={t('common:Email')}
                type="email"
                onChange={e => onChangeField('email', e)}
                margin="normal"
              />
            </FormControl>
          </div>
        </Grid>
        <Grid item xs={12}>
          <div className={classes.cell}>
            <FormControl fullWidth>
              <TextField
                rows="5"
                multiline
                id="bio"
                name="bio"
                InputLabelProps={{
                  classes: { root: classes.label, shrink: classes.labelShrink }
                }}
                InputProps={{
                  classes: { root: classes.input, underline: classes.underline }
                }}
                defaultValue={user.bio || undefined}
                label={t('common:Short Bio')}
                type="text"
                onChange={e => onChangeField('bio', e)}
                margin="normal"
              />
            </FormControl>
          </div>
        </Grid>
        {changePassword}
      </Grid>
    </Grid>
  )
}

export default withStyles(styles)(ProfileFormScreen)
