import gql from 'graphql-tag'

const typeDefs = `
  type UserProfile {
    id: String,
    firstName: String,
    lastName: String,
    accountType: String,
    bio: String
  }
  
  type UserProfileQuery {
    userProfile(id: String): UserProfile
  }
`
const defaults = {
  userProfile: [
    {
      id: '1',
      firstName: 'Vitalik',
      lastName: 'Buterin',
      accountType: 'Mosaic Research Team',
      avatarUrl: '/static/Shape.png',
      bio: 'Buterin is co-founder and inventor of Ethereum',
      __typename: 'UserProfile'
    },
    {
      id: '2',
      firstName: 'Vitalik',
      lastName: 'Buterin',
      accountType: 'Mosaic Research Team',
      avatarUrl: '/static/Shape.png',
      bio: 'Buterin is co-founder and inventor of Ethereum',
      __typename: 'UserProfile'
    }
  ]
}

const resolvers = {
  Query: {
    userProfile: (_, { id }, { cache }) => {
      const query = gql`
        query UserProfileQuery {
          userProfile @client {
            id
            firstName
            lastName
            avatarUrl
            accountType
            bio
          }
        }
      `

      const profile = cache
        .readQuery({ query })
        .userProfile.filter(item => item.id === `${id}`)
        .pop()
      return profile || null
    }
  }
}
export { defaults, typeDefs, resolvers }
