// @flow

import React from 'react'
import { Button, Typography, withStyles } from '@material-ui/core'

type EditProps = {
  t: string => string,
  classes: Object,
  cancel: () => void,
  save: () => void
};

type ViewProps = {
  t: string => string,
  classes: Object,
  close: () => void,
  edit?: () => void
};

const styles = theme => ({
  toolBarActions: {
    marginLeft: 'auto'
  },
  title: {
    color: '#999999'
  }
})

const EditProfileActions = (props: EditProps) => {
  const { t, classes, cancel, save } = props

  return (
    <React.Fragment>
      <Typography variant="subheading" className={classes.title}>
        {t('common:Edit Profile')}
      </Typography>
      <div className={classes.toolBarActions}>
        <Button onClick={cancel} color="secondary">
          {t('common:Cancel')}
        </Button>
        <Button onClick={save} color="primary">
          {t('common:Save And Close')}
        </Button>
      </div>
    </React.Fragment>
  )
}

export const EditProfile = withStyles(styles)(EditProfileActions)

const ViewProfileActions = (props: ViewProps) => {
  const { t, classes, close, edit } = props

  return (
    <React.Fragment>
      <Typography variant="subheading" className={classes.title}>
        {t('common:User Profile')}
      </Typography>
      <div className={classes.toolBarActions}>
        {edit && (
          <Button onClick={edit} color="secondary">
            {t('common:Edit')}
          </Button>
        )}
        <Button onClick={close} color="primary">
          {t('common:Close')}
        </Button>
      </div>
    </React.Fragment>
  )
}

export const ViewProfile = withStyles(styles)(ViewProfileActions)
