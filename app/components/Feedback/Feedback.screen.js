// @flow

import React from 'react'
import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  DialogContentText,
  FormControl,
  TextField,
  Typography,
  Button,
  withStyles
} from '@material-ui/core'

type Props = {
  t: func,
  isOpen: ?boolean,
  onClickLink: (event: Object) => void,
  onClose: () => void,
  classes: Object,
  user: Object,
  onSubmit: func,
  onMessageChange: func
};

const styles = theme => ({
  button: {
    color: 'white',
    backgroundColor: 'transparent',
    boxShadow: 'none',
    '&:hover': {
      backgroundColor: 'transparent'
    }
  },
  feedbackPaper: {
    width: '600px'
  },
  textInput: {
    background: '#909090',
    padding: '0 1rem',
    margin: '1rem auto'
  },
  input: {
    color: theme.palette.background.paper
  },
  label: {
    color: theme.palette.background.paper
  },
  underline: {
    '&::before': {
      borderColor: 'transparent'
    },
    '&::after': {
      borderColor: 'transparent'
    }
  }
})

const FeedbackScreen = (props: Props) => {
  const {
    onClickLink,
    onClose,
    isOpen,
    classes,
    onSubmit,
    onMessageChange,
    t
  } = props
  return (
    <div>
      <Button
        color="secondary"
        variant="contained"
        size="small"
        className={classes.button}
        onClick={onClickLink}
      >
        {t('common:Beta Feedback')}
        {isOpen}
      </Button>
      <Dialog
        open={isOpen}
        onClose={onClose}
        maxWidth="sm"
        classes={{ paper: classes.feedbackPaper }}
      >
        <DialogTitle disableTypography id="form-dialog-title">
          <Typography
            variant="subheading"
            component="h3"
            classes={{ subheading: classes.headline }}
          >
            Provide Feedback / Ask Questions
          </Typography>
        </DialogTitle>
        <DialogContent>
          <DialogContentText>For Help, please see Mosaic FAQ</DialogContentText>
          <div className={classes.textInput}>
            <FormControl fullWidth>
              <TextField
                label="To provide quick feedback or ask a question, enter text here"
                type="text"
                margin="normal"
                multiline
                rows="4"
                onChange={onMessageChange}
                InputLabelProps={{
                  classes: { root: classes.label }
                }}
                InputProps={{
                  classes: { root: classes.input, underline: classes.underline }
                }}
              />
            </FormControl>
          </div>

          <DialogContentText>
            If you are willing to provide more extensive feedback, make feature
            suggestions, or review & rate others feedback or suggestsions,
            please visit our community feedback site here.
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={onClose} color="primary">
            Cancel
          </Button>
          <Button onClick={onSubmit} color="secondary">
            Send
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  )
}

export default withStyles(styles)(FeedbackScreen)
