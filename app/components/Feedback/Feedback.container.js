// @flow

import React from 'react'
import { graphql } from 'react-apollo'
import gql from 'graphql-tag'
import Feedback from './Feedback.screen'

type Props = {
  t: func,
  user: object,
  postFeedback: any => Promise<*>,
  route: string
};

type State = {
  isOpen: boolean,
  message: string
};

class FeedbackContainer extends React.PureComponent<Props, State> {
  props: Props;
  state: State;

  constructor (props: Props) {
    super(props)
    this.state = {
      isOpen: false,
      message: ''
    }
  }

  onClickLink = (event: Object) => {
    this.setState({ isOpen: true })
  };

  onClose = () => {
    this.setState({ isOpen: false })
  };

  onMessageChange = ev => {
    this.setState({ message: ev.target.value })
  };

  onSubmit = () => {
    const { postFeedback } = this.props
    const message = this.state.message
    const page = this.props.route

    this.setState({
      isOpen: false
    })

    postFeedback({
      variables: {
        input: {
          message,
          page
        }
      }
    }).then(response => {})
  };

  render () {
    const { isOpen } = this.state
    return (
      <Feedback
        {...this.props}
        isOpen={isOpen}
        onClose={this.onClose}
        onClickLink={this.onClickLink}
        onMessageChange={this.onMessageChange}
        onSubmit={this.onSubmit}
      />
    )
  }
}

export const POST_FEEDBACK = gql`
  mutation PostFeedback($input: postFeedbackInput!) {
    postFeedback(input: $input) {
      result
    }
  }
`
export const postFeedback = graphql(POST_FEEDBACK, {
  name: 'postFeedback'
})

export default postFeedback(FeedbackContainer)
