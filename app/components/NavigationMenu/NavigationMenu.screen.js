// @flow

import React from 'react'
import {
  List,
  ListItem,
  ListItemText,
  Collapse,
  withStyles
} from '@material-ui/core'
import ChevronRight from '@material-ui/icons/ChevronRight'
import ExpandMore from '@material-ui/icons/ExpandMore'
import { Link } from '../../../lib/routes'

const styles = theme => ({
  root: {},
  pageRoot: {
    paddingBottom: '3px',
    paddingTop: '3px'
  },
  sectionRoot: {
    paddingLeft: theme.spacing.unit,
    color: theme.palette.text.secondary,
    '&:first-child': {
      paddingLeft: theme.spacing.unit
    }
  },
  primaryText: {
    color: theme.palette.secondary.main,
    fontSize: '0.85rem'
  },
  secondaryText: {
    color: theme.palette.text.secondary,
    fontSize: '0.85rem'
  },
  sectionContainer: {
    paddingLeft: theme.spacing.unit * 5,
    paddingTop: theme.spacing.unit,
    paddingBottom: theme.spacing.unit
  },
  iconColor: {
    color: theme.palette.text.main
  },
  mainSectionBorder: {
    borderLeft: `1px solid ${theme.palette.secondary.main}`
  },
  secondarySectionBorder: {
    borderLeft: `1px solid ${theme.palette.text.secondary}`
  },
  sectionLink: {
    paddingLeft: theme.spacing.unit,
    paddingTop: theme.spacing.unit * 0.5,
    paddingBottom: theme.spacing.unit * 0.5
  }
})

type Props = {
  t: func,
  pages: [],
  sections: ?[],
  onPageClick: page => void,
  onSectionClick: (data: { page: string, section: string }) => void,
  baseUrl: string,
  classes: Object
};

const NavigationMenuScreen = (props: Props) => {
  const {
    classes,
    t,
    onSectionClick,
    onPageClick,
    sections,
    pages,
    baseUrl
  } = props

  return (
    <div className={classes.root}>
      <List component="nav">
        {Object.keys(pages).map(pageKey => {
          const page = pages[pageKey]

          return (
            <React.Fragment key={pageKey}>
              <Link route={`${baseUrl}${page.anchor}`} passHref>
                <ListItem
                  disableGutters
                  button
                  component="a"
                  className={page.sections ? null : classes.sectionContainer}
                  classes={{ root: classes.pageRoot }}
                  onClick={() => onPageClick(pageKey)}
                >
                  {' '}
                  {page.sections &&
                    (page.isSelected ? (
                      <ExpandMore color="action" />
                    ) : (
                      <ChevronRight color="action" />
                    ))}
                  <ListItemText primary={t(`common:${page.title}`)} />
                </ListItem>
              </Link>
              <Collapse
                unmountOnExit
                in={page.isSelected}
                timeout="auto"
                classes={{ container: classes.sectionContainer }}
              >
                {page.isSelected &&
                  sections && (
                  <List disablePadding>
                    {Object.keys(sections).map(sectionKey => {
                      const section = sections[sectionKey]
                      return (
                        <ListItem
                          key={sectionKey}
                          button
                          onClick={() =>
                            onSectionClick({
                              page: pageKey,
                              section: sectionKey
                            })
                          }
                          classes={{
                            root: section.isSelected
                              ? classes.mainSectionBorder
                              : classes.secondarySectionBorder
                          }}
                          className={classes.sectionLink}
                          disableGutters
                        >
                          <ListItemText
                            classes={{ root: classes.sectionRoot }}
                            primaryTypographyProps={{
                              classes: {
                                subheading: section.isSelected
                                  ? classes.primaryText
                                  : classes.secondaryText
                              }
                            }}
                            primary={t(`common:${section.title}`)}
                          />
                        </ListItem>
                      )
                    })}
                  </List>
                )}
              </Collapse>
            </React.Fragment>
          )
        })}
      </List>
    </div>
  )
}

export default withStyles(styles)(NavigationMenuScreen)
