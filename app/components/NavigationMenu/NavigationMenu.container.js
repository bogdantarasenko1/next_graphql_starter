// @flow

import React from 'react'
import { goToAnchor } from 'react-scrollable-anchor'
import NavigationMenu from './NavigationMenu.screen'

export const navigationNodes = {
  main: {
    title: 'Main',
    anchor: 'main',
    isSelected: false,
    sections: {
      timeChart: {
        title: 'Time Chart',
        anchor: 'time-chart',
        isSelected: true
      },
      overview: {
        title: 'Overview',
        anchor: 'overview',
        isSelected: false
      },
      keyStats: {
        title: 'Key Stats',
        anchor: 'key-stats',
        isSelected: false
      },
      compTable: {
        title: 'Comp Table',
        anchor: 'comp-table',
        isSelected: false
      }
    }
  },
  'key-data': {
    title: 'Key Data',
    anchor: 'key-data',
    isSelected: false,
    sections: {
      valuation: {
        title: 'Valuation',
        anchor: 'valuation',
        isSelected: false
      },
      trading: {
        title: 'Trading',
        anchor: 'trading',
        isSelected: false
      },
      coinSupply: {
        title: 'Coin Supply',
        anchor: 'coin-supply',
        isSelected: false
      },
      transaction: {
        title: 'Transaction',
        anchor: 'transaction',
        isSelected: false
      } /*,
      ecosystem: {
        title: 'Ecosystem',
        anchor: 'ecosystem',
        isSelected: false
      },
      organization: {
        title: 'Organization',
        anchor: 'organization',
        isSelected: false
      } */
    }
  },
  analysis: {
    title: 'Analysis',
    anchor: 'analysis',
    isSelected: false,
    sections: {
      analysisOverview: {
        title: 'Overview',
        anchor: 'analysis-overview',
        isSelected: false
      },
      industry: {
        title: 'Industry',
        anchor: 'industry',
        isSelected: false
      },
      technology: {
        title: 'Technology',
        anchor: 'technology',
        isSelected: false
      },
      competition: {
        title: 'Competition',
        anchor: 'competition',
        isSelected: false
      },
      risks: {
        title: 'Risks',
        anchor: 'risks',
        isSelected: false
      },
      team: {
        title: 'Team',
        anchor: 'team',
        isSelected: false
      },
      advisors: {
        title: 'Advisors',
        anchor: 'advisors',
        isSelected: false
      },
      investors: {
        title: 'Investors',
        anchor: 'investors',
        isSelected: false
      } /*,
      legalStructure: {
        title: 'Legal structure',
        anchor: 'legal-structure',
        isSelected: false
      },
      partnerships: {
        title: 'Partnerships',
        anchor: 'partnerships',
        isSelected: false
      },
      officeAddresses: {
        title: 'Office Addresses',
        anchor: 'office-addresses',
        isSelected: false
      } */
    }
  } /*,
  'data-visuals': {
    title: 'Data Visuals',
    anchor: 'data-visuals',
    isSelected: false,
    sections: {
      dataVisuals1: {
        title: 'Data Visual 1',
        anchor: 'dataVisual-1',
        isSelected: false
      },
      dataVisuals2: {
        title: 'Data Visual 2',
        anchor: 'dataVisual-2',
        isSelected: false
      },
      dataVisuals3: {
        title: 'Data Visual 3',
        anchor: 'dataVisual-3',
        isSelected: false
      },
      dataVisuals4: {
        title: 'Data Visual 4',
        anchor: 'dataVisual-4',
        isSelected: false
      },
      dataVisuals5: {
        title: 'Data Visual 5',
        anchor: 'dataVisual-5',
        isSelected: false
      },
      dataVisuals6: {
        title: 'Data Visual 6',
        anchor: 'dataVisual-6',
        isSelected: false
      },
      dataVisuals7: {
        title: 'Data Visual 7',
        anchor: 'dataVisual-7',
        isSelected: false
      }
    }
  } */
}

type Props = {
  t: func,
  symbol: string,
  page: string
};

type State = {
  pages: [],
  sections: ?[],
  openedPage: ?string
};

class NavigationMenuContainer extends React.Component<Props, State> {
  props: Props;
  state: State;

  constructor (props: Props) {
    super(props)
    const { page } = props
    const pages = { ...navigationNodes }
    pages[page].isSelected = true

    this.state = {
      pages: pages,
      sections: navigationNodes[page].sections,
      openedPage: page
    }
  }

  baseUrl = () => {
    const { symbol } = this.props
    return `/coin/${symbol.toLowerCase()}/`
  };

  onPageClick = page => {
    const { pages } = this.state
    const { sections } = pages[page]

    Object.keys(pages).forEach(page => (pages[page].isSelected = false))

    Object.keys(sections).forEach(
      section => (sections[section].isSelected = false)
    )
    pages[page].isSelected = true

    this.setState({
      sections: pages[page].sections,
      pages: pages
    })

    if (pages[page].anchor) {
      goToAnchor(pages[page].anchor)
    }
  };

  onSectionClick = ({ page, section }) => {
    const { pages } = this.state
    const { sections } = pages[page]

    Object.keys(sections).forEach(
      section => (sections[section].isSelected = false)
    )
    sections[section].isSelected = true

    this.setState({
      pages: pages
    })

    if (sections[section].anchor) {
      goToAnchor(sections[section].anchor)
    }
  };

  render () {
    const { pages, sections } = this.state
    return (
      <NavigationMenu
        {...this.props}
        baseUrl={this.baseUrl()}
        pages={pages}
        sections={sections}
        onPageClick={this.onPageClick}
        onSectionClick={this.onSectionClick}
      />
    )
  }
}

export default NavigationMenuContainer
