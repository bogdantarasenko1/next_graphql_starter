// @flow

import React from 'react'
import { Grid } from '@material-ui/core'
import TeamCard from './TeamCard.screen'

type Props = {
  data: []
};

const CardsCollectionScreen = (props: Props) => {
  const { data } = props

  return (
    <Grid container justify="center">
      <Grid container item xs={12} md={8} spacing={40} justify="center">
        {data.map((item, index) => (
          <Grid key={index} item xs={12} sm={4}>
            <TeamCard {...item} />
          </Grid>
        ))}
      </Grid>
    </Grid>
  )
}

export default CardsCollectionScreen
