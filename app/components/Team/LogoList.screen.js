// @flow

import React from 'react'
import { List, ListItem, withStyles } from '@material-ui/core'

type Props = {
  logo: []
};

const styles = theme => ({})

const LogoListScreen = (props: Props) => {
  const { logo } = props

  return (
    <List>
      {logo.map((item, index) => (
        <ListItem key={index}>
          <img src={`/static/teamImg/logo/${item}`} height="34" />
        </ListItem>
      ))}
    </List>
  )
}

export default withStyles(styles)(LogoListScreen)
