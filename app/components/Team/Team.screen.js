// @flow

import React from 'react'
import { Typography, withStyles } from '@material-ui/core'
import classnames from 'classnames'
import Layout from '../MainLayout/MainLayout.screen'
import CardsCollection from './CarsCollection.screen'

type Props = {
  t: string => string,
  leadership: [],
  technology: [],
  classes: Object
};

const styles = theme => ({
  container: {
    background: 'url("/static/teamImg/team.jpg") no-repeat',
    backgroundSize: '100%',
    paddingTop: '30vw'
  },
  center: {
    textAlign: 'center'
  },
  uppercase: {
    textTransform: 'uppercase'
  },
  gutter: {
    marginBottom: '60px'
  },
  leadership: {
    paddingBottom: '80px'
  },
  technology: {
    paddingTop: '120px',
    paddingBottom: '80px',
    backgroundColor: theme.palette.background.paper
  }
})

const TeamScreen = (props: Props) => {
  const { leadership, technology, classes, ...other } = props
  const { t } = props

  const content = () => {
    return (
      <div className={classes.container}>
        <Typography
          className={classnames(classes.center, classes.uppercase)}
          gutterBottom
          variant="caption"
          color="textSecondary"
        >
          {t('common:Team')}
        </Typography>
        <div className={classes.leadership}>
          <Typography
            className={classnames(classes.center, classes.gutter)}
            variant="subheading"
          >
            {t('common:Leadership')}
          </Typography>
          <CardsCollection data={leadership} />
        </div>

        <div className={classes.technology}>
          <Typography
            className={classnames(classes.center, classes.gutter)}
            variant="subheading"
          >
            {t('common:Technology/Product')}
          </Typography>
          <CardsCollection data={leadership} />
        </div>
      </div>
    )
  }

  return <Layout {...other} content={content()} />
}

export default withStyles(styles)(TeamScreen)
