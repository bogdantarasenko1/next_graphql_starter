// @flow

import React from 'react'
import {
  Card,
  CardHeader,
  CardMedia,
  CardContent,
  Typography,
  withStyles
} from '@material-ui/core'
import LogoList from './LogoList.screen'

type Props = {
  image: string,
  fullName: string,
  text: "string",
  position: "string",
  logo: [],
  classes: Object
};

const styles = theme => ({
  card: {
    backgroundColor: 'transparent',
    boxShadow: 'none'
  },
  media: {
    height: 0,
    paddingTop: '100%'
  }
})

const TeamCardScreen = (props: Props) => {
  const { image, fullName, text, position, logo, classes } = props

  return (
    <Card className={classes.card}>
      <CardMedia className={classes.media} image={`/static/teamImg/${image}`} />
      <CardHeader title={fullName} subheader={position} />
      <CardContent>
        <Typography component="p">{text}</Typography>
      </CardContent>
      {logo.length > 0 && <LogoList logo={logo} />}
    </Card>
  )
}

export default withStyles(styles)(TeamCardScreen)
