// @flow

import React from 'react'
import { TablePagination } from '@material-ui/core'

type Props = {
  dataLength: number,
  rowsPerPage: number,
  page: number,
  handleChangePage: func
};

const PaginationScreen = (props: Props) => {
  const { dataLength, rowsPerPage, page, handleChangePage } = props

  return (
    <TablePagination
      component="div"
      count={dataLength}
      rowsPerPage={rowsPerPage}
      page={page}
      onChangePage={handleChangePage}
      rowsPerPageOptions={[]}
      labelRowsPerPage={''}
    />
  )
}

export default PaginationScreen
