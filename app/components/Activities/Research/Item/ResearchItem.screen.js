// @flow

import React from 'react'
import {
  ListItemAvatar,
  ListItem,
  ListItemText,
  Avatar
} from '@material-ui/core'
import styles from './styles.css'

type Props = {
  tags: [string],
  image: string,
  text: string,
  date: string,
  onImageError: Object => void
};

const ResearchItemScreen = (props: Props) => {
  return (
    <ListItem>
      <ListItemAvatar>
        <Avatar src={props.image ? props.image : props.onImageError()} />
      </ListItemAvatar>
      <ListItemText
        primary={props.tags}
        secondaryTypographyProps={{ component: 'div' }}
        secondary={
          <React.Fragment>
            <p className={styles.text}>{props.text}</p>
            <p className={styles.date}>{props.date}</p>
          </React.Fragment>
        }
      />
    </ListItem>
  )
}

export default ResearchItemScreen
