// @flow

import React from 'react'
import { List } from '@material-ui/core'
import ResearchItem from './Item/ResearchItem.screen'

type Props = {
  items: [Object],
  onImageError: Object => void
};

const ResearchScreen = (props: Props) => {
  return (
    <List>
      {props.items.map((item, index) => {
        return (
          <ResearchItem
            key={index}
            image={item.image}
            tags={item.tags}
            text={item.text}
            date={item.date}
            onImageError={props.onImageError}
          />
        )
      })}
    </List>
  )
}

export default ResearchScreen
