// @flow

import React from 'react'
import { Grid } from '@material-ui/core'
import ActivityItem from './Item/ActivityItem.screen'

type Props = {
  items: [Object],
  onImageError: Object => void
};

const ActivityScreen = (props: Props) => {
  return (
    <Grid>
      {props.items.map((item, index) => {
        return (
          <Grid item key={index} md={12}>
            <ActivityItem
              text={item.text}
              date={item.date}
              onImageError={props.onImageError}
            />
          </Grid>
        )
      })}
    </Grid>
  )
}

export default ActivityScreen
