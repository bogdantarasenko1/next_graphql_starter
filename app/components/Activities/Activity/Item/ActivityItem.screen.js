// @flow

import React from 'react'
import {
  List,
  ListItemAvatar,
  ListItem,
  ListItemText,
  Avatar
} from '@material-ui/core'
import styles from './styles.css'

type Props = {
  image: string,
  text: string,
  date: string,
  onImageError: Object => void
};

const ActivityItemScreen = (props: Props) => {
  return (
    <div className={styles.container}>
      <List>
        <ListItem>
          <ListItemAvatar>
            <Avatar
              src="/thumbnail.png"
              alt="thumbnail"
              imgProps={{ onError: props.onImageError }}
            />
          </ListItemAvatar>
          <ListItemText primary={props.text} secondary={props.date} />
        </ListItem>
      </List>
    </div>
  )
}

export default ActivityItemScreen
