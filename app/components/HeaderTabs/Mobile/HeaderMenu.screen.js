// @flow

import React from 'react'
import { Button, Menu, MenuItem, withStyles } from '@material-ui/core'
import ArrowDropDown from '@material-ui/icons/ArrowDropDown'

const styles = theme => ({
  button: {
    color: 'white',
    backgroundColor: 'transparent',
    boxShadow: 'none',
    '&:hover': {
      backgroundColor: 'transparent'
    }
  }
})

type Props = {
  t: func,
  classes: Object,
  menuTitle: string,
  menuItems: [],
  closeMenu: () => void,
  clickItem: item => void,
  isMenuOpen: boolean,
  setAnchor: Object => void,
  anchorEl: ?Object
};

const HeaderMenuScreen = (props: Props) => {
  const {
    menuItems,
    classes,
    closeMenu,
    setAnchor,
    clickItem,
    anchorEl,
    isMenuOpen,
    menuTitle
  } = props

  return (
    <React.Fragment>
      <Button
        color="primary"
        variant="contained"
        size="small"
        className={classes.button}
        onClick={setAnchor}
      >
        {menuTitle}
        <ArrowDropDown />
      </Button>
      <Menu anchorEl={anchorEl} open={isMenuOpen} onClose={closeMenu}>
        {menuItems.map((item, key) => (
          <MenuItem key={key} onClick={() => clickItem(item)}>
            {item.title}
          </MenuItem>
        ))}
      </Menu>
    </React.Fragment>
  )
}

export default withStyles(styles)(HeaderMenuScreen)
