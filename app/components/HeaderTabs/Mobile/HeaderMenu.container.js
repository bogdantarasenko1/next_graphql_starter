// @flow

import React from 'react'
import HeaderMenu from './HeaderMenu.screen'

type Props = {
  menuItems: [],
  onClickItem: item
};

type State = {
  menuTitle: string,
  isMenuOpen: boolean,
  anchorEl: ?Object
};

class HeaderMenuContainer extends React.PureComponent<Props, State> {
  props: Props;
  state: State;

  constructor (props: Props) {
    super(props)

    this.state = {
      menuTitle: props.menuItems[0].title,
      isMenuOpen: false,
      anchorEl: null
    }
  }

  onSetAnchor = event => {
    this.setState({ anchorEl: event.currentTarget })
  };

  onCloseMenu = (event, other) => {
    this.setState({ anchorEl: null })
  };

  isMenuOpen = () => {
    return !!this.state.anchorEl
  };

  onClickItem = item => {
    this.setState({
      anchorEl: null,
      menuTitle: item.title
    })
    this.props.onClickItem(item)
  };

  render () {
    const { menuItems, ...other } = this.props
    return (
      <HeaderMenu
        {...other}
        menuTitle={this.state.menuTitle}
        menuItems={menuItems}
        setAnchor={this.onSetAnchor}
        anchorEl={this.state.anchorEl}
        closeMenu={this.onCloseMenu}
        clickItem={this.onClickItem}
        isMenuOpen={this.isMenuOpen()}
      />
    )
  }
}

export default HeaderMenuContainer
