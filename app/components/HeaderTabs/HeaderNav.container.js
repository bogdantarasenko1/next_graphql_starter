// @flow

import React from 'react'
import { Tab, Tabs } from '@material-ui/core'
import withWidth, { isWidthUp } from '@material-ui/core/withWidth'
import HeaderMenu from './Mobile/HeaderMenu.container'

type Props = {
  tabItems: [],
  onChangeTab: Object => void,
  selectedTab: number,
  width: string
};

class HeaderNavContainer extends React.PureComponent<Props> {
  props: Props;

  onChangeTab = (event, index) => {
    const { onChangeTab, tabItems } = this.props
    onChangeTab(tabItems[index])
  };
  render () {
    const { width, selectedTab, onChangeTab, tabItems } = this.props

    if (isWidthUp('md', width)) {
      return (
        <Tabs
          value={selectedTab}
          textColor="secondary"
          onChange={this.onChangeTab}
        >
          {tabItems.map((tab, key) => (
            <Tab key={key} disableRipple label={tab.title} />
          ))}
        </Tabs>
      )
    } else {
      return <HeaderMenu menuItems={tabItems} onClickItem={onChangeTab} />
    }
  }
}

export default withWidth()(HeaderNavContainer)
