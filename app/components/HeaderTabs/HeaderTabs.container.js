// @flow

import React from 'react'
import ViewLink from '../TableColumns/ViewLink/ViewLink.container'
import HeaderTabs from './HeaderTabs.screen'

type Props = {
  t: func,
  title?: string,
  width: string,
  onChangeTab: (key: string) => void,
  tabItems: [],
  onChangeColumns: columns => void,
  userColumns: ?[]
};

type State = {
  selectedTab: number
};

class HeaderTabsContainer extends React.Component<Props> {
  props: Props;
  state: State;

  constructor (props) {
    super(props)
    this.state = {
      selectedTab: 0
    }
  }

  onChangeTab = item => {
    const { onChangeTab, tabItems } = this.props

    onChangeTab(item.key)

    this.setState({
      selectedTab: tabItems.findIndex(tab => tab.key === item.key)
    })
  };

  isViewLinkOnTabs = activeTabIndex => {
    const { tabItems } = this.props
    return tabItems[activeTabIndex].showViewLink
  };

  render () {
    const { selectedTab } = this.state

    const { tabItems, onChangeColumns, userColumns, title, t } = this.props
    return (
      <HeaderTabs
        t={t}
        tabItems={tabItems}
        title={title || 'Categories'}
        onChangeTab={this.onChangeTab}
        selectedTab={selectedTab}
      >
        {' '}
        {this.isViewLinkOnTabs(selectedTab) && (
          <ViewLink
            t={t}
            userColumns={userColumns}
            onChangeColumns={onChangeColumns}
          />
        )}
      </HeaderTabs>
    )
  }
}

export default HeaderTabsContainer
