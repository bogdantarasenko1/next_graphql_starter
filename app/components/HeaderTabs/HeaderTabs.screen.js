// @flow

import React from 'react'
import { Typography, withStyles } from '@material-ui/core'
import HeaderNav from './HeaderNav.container'

const styles = theme => ({
  header: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    padding: '0 .5rem',
    height: '2rem',
    backgroundColor: theme.palette.background.header,
    [theme.breakpoints.down('sm')]: {
      background: 'none',
      height: 'auto',
      padding: 0
    }
  },
  headline: {
    marginRight: '60px',
    color: theme.palette.text.header,
    whiteSpace: 'nowrap'
  },
  viewLink: {
    marginLeft: 'auto'
  }
})

type Props = {
  t: func,
  title: string,
  tabItems: [],
  onChangeTab: item => void,
  selectedTab: number,
  classes: Object,
  children: React.ReactNode
};

const HeaderTabsScreen = (props: Props) => {
  const { tabItems, title, classes, onChangeTab, selectedTab, t } = props

  return (
    <div className={classes.header}>
      <Typography
        variant="subheading"
        component="h3"
        classes={{ subheading: classes.headline }}
      >
        {`${t(`common:${title}`)}`}
      </Typography>
      <HeaderNav
        tabItems={tabItems}
        onChangeTab={onChangeTab}
        selectedTab={selectedTab}
      />
      <div className={classes.viewLink}>{props.children}</div>
    </div>
  )
}

export default withStyles(styles)(HeaderTabsScreen)
