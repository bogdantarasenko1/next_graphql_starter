// @flow

import React from 'react'
import {withStyles} from '@material-ui/core'

import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';


const controls_styles = theme => ({
  root: {
    textAlign: 'right'
  },
  addIcon: {
    color: '#FB771A',
    fontSize: '20px'
  },
  saveIcon: {
    color: '#fff',
    fontSize: '20px'
  },
  button: {
    padding: '3px',
    height: '30px',
    width: '30px'
  }
});

type Props = {};


class DashboardDialogs extends React.Component<State, Props> {
  constructor (props) {
    super(props)
    this.state = {
      add_dialog_open:          props.add_dialog_open,
      save_dialog_open:         props.save_dialog_open,
      notification_dialog_open: props.notification_dialog_open,
      notification_dialog_content: {
        title: '',
        text: ''
      },

      is_current_dash_changed:  props.is_current_dash_changed,
      new_dashboard_name:       props.new_dashboard_name || ''
    }

    this.onCloseDialog            = this.onCloseDialog.bind(this);
    this.onAddDialogSubmitClick   = this.onAddDialogSubmitClick.bind(this);
    this.onSaveDialogSubmitClick  = this.onSaveDialogSubmitClick.bind(this);

  }

  onAddDialogSubmitClick(){
    this.setState({
      add_dialog_open: false
    });
    this.props.onAddDashboardSubmit();
  }

  onSaveDialogSubmitClick(){
    this.props.onSaveDashboardSubmit(this.state.new_dashboard_name)
  }

  onCloseDialog(dialog_key){
    this.setState({
      [dialog_key]: false
    })

    this.props.onCloseDialog(dialog_key)
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      is_current_dash_changed:  nextProps.is_current_dash_changed,
      add_dialog_open:          nextProps.add_dialog_open,
      save_dialog_open:         nextProps.save_dialog_open,
      notification_dialog_open: nextProps.notification_dialog_open,
      notification_dialog_content: nextProps.notification_dialog_content
    });

    if (nextProps.notification_dialog_open) {
      setTimeout(() => {
        this.onCloseDialog('notification_dialog_open')
        this.props.onCloseDialog('notification_dialog_open')
      }, 4000)
    }
  }

  render () {
    let classes = this.props.classes

    return (
      <React.Fragment>

        <Dialog
          open={this.state.add_dialog_open}
          onClose={() => this.onCloseDialog('add_dialog_open')}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <DialogTitle id="alert-dialog-title">{"You have unsaved changes in your dashboard."}</DialogTitle>
          <DialogContent>
            <DialogContentText id="alert-dialog-description">
              If you don't save you edited dashboard, all changes will be lost.
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={() => this.onCloseDialog('add_dialog_open')} color="primary">
              Disagree
            </Button>
            <Button onClick={this.onAddDialogSubmitClick} color="primary" autoFocus>
              Agree
            </Button>
          </DialogActions>
        </Dialog>



        <Dialog
          open={this.state.save_dialog_open}
          onClose={() => {
            this.onCloseDialog('save_dialog_open')
            this.setState({new_dashboard_name: ''})
          }}
          aria-labelledby="form-dialog-title"
        >
          <DialogTitle id="form-dialog-title">Save dashboard</DialogTitle>
          <DialogContent>
            <DialogContentText>
              Please, enter name for your new dashboard.
            </DialogContentText>
            <TextField
              autoFocus
              margin="dense"
              id="name"
              label="Dashboard name"
              type="text"
              fullWidth
              value={this.state.new_dashboard_name}
              onChange={event => this.setState({new_dashboard_name: event.target.value})}
            />
          </DialogContent>
          <DialogActions>
            <Button onClick={() => {
              this.onCloseDialog('save_dialog_open')
              this.setState({new_dashboard_name: ''})
            }} color="primary">
              Cancel
            </Button>
            <Button onClick={this.onSaveDialogSubmitClick} color="primary">
              Save
            </Button>
          </DialogActions>
        </Dialog>


        <Dialog
          open={this.state.notification_dialog_open}
          onClose={() => {
            this.onCloseDialog('notification_dialog_open')
          }}
        >
          <DialogTitle id="form-dialog-title">{this.state.notification_dialog_content.title}</DialogTitle>
          <DialogContent>
            <DialogContentText>
              {this.state.notification_dialog_content.text}
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={() => this.onCloseDialog('notification_dialog_open')} color="primary">
              OK
            </Button>
          </DialogActions>
        </Dialog>



      </React.Fragment>

    )
  }
}

export default withStyles(controls_styles)(DashboardDialogs)
